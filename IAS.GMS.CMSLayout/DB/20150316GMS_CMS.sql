CREATE DATABASE  IF NOT EXISTS `gms_cms` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `gms_cms`;
-- MySQL dump 10.13  Distrib 5.1.40, for Win32 (ia32)
--
-- Host: 127.0.0.1    Database: gms_cms
-- ------------------------------------------------------
-- Server version	5.5.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_contact`
--

DROP TABLE IF EXISTS `tbl_contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_contact` (
  `ContactID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `LanguageCode` varchar(5) NOT NULL,
  `ContactTitle` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `ContactPhone` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `ContactAddress` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `Contactfor` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `ContactContent` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `ContactLat` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ContactLong` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `CreateOn` datetime DEFAULT NULL,
  `CreateBy` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `UpdateOn` datetime DEFAULT NULL,
  `UpdateBy` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ContactID`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_contact`
--

LOCK TABLES `tbl_contact` WRITE;
/*!40000 ALTER TABLE `tbl_contact` DISABLE KEYS */;
INSERT INTO `tbl_contact` VALUES (1,'EN','Contact intelisys','0988725702','HCM','HCM','Contact intelisys is updating','10.801263','106.657356',NULL,NULL,NULL,NULL),(3,'VI','','01656146531','DINH BINH','BINH DINH','<p>DINH BINHvxcvxvxvcxvxcv</p>\n','','','2015-01-17 00:00:00','','2015-01-17 00:00:00','hung.nguyen@exe.com.vn');
/*!40000 ALTER TABLE `tbl_contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_informationpage`
--

DROP TABLE IF EXISTS `tbl_informationpage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_informationpage` (
  `InfoID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `LanguageCode` varchar(5) DEFAULT NULL,
  `InfoTitle` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `InfoDescription` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `InfoContent` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `CreateOn` datetime DEFAULT NULL,
  `CreateBy` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `UpdateOn` datetime DEFAULT NULL,
  `UpdateBy` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `InfoSlug` varchar(50) NOT NULL,
  `InfoGroup` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`InfoID`)
) ENGINE=MyISAM AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_informationpage`
--

LOCK TABLES `tbl_informationpage` WRITE;
/*!40000 ALTER TABLE `tbl_informationpage` DISABLE KEYS */;
INSERT INTO `tbl_informationpage` VALUES (3,'VI','Giới thiệu','Giới thiệu1','<p>Creates a promise that is resolved as rejected with the specified&nbsp;<code>reason</code>. This api should be used to forward rejection in a chain of promises. If you are dealing with the last promise in a promise chain, you don&#39;t need to worry about it.</p>\n\n<p>When comparing deferreds/promises to the familiar behavior of try/catch/throw, think of&nbsp;<code>reject</code>&nbsp;as the&nbsp;<code>throw</code>&nbsp;keyword in JavaScript. This also means that if you &quot;catch&quot; an error via a promise error callback and you want to forward the error to the promise derived from the current promise, you have to &quot;rethrow&quot; the error by returning a rejection constructed via&nbsp;<code>reject</code>.</p>\n','2015-01-17 00:00:00','','2015-02-10 00:00:00','thi.nguyen@exe.com.vn','2','INFO'),(2,'EN','About Us','About Us','<h2 style=\"font-style:inherit\">Plan</h2>\n\n<p>Plan for success, and achieve better business agility from strategy to execution.</p>\n\n<h2 style=\"font-style:inherit\">Deliver</h2>\n\n<p>Increase the velocity of enterprise application delivery &mdash; while simultaneously driving quality and reducing the cost of innovation.</p>\n\n<h2 style=\"font-style:inherit\">Manage</h2>\n\n<p>Manage IT operations to drive productivity, market differentiation, business growth and profitability.</p>\n\n<h2 style=\"font-style:inherit\">Secure</h2>\n\n<p>Protect your business in a world of cloud, mobile, and social media.</p>\n\n<ul>\n</ul>\n','2014-12-23 00:00:00','','2015-01-29 00:00:00','thi.nguyen@exe.com.vn','2','INFO'),(36,'EN','Eco','Eco','<p>Eco</p>\n','2015-03-03 00:00:00','thi.nguyen@exe.com.vn',NULL,NULL,'B_Eco','FARERULE'),(37,'VI','Eco','Eco','<p>Eco</p>\n','2015-03-03 00:00:00','thi.nguyen@exe.com.vn',NULL,NULL,'B_Eco','FARERULE'),(38,'EN','Eco','Eco','<p>Eco</p>\n','2015-03-03 00:00:00','thi.nguyen@exe.com.vn',NULL,NULL,'J_Eco','FARERULE'),(39,'VI','Eco','Eco','<p>Eco</p>\n','2015-03-03 00:00:00','thi.nguyen@exe.com.vn',NULL,NULL,'J_Eco','FARERULE'),(40,'EN','Eco','Eco','<p>Eco</p>\n','2015-03-03 00:00:00','thi.nguyen@exe.com.vn',NULL,NULL,'T_Eco','FARERULE'),(41,'VI','Eco','Eco','<p>Eco</p>\n','2015-03-03 00:00:00','thi.nguyen@exe.com.vn',NULL,NULL,'T_Eco','FARERULE'),(42,'EN','Eco','Eco','<p>Eco</p>\n','2015-03-03 00:00:00','thi.nguyen@exe.com.vn',NULL,NULL,'U_Eco','FARERULE'),(43,'VI','Eco','Eco','<p>Eco</p>\n','2015-03-03 00:00:00','thi.nguyen@exe.com.vn',NULL,NULL,'U_Eco','FARERULE'),(24,'VI','Skype Boss','Skype Boss','<p>Skype Boss</p>\n','2015-02-27 00:00:00','thi.nguyen@exe.com.vn',NULL,NULL,'Y_SBoss','FARERULE'),(31,'EN','InfomationPageTest1','InfomationPageTest1','<p>InfomationPageTest1</p>\n','2015-03-03 00:00:00','thi.nguyen@exe.com.vn',NULL,NULL,'InfomationPageTest1','INFO'),(34,'EN','Eco','Eco','<p>Eco</p>\n','2015-03-03 00:00:00','thi.nguyen@exe.com.vn',NULL,NULL,'H_Eco','FARERULE'),(35,'VI','Eco','Eco','<p>Eco</p>\n','2015-03-03 00:00:00','thi.nguyen@exe.com.vn',NULL,NULL,'H_Eco','FARERULE'),(32,'VI','InfomationPageTest1','InfomationPageTest1','<p>InfomationPageTest1</p>\n','2015-03-03 00:00:00','thi.nguyen@exe.com.vn',NULL,NULL,'InfomationPageTest1','INFO'),(30,'VI','Test33','Test33','<p>Test33</p>\n','2015-03-03 00:00:00','thi.nguyen@exe.com.vn',NULL,NULL,'Test33','FARERULE'),(9,'EN','CVV','CVV','<p><strong>What is a CVV/CID and where do I find it?</strong><br />\n<br />\nTo help protect your card from unauthorized usage, issuers print a security code on the card. The security code can be entered during mail, telephone, or Internet orders to help validate that the person entering the order has all of the card information in their possession.<br />\n<br />\nFor Visa, MasterCard, and Discover cards, the card code is the last 3 digit number located on the back of your card on or above your signature line. For an American Express card, it is the 4 digits on the FRONT above the end of your card number.</p>\n','2015-02-06 00:00:00','thi.nguyen@exe.com.vn',NULL,NULL,'CVV','FARERULE'),(19,'VI','CVV','CVV','<p><strong>What is a CVV/CID and where do I find it?</strong><br />','2015-02-10 00:00:00','thi.nguyen@exe.com.vn',NULL,NULL,'CVV','FARERULE'),(21,'EN','Eco','Eco','<p>Eco</p>\n','2015-02-27 00:00:00','thi.nguyen@exe.com.vn',NULL,NULL,'I_Eco','FARERULE'),(15,'EN','Info Page Test','Info Page Test','','2015-02-10 00:00:00','thi.nguyen@exe.com.vn',NULL,NULL,'8','INFO'),(16,'VI','Info Page Test','Info Page Test','','2015-02-10 00:00:00','thi.nguyen@exe.com.vn',NULL,NULL,'8','INFO'),(23,'EN','Skype Boss','Skype Boss','<p>Skype Boss</p>\n','2015-02-27 00:00:00','thi.nguyen@exe.com.vn',NULL,NULL,'Y_SBoss','FARERULE'),(33,'VI','hgjh','','','2015-03-03 00:00:00','thi.nguyen@exe.com.vn',NULL,NULL,'','INFO'),(22,'VI','Eco','Eco','<p>Eco</p>\n','2015-02-27 00:00:00','thi.nguyen@exe.com.vn',NULL,NULL,'I_Eco','FARERULE');
/*!40000 ALTER TABLE `tbl_informationpage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_phrase`
--

DROP TABLE IF EXISTS `tbl_phrase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_phrase` (
  `PhraseID` int(11) NOT NULL AUTO_INCREMENT,
  `PhraseName` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`PhraseID`)
) ENGINE=InnoDB AUTO_INCREMENT=237 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_phrase`
--

LOCK TABLES `tbl_phrase` WRITE;
/*!40000 ALTER TABLE `tbl_phrase` DISABLE KEYS */;
INSERT INTO `tbl_phrase` VALUES (1,'DATA_BOOK_ORIGIN'),(2,'DATA_BOOK_DESTINATION'),(3,'DATA_BOOK_DEPARTDATE'),(4,'DATA_BOOK_RETURNDATE'),(5,'DATA_BOOK_ADULT'),(6,'DATA_BOOK_CHILD'),(7,'DATA_BOOK_INFANT'),(8,'DATA_BOOK_PROMO'),(9,'BUTTON_FIND_FLIGHT'),(10,'DATA_FLIGHT'),(11,'DATA_TO'),(12,'DATA_MESSAGE_PROMO_CODE_INVALID'),(13,'DATA_PROMO_CODE_DISCOUNT1'),(14,'DATA_PROMO_CODE_DISCOUNT2'),(15,'BUTTON_COUNTINUE'),(16,'DATA_FROM'),(17,'DATA_DEPART'),(18,'DATA_REQUIRED_DEPART_FLIGHT'),(19,'DATA_REQUIRED_RETURN_FLIGHT'),(20,'DATA_MESSAGE_DEPART_DATE_MUST_GREATER_EQUAL_TODAY'),(21,'DATA_MESSAGE_DEPART_DATE_MUST_NOT_GREATER_EQUAL_TODAY'),(22,'DATA_MESSAGE_RETURN_DATE_MUST_GREATER_DEPART_DATE'),(23,'DATA_PASSENGER'),(24,'DATA_TITLE'),(25,'DATA_FIRST_NAME'),(26,'DATA_LAST_NAME'),(27,'DATA_DOB'),(28,'DATA_MOBILE'),(29,'DATA_EMAIL'),(30,'DATA_PASSENGER_DETAIL'),(31,'DATA_INFANT_PASSENGER'),(32,'DATA_FILL_INFO_OF_PASSENGER'),(33,'DATA_BAGGAGE'),(34,'DATA_TOTAL'),(35,'DATA_ADDITIONAL_COSTS'),(36,'DATA_TOTAL_AMOUNT'),(37,'BUTTON_PAYMENT'),(38,'DATA_PAYMENT_DETAIL'),(39,'DATA_PAYMENT_METHOD'),(40,'DATA_CARD_NUMBER'),(41,'DATA_EXPIRY'),(42,'DATA_NAME_OF_CARD'),(43,'DATA_MESSAGE_ACCEPT_THE_TERM'),(44,'DATA_MESSAGE_PAYMENT_SUCCESSFUL'),(45,'DATA_BOOKING_NUMBER'),(46,'DATA_TOTAL_PAID'),(47,'DATA_MESSAGE_BOOKING_SUCCESSFUL'),(48,'DATA_MESSAGE_ON_HOLD_SUCCESSFUL_1'),(49,'DATA_SELECT_OGRIGIN'),(50,'DATA_SELECT_DESTINATION'),(51,'DATA_SELECT_GENDER'),(52,'DATA_GENDER'),(53,'DATA_MALE'),(54,'DATA_FEMALE'),(55,'DATA_SEGMENT'),(56,'DATA_MESSAGE_ADDITIONAL_SERVICE'),(57,'DATA_BAGGAGE_SERVICES'),(58,'DATA_NO_THANKS'),(59,'DATA_MEALS_SERVICES'),(60,'DATA_ADDITIONAL_COST'),(61,'DATA_SELECT'),(62,'DATA_EXPIRED_MONTH'),(63,'DATA_EXPIRED_YEAR'),(64,'DATA_BILLING_ADDRESS'),(65,'DATA_SELECT_PERSON'),(66,'DATA_COUNTRY'),(67,'DATA_TOWN_CITY'),(68,'DATA_PURCHASER_INFO'),(69,'DATA_CONFIRM_TERMS_CONDITIONS'),(70,'BUTTON_PAY_NOW'),(71,'DATA_MESSAGE_CHECK_YOUR_EMAIL'),(72,'DATA_CHECK_IN_ONLINE'),(73,'DATA_PASSENGER_INFANT'),(74,'DATA_PASSENGER_SPECIAL_NEEDS'),(75,'DATA_PASSENGER_BOOKING'),(76,'DATA_MESSAGE_CHECK_IN_AVAILABLE_FOR_PASSENGER'),(77,'DATA_PASSENGER_INFO'),(78,'DATA_RESERVATION_CODE'),(79,'DATA_MESSAGE_NOT_FLIGHT'),(80,'DATA_MESSAGE_CHECK_RESERVATION'),(81,'BUTTON_BACK'),(82,'DATA_CHOOSE_FLIGHT'),(83,'DATA_MESSAGE_WAITING'),(84,'DATA_CHECKED_IN'),(85,'BUTTON_CLOSED'),(86,'DATA_PASSENGER_LIST'),(87,'DATA_MESSAGE_INFANT_CHECKIN_AIRPORT'),(88,'DATA_GUESTS_SEATED'),(89,'DATA_PHYSICALLY_MENTALLY'),(90,'DATA_CAPABLE'),(91,'DATA_MESSAGE_NOT_STAGE_OF_PREGNACY'),(92,'DATA_MESSAGE_NOT_TRAVELLING_INFANT_NOT_PURCHASE'),(93,'DATA_SAFETY RESONS'),(94,'DATA_LEGEND'),(95,'DATA_AVAILABLE'),(96,'DATA_UNAVALIABLE'),(97,'DATA_OCCUPIED'),(98,'DATA_EXIT_ROW'),(99,'DATA_TRAVELLING_PARTY'),(100,'DATA_LAVATORY'),(101,'DATA_EXIT'),(102,'DATA_SEAT_SELECTION'),(103,'BUTTON_ACCEPT'),(104,'DATA_MESSAGE_BACK_BROWSER_DEVICE'),(105,'BUTTON_CONFIRM'),(106,'DATA_BOARDING_PASS'),(107,'DATA_MESSAGE_SUCCESSFUL_CHECK_IN'),(108,'DATA_MESSAGE_BOARDING_PASS_READY'),(109,'DATA_READY_FLIGHT'),(110,'BUTTON_FINISH'),(111,'DATA_MESSAGE_ON_HOLD_SUCCESSFUL_2'),(112,'DATA_MESSAGE_ON_HOLD_SUCCESSFUL_3'),(113,'BUTTON_CHECK_STATUS'),(114,'DATA_FLIGHT_STATUS'),(115,'BUTTON_VIEW_SEAT'),(116,'DATA_MESSAGE_CANCELLED'),(117,'DATA_SEAT_REVIEW'),(118,'BUTTON_SEARCH_OTHER'),(119,'DATA_EMAIL_ADDRESS'),(120,'BUTTON_SIGN_IN'),(121,'BUTTON_FORGOT_PASSWORD'),(122,'BUTTON_NEW_HERE_SIGN_UP'),(123,'DATA_MESSAGE_EMAIL_ADDRESS'),(124,'DATA_MESSAGE_PASSWORD'),(125,'DATA_MESSAGE_EMAIL_OR_PASSWORD_INCORRECT'),(126,'DATA_MESSAGE_ENTER_EMAIL_ADDRESS'),(127,'DATA_MESSAGE_EMAIL_ADDRESS_REGISTERED'),(128,'BUTTON_SUBMIT'),(129,'DATA_CREATE_ACCOUNT'),(130,'DATA_NEW_ACCOUNT'),(131,'DATA_PASSWORD'),(132,'DATA_VERIFY_PASSWORD'),(133,'DATA_MESSAGE_VERIFY_YOUR_IDENTITY'),(134,'DATA_QUESTION'),(135,'DATA_ANSWER'),(136,'DATA_ACCOUNT_INFORMATION'),(137,'DATA_PASSPORT_NO'),(138,'DATA_NATIONAL'),(139,'DATA_CITY_TOWN'),(140,'DATA_PROVICE_STATE'),(141,'BUTTON_CREATE_NEW_ACCOUNT'),(142,'DATA_READ_AND_AGREE'),(143,'DATA_MESSAGE_EMAIL_EXISTS'),(144,'DATA_MESSAGE_INVALID'),(145,'DATA_YOUR_VERIFY_PASSWORD'),(146,'DATA_MESSAGE_PASSWORD_DO_NOT_MATCH'),(147,'DATA_MESSAGE_QUESTION'),(148,'DATA_MESSAGE_YOUR_ANSWER'),(149,'DATA_MESSAGE_SELECT_TITLE'),(150,'DATA_MESSAGE_FIRST_NAME'),(151,'DATA_MESSAGE_LAST_NAME'),(152,'DATA_MESSAGE_DATE_OF_BIRHT'),(153,'DATA_MESSAGE_YOUR_NATIONAL'),(154,'DATA_MESSAGE_YOUR_PASSPORT_NUMBER'),(155,'DATA_MESSAGE_YOUR_COUNTRY'),(156,'DATA_MESSAGE_YOUR_PROVINCE_STATE'),(157,'DATA_MESSAGE_YOUR_CITY_TOWN'),(158,'DATA_MESSAGE_YOU_MUST_ACCEPT'),(159,'DATA_ACCOUNT_CREATED'),(160,'DATA_WELCOME'),(161,'DATA_MESSAGE_ENJOIN_OUR_SERVICES'),(162,'DATA_MESSAGE_SENT_YOU_EMAIL_TO_VERIFY'),(163,'BUTTON_HOME'),(164,'DATA_UPDATE_PROFILE'),(165,'DATA_BOOKING_FILGHT'),(166,'DATA_RESERVATION_HISTORY'),(167,'DATA_CHANGE_PASSWORD'),(168,'DATA_YOU_EARN'),(169,'DATA_POINTS'),(170,'DATA_OLD_PASSWORD'),(171,'DATA_NEW_PASSWORD'),(172,'DATA_VERIFY_NEW_PASSWORD'),(173,'DATA_MESSAGE_YOUR_OLD_PASSWORD'),(174,'DATA_MESSAGE_YOUR_NEW_PASSWORD'),(175,'DATA_MESSAGE_YOUR_VERIFY_NEW_PASSWORD'),(176,'DATA_MESSAGE_YOUR_PASSWORD_CHANGED'),(177,'BUTTON_UPDATE'),(178,'DATA_GUEST_NAME'),(179,'DATA_PHONE_NO'),(180,'DATA_DONE'),(181,'DATA_RETURN'),(182,'DATA_PROMO_CODE'),(183,'DATA_TRANSIT_TIME'),(184,'DATA_BUSSINESS'),(185,'DATA_SOLDOUT'),(186,'DATA_CHOOSE_TITLE'),(187,'DATA_CHILDREN_DETAILS'),(188,'DATA_ADDRESS'),(189,'DATA_CVV'),(190,'DATA_PHONE_NUMBER'),(191,'DATA_READ_UNDERSTOOD_ACCEPTED'),(192,'DATA_TERMS_CONDITIONS'),(193,'DATA_OF_IAS'),(194,'DATA_HOURS'),(195,'DATA_MESSAGE_LEFT_UNPAID'),(196,'DATA_CHECK_IN_ONLINE_NOT_AVAILABLE'),(197,'DATA_CHECK_IN_ONLINE_AVAILABLE'),(198,'DATA_CHECK_IN_ONLINE_AVAILABLE_2'),(199,'DATA_CHECK_IN_ONLINE_AVAILABLE_1'),(200,'DATA_RESERVATION_INFO'),(201,'BUTTON_CHECK_IN'),(202,'DATA_MESSAGE_PASSENGER_WITH_INFANT_NOT_CHECKIN_ONLINE'),(203,'DATA_MESSAGE_RESERVATION_BALANCE_OWING'),(204,'DATA_MESSAGE_UNABLE_CHECKIN_BALANCE_PAID'),(205,'DATA_MESSAGE_WITH_INFANT_CHECKIN_AIRPORT'),(206,'DATA_BOOKING_FLIGHT'),(207,'DATA_MEMBER'),(208,'DATA_INFO'),(209,'DATA_LANGUAGE'),(210,'DATA_LOADING_DATA'),(211,'DATA_RESERVATION_NO'),(212,'DATA_CONTACT_NAME'),(213,'DATA_CONTACT_FOR'),(214,'DATA_CONTACT_IAS'),(215,'DATA_LOGOUT'),(216,'DATA_PASPORT_NO'),(217,'DATA_CHOOSE_QUESTION_ANSWER_VERIFY_IF_FORGET_PWD'),(218,'DATA_MESSAGE_ADULTS_MUST_RANGE_1_TO_9'),(219,'DATA_MESSAGE_VALUES_MUST_LESS_THAN_OR_EQUAL9'),(220,'DATA_MESSAGE_CHILDREN_MUST_RANGE_1_TO_9'),(221,'DATA_MESSAGE_TOTAL_NUMBER_OF_ADULT_AND_CHILDREN_EQUAL9'),(222,'DATA_MESSAGE_TOTAL_NUMBER_OF_ADULT_IS_EQUAL_TOTAL_NUMBER_ADULTS'),(223,'DATA_MESSAGE_CHOOSE_DESTINATION'),(224,'DATA_MESSAGE_CHOOSE_FLIGHTS_RETURN'),(225,'DATA_MESSAGE_DEPART_DATE_GREATER_RETURN_DATE'),(226,'DATA_MESSAGE_CHOOSE_FLIGHTS_DEPART'),(227,'DATA_MESSAGE_SEAT_NOT_AVAILABLE'),(228,'DATA_MESSAGE_CHOOSE_SEAT_FOR_PASSENGER'),(229,'DATA_MESSAGE_ENTER_PHONE_NUMBER'),(230,'DATA_MESSAGE_ENTER_YOUR_ADDRESS'),(231,'DATA_MESSAGE_FILL_ALL_INFORMATION_OF_USER_REGISTER'),(232,'DATA_MESSAGE_EMAIL_ALREADY_EXISTS'),(233,'DATA_MESSAGE_FILL_ALL_FIELD'),(234,'DATA_USE_PRIMARY_PAX'),(235,'DATA_STOP'),(236,'DATA_CONNECTION');
/*!40000 ALTER TABLE `tbl_phrase` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_user`
--

DROP TABLE IF EXISTS `tbl_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_user` (
  `UserID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `UserName` varchar(60) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `DisplayName` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `Email` varchar(320) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `Role` bit(1) NOT NULL,
  `CreateOn` datetime DEFAULT NULL,
  PRIMARY KEY (`UserID`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_user`
--

LOCK TABLES `tbl_user` WRITE;
/*!40000 ALTER TABLE `tbl_user` DISABLE KEYS */;
INSERT INTO `tbl_user` VALUES (1,'nguyen thi','nguyenthi','thi.nguyen@exe.com.vn','6+KoBV/JHdE=','','2014-12-22 00:00:00'),(2,NULL,'Hung Nguyen','hung.nguyen@exe.com.vn','B5Dve3pW1Qc=','','2014-12-31 16:26:03'),(3,NULL,'Tien Nguyen','tien.nguyen@exe.com.vn','B5Dve3pW1Qc=','','2015-01-20 17:35:40'),(4,NULL,'Lam Dam','lam.dam@exe.com.vn','B5Dve3pW1Qc=','','2015-01-20 17:36:02'),(7,NULL,'THI NGUYEN','mrthiitvn@gmail.com','B5Dve3pW1Qc=','','2015-03-03 14:51:23');
/*!40000 ALTER TABLE `tbl_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_configuration`
--

DROP TABLE IF EXISTS `tbl_configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_configuration` (
  `ConfigID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ConfigCode` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `ConfigDescription` text COLLATE utf8_unicode_ci,
  `ConfigGroup` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ConfigData` text COLLATE utf8_unicode_ci,
  `ConfigValue` int(11) DEFAULT NULL,
  `CreateOn` datetime DEFAULT NULL,
  `CreateBy` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `UpdateOn` datetime DEFAULT NULL,
  `UpdateBy` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ConfigID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_configuration`
--

LOCK TABLES `tbl_configuration` WRITE;
/*!40000 ALTER TABLE `tbl_configuration` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_configuration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_languagephrase`
--

DROP TABLE IF EXISTS `tbl_languagephrase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_languagephrase` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `LanguageID` int(11) NOT NULL,
  `PhraseID` int(11) NOT NULL,
  `PhraseValue` text CHARACTER SET utf8,
  PRIMARY KEY (`ID`),
  KEY `LanguageID_index` (`LanguageID`),
  KEY `PhraseID_index` (`PhraseID`)
) ENGINE=InnoDB AUTO_INCREMENT=473 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_languagephrase`
--

LOCK TABLES `tbl_languagephrase` WRITE;
/*!40000 ALTER TABLE `tbl_languagephrase` DISABLE KEYS */;
INSERT INTO `tbl_languagephrase` VALUES (1,2,1,'Điểm khởi hành'),(2,2,2,'Điển đến'),(3,2,3,'Ngày khởi hành'),(4,2,4,'Ngày về'),(5,2,5,'Người lớn'),(6,2,6,'Trẻ em'),(7,2,7,'Em bé'),(8,2,8,'Mã khuyến mại'),(9,2,9,'TÌM CHUYẾN BAY'),(10,2,10,'Chuyến bay'),(11,2,11,'đến'),(12,2,12,'Mã khuyến mại không hợp lệ cho tất cả các vé.'),(13,2,13,'Mã khuyến mại'),(14,2,14,'giảm giá'),(15,2,15,'Tiếp tục'),(16,2,16,'Từ'),(17,2,17,'Khởi hành'),(18,2,18,'Vui lòng chọn chuyến khởi hành'),(19,2,19,'Vui lòng chọn chuyến trở về'),(20,2,20,'Ngày khởi hành phải lớn hơn hoặc bằng ngày hiện tại'),(21,2,21,'Ngày khởi hành phải nhỏ hơn hoặc bằng ngày trở về'),(22,2,22,'Ngày trở về phải lớn hơn ngày khởi hành'),(23,2,23,'Hành khách'),(24,2,24,'Quý danh'),(25,2,25,'Họ'),(26,2,26,'Tên'),(27,2,27,'Ngày sinh'),(28,2,28,'Điện thoại'),(29,2,29,'Email'),(30,2,30,'Thông tin hành khách'),(31,2,31,'Thông tin em bé'),(32,2,32,'Vui lòng điền đầy đủ thông tin của tất cả các hành khách'),(33,2,33,'Hành lý'),(34,2,34,'Tổng cộng'),(35,2,35,'Chi phí thêm'),(36,2,36,'Tổng số tiền'),(37,2,37,'Thanh Toán'),(38,2,38,'Thông tin thanh toán'),(39,2,39,'Hình thức thanh toán'),(40,2,40,'Số thẻ'),(41,2,41,'Ngày hết hạn'),(42,2,42,'Tên chủ thẻ'),(43,2,43,'Bạn phải chấp nhận điều lệ để tiếp tục thao tác'),(44,2,44,'Thanh toán thành công'),(45,2,45,'Mã số đặt chỗ'),(46,2,46,'Tổng số tiền'),(47,2,47,'Đặt chỗ thành công'),(48,2,48,'Đặt chỗ của bạn được giữ đến'),(49,2,49,'-- Chọn điểm khởi hành --'),(50,2,50,'-- Chọn điểm đến --'),(51,2,51,'-- Chọn giới tính --'),(52,2,52,'Giới tính'),(53,2,53,'Nam'),(54,2,54,'Nữ'),(55,2,55,'Chặng bay'),(56,2,56,'Không có dịch vụ bổ sung'),(57,2,57,'Dịch vụ hành lý'),(58,2,58,'Không cảm ơn'),(59,2,59,'Dịch vụ thức ăn'),(60,2,60,'Chi phí khác'),(61,2,61,'Chọn'),(62,2,62,'Tháng hết hạn'),(63,2,63,'Năm hết hạn'),(64,2,64,'Địa chỉ thanh toán'),(65,2,65,'Chọn hành khách'),(66,2,66,'Nước'),(67,2,67,'Thành phố'),(68,2,68,'Thông tin người mua'),(69,2,69,'Tôi đã đọc, hiểu và chấp nhận điều lệ'),(70,2,70,'THANH TOÁN NGAY'),(71,2,71,'Vui lòng kiểm tra email để xem lại thông tin thanh toán của bạn hoặc xem thêm thông tin tại trang Intelysis'),(72,2,72,'Check In trực tuyến không hỗ trợ đối với:'),(73,2,73,'Hành khách mang theo em bé'),(74,2,74,'Hành khách mangg theo hàng hóa đặc biệt'),(75,2,75,'Nhiều hơn 10 hành khách trên một đặt vé'),(76,2,76,'Hành khách có thể check in trực tuyến trước giờ bay 48 giờ đến 4 giờ'),(77,2,77,'Thông tin hành khách'),(78,2,78,'Mã đặt chỗ'),(79,2,79,'Không có chuyến bay nào'),(80,2,80,'Vui lòng kiểm tra lại đặt chỗ của bạn'),(81,2,81,'QUAY LẠI'),(82,2,82,'CHỌN CHUYẾN BAY'),(83,2,83,'Đợi'),(84,2,84,'Đã Check In'),(85,2,85,'Đã bay'),(86,2,86,'DANH SÁCH HÀNH KHÁCH'),(87,2,87,'Với em bé, vui lòng check in tại sân bay'),(88,2,88,'Hành khách ngồi tại hàng ghế thoát hiểm phải đáp ứng các điều kiện:'),(89,2,89,'Dành cho hành khách có điều kiện sức khỏe và tinh thần phù hợp'),(90,2,90,'Có khả năng đọc/nghe hiểu các hướng dẫn trong trường hợp khẩn cấp'),(91,2,91,'Không dành cho phụ nữ mang thai'),(92,2,92,'Không có trẻ sơ sinh đi kèm'),(93,2,93,'Vì lý do an toàn, phi hành đoàn của chúng tôi bảo lưu quyền chuyển nhượng một chỗ ngồi trên máy bay mới, nếu khách hàng không đáp ứng các tiêu chí trên.'),(94,2,94,'Chú thích'),(95,2,95,'Sẵn sàng'),(96,2,96,'Không sẵn sàng'),(97,2,97,'Đang sử dụng'),(98,2,98,'Hàng ghế thoát hiểm'),(99,2,99,'Travelling Party'),(100,2,100,'Nhà vệ sinh'),(101,2,101,'Thoát'),(102,2,102,'Chọn ghế ngồi'),(103,2,103,'Đồng ý'),(104,2,104,'Để thay đổi ghế, vui lòng chạm vào nút quay lại của trình duyệt hoặc của thiết bị'),(105,2,105,'XÁC NHẬN'),(106,2,106,'THẺ LÊN MÁY BAY'),(107,2,107,'BẠN ĐÃ CHECK IN THÀNH CÔNG'),(108,2,108,'THẺ LÊN MÁY BAY CỦA BẠN ĐÃ SẴN SÀNG'),(109,2,109,'SẴN SÀNG ĐỂ BAY'),(110,2,110,'HOÀN THÀNH'),(111,2,111,'Nếu chưa thanh toán, vào lúc'),(112,2,112,'đặt chỗ của bạn sẽ tự động bị hủy bỏ'),(113,2,113,'Kiểm tra trạng thái'),(114,2,114,'Trạng thái chuyến bay'),(115,2,115,'Xem chỗ ngồi'),(116,2,116,'Đã hủy'),(117,2,117,'Xem lại chỗ ngồi'),(118,2,118,'Tìm kiếm khác'),(119,2,119,'Địa chỉ email'),(120,2,120,'Đăng nhập'),(121,2,121,'Quên mật khẩu'),(122,2,122,'Đăng ký'),(123,2,123,'Vui lòng nhập địa chỉ email'),(124,2,124,'Vui lòng nhập mật khẩu'),(125,2,125,'Email hoặc mật khẩu không chính xác'),(126,2,126,'Nhập địa chỉ email'),(127,2,127,'Nhập vào địa chỉ hộp thư bạn đã sử dụng khi đăng ký với Intelisys. Sau đó chúng tôi sẽ gửi mật khẩu vào hộp thư của bạn'),(128,2,128,'Gửi'),(129,2,129,'Tạo tài khoản'),(130,2,130,'Tài khoản mới'),(131,2,131,'Mật khẩu'),(132,2,132,'Xác nhận mật khẩu'),(133,2,133,'Nhập câu hỏi và câu trả lời của bạn để chúng tôi xác minh danh tính nếu bạn quên mật khẩu'),(134,2,134,'Câu hỏi'),(135,2,135,'Trả lời'),(136,2,136,'Thông tin tài khoản'),(137,2,137,'Sô hộ chiếu'),(138,2,138,'Quốc tịch'),(139,2,139,'Thành phố'),(140,2,140,'Tỉnh'),(141,2,141,'Tạo tài khoản mới'),(142,2,142,'Tôi đã đọc và đồng ý với điều lệ'),(143,2,143,'Email mail đã tồn tại. Xin thử lại'),(144,2,144,'Email không hợp lệ. Xin thử lại'),(145,2,145,'Vui lòng nhập xác nhận mật khẩu'),(146,2,146,'Mật khẩu không khớp. Thử lại?'),(147,2,147,'Vui lòng nhập câu hỏi'),(148,2,148,'Vui lòng nhập câu trả lời'),(149,2,149,'Vui lòng chọn quý danh'),(150,2,150,'Vui lòng nhập họ của bạn'),(151,2,151,'Vui lòng nhập tên của bạn'),(152,2,152,'Vui lòng nhập ngày sinh của bạn'),(153,2,153,'Vui lòng chọn quốc tịch của bạn'),(154,2,154,'Vui lòng nhập số hộ chiếu của bạn'),(155,2,155,'Vui lòng chọn quốc gia'),(156,2,156,'Vui lòng chọn tỉnh/bang'),(157,2,157,'Vui lòng chọn thành phố'),(158,2,158,'Bạn phải đồng ý với điều lệ để tiếp tục'),(159,2,159,'Tài khoản đã được tạo'),(160,2,160,'Xin chào!'),(161,2,161,'Cảm ơn bạn đã sử dụng dịch vụ của Intelisys'),(162,2,162,'Chúng tôi đã gửi cho bạn một email để xác minh địa chỉ email của bạn. Hãy truy cập vào email của bạn và nhấp vào liên kết được cung cấp để xác nhận tài khoản Intelisys của bạn.'),(163,2,163,'Trang chủ'),(164,2,164,'Cập nhật hồ sơ'),(165,2,165,'Đạt vé chuyến bay'),(166,2,166,'Lịch sử đặt vé'),(167,2,167,'Thay đổi mật khẩu'),(168,2,168,'Bạn kiếm'),(169,2,169,'điềm'),(170,2,170,'Mật khẩu cũ'),(171,2,171,'Mật khẩu mới'),(172,2,172,'Mật khẩu mới xác thực'),(173,2,173,'Vui lòng nhập vào mật khẩu cũ của bạn'),(174,2,174,'Vui lòng nhập vào mật khẩu mới của bạn'),(175,2,175,'Vui lòng nhập vào mật khẩu mới xác minh của bạn'),(176,2,176,'Mật khẩu của bạn đã được thay đổi'),(177,2,177,'Cập nhật'),(178,2,178,'Tên khách hàng'),(179,2,179,'Điện thoại'),(180,2,180,'Xong'),(181,2,181,'Trở về'),(182,2,182,'Mã khuyến mãi'),(183,2,183,'Thời gian quá cảnh'),(184,2,184,'Kinh doanh'),(185,2,185,'Đã hết bán'),(186,2,186,'--Chọn tiêu đề--'),(187,2,187,'Chi tiết trẻ em'),(188,2,188,'Địa chỉ'),(189,2,189,'CVV'),(190,2,190,'Số điện thoại'),(191,2,191,'Tôi đã đọc, hiểu và chấp nhận'),(192,2,192,'điều khoản và điều lệ'),(193,2,193,'của Intelysis'),(194,2,194,'giờ'),(195,2,195,'Nếu không thanh toán vào lúc'),(196,2,196,'Check in trực tuyến không hỗ trợ cho các trường hợp:'),(197,2,197,'Check in trực tuyến chỉ dành cho hành khách check in trong vòng'),(198,2,198,'trước chuyến bay'),(199,2,199,'và'),(200,2,200,'Thông tin đặt vé'),(201,2,201,'Kiểm tra'),(202,2,202,'Hành khách du lịch cùng với trẻ sơ sinh, không kiểm tra trực tuyến'),(203,2,203,'Đặt chỗ có dư nợ'),(204,2,204,'Không thể kiểm tra cho đến khi cân đối được chi trả'),(205,2,205,'Với trẻ sơ sinh. Vui lòng check in tại sân bay'),(206,2,206,'Đặt vé chuyến bay'),(207,2,207,'Thành viên'),(208,2,208,'Thông tin'),(209,2,209,'Ngôn ngữ'),(210,2,210,'Đang tải dữ liệu'),(211,2,211,'Mã đặt vé'),(212,2,212,'Tên liên hệ'),(213,2,213,'Liên hệ'),(214,2,214,'Thông tin liên hệ IAS'),(215,2,215,'Đăng xuất'),(216,2,216,'Số hộ chiếu'),(217,2,217,'chọn câu hỏi và trả lời để giúp chúng tôi xác minh danh tính của bạn nếu bạn quên mật khẩu của bạn'),(218,2,218,'Số lượng người lớn phải nằm trong phạm vi từ 1 tới 9'),(219,2,219,'Giá trị phải nhỏ hơn hoặc bằng 9'),(220,2,220,'Số lượng trẻ em phải nằm trong phạm vi từ 1 tới 9'),(221,2,221,'Tổng số lượng của người lớn và trẻ em là 9'),(222,2,222,'Tổng số trẻ sơ sinh là tương đương với tổng số người lớn'),(223,2,223,'Vui lòng chọn điểm đến'),(224,2,224,'Vui lòng chọn chuyến bay trở về'),(225,2,225,'Ngày khởi hành phải không lớn hơn ngày trở về'),(226,2,226,'Vui lòng chọn chuyến bay khởi hành'),(227,2,227,'Chỗ ngồi không sẵng sàng'),(228,2,228,'Vui lòng chọn chỗ ngồi cho hành khách'),(229,2,229,'Vui lòng nhập số điện thoại của bạn'),(230,2,230,'Vui lòng nhập địa chỉ email của bạn'),(231,2,231,'Vui lòng điền tất cả thông tin đăng ký của người dùng'),(232,2,232,'Email này đã tồn tại. Hãy thử email khác.'),(233,2,233,'Vui lòng điền vào tất cả các trường'),(234,4,1,'Origin'),(235,4,2,'Destination'),(236,4,3,'Depart Date'),(237,4,4,'Return Date'),(238,4,5,'Adults'),(239,4,6,'Children'),(240,4,7,'Infant'),(241,4,8,'Promo Code'),(242,4,9,'FIND FLIGHT'),(243,4,10,'Flight'),(244,4,11,'to'),(245,4,12,'The Promo Code is invalid for all available fares'),(246,4,13,'The Promo Code'),(247,4,14,'Discount'),(248,4,15,'Continue'),(249,4,16,'From'),(250,4,17,'Depart'),(251,4,18,'Please select a fare for the outbound flight'),(252,4,19,'Please select a fare for the return flight'),(253,4,20,'Depart date must be greater than or equal today'),(254,4,21,'Depart date must not be greater than return date'),(255,4,22,'Return date must be greater than depart date'),(256,4,23,'Passenger'),(257,4,24,'Title'),(258,4,25,'First Name'),(259,4,26,'Last Name'),(260,4,27,'DOB'),(261,4,28,'Mobile'),(262,4,29,'Email'),(263,4,30,'Passenger Detail'),(264,4,31,'Infant Passenger'),(265,4,32,'Please fill in all information of all passengers'),(266,4,33,'Baggage'),(267,4,34,'Total'),(268,4,35,'Additional Costs'),(269,4,36,'Total amount'),(270,4,37,'Payment'),(271,4,38,'Payment Detail'),(272,4,39,'Payment method'),(273,4,40,'Card number'),(274,4,41,'Expiry'),(275,4,42,'Name of Card'),(276,4,43,'You must accept the term and conditions to continue'),(277,4,44,'Payment Successful'),(278,4,45,'Booking number'),(279,4,46,'Total paid'),(280,4,47,'Booking Successful'),(281,4,48,'Your Reservation has been placed on hold for'),(282,4,49,'-- choose origin --'),(283,4,50,'-- choose destination --'),(284,4,51,'-- choose gender --'),(285,4,52,'Gender'),(286,4,53,'Male'),(287,4,54,'Female'),(288,4,55,'Segment'),(289,4,56,'There is not additional services'),(290,4,57,'Baggage Services'),(291,4,58,'No thanks'),(292,4,59,'Meals Services'),(293,4,60,'Additional Cost'),(294,4,61,'Select'),(295,4,62,'Expired month'),(296,4,63,'Expired year'),(297,4,64,'Billing address'),(298,4,65,'Select person'),(299,4,66,'Country'),(300,4,67,'Town/City'),(301,4,68,'Purchaser Infomation'),(302,4,69,'I have read, understand and accepted the terms & conditions'),(303,4,70,'PAY NOW'),(304,4,71,'Please check your email to review your payment or view more information at Intelysis full site'),(305,4,72,'Check In online is not avaiable for:'),(306,4,73,'Passenger(s); travelling with infant'),(307,4,74,'Passenger(s); travelling with special needs'),(308,4,75,'more than 10 Passengers on one Booking'),(309,4,76,'Check In online is avaiable for passenger check in 48 hours prior and 4 hours before the flight'),(310,4,77,'Passenger information'),(311,4,78,'Reservation code'),(312,4,79,'There is not flights'),(313,4,80,'Please check your reservation'),(314,4,81,'BACK'),(315,4,82,'CHOOSE YOUR FLIGHT'),(316,4,83,'Waiting'),(317,4,84,'Checked In'),(318,4,85,'Closed'),(319,4,86,'PASSENGER LIST'),(320,4,87,'With infant, Please check in the airport'),(321,4,88,'Guests seated at the emergency exit row MUST meet the following criteria'),(322,4,89,'Physically and mentally fit to assist crew in an emergency.'),(323,4,90,'Capable of understanding printed/ spoken emergency instructions.'),(324,4,91,'Not in any stage of pregnancy.'),(325,4,92,'Not travelling with infants and did not purchase an extra seat'),(326,4,93,'For safety reasons, our cabin crew reserves the right to assign a new seat onboard if guests do not meet the above criteria.'),(327,4,94,'Legend'),(328,4,95,'Available'),(329,4,96,'Unavailable'),(330,4,97,'Occupied'),(331,4,98,'Exit Row'),(332,4,99,'Travelling Party'),(333,4,100,'Lavatory'),(334,4,101,'Exit'),(335,4,102,'Seat Selection'),(336,4,103,'Accept'),(337,4,104,'To change the seat. Please tap on the back button of browser or device'),(338,4,105,'CONFIRM'),(339,4,106,'BOARDING PASS'),(340,4,107,'YOU HAVE SUCCESSFUL CHECKED IN.'),(341,4,108,'YOUR BOARDING PASS IS READY'),(342,4,109,'READY TO FLIGHT'),(343,4,110,'FINISH'),(344,4,111,'If left unpaid, at'),(345,4,112,'your reservation will automatically be cancelled'),(346,4,113,'Check Status'),(347,4,114,'Flight Status'),(348,4,115,'View Seat'),(349,4,116,'Cancelled'),(350,4,117,'Seat review'),(351,4,118,'Search other'),(352,4,119,'E-mail address'),(353,4,120,'Sign in'),(354,4,121,'Forgot password'),(355,4,122,'New here? Sign Up'),(356,4,123,'Please enter your email address'),(357,4,124,'Please enter your password'),(358,4,125,'The email or password you entered is incorrect'),(359,4,126,'Enter your email address'),(360,4,127,'Type in the email address you used when you registered with Intelisys. Then we\'ll email password to this address'),(361,4,128,'Submit'),(362,4,129,'Create account'),(363,4,130,'New account'),(364,4,131,'Password'),(365,4,132,'Verify password'),(366,4,133,'Enter your question and answer that to help us verify your identity if you forget your password'),(367,4,134,'Question'),(368,4,135,'Answer'),(369,4,136,'Account Information'),(370,4,137,'Passport no.'),(371,4,138,'National'),(372,4,139,'City/Town'),(373,4,140,'Province/State'),(374,4,141,'Create a new account'),(375,4,142,'I have read and agree with the terms & conditions'),(376,4,143,'This email already exists. Try another'),(377,4,144,'This email is invalid. Try another'),(378,4,145,'Please enter your verify password'),(379,4,146,'These password don’t match. Try again?'),(380,4,147,'Please enter a question'),(381,4,148,'Please enter your answer'),(382,4,149,'Please select a title'),(383,4,150,'Please enter your first name'),(384,4,151,'Please enter your last name'),(385,4,152,'Please select your date of birth'),(386,4,153,'Please select your national'),(387,4,154,'Please enter your passport number'),(388,4,155,'Please select your country'),(389,4,156,'Please select your province/state'),(390,4,157,'Please enter your city/town'),(391,4,158,'You must accept the terms and conditions to continue'),(392,4,159,'Account created'),(393,4,160,'Welcome!'),(394,4,161,'Thanks for joining Intelisys. We hope you enjoy our services'),(395,4,162,'We have sent you an email to verify your email address. Please access your email and click on the provided link to confirm your Intelisys account.'),(396,4,163,'Home'),(397,4,164,'Update profile'),(398,4,165,'Booking flight'),(399,4,166,'Reservation History'),(400,4,167,'Change password'),(401,4,168,'You earn'),(402,4,169,'points'),(403,4,170,'Old password'),(404,4,171,'New password'),(405,4,172,'Verify new password'),(406,4,173,'Please enter your old password'),(407,4,174,'Please enter your new password'),(408,4,175,'Please enter your verify new password'),(409,4,176,'Your password has been changed'),(410,4,177,'Update'),(411,4,178,'Guest name'),(412,4,179,'Phone'),(413,4,180,'Done'),(414,4,181,'Return'),(415,4,182,'The Promo Code'),(416,4,183,'Connection Time'),(417,4,184,'Business'),(418,4,185,'Sold out'),(419,4,186,'-- choose title --'),(420,4,187,'Children details'),(421,4,188,'Address'),(422,4,189,'CVV'),(423,4,190,'Phone number'),(424,4,191,'I have read, understood and accepted the'),(425,4,192,'terms & conditions'),(426,4,193,'of Intelysis'),(427,4,194,'hours'),(428,4,195,'If left unpaid at'),(429,4,196,'Check in online is NOT available for'),(430,4,197,'Check in online is available for passenger check in'),(431,4,198,'before the flights'),(432,4,199,'prior and'),(433,4,200,'Reservation information'),(434,4,201,'Check In'),(435,4,202,'Passenger(s); travelling with infant, not checkin online'),(436,4,203,'This reservation has a balance owing of'),(437,4,204,'Unable to check in until balance is paid'),(438,4,205,'With infant. Please check in at the airport'),(439,4,206,'Booking a Flight'),(440,4,207,'Member'),(441,4,208,'Information'),(442,4,209,'Language'),(443,4,210,'Loading data'),(444,4,211,'Reservation No'),(445,4,212,'Contact Name'),(446,4,213,'Contact for'),(447,4,214,'Contact intelisys'),(448,4,215,'Logout'),(449,4,216,'Passport no'),(450,4,217,'choose question and answer that to help us verify your identity if you forget your password'),(451,4,218,'The natural number of adults must be in the range 1 to 9'),(452,4,219,'Value must be less than or equal 9'),(453,4,220,'The natural number of children must be in the range 1 to 9'),(454,4,221,'Total number of adult and children is equal 9'),(455,4,222,'Total number of infant is equal total number of adults'),(456,4,223,'Please choose Destination'),(457,4,224,'Please choose Flights return'),(458,4,225,'Depart date time must not be greater than return date time'),(459,4,226,'Please choose Flights depart'),(460,4,227,'Seat not available'),(461,4,228,'Please choose seat for passenger'),(462,4,229,'Please enter your phone number'),(463,4,230,'Please enter your address'),(464,4,231,'Please fill in all information of user register form'),(465,4,232,'This email already exists. Try another.'),(466,4,233,'Please fill in all field'),(467,4,234,'Use Passenger 1 Address for Billing Address?'),(468,2,234,'Dùng địa chỉ của hành khách 1 để làm địa chỉ gửi hóa đơn ?'),(469,4,235,'Stop'),(470,2,235,'Điểm dừng'),(471,4,236,'Connection'),(472,2,236,'Chuyển tiếp');
/*!40000 ALTER TABLE `tbl_languagephrase` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_language`
--

DROP TABLE IF EXISTS `tbl_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_language` (
  `LanguageID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `LanguageName` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `LanguageCode` varchar(10) NOT NULL,
  `IsDefault` bit(1) NOT NULL,
  PRIMARY KEY (`LanguageID`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_language`
--

LOCK TABLES `tbl_language` WRITE;
/*!40000 ALTER TABLE `tbl_language` DISABLE KEYS */;
INSERT INTO `tbl_language` VALUES (4,'English','EN',''),(2,'Vietnamese','VI','\0');
/*!40000 ALTER TABLE `tbl_language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'gms_cms'
--
/*!50003 DROP PROCEDURE IF EXISTS `ContactTest` */;
ALTER DATABASE gms_cms CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `ContactTest`(IN `@LanguageCode` varchar(5))
BEGIN
 SELECT * FROM tbl_Contact WHERE LanguageCode=@LanguageCode;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
ALTER DATABASE gms_cms CHARACTER SET utf8 COLLATE utf8_general_ci ;
/*!50003 DROP PROCEDURE IF EXISTS `new_routine` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `new_routine`(
IN `s_PhraseID` INT UNSIGNED,
IN s_PhraseName VARCHAR(250)
)
BEGIN
 UPDATE tbl_Phrase
 SET PhraseName=s_PhraseName
 WHERE PhraseID=s_PhraseID;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_ChangePassword` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `sp_ChangePassword`(
IN s_Email VARCHAR(250) CHARSET utf8,
IN s_Password VARCHAR(250) CHARSET utf8
)
BEGIN
SET SQL_SAFE_UPDATES=0;
 UPDATE tbl_user SET Password=s_Password WHERE Email=s_Email;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_ConfigurationDelete` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `sp_ConfigurationDelete`(
IN s_ConfigID INT
)
BEGIN
    DELETE FROM  tbl_Configuration WHERE ConfigID=s_ConfigID;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_ConfigurationUpdate` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `sp_ConfigurationUpdate`(
IN s_ConfigID INT,
IN s_ConfigCode VARCHAR(10),
IN sConfigDescription TEXT,
IN s_ConfigGroup VARCHAR(60),
IN s_ConfigData VARCHAR(255),
IN s_ConfigValue INT,
IN s_UpdateBy VARCHAR(255),
IN s_UpdateOn DATETIME
)
BEGIN
    UPDATE tbl_Configuration
        SET ConfigID=s_ConfigID, ConfigCode=s_ConfigCode,ConfigDescription=s_ConfigDescription,ConfigGrouup=s_ConfigGrouup,ConfigData=s_ConfigData,ConfigValue=s_ConfigValue, UpdateBy=s_UpdateBy, UpdateOn=s_UpdateOn;
    
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_Contact_Delete` */;
ALTER DATABASE gms_cms CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `sp_Contact_Delete`(IN `s_ContactID` INT UNSIGNED)
    NO SQL
BEGIN
DELETE FROM tbl_Contact WHERE ContactID=s_ContactID;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
ALTER DATABASE gms_cms CHARACTER SET utf8 COLLATE utf8_general_ci ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_Contact_Insert` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `sp_Contact_Insert`(IN `s_ContactID` INT(5) UNSIGNED,
IN `s_ContactTitle` TEXT CHARSET utf8,
IN `s_LanguageCode` VARCHAR(5) CHARSET utf8,
IN `s_ContactPhone` TEXT,
IN `s_Contactfor` TEXT CHARSET utf8,
IN `s_ContactAddress` TEXT CHARSET utf8, 
IN `s_ContactContent` TEXT CHARSET utf8, 
IN `s_ContactLat` VARCHAR(255) CHARSET utf8, 
IN `s_ContactLong` VARCHAR(255) CHARSET utf8,
IN `s_CreateOn` DATE,
IN `s_CreateBy` VARCHAR(255) CHARSET utf8)
    NO SQL
    DETERMINISTIC
BEGIN
	
		INSERT INTO tbl_Contact (ContactTitle,LanguageCode,ContactPhone, ContactAddress, Contactfor, ContactContent, ContactLat, ContactLong, CreateOn,CreateBy)
				VALUES(s_ContactTitle,s_LanguageCode, s_ContactPhone, s_ContactAddress, s_Contactfor, s_ContactContent, s_ContactLat, s_ContactLong,s_CreateOn,s_CreateBy);
	
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_Contact_Update` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `sp_Contact_Update`(IN `s_ContactID` INT(5) UNSIGNED, 
IN `s_ContactTitle` TEXT CHARSET utf8, 
IN `s_LanguageCode` VARCHAR(10) CHARSET utf8, 
IN `s_ContactPhone` TEXT CHARSET utf8, 
IN `s_ContactAddress` TEXT CHARSET utf8, 
IN `s_Contactfor` TEXT CHARSET utf8,
IN `s_ContactContent` TEXT CHARSET utf8,
IN `s_UpdateOn` DATE, 
IN `s_UpdateBy` VARCHAR(255) CHARSET utf8,
IN `s_ContactLat` VARCHAR(255),
IN `s_ContactLong` VARCHAR(255))
    NO SQL
BEGIN
UPDATE tbl_Contact SET 
ContactTitle= s_ContactTitle,
LanguageCode=s_LanguageCode, 
ContactPhone=s_ContactPhone, 
ContactAddress=s_ContactAddress, 
Contactfor=s_Contactfor,
ContactContent= s_ContactContent,
ContactLat=s_ContactLat, 
ContactLong=s_ContactLong,
UpdateOn=s_UpdateOn,
UpdateBy=s_UpdateBy 
WHERE ContactID=s_ContactID;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_GetFareRuleList` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `sp_GetFareRuleList`()
BEGIN
    SELECT * FROM tbl_InformationPage 
    WHERE  InfoGroup='FARERULE' AND LanguageCode='EN'
    GROUP BY InfoSlug;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_GetInformationPageByInfoSlug` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `sp_GetInformationPageByInfoSlug`(
IN `s_InfoSlug` VARCHAR(250)
)
BEGIN
    	SELECT * FROM tbl_InformationPage
	WHERE InfoSlug=s_InfoSlug;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_GetListContact` */;
ALTER DATABASE gms_cms CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `sp_GetListContact`()
begin 
SELECT * FROM tbl_Contact ct INNER JOIN tbl_Language lg ON ct.LanguageCode=lg.LanguageCode;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
ALTER DATABASE gms_cms CHARACTER SET utf8 COLLATE utf8_general_ci ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_GetListContactByLanguageCode` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `sp_GetListContactByLanguageCode`(IN `s_LanguageCode` VARCHAR(5) CHARSET utf8)
BEGIN
SELECT * FROM tbl_Contact
	WHERE LanguageCode=s_LanguageCode;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_GetListInformationPage` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `sp_GetListInformationPage`()
    NO SQL
BEGIN
SELECT * FROM tbl_InformationPage 
WHERE  InfoGroup = 'INFO' AND LanguageCode='EN'
GROUP BY InfoSlug;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_GetListInformationPageByInfoSlug` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `sp_GetListInformationPageByInfoSlug`(
IN `s_InfoSlug` VARCHAR(250),
IN `s_LanguageCode` VARCHAR(10))
    NO SQL
BEGIN
	SELECT * FROM tbl_InformationPage
	WHERE LanguageCode=s_LanguageCode AND InfoSlug=s_InfoSlug;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_GetListInformationPageByLanguageCode` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `sp_GetListInformationPageByLanguageCode`(
IN `s_LanguageCode` VARCHAR(10) CHARSET utf8
)
    NO SQL
BEGIN
	SELECT * FROM tbl_InformationPage 
	WHERE InfoGroup='INFO' AND LanguageCode=s_LanguageCode ;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_GetListLanguage` */;
ALTER DATABASE gms_cms CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `sp_GetListLanguage`()
    NO SQL
BEGIN
	SELECT * FROM tbl_Language;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
ALTER DATABASE gms_cms CHARACTER SET utf8 COLLATE utf8_general_ci ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_GetListLanguagePhrase` */;
ALTER DATABASE gms_cms CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `sp_GetListLanguagePhrase`()
    NO SQL
BEGIN
	SELECT lgp.ID,lg.LanguageID, lg.LanguageCode,lg.LanguageName, lg.IsDefault, lgp.PhraseID, lgp.PhraseValue, ph.PhraseName
	 FROM tbl_Language lg INNER JOIN tbl_LanguagePhrase lgp ON lg.LanguageID=lgp.LanguageID
	 INNER JOIN tbl_Phrase ph ON lgp.PhraseID=ph.PhraseID;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
ALTER DATABASE gms_cms CHARACTER SET utf8 COLLATE utf8_general_ci ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_GetListLanguagePhraseByLanguageCode` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `sp_GetListLanguagePhraseByLanguageCode`(IN `s_LanguageCode` VARCHAR(10))
    NO SQL
BEGIN
	SELECT  ph.PhraseName, lgp.PhraseValue
	 FROM tbl_Language lg INNER JOIN tbl_LanguagePhrase lgp ON lg.LanguageID=lgp.LanguageID
	 INNER JOIN tbl_Phrase ph ON lgp.PhraseID=ph.PhraseID
	WHERE lg.LanguageCode=s_LanguageCode;


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_GetListPhrase` */;
ALTER DATABASE gms_cms CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `sp_GetListPhrase`()
    NO SQL
BEGIN
	SELECT * FROM tbl_Phrase ;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
ALTER DATABASE gms_cms CHARACTER SET utf8 COLLATE utf8_general_ci ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_GetListUser` */;
ALTER DATABASE gms_cms CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `sp_GetListUser`()
    NO SQL
BEGIN
	SELECT * FROM tbl_User;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
ALTER DATABASE gms_cms CHARACTER SET utf8 COLLATE utf8_general_ci ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_GetListUserByID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `sp_GetListUserByID`(IN `s_Email` VARCHAR(320) CHARSET utf8, IN `s_Password` VARCHAR(60) CHARSET utf8)
    NO SQL
BEGIN
	SELECT * FROM tbl_User WHERE  Email=s_Email AND Password=s_Password AND Role=1;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_InformationPage_Delete` */;
ALTER DATABASE gms_cms CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `sp_InformationPage_Delete`(IN `s_InfoID` INT UNSIGNED)
    NO SQL
DELETE FROM tbl_informationpage
WHERE InfoID=s_InfoID */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
ALTER DATABASE gms_cms CHARACTER SET utf8 COLLATE utf8_general_ci ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_InformationPage_Insert` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `sp_InformationPage_Insert`(IN `s_InfoID` INT UNSIGNED,
IN `s_InfoTitle` TEXT CHARSET utf8,
IN `s_InfoDescription` TEXT CHARSET utf8,
IN `s_InfoContent` TEXT CHARSET utf8, 
IN `s_CreateOn` DATE, 
IN `s_CreateBy` VARCHAR(255) CHARSET utf8, 
IN `s_InfoSlug`VARCHAR(50), 
IN `s_LanguageCode` VARCHAR(5) CHARSET utf8,
IN s_InfoGroup VARCHAR(50))
    NO SQL
INSERT INTO tbl_InformationPage (InfoTitle,InfoDescription,InfoContent,CreateOn,CreateBy,InfoSlug,LanguageCode,InfoGroup)
						 VALUES(s_InfoTitle,s_InfoDescription,s_InfoContent,s_CreateOn,s_CreateBy,s_InfoSlug,s_LanguageCode,s_InfoGroup) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_InformationPage_Update` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `sp_InformationPage_Update`(IN `s_InfoID` INT UNSIGNED, IN `s_InfoTitle` TEXT CHARSET utf8, IN `s_InfoDescription` TEXT CHARSET utf8, IN `s_InfoContent` TEXT CHARSET utf8,  IN `s_UpdateOn` DATE, IN `s_UpdateBy` VARCHAR(255) CHARSET utf8, IN `s_InfoSlug` VARCHAR(50), IN `s_LanguageCode` VARCHAR(5) CHARSET utf8)
    NO SQL
UPDATE tbl_InformationPage SET InfoTitle=s_InfoTitle,InfoDescription=s_InfoDescription,InfoContent=s_InfoContent,UpdateOn=s_UpdateOn,UpdateBy=s_UpdateBy,InfoSlug=s_InfoSlug, LanguageCode=s_LanguageCode
		WHERE InfoID=s_InfoID */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_LanguagePhrase_Delete` */;
ALTER DATABASE gms_cms CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `sp_LanguagePhrase_Delete`(IN `s_ID` INT UNSIGNED)
    NO SQL
DELETE FROM tbl_LanguagePhrase
		WHERE ID=s_ID */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
ALTER DATABASE gms_cms CHARACTER SET utf8 COLLATE utf8_general_ci ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_LanguagePhrase_Insert` */;
ALTER DATABASE gms_cms CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `sp_LanguagePhrase_Insert`(IN `s_ID` INT UNSIGNED, IN `s_LanguageID` INT, IN `s_PhraseID` INT, IN `s_PhraseValue` TEXT CHARSET utf8)
    NO SQL
INSERT INTO tbl_LanguagePhrase(LanguageID,PhraseID, PhraseValue) VALUES(s_LanguageID,s_PhraseID, s_PhraseValue) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
ALTER DATABASE gms_cms CHARACTER SET utf8 COLLATE utf8_general_ci ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_LanguagePhrase_Update` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `sp_LanguagePhrase_Update`(IN `s_ID` INT UNSIGNED, 
IN `s_LanguageID` INT,
IN `s_PhraseID` INT,
IN `s_PhraseValue` TEXT)
    NO SQL
UPDATE tbl_LanguagePhrase SET 
        LanguageID=s_LanguageID,
        PhraseValue=s_PhraseValue,
        PhraseID=s_PhraseID
		WHERE ID=s_ID */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_Language_Delete` */;
ALTER DATABASE gms_cms CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `sp_Language_Delete`(IN `s_LanguageID` INT UNSIGNED)
    NO SQL
DELETE FROM tbl_Language WHERE LanguageID=s_LanguageID */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
ALTER DATABASE gms_cms CHARACTER SET utf8 COLLATE utf8_general_ci ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_Language_Insert` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `sp_Language_Insert`(IN `s_LanguageID` INT UNSIGNED, IN `s_LanguageName` VARCHAR(50) CHARSET utf8, IN `s_LanguageCode` VARCHAR(10) CHARSET utf8, IN `s_IsDefault` BIT)
    NO SQL
INSERT INTO tbl_Language(LanguageName,LanguageCode, IsDefault)
						 VALUES(s_LanguageName,s_LanguageCode,s_IsDefault) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_Language_Update` */;
ALTER DATABASE gms_cms CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `sp_Language_Update`(IN `s_LanguageID` INT UNSIGNED, 
IN `s_LanguageName` VARCHAR(50) CHARSET utf8, IN `s_LanguageCode` VARCHAR(5) CHARSET utf8,
IN `s_IsDefault` BIT)
    NO SQL
BEGIN
INSERT INTO tbl_Language SET LanguageName=s_LanguageName,LanguageCode=s_LanguageCode, IsDefault=s_IsDefault;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
ALTER DATABASE gms_cms CHARACTER SET utf8 COLLATE utf8_general_ci ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_Phrase_Delete` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `sp_Phrase_Delete`(
IN `s_PhraseID` INT UNSIGNED
)
BEGIN
DELETE FROM  tbl_Phrase
 WHERE PhraseID=s_PhraseID;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_Phrase_Insert` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `sp_Phrase_Insert`(
IN s_PhraseName VARCHAR(250)
)
BEGIN
 INSERT INTO tbl_Phrase(PhraseName)
 VALUES(s_PhraseName);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_Phrase_Update` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `sp_Phrase_Update`(
IN `s_PhraseID` INT UNSIGNED,
IN s_PhraseName VARCHAR(250)
)
BEGIN
 UPDATE tbl_Phrase
 SET PhraseName=s_PhraseName
 WHERE PhraseID=s_PhraseID;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_UserDelete` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `sp_UserDelete`( IN s_UserID int)
BEGIN
    DELETE FROM tbl_user WHERE UserID=s_UserID;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_UserInsert` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `sp_UserInsert`(
IN s_DisplayName VARCHAR(255),
IN s_Email VARCHAR(320) CHARSET utf8,
IN s_Password VARCHAR(60) CHARSET utf8,
IN s_Role bit,
IN s_CreateOn DATETIME
)
BEGIN
    INSERT INTO tbl_user(DisplayName,Email,Password,Role,CreateOn)
        VALUES(s_DisplayName,s_Email,s_Password,s_Role,s_CreateOn);
    
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_UserUpdate` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `sp_UserUpdate`(
IN s_UserID int,
IN s_DisplayName VARCHAR(255),
IN s_Email VARCHAR(320) CHARSET utf8
)
BEGIN
    UPDATE  tbl_user SET DisplayName=s_DisplayName,Email=s_Email
        WHERE UserID=s_UserID;
    
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-03-16 14:12:53
