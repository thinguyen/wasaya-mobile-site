﻿/// <reference path="../../../Scripts/typings/angularjs/angular.d.ts"/>
module statistics {
    class StatisticsController {
        constructor($scope, $window, $state, $sessionStorage, $location, Statistics) {
            if ($sessionStorage.isLogin == false) {
                $scope.isLogin = false;
                $location.path('/login');
            }

            //function check date start and date end when update date start
            $scope.updateDepartDate = function () {
                //check if date start larger date end
                if ($scope.dateTo < $scope.dateFrom) {
                    $scope.dateTo = new Date($scope.dateFrom -1);
                }
            };
            $scope.dateFrom = new Date();
            $scope.dateTo = new Date();
            Statistics.query({
                dateFrom: formatDate($scope.dateFrom) + " " + "00:00:00",
                dateTo: formatDate($scope.dateTo) + " " + "23:59:00"
            })
            $scope.report1 = function (dateFrom,dateTo) {
                $scope.statisticsList = Statistics.query({
                    dateFrom: formatDate(dateFrom) + " " + "00:00:00",
                    dateTo: formatDate(dateTo) + " " + "23:59:00"
                })
            }
        }

    }
    function formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }

    function StatisticsFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/statistics");
    }
    // Register controllers to angular 
    angular.module('gms')
        .controller('StatisticsController', StatisticsController)
        .factory('Statistics', StatisticsFactory);

} 