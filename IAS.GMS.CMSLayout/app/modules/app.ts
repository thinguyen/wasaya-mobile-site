﻿/// <reference path="../../Scripts/typings/angularjs/angular.d.ts"/>
/// <reference path="../../Scripts/typings/accounting/accounting.d.ts"/>

'use strict'

angular.module('gms', ['ngResource',
    'ngStorage',
    'ui.bootstrap.showErrors',
    'ui.router',
    'gms-templates',
    'mgcrea.ngStrap',
    'angularFileUpload']
    );

angular.module('gms').config(function ($locationProvider) {
    $locationProvider.html5Mode(true);
});
angular.module('gms').directive('numbersOnly', function () {
    return function (scope, element, attrs) {

        var keyCode = [8, 9, 37, 39, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 110];
        element.bind("keydown", function (event) {
            //console.log(event.which);
            if ($.inArray(event.which, keyCode) == -1) {
                scope.$apply(function () {
                    scope.$eval(attrs.numbersOnly);
                    event.preventDefault();
                });
                event.preventDefault();
            }

        });
    };
});




angular.module('gms').config(function ($datepickerProvider) {
    angular.extend($datepickerProvider.defaults, {
        dateFormat: 'dd/MM/yyyy'
    });
});

angular.module('gms').config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
    /* Core Page */
        .state('core', {
            abstract: true,
            url: '',
            templateUrl: 'core/views/core.html',
            controller: 'CoreController'
        })
    /* Home Page */
        .state('login', {
            url: '/login',
            views: {
                'main': {
                    templateUrl: 'user/views/login.html',
                    controller: 'UserController'
                }
            },
            parent: 'core'
        })
   
    /*Language Page*/
        .state('language', {
            url: '/language',
            views: {
                'main': {
                    templateUrl: 'language/views/language.html',
                    controller: 'LanguageListController'
                }
            },
            parent: 'core'
        })
    /*LanguagePhrase Page*/
        .state('languagephrase', {
            url: '/languagephrase',
            views: {
                'main': {
                    templateUrl: 'language/views/languagephrase.html',
                    controller: 'LanguagePhraseController'
                }
            },
            parent: 'core'
        })
        /*ImportLanguagePhrase Page*/
        .state('importlanguagephrase', {
            url: '/importlanguagephrase',
            views: {
                'main': {
                    templateUrl: 'language/views/importlanguagephrase.html',
                    controller: 'ImportLanguagePhraseController'
                }
            },
            parent: 'core'
        })
    /*Background Page*/
        .state('background', {
            url: '/background',
            views: {
                'main': {
                    templateUrl: 'language/views/background.html',
                    controller: 'UploadBackgroundController'
                }
            },
            parent: 'core'
        })
    /* Contact */
        .state('phrase', {
            url: '/phrase',
            views: {
                'main': {
                    templateUrl: 'phrase/views/phrase.html',
                    controller: 'PhraseListController'
                }
            },
            parent: 'core'
        })
    /*infomationPage Page*/
        .state('informationpage', {
            url: '/informationpage',
            views: {
                'main': {
                    templateUrl: 'information/views/informationpage.html',
                    controller: 'InformationPageListController'
                }
            },
            parent: 'core'
        })
    /*infomationPage Page*/
        .state('informationpagedetail', {
            url: '/informationpagedetail',
            views: {
                'main': {
                    templateUrl: 'information/views/informationpagedetail.html',
                    controller: 'InformationDetailController'
                }
            },
            parent: 'core'
        })

    /*Fare rule*/
        .state('farerule', {
            url: '/farerule',
            views: {
                'main': {
                    templateUrl: 'farerule/views/farerule.html',
                    controller: 'FareruleListController'
                }
            },
            parent: 'core'
        })
    /*Fare rule detail*/
        .state('fareruledetail', {
            url: '/fareruledetail',
            views: {
                'main': {
                    templateUrl: 'farerule/views/fareruledetail.html',
                    controller: 'FareruleDetailController'
                }
            },
            parent: 'core'
        })
    /* Contact */
        .state('contact', {
            url: '/contact',
            views: {
                'main': {
                    templateUrl: 'contact/views/contact.html',
                    controller: 'ContactListController'
                }
            },
            parent: 'core'
        })
    /* Users */
        .state('userlist', {
            url: '/userlist',
            views: {
                'main': {
                    templateUrl: 'user/views/userlist.html',
                    controller: 'UserController'
                }
            },
            parent: 'core'
        })
    /* ChangPass */
        .state('changepassword', {
            url: '/changepassword',
            views: {
                'main': {
                    templateUrl: 'user/views/changepassword.html',
                    controller: 'UserController'
                }
            },
            parent: 'core'
        })
    
    /*infomationPage Page*/
        .state('statistics', {
            url: '/statistics',
            views: {
                'main': {
                    templateUrl: 'report/views/report01.html',
                    controller: 'StatisticsController'
                }
            },
            parent: 'core'
        })
    /* Return Home for any other urls */
    $urlRouterProvider.otherwise('/login');
});
