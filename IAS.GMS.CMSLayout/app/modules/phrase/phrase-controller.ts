﻿ /// <reference path="../../../Scripts/typings/angularjs/angular.d.ts"/>
module Phrase {
    class PhraseListController {
        constructor($scope, $window, $state, $sessionStorage, $location, Phrase) {
            if ($sessionStorage.isLogin == false) {
                $scope.isLogin = false;
                $location.path('/login');
            }
            $scope.phraseList = Phrase.query();
            ///Languages List
            $scope.save = function () {
                Phrase.save($scope.currentPhrase, function (data) {
                    $scope.phraseList = Phrase.query();
                    $('#comment-editor').toggle(false);
                    $scope.currentPhrase = null;
                });
            }

            //Update row on table
            $scope.editRow = function (index) {
               $('#comment-editor').toggle(true);
                $scope.currentPhrase = $scope.phraseList[index];
            }

             //Delete row on table
            $scope.deleteRow = function (index) {
                var deleteItem = $window.confirm('Are you absolutely sure you want to delete?');   
                if (deleteItem) {
                    Phrase.delete({
                       
                        phraseID: $scope.phraseList[index].PhraseID
                    });
                    $scope.phraseList.splice(index, 1);
                }
            }
        }

    }
    

    function PhraseFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/phrase/:phraseID", { phraseID: '@PhraseID' });
    }

    // Register controllers to angular 
    angular.module('gms')
        .controller('PhraseListController', PhraseListController)
        .factory('Phrase', PhraseFactory);

} 