﻿/// <reference path="../../../Scripts/typings/angularjs/angular.d.ts"/>
module Farerule {
    class FareruleListController {
        constructor($scope, $window, $state, $location, $sessionStorage, Farerule, Language) {
            if ($sessionStorage.isLogin == false) {
                $scope.isLogin = false;
                $location.path('/login');
            }
            $scope.fareRuleList = Farerule.query();
            $scope.languageList = Language.query();

            ///Farerule List
            $scope.save = function () {
                $scope.currentFarerule.createBy = $sessionStorage.userName;
                $scope.currentFarerule.updateBy = $sessionStorage.userName;
                Farerule.save($scope.currentFarerule, function (data) {
                    $scope.currentFarerule = null;
                    $scope.fareRuleList = Farerule.query();
                    $('#comment-editor').toggle(false);
                });
            }

            //Update row on table
            $scope.viewRow = function (infoSlug) {
                $scope.InfoSlug = infoSlug;
                $sessionStorage.infoSlug = infoSlug;
                $location.path('/fareruledetail');
            }

        }

    }

    function FareruleDetailController($scope, $window, $state, $location, $sessionStorage, Farerule, Language) {
        if ($sessionStorage.isLogin == false) {
            $scope.isLogin = false;
            $location.path('/login');
        }
        $scope.state = $state.current.name;
        $scope.fareRuleList = Farerule.query();
        $scope.languageList = Language.query();
        //List infomation page by infoSlug
        $scope.fareRuleList = Farerule.query({
            infoSlug: $sessionStorage.infoSlug,
            languageCode: 'null',
            languageName: 'null'
        }, function (data) {
            $scope.infoTitle = _.find(data, { LanguageCode: 'EN' }).InfoTitle;
            $scope.infoSlug = _.find(data, { LanguageCode: 'EN' }).InfoSlug;
            });
        $scope.editRow = function (index) {
            $('#comment-editor').toggle(true);
            $scope.currentFarerule = $scope.fareRuleList[index];
        }
        ///Languages List
        $scope.save = function () {
            $scope.currentFarerule.createBy = $sessionStorage.userName;
            $scope.currentFarerule.updateBy = $sessionStorage.userName;
            Farerule.save($scope.currentFarerule, function (data) {
                $scope.currentFarerule = null;
                $scope.fareRuleList = Farerule.query({
                    infoSlug: $sessionStorage.infoSlug,
                    languageCode: 'null',
                    languageName: 'null'
                });
                $('#comment-editor').toggle(false);
            });
        }
        //Delete row on table
        $scope.deleteRow = function (index) {
            var deleteItem = $window.confirm('Are you absolutely sure you want to delete?');
            if (deleteItem) {
                Farerule.delete({
                    infoID: $scope.fareRuleList[index].InfoID
                });
                $scope.fareRuleList.splice(index, 1);
            }
        }
    }

    function FareruleFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/FareRule/:infoID", { infoID: '@InfoID' });
    }

    // Register controllers to angular 
    angular.module('gms')
        .controller('FareruleListController', FareruleListController)
        .controller('FareruleDetailController', FareruleDetailController)
        .factory('Farerule', FareruleFactory);

}  