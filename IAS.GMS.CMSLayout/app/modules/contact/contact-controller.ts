﻿/// <reference path="../../../Scripts/typings/angularjs/angular.d.ts"/>
module Contact {
    class ContactListController {
        constructor($scope, $window, $state, $sessionStorage, $location, Contact, Language) {
            $scope.contactList = Contact.query();
            console.log($scope.contactList);
            $scope.languageList = Language.query();
            ///Languages List
            $scope.save = function () {
                $scope.currentContact.createBy = $sessionStorage.userName;
                $scope.currentContact.updateBy = $sessionStorage.userName;
                Contact.save($scope.currentContact, function (data) {
                    $scope.contactList = Contact.query();
                    $('#comment-editor').toggle(false);
                    $scope.currentContact = null;
                });
            }

            //Update row on table
            $scope.editRow = function (index) {
                $('#comment-editor').toggle(true);
                $scope.currentContact = $scope.contactList[index];
            }

             //Delete row on table
            $scope.deleteRow = function (index) {
                var deleteItem = $window.confirm('Are you absolutely sure you want to delete?');
                if (deleteItem) {
                    Contact.delete({
                        contactID: $scope.contactList[index].ContactID
                    });
                    $scope.contactList.splice(index, 1);
                }
            }
        }

    }


    function ContactFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/contact/:contactID", { contactID: '@ContactID' });
    }

    // Register controllers to angular 
    angular.module('gms')
        .controller('ContactListController', ContactListController)
        .factory('Contact', ContactFactory);

} 