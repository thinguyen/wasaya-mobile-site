﻿/// <reference path="../../../Scripts/typings/angularjs/angular.d.ts"/>
module Core {
    // Core Controller
    export class CoreController {
        constructor($scope, $state, $sessionStorage, $location, $window) {
            //change header for homepage or page
            $scope.state = $state.current.name;
            $scope.toogleDropdown = function () {
                $scope.dropdownOpenned = !$scope.dropdownOpenned;
            };
            $scope.logout = function () {
                $scope.isLogin = false;
                $sessionStorage.isLogin = false;
                $sessionStorage.userName = null;
                $location.path('/login');
                //$window.location.reload();
            }
            
            if ($sessionStorage.isLogin == true ) {
                $scope.isLogin = true;
                $scope.userName = $sessionStorage.userName;
            }
            else
            {
                $scope.isLogin = false;
                $sessionStorage.isLogin = false;
            }
            $scope.$on('$stateChangeStart', function (event, toState) {
                $scope.state = toState.name;
                switch ($scope.state) {
                    case 'information':
                        $scope.title_name = "Information";
                        break;
                    case 'aboutus':
                        $scope.title_name = "About Intelisys";
                        break;
                    case 'contactus':
                        $scope.title_name = "Contact Intelisys";
                        break;
                    case 'language':
                        $scope.title_name = "Language";
                        break;
                    case 'step1':
                        $scope.title_name = "Search Flight";
                        break;
                    case 'step2':
                        $scope.title_name = "Select Flight";
                        break;
                    case 'step3':
                        $scope.title_name = "Passenger Details";
                        break;
                    case 'step4':
                        $scope.title_name = "Additional Services";
                        break;
                    case 'step5':
                        $scope.title_name = "Booking Detail";
                        break;
                    case 'step6':
                        $scope.title_name = "Payment Details";
                        break;
                    case 'step7':
                        $scope.title_name = "Confirmed";
                        break;
                    case 'finish':
                        $scope.title_name = "Confirmed";
                        break;
                    case 'checkin':
                        $scope.title_name = "Check In";
                        break;
                    case 'checkinstep2':
                        $scope.title_name = "Choose Your Flight";
                        break;
                    case 'checkinstep3':
                        $scope.title_name = "Passenger List";
                        break;
                    case 'checkinstep3_1':
                        $scope.title_name = "Seat Selection";
                        break;
                    case 'checkinstep3_2':
                        $scope.title_name = "Passenger List";
                        break;
                    case 'checkinstep4':
                        $scope.title_name = "Boarding Pass";
                        break;
                    case 'flightstatus':
                        $scope.title_name = "Flight Status";
                        break;
                    case 'flightstatus2':
                        $scope.title_name = "Flight Status";
                        break;
                    case 'flightstatus3':
                        $scope.title_name = "View Seat";
                        break;
                    default:
                        $scope.title_name = "Intelisys";
                }
            });

        }
    }
    // Head Controller
    export class HeadController {
        constructor($scope, $state, $location) {
            $scope.favicon = "http://exe.com.vn/wp-content/themes/exe/images/favicon.ico";
            $scope.title = "Generic Mobile Site";
            $scope.type = "cms";
            $scope.desc = "Generic Mobile Site";
            $scope.url = "URL";
        }
    }

    // Register controllers to angular module
    angular.module('gms').controller('CoreController', CoreController)
        .controller('HeadController', HeadController);
}