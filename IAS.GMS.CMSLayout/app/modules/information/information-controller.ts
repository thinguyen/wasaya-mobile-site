﻿/// <reference path="../../../Scripts/typings/angularjs/angular.d.ts"/>


module InformationPage {
    class InformationPageListController {
        constructor($scope, $window, $state, $location, $sessionStorage, InformationPage, Language) {
            if ($sessionStorage.isLogin == false) {
                $scope.isLogin = false;
                $location.path('/login');
            }
            $scope.informationPageList = InformationPage.query();
            $scope.languageList = Language.query();
            
            ///Languages List
            $scope.save = function () {
                $scope.currentInformationPage.createBy = $sessionStorage.userName;
                $scope.currentInformationPage.updateBy = $sessionStorage.userName;
                InformationPage.save($scope.currentInformationPage, function (data) {
                    $scope.currentInformationPage = null;
                    $scope.informationPageList = InformationPage.query();
                    $('#comment-editor').toggle(false);
                });
            }
          
            //Update row on table
            $scope.viewRow = function (infoSlug) {
                
                //$('#comment-editor').toggle(true);
                //$scope.currentInformationPage = $scope.informationPageList[index];
                $scope.InfoSlug = infoSlug;
                $sessionStorage.infoSlug = infoSlug;
                $location.path('/informationpagedetail');
            }
            
        }

    }
  
    function InformationDetailController($scope, $window, $state, $location, $sessionStorage, InformationPage, Language) {
        if ($sessionStorage.isLogin == false) {
            $scope.isLogin = false;
            $location.path('/login');
        }
        $scope.state = $state.current.name;
        $scope.informationPageList = InformationPage.query();
        $scope.languageList = Language.query();
        //List infomation page by infoSlug
        $scope.informationPageList = InformationPage.query({
            infoSlug: $sessionStorage.infoSlug,
            languageCode: 'null',
            languageName: 'null'
        }, function (data) {
            $scope.infoTitle = _.find(data, { LanguageCode: 'EN' }).InfoTitle;
            $scope.infoSlug = _.find(data, { LanguageCode: 'EN' }).InfoSlug;
        });
        $scope.editRow = function (index) {
            $('#comment-editor').toggle(true);
            $scope.currentInformationPage = $scope.informationPageList[index];
        }
        ///Languages List
        $scope.save = function () {
            $scope.currentInformationPage.createBy = $sessionStorage.userName;
            $scope.currentInformationPage.updateBy = $sessionStorage.userName;
            InformationPage.save($scope.currentInformationPage, function (data) {
                $scope.currentInformationPage = null;
                $scope.informationPageList = InformationPage.query({
                    infoSlug: $sessionStorage.infoSlug,
                    languageCode: 'null',
                    languageName: 'null'
                });
                $('#comment-editor').toggle(false);
            });
        }
        //Delete row on table
        $scope.deleteRow = function (index) {
        var deleteItem = $window.confirm('Are you absolutely sure you want to delete?');
        if (deleteItem) {
            InformationPage.delete({
                infoID: $scope.informationPageList[index].InfoID
            });
            $scope.informationPageList.splice(index, 1);
        }
    }
    }

    function InformationPageFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/InformationPage/:infoID", { infoID: '@InfoID' });
    }

    // Register controllers to angular 
    angular.module('gms')
        .controller('InformationPageListController', InformationPageListController)
        .controller('InformationDetailController', InformationDetailController)
        .factory('InformationPage', InformationPageFactory);

} 