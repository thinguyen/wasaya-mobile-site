﻿/// <reference path="../../../Scripts/typings/angularjs/angular.d.ts"/>
//module Language {
//    // Language Controller
//    class LanguageController {
//        constructor($scope, $state, $location) {
//            $scope.state = $state.current.name;
//        }
//    }
//    // Register controllers to angular module
//    angular.module('gms').controller('LanguageController', LanguageController);
//}
module Language {
    class LanguageListController {
        constructor($scope, $window, $state, $location, $sessionStorage, Language) {
            if ($sessionStorage.isLogin == false)
            {
                $scope.isLogin = false;
                $location.path('/login');
            }
            else if ($sessionStorage.isLogin == true && $sessionStorage.isReload == false) {
                $scope.isLogin = true;
                $scope.userName = $sessionStorage.userName;
                $location.path('/language');
                $window.location.reload();
                $sessionStorage.isReload = true;

            }
            $scope.languageList = Language.query();
            ///Languages List
            $scope.save = function () {
                Language.save($scope.currentLanguage, function (data) {
                    $scope.languageList = Language.query();
                    // $('.add_language').hide();
                    //document.getElementById('comment-editor').style.display = 'none';
                    $('#comment-editor').toggle(false);
                    $scope.currentLanguage = null;
                });
            }

         
            //Update row on table
            $scope.editRow = function (index) {
                $('#comment-editor').toggle(true);
                $scope.currentLanguage = $scope.languageList[index];
            }

             //Delete row on table
            $scope.deleteRow = function (index) {
                var deleteItem = $window.confirm('Are you absolutely sure you want to delete?');
                if (deleteItem) {
                    Language.delete({
                        languageID: $scope.languageList[index].LanguageID
                    });
                    $scope.languageList.splice(index, 1);
                }
            }
        }
        
    }

    class LanguagePhraseController {
        constructor($scope, $window, $state, $location, Language, Phrase, LanguagePhrase) {
            $scope.languageList = Language.query();
            $scope.phraseList = Phrase.query();
            $scope.languagePhraseList = LanguagePhrase.query();

            $scope.languagePhrase = new LanguagePhrase;

            ///Languages List
            $scope.save = function () {
                $scope.languagePhrase.$save(function () {
                    $scope.languagePhraseList = LanguagePhrase.query();
                     $('#comment-editor').toggle(false);
                });
                $scope.languagePhrase = new LanguagePhrase;
            }

            //Update row on table
            $scope.editRow = function (languagePhrase) {
                $('#comment-editor').toggle(true);
                $scope.languagePhrase = languagePhrase;
            }

             //Delete row on table
            $scope.deleteRow = function (index) {
                var deleteItem = $window.confirm('Are you absolutely sure you want to delete?');
                if (deleteItem) {
                    $scope.languagePhraseList[index].$delete(function () {
                        $scope.languagePhraseList.splice(index, 1);
                    });
                }
            }
        }
        
    }
    class ImportLanguagePhraseController {
        constructor($scope, $upload, $window, $state, $location, $sessionStorage, ImportLanguagePhrase) {
            ///Languages List
          
               $scope.onFileSelect = function ($files) {
                //s$files: an array of files selected, each file has name, size, and type.
                for (var i = 0; i < $files.length; i++) {
                    var $file = $files[i];
                    $upload.upload({
                        url:'http://localhost:27746/api/Upload',
                        file: $file,
                     headers: {
                            'Content-Type': 'application/json'
                        }
                    });
                }
                   //var file = $scope.myFile;
                   //var uploadUrl = 'http://localhost:27746/api/Uploads';
                   //$upload.uploadFileToUrl(file, uploadUrl);
               }
            //$scope.FileUpload = function ($file) {
            //    console.log("import");
            //    console.log($file);
            //}

            $scope.save = function () {
                ImportLanguagePhrase.save({ files: "1" }, function (data) {
                
                });
            }
            }

    }
    class UploadBackgroundController {
        constructor($scope, $upload, $window, $state, $location, $sessionStorage, ImportLanguagePhrase) {
            ///Languages List

            $scope.onFileSelect = function ($files) {
                //s$files: an array of files selected, each file has name, size, and type.
                for (var i = 0; i < $files.length; i++) {
                    var $file = $files[i];
                    $upload.upload({
                        url: 'http://localhost:27746/api/Uploads',
                        file: $file,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    });
                }
               

            }
         
            $scope.save = function () {
                ImportLanguagePhrase.save({ files: "1" }, function (data) {

                });
            }
            }

    }

    function LanguageFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/Language/:languageID", { languageID: '@LanguageID' });
    }

    function PhraseFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/Phrase/:phraseID", { phraseID: '@PhraseID' });
    }

    function LanguagePhraseFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/LanguagePhrase/:ID", { ID: '@ID' });
    }
    function ImportLanguagePhraseFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/ImportLanguagePhrase");
    }
    function UploadBackgroundFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/Configuration");
    }
    

    // Register controllers to angular 
    angular.module('gms')
        .controller('LanguageListController', LanguageListController)
        .controller('LanguagePhraseController', LanguagePhraseController)
        .controller('ImportLanguagePhraseController', ImportLanguagePhraseController)
        .controller('UploadBackgroundController', UploadBackgroundController)
        .factory('Language', LanguageFactory)
        .factory('Phrase', PhraseFactory)
        .factory('LanguagePhrase', LanguagePhraseFactory)
        .factory('UploadBackground', UploadBackgroundFactory)
        .factory('ImportLanguagePhrase', ImportLanguagePhraseFactory);
}  