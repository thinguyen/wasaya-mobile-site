﻿/// <reference path="../../../Scripts/typings/angularjs/angular.d.ts"/>
/// <reference path="../../../Scripts/typings/angularjs/angular-resource.d.ts"/>

module User {
    class UserController {
        constructor($scope, $state, $window, $location, $sessionStorage, UserProfile) {
            if ($sessionStorage.isLogin == false) {
                $scope.isLogin = false;
                $location.path('/login');
            }
            $scope.userList = UserProfile.query();
            ///Languages List
            $scope.save = function () {
                UserProfile.save($scope.currentUser, function (data) {
                    $scope.userList = UserProfile.query();
                    $('#comment-editor').toggle(false);
                    $scope.currentUser = null;
                });
            }
            //Update row on table
            $scope.editRow = function (index) {
                $('#comment-editor').toggle(true);
                $(' #password').hide();
                $(' #role').hide();
                $scope.currentUser = $scope.userList[index];
            }

             //Delete row on table
            $scope.deleteRow = function (index) {
                var deleteItem = $window.confirm('Are you absolutely sure you want to delete?');
                if (deleteItem) {
                    UserProfile.delete({
                        userID: $scope.userList[index].UserID
                    });
                    $scope.UserProfile.splice(index, 1);
                }
            }
            $scope.state = $state.current.name;
            //if ($sessionStorage.userName != null)
            //{
            //    $location.path('/language');
            //}
          
            $scope.login = function () {
                if ($scope.userName == undefined) {
                    alert("Please enter your email address");
                    return;
                }
                if ($scope.userPassword == undefined) {
                    alert("Please enter your password");
                    return;
                }
                if ($scope.registerForm.$invalid) {
                    $scope.$broadcast('show-errors-check-validity');
                    alert("Please fill in all field");
                    return;
                }
                
                UserProfile.get({
                    email: $scope.userName,
                    password: $scope.userPassword
                    
                }, function (data) {
                    if (data.CheckLogin) {
                        $sessionStorage.userName = $scope.userName;
                        $sessionStorage.userPassword = $scope.userPassword;
                        $sessionStorage.isLogin = true;
                        $location.path('/language');
                        $sessionStorage.isReload = false;
                            
                            //console.log($sessionStorage.isLogin);
                        } else {
                            alert("The email or password you entered is incorrect!");
                            //$sessionStorage.isLogin = false;
                            return false;
                           
                        }
                    });
            };
            $scope.changepass = function () {
                if ($scope.currentUser.CurrentPassword != $sessionStorage.userPassword) {
                    alert("Old password is incorrect");
                    return;
                }
                if ($scope.currentUser.NewPassword != $scope.currentUser.ConfirmPassword) {
                    alert("Password Mismatch");
                    return;
                }
                UserProfile.save(
                    {
                        userID: 1,
                        email: $sessionStorage.userName,
                        password: $scope.currentUser.NewPassword
                    }, function (data) {
                        alert("Password changed Successfully");
                        $location.path('/language');
                });
            };
        }
    }
    function UserProfileFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/users");
    }
    // Register controllers to angular module
    angular.module('gms')
        .controller('UserController', UserController)
        .factory('UserProfile', UserProfileFactory)
    ;

}