﻿/// <reference path="../../../Scripts/typings/angularjs/angular.d.ts"/>
module Home {
    // Home Controller
    class HomeController {
        constructor($scope, $state, $location) {
            $scope.state = $state.current.name;
        }
    }

    // Register controllers to angular module
    angular.module('gms').controller('HomeController', HomeController);
}