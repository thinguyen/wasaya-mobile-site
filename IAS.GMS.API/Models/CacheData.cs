﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace IAS.GMS.API.Models
{
    public class CacheData
    {
        // Properties
        protected DateTime lastUpdated;
        protected Array data;

        public DateTime LastUpdated
        {
            get { return lastUpdated; }
            set { lastUpdated = value; }
        }

        public Array Data
        {
            get { return data; }
            set { data = value; }
        }

        /// <summary>
        /// Write a given data instance to a Jsonfile
        /// </summary>
        /// <param name="fileName">Name of the cache file</param>
        /// <param name="data">Data content</param>
        /// <param name="append">If no it will override the file, otherwise append to it</param>
        public void saveCacheFile(string fileName, bool append)
        {
            string cacheDir = ConfigurationManager.AppSettings["CacheDirectory"];
            string filePath = cacheDir + "/" + fileName;
            // Create new cache directory if it does not exists
            if (!Directory.Exists(cacheDir))
            {
                Directory.CreateDirectory(cacheDir);
            }
            // Create new cache file if it does not exists
            if (!File.Exists(filePath))
            {
                File.Create(filePath).Close();
            }
            
            // Serialize and save data
            FileStream fs = new FileStream(filePath, FileMode.Truncate, FileAccess.ReadWrite);
            StreamWriter writer = new StreamWriter(fs);
            try
            {
                var data = JsonConvert.SerializeObject(Data);
                writer.BaseStream.Seek(0, SeekOrigin.End); 
                writer.WriteLine(LastUpdated);
                writer.WriteLine(data);
                writer.Flush();
                writer.Close();
                fs.Close();
            }
            finally
            {
                if (writer != null)
                {
                    writer.Close();
                }
            }
        }

        /// <summary>
        /// Load data from stored cache file
        /// </summary>
        /// <param name="fileName">Name of the cache file</param>
        /// <param name="dataType">Type of the data need to load</param>
        public void loadCacheFile(string fileName, Type dataType)
        {
            string cacheDir = ConfigurationManager.AppSettings["CacheDirectory"];
            string filePath = cacheDir + "/" + fileName;
            // Load data only if the file is existed
            if (File.Exists(filePath))
            {
                StreamReader reader = null;
                try
                {
                    // Read data from cache file
                    reader = new StreamReader(filePath);
                    var time = reader.ReadLine();
                    var data = reader.ReadLine();

                    // Parse data to suitable type
                    if (time != null)
                    {
                        LastUpdated = DateTime.Parse(time);
                    }
                    if (data != null)
                    {
                        Data = (Array)JsonConvert.DeserializeObject(data, dataType);
                    }

                    reader.Close();
                }
                finally
                {
                    if (reader != null)
                    {
                        reader.Close();
                    }
                }
            }
        }
        /// <summary>
        /// Check cache file life time expiry
        /// </summary>
        /// <returns>True if the cache is expired, False if not</returns>
        public bool checkTimeExpiry()
        {
            bool result = false;
            if (LastUpdated != null)
            {

                var cacheLifeTimeInMinute = Int32.Parse(ConfigurationManager.AppSettings["CacheLifeTimeInMinute"]);
                var timeInterval = TimeSpan.FromMinutes(cacheLifeTimeInMinute);

                if (DateTime.Now.Subtract(LastUpdated) >= timeInterval)
                {
                    result = true;
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

    }
}