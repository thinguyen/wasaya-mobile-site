﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using IAS.GMS.API.BookingService;
using IAS.GMS.API.ReservationInvoiceService;
using IAS.GMS.API.CheckInService;
using IAS.GMS.API.UserProfileService;
using IAS.GMS.API.FlightWatchService;
using IAS.GMS.API.BoardingService;

namespace IAS.GMS.API.Models
{
    public class APIConnection
    {
        private static string apiUsername = ConfigurationManager.AppSettings["APIUsername"];
        private static string apiPassword = ConfigurationManager.AppSettings["APIPassword"];

        //create booking service client
        public static BookingServiceClient createBookingService() {
            BookingServiceClient client = new BookingServiceClient();
            client.ClientCredentials.Windows.ClientCredential.UserName = apiUsername;
            client.ClientCredentials.Windows.ClientCredential.Password = apiPassword;
            client.ClientCredentials.UserName.UserName = apiUsername;
            client.ClientCredentials.UserName.Password = apiPassword;
            return client;
        }

        //create reservation invoice service
        public static ReservationInvoiceServiceClient createReservationInvoiceService()
        {
            ReservationInvoiceServiceClient client = new ReservationInvoiceServiceClient();
            client.ClientCredentials.UserName.UserName = apiUsername;
            client.ClientCredentials.UserName.Password = apiPassword;
            client.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Identification;
            return client;
        }

        //create checkin service
        public static CheckInServiceClient createCheckInService()
        {
            CheckInServiceClient client = new CheckInServiceClient();
            client.ClientCredentials.UserName.UserName = apiUsername;
            client.ClientCredentials.UserName.Password = apiPassword;
            return client;
        }

        //create user profile service
        public static UserProfileServiceClient createUserProfileService()
        {
            UserProfileServiceClient client = new UserProfileServiceClient();
            client.ClientCredentials.UserName.UserName = apiUsername;
            client.ClientCredentials.UserName.Password = apiPassword;
            return client;
        }
        public static FlightWatchServiceClient createFlightWatchService()
        {
            FlightWatchServiceClient client = new FlightWatchServiceClient();
            client.ClientCredentials.UserName.UserName = apiUsername;
            client.ClientCredentials.UserName.Password = apiPassword;
            return client;
        }
        public static BoardingServiceClient createBoardingService()
        {
            BoardingServiceClient client = new BoardingServiceClient();
            client.ClientCredentials.UserName.UserName = apiUsername;
            client.ClientCredentials.UserName.Password = apiPassword;
            return client;
        }
    }
}