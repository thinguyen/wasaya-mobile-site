﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace IAS.GMS.API.Models
{
    public class DBConnection
    {
        /// <summary>
        /// Get connection string from the configuration file
        /// </summary>
        /// <returns>Connection string</returns>
        static private string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString;
        }
        /// <summary>
        /// Execute Insert/Update/Delete query
        /// </summary>
        /// <param name="query">Query string</param>
        /// <returns>True/False</returns>
        public bool execute (string query)
        {
            Debug.WriteLine(query);
            string connectionString = GetConnectionString();
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                // Create a SQL connection
                if (con.State != ConnectionState.Open)
                {
                    con.ConnectionString = GetConnectionString();
                    con.Open();
                }

                //Start a transaction
                SqlTransaction transaction;
                transaction = con.BeginTransaction();

                try
                {
                    // Generate a command to execute the insert query with establised connection
                    SqlCommand command = new SqlCommand(query, con);
                    command.Transaction = transaction;
                    command.ExecuteNonQuery();
                    transaction.Commit();
                    return true;
                }
                catch (SqlException ex)
                {
                    Debug.WriteLine("SQL Exception Type: {0}", ex.GetType());
                    Debug.WriteLine("Message: {0}", ex.Message);
                    try
                    {
                        transaction.Rollback();
                        return false;
                    }
                    catch (Exception ex2)
                    {
                        // This catch block will handle any errors that may have occurred 
                        // on the server that would cause the rollback to fail, such as 
                        // a closed connection.
                        Debug.WriteLine("Rollback Exception Type: {0}", ex2.GetType());
                        Debug.WriteLine("Message: {0}", ex2.Message);
                        return false;
                    }
                }
            }
        }
        /// <summary>
        /// Execute a select query and return a data set of the table
        /// </summary>
        /// <param name="query">Query string</param>
        /// <returns>Data set of a table</returns>
        public DataTable select (string query)
        {
            try {
                // Try to create a SQL connection
                SqlConnection con = new SqlConnection();
                con.Open();
                con.ConnectionString = GetConnectionString();

                // Generate an adapter and assign the command 
                // to execute the select query with establised connection
                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                SqlCommand command = new SqlCommand(query, con);
                dataAdapter.SelectCommand = command;

                // Return the result
                DataTable dataTable = new DataTable();
                dataAdapter.Fill(dataTable);

                return dataTable;
            }
            catch (SqlException ex){
                // Return errors of the SQL Execution
                Console.WriteLine("SQL Exception Type: {0}", ex.GetType());
                Console.WriteLine("Message: {0}", ex.Message);
                return null;
            }
        }
    }
}