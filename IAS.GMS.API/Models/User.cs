﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IAS.GMS.API.Models
{
    public class User
    {
        DataAccessLayer da = new DataAccessLayer();
        // User Properties
        protected int _id;
        protected string _username;
        protected string _displayName;
        protected string _email;
        protected string _salt;
        protected string _password;
        protected DateTime _createdAt;
        protected DateTime _updatedAt;
        protected int _isActive;
        protected int _roleId;
        
        // User Ancessors
        public int id
        {
            get { return _id; }
            set { _id = value; }
        }
        public string username
        {
            get { return _username; }
            set { _username = value; }
        }
        public string displayName
        {
            get { return _displayName; }
            set { _displayName = value; }
        }
        public string email
        {
            get { return _email; }
            set { _email = value; }
        }
        public string salt
        {
            get { return _salt; }
            set { _salt = value; }
        }
        public string password 
        {
            get { return _password; }
            set { _password = value; }
        }
        public DateTime createdAt
        {
            get { return _createdAt; }
            set { _createdAt = value; }
        }
        public DateTime updatedAt
        {
            get { return _updatedAt; }
            set { _updatedAt = value; }
        }
        public int isActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }
        public int roleId
        {
            get { return _roleId; }
            set { _roleId = value; }
        }
        
        //Functionalities
        public string inputValidation(User user)
        {
            return "aaa";
        }

    }
}