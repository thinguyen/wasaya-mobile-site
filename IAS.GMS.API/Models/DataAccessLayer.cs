﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Data.OleDb;
using MySql.Data.MySqlClient;
using MySql;

namespace IAS.GMS.API.Models
{
    public class DataAccessLayer
    {
        private static string _ConnectionString = ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString;
        //public DataAccessLayer(string DatabaseConnectString)
        //{
        //    _ConnectionString = DatabaseConnectString;
        //}
        public bool CheckConnection(string ConnectionString = "")
        {
            string strCnStr = string.IsNullOrEmpty(ConnectionString) ? _ConnectionString : ConnectionString;

            MySqlConnection cn = new MySqlConnection(strCnStr);
            try
            {
                cn.Open();
                return true;
            }
            catch// (Exception ex)
            {
                return false;
            }
            finally
            {
                cn.Close();
            }
        }
        public DataSet GetDataSet(string SQLString)
        {
            string strCnStr = _ConnectionString;
            MySqlConnection cn = new MySqlConnection(strCnStr);
            MySqlDataAdapter da = new MySqlDataAdapter(SQLString, cn);
            DataSet ds = new DataSet();
            try
            {
                cn.Open();
                da.Fill(ds);
                return ds;
            }
            catch (MySqlException ex)
            {
                // return null;
                throw ex;
                return null;

            }
            finally
            {
                cn.Close();
            }
        }

        public bool ExecuteSQL(string SQLString)
        {
            string strCnStr = _ConnectionString;
            MySqlConnection cn = new MySqlConnection(strCnStr);
            MySqlCommand cmd = new MySqlCommand(SQLString, cn);
            cmd.CommandType = CommandType.Text;
            try
            {
                cn.Open();
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (MySqlException ex)
            {
                throw ex;
                return false;

            }
            finally
            {
                cn.Close();
            }
        }

        public MySqlDataReader GetDataReader(string SQLString)
        {
            string strCnStr = _ConnectionString;
            MySqlConnection cn = new MySqlConnection(strCnStr);
            MySqlCommand cmd = new MySqlCommand(SQLString, cn);
            cmd.CommandType = CommandType.Text;
            MySqlDataReader dr = null;
            try
            {
                cn.Open();
                dr = cmd.ExecuteReader();
                return dr;
            }
            catch (Exception ex)
            {
                return null;
                throw ex;
            }
            finally
            {
                cn.Close();
            }
        }

        public int GetScalarData(string SQLString)
        {
            string strCnStr = _ConnectionString;
            MySqlConnection cn = new MySqlConnection(strCnStr);
            MySqlCommand cmd = new MySqlCommand(SQLString, cn);
            cmd.CommandType = CommandType.Text;
            try
            {
                cn.Open();
                return Convert.ToInt32(cmd.ExecuteScalar());
            }
            catch (Exception ex)
            {
                return 0;
                throw ex;
            }
            finally
            {
                cn.Close();
            }
        }

        public string GetScalarDataText(string SQLString,int timeout=60)
        {
            string result = "";
            string strCnStr = _ConnectionString;
            MySqlConnection cn = new MySqlConnection(strCnStr);
            MySqlCommand cmd = new MySqlCommand(SQLString, cn);
            cmd.CommandType = CommandType.Text;
            try
            {
                cn.Open();
                cmd.CommandTimeout = timeout;
                result = cmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                // result = ex.ToString();
                result = null;
               // throw ex;
            }
            finally
            {
                cn.Close();
            }
            return result;
        }
        
        public JArray ToJson(DataTable source)
        {
            JArray result = new JArray();
            JObject row;
            foreach (DataRow dr in source.Rows)
            {
                row = new JObject();
                foreach (System.Data.DataColumn col in source.Columns)
                {
                    row.Add(col.ColumnName.Trim(), JToken.FromObject(dr[col]));
                }
                result.Add(row);
            }
            return result;
        }
        public void WriteDataToFile(DataTable submittedDataTable, string submittedFilePath)
        {
            int i = 0;
            StreamWriter sw = null;

            sw = new StreamWriter(submittedFilePath, false);

            for (i = 0; i < submittedDataTable.Columns.Count - 1; i++)
            {

                sw.Write(submittedDataTable.Columns[i].ColumnName + ";");

            }
            sw.Write(submittedDataTable.Columns[i].ColumnName.Replace("LanguagePhrase","{"));
            sw.WriteLine();
            int countRow = 0;
            foreach (DataRow row in submittedDataTable.Rows)
            {
                countRow += 1;
                object[] array = row.ItemArray;

                //for (i = 0; i < array.Length - 1; i++)
                //{
                //    sw.Write(array[i].ToString() + ";");
                //}
                sw.Write('"');
                sw.Write(array[i].ToString());
               
                if (countRow < submittedDataTable.Rows.Count)
                {
                    sw.Write('"');
                    sw.Write(",");
                }
                sw.WriteLine();

            }
            sw.Write("}");
            sw.WriteLine();
            sw.Close();
        }
        public string GetMD5(string chuoi)
        {
            string str_md5 = "";
            byte[] mang = System.Text.Encoding.UTF8.GetBytes(chuoi);

            MD5CryptoServiceProvider my_md5 = new MD5CryptoServiceProvider();
            mang = my_md5.ComputeHash(mang);
            foreach (byte b in mang)
            {
                str_md5 += b.ToString("x2");//Nếu là "X2" thì kết quả sẽ tự chuyển sang ký tự in Hoa
            }
            return str_md5;
        }
        public string EncryptTripleDES(string sIn, string sKey)
        {
            System.Security.Cryptography.TripleDESCryptoServiceProvider DES = new System.Security.Cryptography.TripleDESCryptoServiceProvider();
            System.Security.Cryptography.MD5CryptoServiceProvider hashMD5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
            // scramble the key
            sKey = ScrambleKey(sKey);
            // Compute the MD5 hash.
            DES.Key = hashMD5.ComputeHash(System.Text.ASCIIEncoding.ASCII.GetBytes(sKey));
            // Set the cipher mode.
            DES.Mode = System.Security.Cryptography.CipherMode.ECB;
            // Create the encryptor.
            System.Security.Cryptography.ICryptoTransform DESEncrypt = DES.CreateEncryptor();
            // Get a byte array of the string.
            byte[] Buffer = System.Text.ASCIIEncoding.ASCII.GetBytes(sIn);
            // Transform and return the string.
            return Convert.ToBase64String(DESEncrypt.TransformFinalBlock(Buffer, 0, Buffer.Length));
        }

        public string DecryptTripleDES(string sOut, string sKey)
        {
            try
            {
                System.Security.Cryptography.TripleDESCryptoServiceProvider DES = new System.Security.Cryptography.TripleDESCryptoServiceProvider();
                System.Security.Cryptography.MD5CryptoServiceProvider hashMD5 = new System.Security.Cryptography.MD5CryptoServiceProvider();

                // scramble the key
                sKey = ScrambleKey(sKey);
                // Compute the MD5 hash.
                DES.Key = hashMD5.ComputeHash(System.Text.ASCIIEncoding.ASCII.GetBytes(sKey));
                // Set the cipher mode.
                DES.Mode = System.Security.Cryptography.CipherMode.ECB;
                // Create the decryptor.
                System.Security.Cryptography.ICryptoTransform DESDecrypt = DES.CreateDecryptor();
                byte[] Buffer = Convert.FromBase64String(sOut);
                // Transform and return the string.
                return System.Text.ASCIIEncoding.ASCII.GetString(DESDecrypt.TransformFinalBlock(Buffer, 0, Buffer.Length));
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        private string ScrambleKey(string v_strKey)
        {
            try
            {
                System.Text.StringBuilder sbKey = new System.Text.StringBuilder();
                int intPtr = 0;
                for (intPtr = 1; intPtr <= v_strKey.Length; intPtr++)
                {
                    int intIn = v_strKey.Length - intPtr;
                    sbKey.Append(v_strKey.Substring(intIn, 1));
                }
                string strKey = sbKey.ToString();
                return sbKey.ToString();
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }


        public string DecryptTest(string cipherString, string key, bool useHashing)
        {
            byte[] keyArray;
            //get the byte code of the string

            byte[] toEncryptArray = Convert.FromBase64String(cipherString);

            System.Configuration.AppSettingsReader settingsReader =
                                                new System.Configuration.AppSettingsReader();
            //Get your key from config file to open the lock!
            //string key = (string)settingsReader.GetValue("SecurityKey",
            //                                             typeof(String));

            if (useHashing)
            {
                //if hashing was used get the hash code with regards to your key
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                //release any resource held by the MD5CryptoServiceProvider

                hashmd5.Clear();
            }
            else
            {
                //if hashing was not implemented get the byte code of the key
                keyArray = UTF8Encoding.UTF8.GetBytes(key);
            }

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            //set the secret key for the tripleDES algorithm
            tdes.Key = keyArray;
            //mode of operation. there are other 4 modes. 
            //We choose ECB(Electronic code Book)

            tdes.Mode = CipherMode.ECB;
            //padding mode(if any extra byte added)
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(
                                 toEncryptArray, 0, toEncryptArray.Length);
            //Release resources held by TripleDes Encryptor                
            tdes.Clear();
            //return the Clear decrypted TEXT
            return UTF8Encoding.UTF8.GetString(resultArray);
        }
    }

}
