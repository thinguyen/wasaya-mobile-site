﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Security.Cryptography;
using System.Globalization;

namespace IAS.GMS.API.Models
{
    public class Hashing
    {
        // Hashing Properties
        protected string hash;
        protected string salt;

        // Hashing Accessors
        public string Hash 
        {
            get { return hash; }
            private set { hash = value; }
        }

        public string Salt
        {
            get { return salt; }
            private set { salt = value;  }
        }
        
        // ----- Functionalities -----
        /// <summary>
        /// Generate a Salt value, hash the password input with the generated salt
        /// Store the result in the Properties
        /// </summary>
        /// <param name="password">The password value need to hash</param>
        public void SaltedHash(string password)
        {
            var saltBytes = new byte[32];
            
            using (var provider = new RNGCryptoServiceProvider())
            {
                provider.GetNonZeroBytes(saltBytes);
            }

            Salt = Convert.ToBase64String(saltBytes);
            Hash = ComputeHash(Salt, password);
        }
        
        /// <summary>
        /// Validate the password input with the hashed password
        /// </summary>
        /// <param name="salt">The salt value stored in the system</param>
        /// <param name="hash">The hashed password stored in the system</param>
        /// <param name="password">The input password</param>
        /// <returns>boolean(True/False)</returns>
        public static bool Verify(string salt, string hash, string password)
        {
            return hash == ComputeHash(salt, password);
        }

        /// <summary>
        /// Generate the hash string based on the input salt and password
        /// </summary>
        /// <param name="salt">The generated salt</param>
        /// <param name="password">The input password</param>
        /// <returns>The hash string</returns>
        internal static string ComputeHash(string salt, string password)
        {
            var saltBytes = Convert.FromBase64String(salt);
            using (var rfc2898DeriveBytes = new Rfc2898DeriveBytes(password, saltBytes, 1000))
            {
                return Convert.ToBase64String(rfc2898DeriveBytes.GetBytes(128));
            }
        }
    }
}