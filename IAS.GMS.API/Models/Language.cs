﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IAS.GMS.API.Models
{
    public class Language
    {
        private DataAccessLayer dal;

        private int m_languageID;

        public int LanguageID
        {
            get { return m_languageID; }
            set { m_languageID = value; }
        }
        private string m_languageName;

        public string LanguageName
        {
            get { return m_languageName; }
            set { m_languageName = value; }
        }
        private string m_languageCode;

        public string LanguageCode
        {
            get { return m_languageCode; }
            set { m_languageCode = value; }
        }
        private bool m_isDefault;

        public bool IsDefault
        {
            get { return m_isDefault; }
            set { m_isDefault = value; }
        }
        public Language()
        {
            dal = new DataAccessLayer();
        }
        public Language(int languageID, string languageName, string languageCode, bool isDefault)
        {
            m_languageID = languageID;
            m_languageName = languageName;
            m_languageCode = languageCode;
            m_isDefault = isDefault;
        }
        public Language(Language lg)
        {
            m_languageID = lg.m_languageID;
            m_languageName = lg.m_languageName;
            m_languageCode = lg.m_languageCode;
            m_isDefault = lg.m_isDefault;
        }
        
        
        
        
    }
}