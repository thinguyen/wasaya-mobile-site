if not exists (select * from sysobjects where name= +'ais_gms_users' and xtype='U')
    create table ais_gms_users (
        id				int not null identity(1,1) primary key,
        username		nvarchar(100) not null unique,
        display_name	nvarchar(255) not null,
        email			nvarchar(255) not null,
        [password]		nvarchar(255) not null,
        salt			nvarchar(100)  not null,
        created_at		nvarchar(100) not null,
        updated_at		nvarchar(100) not null,
        role_id			int not null default 0,
        is_active		int not null default 1
    ) 
go