﻿using IAS.GMS.API.Models;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;


namespace IAS.GMS.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ImportLanguagePhraseController : ApiController
    {
        private string _ConnectionString = ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString;
        private HttpRequest request = HttpContext.Current.Request;
        DataAccessLayer dal = new DataAccessLayer();
        private string fname = "";       
        [HttpPost]
        public bool Post()
        {
            try
            {
                fname = System.Web.HttpContext.Current.Server.MapPath("~\\Uploads\\LanguagePhrase.xlsx");
                string strConnection = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source='" + fname + "';Extended Properties='Excel 12.0 Xml;HDR=YES';";
                OleDbConnection conn = new OleDbConnection(strConnection);
                conn.Open();
                OleDbCommand command = new OleDbCommand("SELECT * FROM [LanguagePhrase$]", conn);
                DataSet ds = new DataSet();
                OleDbDataAdapter da = new OleDbDataAdapter(command);
                da.Fill(ds);
                DataTable dt = new DataTable();
                dt = ds.Tables[0];
                string sqlDel = "DELETE FROM tbl_LanguagePhrase";
                dal.ExecuteSQL(sqlDel);
                foreach (DataRow dr in dt.Rows)
                {
                    string sql = "INSERT INTO tbl_LanguagePhrase VALUES(" + dr["ID"] + "," + dr["LanguageID"] + "," + dr["PhraseID"] + "," + "'" + dr["PhraseValue"].ToString() + "'" + ")";
                    dal.ExecuteSQL(sql);
                }
              
                return true;
            }
            catch (Exception)
            {
                throw;
                return false;
            }


        }
       
    }
}
