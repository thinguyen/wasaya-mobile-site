﻿using IAS.GMS.API.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace IAS.GMS.API.Controllers
{
        [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UsersController : ApiController
    {
        private HttpRequest request = HttpContext.Current.Request;
        DataAccessLayer da = new DataAccessLayer();
        DataTable dt = new DataTable();
        private string keyPWD =ConfigurationManager.AppSettings["keyPWD"];
        //Create Json ListUser
        public JArray Get()
        {
            dt=da.GetDataSet("CALL sp_GetListUser").Tables[0];
            return da.ToJson(dt);
        }
      
            
        public class UserDataReceive
        {
            public int userID { get; set; }
            public string userName { get; set; }
            public string displayName { get; set; }
            public bool role { get; set; }
            public DateTime createOn { get; set; }
            public DateTime updateOn { get; set; }
            public string email { get; set; }
            public string password { get; set; }
        }
        public JObject Get(string email, string password)
        {
            string strSQL = string.Format("CALL sp_GetListUserByID('{0}','{1}')", email, da.EncryptTripleDES(password,keyPWD));
            //string strSQL = string.Format("	SELECT * FROM tbl_User WHERE  Email='{0}' AND Password='{1}' AND Role=1;", email, da.EncryptTripleDES(password, keyPWD));
            var result = da.GetScalarDataText(strSQL);
            var data = new JObject();
            data.Add("CheckLogin",result !=null);
            return data;
        }
        //public bool Get(string email, string password)
        //{
        //    string strSQL = string.Format("CALL sp_GetListUserByID('{0}','{1}')", email, password);
        //    var result = da.GetScalarDataText(strSQL);
        //    if (result != null)
        //        return true;
        //    return false;
        //}
          
        [HttpPost]
        public bool Post(UserDataReceive data)
        {
            if (data.userID == 0)
            {
                string strSQL = string.Format("CALL sp_UserInsert(N'{0}','{1}','{2}',{3},'{4}')",
                                               data.displayName, data.email, da.EncryptTripleDES(data.password,keyPWD), data.role, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                return da.ExecuteSQL(strSQL);
            }
            else
            {
                if (data.displayName != null)
                {
                    string strSQL = string.Format("CALL sp_UserUpdate({0},N'{1}','{2}')",
                                                  data.userID, data.displayName, data.email);
                    return da.ExecuteSQL(strSQL);
                }
                else
                {
                    string strSQL = string.Format("CALL sp_ChangePassword('{0}',N'{1}')",
                                                 data.email, da.EncryptTripleDES(data.password, keyPWD));
                    return da.ExecuteSQL(strSQL);
                }
            }
        }
        public void Delete(int id)
        {
            string strSQL = string.Format("CALL sp_UserDelete ({0})", id);
            da.ExecuteSQL(strSQL);
        }
    }
}
