﻿using IAS.GMS.API.Models;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace IAS.GMS.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UploadController : ApiController
    {
        private string _ConnectionString = ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString;
        private HttpRequest request = HttpContext.Current.Request;
        DataAccessLayer dal = new DataAccessLayer();
      
        [HttpPost]
        public bool Post()
        {
            try
            {
                string fname = "";
                var context = HttpContext.Current;
                HttpFileCollection files = context.Request.Files;
                for (int i = 0; i < files.Count; i++)
                {
                    HttpPostedFile file = files[i];
                    fname = context.Server.MapPath("~\\Uploads\\" + file.FileName);
                    file.SaveAs(fname);
                }
                return true;
            }
            catch (Exception)
            {
                throw;
                return false;
            }


        }
    }
}
