using IAS.GMS.API.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
namespace IAS.GMS.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ConfigurationController : ApiController
    {
        private HttpRequest request = HttpContext.Current.Request;
        DataAccessLayer da = new DataAccessLayer();
        DataTable dt = new DataTable();
        //
        // GET: /InformationPage/
        //Create Json Contact
        public JArray Get()
        {
            dt = da.GetDataSet("CALL sp_GetListConfiguration()").Tables[0];
            return da.ToJson(dt);
        }

        //public JArray Get(string languageCode)
        //{
        //    string strSQL = string.Format("CALL sp_GetListContactByLanguageCode('{0}')", languageCode);
        //    dt = da.GetDataSet(strSQL).Tables[0];
        //    return da.ToJson(dt);
        //}
        public class ConfigurationDataReceive
        {
            public int configID { get; set; }
            public string configCode { get; set; }
            public string configDescription { get; set; }
            public string configGroup { get; set; }
            public string configData { get; set; }
            public int configValue { get; set; }
           
            public DateTime createOn { get; set; }
            public DateTime updateOn { get; set; }
            public string createBy { get; set; }
            public string updateBy { get; set; }
        }
        [HttpPost]
        public bool Post(ConfigurationDataReceive data)
        {
            if (data.configID == 0)
            {

                string strSQL = string.Format("CALL sp_ConfigurationInsert({0}, N'{1}',N'{2}',N'{3}',N'{4}',{5},'{6}')",
                                               data.configID,data.configCode,data.configDescription,data.configGroup,data.configData,data.configValue, data.createBy,DateTime.Now);
                return da.ExecuteSQL(strSQL);
            }
            else
            {
                string strSQL = string.Format("{0}, N'{1}',N'{2}',N'{3}',N'{4}',{5},'{6}')",
                                               data.configID, data.configCode, data.configDescription, data.configGroup, data.configData, data.configValue, data.updateBy, DateTime.Now);
                return da.ExecuteSQL(strSQL);
            }
        }
        public void Delete(int id)
        {
            string strSQL = string.Format("CALL sp_ConfigurationDelete ({0})", id);
            da.ExecuteSQL(strSQL);
        }

    }
}
