﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using IAS.GMS.API.UserProfileService;
using IAS.GMS.API.Models;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Web;
using System.Web.Http.Cors;

namespace IAS.GMS.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UserProfileController : ApiController
    {
        // GET api/userprofile
        public UserProfileLoginResponse Get()
        {
            // Get type params
            HttpRequest httpRequest = HttpContext.Current.Request;

            if (httpRequest.QueryString != null)
            {

                try
                {
                    UserProfileLoginRequest request = new UserProfileLoginRequest();
                    request.ProfileEmail = httpRequest.QueryString["profileEmail"];
                    request.ProfileName = httpRequest.QueryString["profileName"];
                    request.ProfilePassword = httpRequest.QueryString["profilePassword"];

                    //create connection user profile service
                    var client = APIConnection.createUserProfileService();

                    //create user profile response from service when login
                    UserProfileLoginResponse response = new UserProfileLoginResponse();

                    //get user profile to response
                    response = client.LoginUserProfile(request);

                    return response;
                }
                catch (Exception e)
                {
                    throw (e);
                }
            }

            return null;
        }

        //defince user profile receive
        public class UserProfileDataReceive
        {
            public bool isAddUser { get; set; }
            public UserProfile profile { get; set; }
        }
        [HttpPost]
        // POST api/userprofile
        public UserProfileResponse Post(UserProfileDataReceive data)
        {
            //create connection user profile service
            var client = APIConnection.createUserProfileService();
            
            //create user profile request
            UserProfileRequest request = new UserProfileRequest();

            var _profile = data.profile;
            request.UserProfile = new UserProfile();
            request.UserProfile.IsActive = _profile.IsActive;
            request.UserProfile.IsVIP = false;
            request.UserProfile.DepartureAirportID = _profile.DepartureAirportID;
            request.UserProfile.ArrivalAirportID = _profile.ArrivalAirportID;
            request.UserProfile.UserProfileID = _profile.UserProfileID;
            request.UserProfile.ProfileName = _profile.ProfileName;
            request.UserProfile.Email = _profile.Email;
            request.UserProfile.Password = _profile.Password;
            request.UserProfile.SecurityQuestion = _profile.SecurityQuestion;
            request.UserProfile.SecurityAnswer = _profile.SecurityAnswer;

            PaxProfile pax = new PaxProfile();

            AgeCategory age_cat = new AgeCategory();
            age_cat = (AgeCategory)Enum.Parse(typeof(AgeCategory),_profile.PaxProfile.AgeCategory.ToString());
            pax.AgeCategory = age_cat;

            pax.IsActive = _profile.PaxProfile.IsActive;
            pax.PreBoard = _profile.PaxProfile.PreBoard;
            pax.DateOfBirth = _profile.PaxProfile.DateOfBirth;
            pax.PassportExires = _profile.PaxProfile.PassportExires;
            pax.PassportIssuedDate = _profile.PaxProfile.PassportIssuedDate;

            Gender profileGender = new Gender();
            profileGender = (Gender)Enum.Parse(typeof(Gender), _profile.PaxProfile.Gender.ToString());
            pax.Gender = profileGender;

            pax.LoyaltyPoints = _profile.PaxProfile.LoyaltyPoints;
            pax.CategoryID = _profile.PaxProfile.CategoryID;
            //pax.ProvinceID = _profile.PaxProfile.ProvinceID;
            //pax.CountryID = _profile.PaxProfile.CountryID;
            pax.PaxID = _profile.PaxProfile.PaxID;
            pax.GeneralNumber1 = _profile.PaxProfile.GeneralNumber1;
            pax.GeneralNumber2 = _profile.PaxProfile.GeneralNumber2;
            pax.PassportCountryCode = _profile.PaxProfile.PassportCountryCode;
            pax.PassportIssuedCity = _profile.PaxProfile.PassportIssuedCity;
            pax.Lastname = _profile.PaxProfile.Lastname;
            pax.Firstname = _profile.PaxProfile.Firstname;
            pax.Middlename = _profile.PaxProfile.Middlename;
            pax.EmployeeNumber = _profile.PaxProfile.EmployeeNumber;
            pax.Addr1 = _profile.PaxProfile.Addr1;
            pax.Addr2 = _profile.PaxProfile.Addr2;
            pax.City = _profile.PaxProfile.City;
            pax.PostalCode = _profile.PaxProfile.PostalCode;
            pax.CountryCode = _profile.PaxProfile.CountryCode;
            pax.ProvinceCode = _profile.PaxProfile.ProvinceCode;
            pax.BusTel = _profile.PaxProfile.BusTel;
            pax.BusExt = _profile.PaxProfile.BusExt;
            pax.BusFax = _profile.PaxProfile.BusFax;
            pax.HomeTel = _profile.PaxProfile.HomeTel;
            pax.HomeFax = _profile.PaxProfile.HomeFax;
            pax.Email = _profile.PaxProfile.Email;
            pax.AeroplanNumber = _profile.PaxProfile.AeroplanNumber;
            pax.Notes = _profile.PaxProfile.Notes;
            pax.SpecialNeeds = _profile.PaxProfile.SpecialNeeds;
            pax.Nationality = _profile.PaxProfile.Nationality;
            pax.PassportNumber = _profile.PaxProfile.PassportNumber;

            request.UserProfile.PaxProfile = pax;

            UserProfileResponse respone = new UserProfileResponse();
            if(data.isAddUser)
                respone = client.AddUserProfile(request);
            else
                respone = client.UpdateUserProfile(request);

            return respone;
        }
        // PUT api/userprofile/1
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/userprofile/1
        public void Delete(int id)
        {
        }
    }
}

