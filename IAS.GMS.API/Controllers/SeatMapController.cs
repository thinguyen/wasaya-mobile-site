﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using IAS.GMS.API.ReservationInvoiceService;
using IAS.GMS.API.CheckInService;
using IAS.GMS.API.Models;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Web;
using System.Web.Http.Cors;

namespace IAS.GMS.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SeatMapController : ApiController
    {
        // GET api/seatmap
        public Seatmap Get()
        {
            // Get type params
            HttpRequest httpRequest = HttpContext.Current.Request;

            if (httpRequest.QueryString != null)
            {

                try
                {
                    SeatmapQuery smquery = new SeatmapQuery();
                    smquery.ReservationNumber = Int32.Parse(httpRequest.QueryString["reservationNumber"]);
                    smquery.LegNumber = Int32.Parse(httpRequest.QueryString["legNumber"]);

                    //create client to call reservation invoice service
                    var client = APIConnection.createCheckInService();

                    //create reservation request with option
                    SeatMapRequest request = new SeatMapRequest();
                    request.Query = smquery;

                    //create reservation response from service
                    SeatMapResponse response = new SeatMapResponse();
                    Console.WriteLine(client.GetSeatMap(request));
                    //get reservation to response
                    response = client.GetSeatMap(request);

                    return response.Seatmap;
                }
                catch (Exception e)
                {
                    throw (e);
                }
            }

            return null;
        }

        // GET api/seatmap/1
        public string Get(int id)
        {
            return null;
        }

        public class SeatAssigmentDataReceive
        {
            public int reservationNumber { get; set; }
            public int legNumber { get; set; }
            public List<SeatAssignmentPax> passengers { get; set; }
        }
        [HttpPost]
        // POST api/seatmap
        public SeatAssigmentResponse Post(SeatAssigmentDataReceive data)
        {
            var client = APIConnection.createCheckInService();

            SeatAssigmentRequest request = new SeatAssigmentRequest();

            //ReservationNumber
            request.ReservationNumber = data.reservationNumber;

            //LegNumber
            request.LegNumber = data.legNumber;

            //Array paxlist
            var passengers = data.passengers.ToArray();

            //paxlist length
            request.PaxList = new SeatAssignmentPax[passengers.Length];

            //query paxlist
            for (var i = 0; i < passengers.Length; i++)
            {
                //SeatAssignmentPax of pax list
                request.PaxList[i] = new SeatAssignmentPax();
                request.PaxList[i].Lastname = passengers[i].Lastname;
                request.PaxList[i].Firstname = passengers[i].Firstname;
                request.PaxList[i].RowNumber = passengers[i].RowNumber;
                request.PaxList[i].SeatNumber = passengers[i].SeatNumber;
                request.PaxList[i].PassportNumber = passengers[i].PassportNumber;
            }

            SeatAssigmentResponse respone = new SeatAssigmentResponse();
            respone = client.SetSeatAssignments(request);

            return respone;
        }
        // PUT api/seatmap/1
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/seatmap/1
        public void Delete(int id)
        {
        }
    }
}

