﻿using IAS.GMS.API.BookingService;
using IAS.GMS.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace IAS.GMS.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SeatMapByFlightController : ApiController
    {
        public class SeatMapByFlightDataReceive
        {
            public string fareCategory { get; set; }
            public List<SegmentOption> segment { get; set; }
        }
        // GET: /SeatMapByFlight/
        [HttpPost]
        public Array Post(SeatMapByFlightDataReceive data)
        {
            //http request client
            HttpRequest httpRequest = HttpContext.Current.Request;

            GetSeatMapByFlightRequest request = new GetSeatMapByFlightRequest();
            request.FareCategory = data.fareCategory;
            var segments = data.segment.ToArray();
            for (var i = 0; i < segments.Length; i++)
            {
                //segments parameter
                request.FlightList[i] = new Flight();
                //request.FlightList[i].Aircraft.ModelIdent = segments[i].Flight.Aircraft.ModelIdent;
                //request.FlightList[i].Aircraft.ModelName = segments[i].Flight.Aircraft.ModelName;
                request.FlightList[i].ArrivalAirport.Code = segments[i].Flight.ArrivalAirport.Code;
                //request.FlightList[i].ArrivalAirport.Name = segments[i].Flight.ArrivalAirport.Name;
                request.FlightList[i].DepartureAirport.Code = segments[i].Flight.DepartureAirport.Code;
                //request.FlightList[i].DepartureAirport.Name = segments[i].Flight.DepartureAirport.Name;
                //request.FlightList[i].ETA = segments[i].Flight.ETA;
                //request.FlightList[i].ETALocal = segments[i].Flight.ETALocal;
                //request.FlightList[i].ETD = segments[i].Flight.ETD;
                request.FlightList[i].ETDLocal = segments[i].Flight.ETDLocal;
                request.FlightList[i].Number = segments[i].Flight.Number;
            }
            var client = APIConnection.createBookingService();
            GetSeatMapByFlightResponse response = new GetSeatMapByFlightResponse();
            response = client.GetSeatMapByFlight(request);
            return response.SeatMapList.ToArray();
        }

    }
}
