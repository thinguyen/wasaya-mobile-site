﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using IAS.GMS.API.UserProfileService;
using IAS.GMS.API.Models;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Web;
using System.Web.Http.Cors;
using IAS.GMS.API.ReservationInvoiceService;

namespace IAS.GMS.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UserProfileReservationController : ApiController
    {
        // GET api/userprofilereservation
        public UserProfileReservationListResponse Get()
        {
            // Get type params
            HttpRequest httpRequest = HttpContext.Current.Request;

            if (httpRequest.QueryString != null)
            {

                try
                {
                    UserProfileReservationListRequest request = new UserProfileReservationListRequest();
                    ReservationSearchQuery rsq = new ReservationSearchQuery();
                    rsq.UserProfileID = int.Parse(httpRequest.QueryString["userProfileID"]);
                    request.Query = rsq;
                   
                    //create connection user profile service
                    var client = APIConnection.createUserProfileService();

                    //create user profile response from service when login
                    UserProfileReservationListResponse response = new UserProfileReservationListResponse();

                    //get user profile to response
                    response = client.GetProfileReservationList(request);

                    return response;
                }
                catch (Exception e)
                {
                    throw (e);
                }
            }

            return null;
        }

        [HttpPost]
        // POST api/userprofilereservation
        public void Post()
        {
        }
        // PUT api/userprofilereservation/1
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/userprofilereservation/1
        public void Delete(int id)
        {
        }
    }
}

