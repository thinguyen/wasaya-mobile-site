﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Configuration;
using System.Web;
using IAS.GMS.API.BookingService;
using IAS.GMS.API.Models;
using System.Web.Http.Cors;

namespace IAS.GMS.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UploadFileController : ApiController
    {
        //
        // GET: /UploadFile/


        public bool Get(HttpContext context)
        {
            try
            {
                HttpFileCollection files = context.Request.Files;
                for (int i = 0; i < files.Count; i++)
                {
                    HttpPostedFile file = files[i];
                    string fname = context.Server.MapPath("Uploads\\" + file.FileName);
                    file.SaveAs(fname);
                }
                return true;
            }
            catch (Exception)
            {
                throw;
                return false;
            }


        }
    }
}
