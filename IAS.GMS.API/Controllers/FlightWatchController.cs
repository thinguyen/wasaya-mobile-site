﻿using IAS.GMS.API.BoardingService;
using IAS.GMS.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace IAS.GMS.API.Controllers
{
     [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class FlightWatchController : ApiController
    {
         // GET api/flight
        public FlightWatchDetails Get()
        {
            // Get type params
            HttpRequest httpRequest = HttpContext.Current.Request;

            if (httpRequest.QueryString != null)
            {

                try
                {
                    FlightWatchRequest request = new FlightWatchRequest();
                    request.ETD_StartRange = DateTime.Parse(httpRequest.QueryString["etd_StartRange"]);
                    request.ETD_EndRange = DateTime.Parse(httpRequest.QueryString["etd_EndRange"]);
                    request.DepartureAirportCode = httpRequest.QueryString["departureAirportCode"];
                    request.ArrivalAirportCode = httpRequest.QueryString["arrivalAirportCode"];
                    request.FlightNumber = httpRequest.QueryString["flightNumber"];
                    var client = APIConnection.createBoardingService();

                    FlightWatchResponse response = new FlightWatchResponse();

                    response = client.GetFlightWatchDetails(request);

                    return response.Details;
                }
                catch (Exception e)
                {
                    throw (e);
                }
            }

            return null;
        }
    }
 }