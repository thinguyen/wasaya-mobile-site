﻿using IAS.GMS.API.BookingService;
using IAS.GMS.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace IAS.GMS.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PaymentMethodController : ApiController
    {
        private Array GetPaymentMethodList()
        {
            PaymentMethodListResponse response = new PaymentMethodListResponse();
            try
            {
                var client = APIConnection.createBookingService();
                response = client.GetPaymentMethodList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return response.PaymentMethodList;

        }
        // GET api/paymentmethod
        public Array Get()
        {
            return GetPaymentMethodList();
        }

        // GET api/paymentmethod/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/paymentmethod
        public void Post([FromBody]string value)
        {
        }

        // PUT api/paymentmethod/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/paymentmethod/5
        public void Delete(int id)
        {
        }
    }
}
