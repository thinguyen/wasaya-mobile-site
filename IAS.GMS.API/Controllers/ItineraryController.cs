﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Configuration;
using System.Web;
using IAS.GMS.API.BookingService;
using IAS.GMS.API.Models;
using System.Web.Http.Cors;

namespace IAS.GMS.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ItineraryController : ApiController
    {
        // GET api/itinerary
        public bool Get()
        {
            //http request client
            HttpRequest httpRequest = HttpContext.Current.Request;
            
            //create send email data from client
            SendItineraryRequest request = new SendItineraryRequest();
            //set atribute for request
            request.IncludeAllPax = true;
            request.ReservationNumber = Int32.Parse(httpRequest.QueryString["reservationNumber"]);
            request.LanguageCode = httpRequest.QueryString["languageCode"];
            request.AltToAddress = httpRequest.QueryString["altToAddress"];

            //create connection booking service
            var client = APIConnection.createBookingService();
            //create send email respone from api
            SendItineraryResponse respone = new SendItineraryResponse();
            respone = client.EmailItinerary(request);
            
            //return boolean 
            return respone.OperationSucceeded;
        }

        // GET api/itinerary/id
        public bool Get(string id)
        {
            return false;
        }

        // POST api/itinerary
        public bool Post()
        {
            return false;
        }

        // PUT api/itinerary/id
        public bool Put()
        {
            return false;
        }

        // DELETE api/itinerary/id
        public bool Delete(int id)
        {
            return false;
        }

    }
}
