﻿using IAS.GMS.API.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace IAS.GMS.API.Controllers
{
      [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PhraseController : ApiController
    {
        private HttpRequest request = HttpContext.Current.Request;
        DataAccessLayer da = new DataAccessLayer();
        DataTable dt = new DataTable();
      
        // GET: /Language/
        //Create Json ListUser
        public JArray Get()
        {
            dt = da.GetDataSet("CALL sp_GetListPhrase").Tables[0];
            return da.ToJson(dt);
        }
        public class ContactDataReceive
        {
            public int phraseID { get; set; }
            public string phraseName { get; set; }
        }
        [HttpPost]
        public bool Post(ContactDataReceive data)
        {
            if (data.phraseID == 0)
            {

                string strSQL = string.Format("CALL sp_Phrase_Insert('{0}')",
                                                 data.phraseName);
                return da.ExecuteSQL(strSQL);
            }
            else
            {
                string strSQL = string.Format("CALL sp_Phrase_Update({0}, '{1}')",
                                               data.phraseID, data.phraseName);
                return da.ExecuteSQL(strSQL);
            }
        }
        public void Delete(int id)
        {
            string strSQL = string.Format("CALL sp_Phrase_Delete ({0})", id);
            da.ExecuteSQL(strSQL);
        }

    }
}
