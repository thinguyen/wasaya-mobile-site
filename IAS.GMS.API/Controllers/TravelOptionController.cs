﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using IAS.GMS.API.Models;
using IAS.GMS.API.BookingService;
using System.Web.Http.Cors;

namespace IAS.GMS.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class TravelOptionController : ApiController
    {
        // GET api/traveloption
        public TravelOptions Get()
        {
            // Get type params
            HttpRequest httpRequest = HttpContext.Current.Request;

            if (httpRequest.QueryString != null)
            {
                TravelOptionsQuery query = new TravelOptionsQuery();

                var roundTrip = Int32.Parse(httpRequest.QueryString["roundTrip"]);
                if (roundTrip == 1)
                {
                    query.InboundDate = DateTime.Parse(httpRequest.QueryString["InboundDate"]);
                }
                query.OutboundDate = DateTime.Parse(httpRequest.QueryString["OutboundDate"]);
                query.DaysBefore = Int32.Parse(httpRequest.QueryString["DaysBefore"]);
                query.DaysAfter = Int32.Parse(httpRequest.QueryString["DaysAfter"]);
                query.AdultCount = Int32.Parse(httpRequest.QueryString["AdultCount"]);
                query.ChildCount = Int32.Parse(httpRequest.QueryString["ChildCount"]);
                query.InfantCount = Int32.Parse(httpRequest.QueryString["InfantCount"]);
                query.DepartureAirportCode = httpRequest.QueryString["DepartureAirportCode"];
                query.ArrivalAirportCode = httpRequest.QueryString["ArrivalAirportCode"];
                //query.TravelClassCode = httpRequest.QueryString["TravelClassCode"];
                query.TravelClassCode = "";
                query.CurrencyCode = httpRequest.QueryString["CurrencyCode"];
                query.PromoCode = httpRequest.QueryString["PromoCode"];

                // Get travel options data from service
                try
                {
                    var client = APIConnection.createBookingService();
                    TravelOptionsRequest request = new TravelOptionsRequest();
                    request.Query = query;
                    TravelOptionsResponse response = new TravelOptionsResponse();
                    response = client.GetTravelOptions(request);
                    return response.TravelOptions;
                }
                catch (Exception e)
                {
                    throw (e);
                }
            }

            return null;
        }

        // GET api/traveloption/5
        public string Get(int id)
        {
            return null;
        }

        // POST api/traveloption
        public TravelOptions Post()
        {
            // Get type params
            HttpRequest httpRequest = HttpContext.Current.Request;



            if (httpRequest.QueryString != null)
            {
                TravelOptionsQuery query = new TravelOptionsQuery();

                var roundTrip = Int32.Parse(httpRequest.QueryString["roundTrip"]);
                if (roundTrip == 1)
                {
                    query.InboundDate = DateTime.Parse(httpRequest.QueryString["InboundDate"]);
                }
                query.OutboundDate = DateTime.Parse(httpRequest.QueryString["OutboundDate"]);
                query.DaysBefore = Int32.Parse(httpRequest.QueryString["DaysBefore"]);
                query.DaysAfter = Int32.Parse(httpRequest.QueryString["DaysAfter"]);
                query.AdultCount = Int32.Parse(httpRequest.QueryString["AdultCount"]);
                query.ChildCount = Int32.Parse(httpRequest.QueryString["ChildCount"]);
                query.InfantCount = Int32.Parse(httpRequest.QueryString["InfantCount"]);
                query.DepartureAirportCode = httpRequest.QueryString["DepartureAirportCode"];
                query.ArrivalAirportCode = httpRequest.QueryString["ArrivalAirportCode"];
                query.TravelClassCode = "";
                query.CurrencyCode = httpRequest.QueryString["CurrencyCode"];
                query.PromoCode = httpRequest.QueryString["PromoCode"];

                // Get travel options data from service
                try
                {
                    var client = APIConnection.createBookingService();
                    TravelOptionsRequest request = new TravelOptionsRequest();
                    request.Query = query;
                    TravelOptionsResponse response = new TravelOptionsResponse();

                    response = client.GetTravelOptions(request);

                    return response.TravelOptions;
                }
                catch (Exception e)
                {
                    throw (e);
                }
            }

            return null;
        }

        // PUT api/traveloption/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/traveloption/5
        public void Delete(int id)
        {
        }
    }
}
