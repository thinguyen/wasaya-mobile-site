﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using IAS.GMS.API.BookingService;
using IAS.GMS.API.Models;
using System.Web.Http.Cors;

namespace IAS.GMS.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AddonController : ApiController
    {
        private Array getAirportSSRAllocations(DateTime date, String indicator, String airportCode, String currencyCode)
        {
            // Return if one of any params is null
            if (date != null && indicator != null && airportCode != null && currencyCode != null)
            {
                if (indicator == "Arrival" || indicator == "Departure")
                {
                    try
                    {
                        // Get airport pairs data from service
                        var client = APIConnection.createBookingService();

                        // Generate a request
                        AirportAllocRequest request = new AirportAllocRequest();
                        request.DateOfDepartureOrArrival = date;
                        request.DepartureOrArrivalIndicator = indicator == "Arrival" ? DepartureArrival.Arrival : DepartureArrival.Departure;
                        request.AirportCode = airportCode;
                        request.CurrencyCode = currencyCode;

                        // Get result
                        AirportAllocResponse response = new AirportAllocResponse();
                        response = client.GetAirportSSRAllocations(request);
                        return response.Allocations;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                }
            }
            return null;
        }

        private Array getFlightSSRAllocation(DateTime departureDate, DateTime arrivalDate, String departure, String arrival, String flightCode, String currencyCode)
        {
            // Return if one of any params is null
            if (departureDate != null && arrivalDate != null && departure != null && arrival != null && flightCode != null && currencyCode != null)
            {
                try
                {
                    // Get airport pairs data from service
                    var client = APIConnection.createBookingService();

                    // Generate a request
                    FlightAllocRequest request = new FlightAllocRequest();
                    request.CurrencyCode = currencyCode;

                    BookingSegment segment = new BookingSegment();
                    segment.DepartureAirportCode = departure;
                    segment.ArrivalAirportCode = arrival;
                    segment.ETDLocal = departureDate;
                    segment.ETALocal = arrivalDate;
                    segment.FlightCode = flightCode;
                    request.Segments = new BookingSegment[] { segment };

                    // Get result
                    FlightAllocResponse response = new FlightAllocResponse();
                    response = client.GetFlightSSRAllocations(request);
                    if (response.Allocations == null)
                        return new CategoryOfFlightAllocations[0];
                    else
                        return response.Allocations;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
            return null;
        }

        // GET api/addon
        public Array Get()
        {
            Array result = null;

            // Get type params
            HttpRequest request = HttpContext.Current.Request;
            var currencyCode = request.QueryString["currency"];
            var type = request.QueryString["type"];
            if (type != null)
            {
                type = type.ToLower();
            }

            // Switch data type
            switch (type)
            {
                case "airport":
                    var date = DateTime.Parse(request.QueryString["date"]);
                    var indicator = request.QueryString["indicator"];
                    var airportCode = request.QueryString["airport"];

                    result = getAirportSSRAllocations(date, indicator, airportCode, currencyCode);
                    break;
                case "flight":
                    var departureDate = DateTime.Parse(request.QueryString["depDate"]);
                    var arrivalDate = DateTime.Parse(request.QueryString["arrDate"]);
                    var departure = request.QueryString["depAirport"];
                    var arrival = request.QueryString["arrAirport"];
                    var flightCode = request.QueryString["flight"];

                    result = getFlightSSRAllocation(departureDate, arrivalDate, departure, arrival, flightCode, currencyCode);
                    break;
                default:
                    result = null;
                    break;
            }

            return result;
        }

        // GET api/addon/5
        public string Get(int id)
        {
            return null;
        }

        // POST api/addon

        public string Post([FromBody]string value)
        {
            return null;
        }

        // PUT api/addon/5
        public bool Put(int id, [FromBody]string value)
        {
            return false;
        }

        // DELETE api/addon/5
        public bool Delete(int id)
        {
            return false;
        }
    }
}
