﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Configuration;
using System.Web;
using IAS.GMS.API.BookingService;
using IAS.GMS.API.Models;
using System.Web.Http.Cors;

namespace IAS.GMS.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CurrencyController : ApiController
    {
        //
        // GET: /Currency/

        public Array Get()
        {
            var client = APIConnection.createBookingService();
            CurrencyListResponse response = new CurrencyListResponse();
            response = client.GetCurrencyList();
            return response.Currencies;
        }

    }
}
