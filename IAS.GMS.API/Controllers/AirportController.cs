﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Configuration;
using System.Web;
using IAS.GMS.API.BookingService;
using IAS.GMS.API.Models;
using System.Web.Http.Cors;

namespace IAS.GMS.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AirportController : ApiController
    {

        // Get Airport City Pairs List from service
        private Array getAirportCityPairsList()
        {
            AirportPairsResponse response = new AirportPairsResponse();
            try
            {
                // Get airport pairs data from service
                var client = APIConnection.createBookingService();
                response = client.GetAirportCityPairsList();
            }
            catch (Exception)
            {
                throw;
            }
            return response.AirportPairs;
        }
        // Get Airport List from service
        private Array GetAirportList()
        {
            AirportListResponse response = new AirportListResponse();
            try
            {
                // Get airport pairs data from service
                var client = APIConnection.createBookingService();
                response = client.GetAirportList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return response.Airports;
        }
        // GET api/airport
        public Array Get()
        {
            List<Airport> airportlist = new List<Airport>();
            try
            {
                // Get city pairs list for filter
                var airportPairsList = getAirportCityPairsList();
                // Find the selected airport
                foreach (AirportPairs airportPairs in airportPairsList)
                {
                    Airport airport = new Airport();
                    airport.Code = airportPairs.DepartureAirport.Code;
                    airport.Name = airportPairs.DepartureAirport.Name;
                    airportlist.Add(airport);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            return airportlist.ToArray();
        }

        // GET api/airport/SGN
        public AirportPairs Get(string id)
        {
            // Get city pairs list for filter
            var airportPairsList = getAirportCityPairsList();

            // Find the selected airport
            foreach (AirportPairs airportPairs in airportPairsList)
            {
                if (airportPairs.DepartureAirport.Code == id.ToUpper())
                {
                    return airportPairs;
                }
            }
            return null;
        }

        // POST api/airport
        public bool Post(string value)
        {
            return false;
        }

        // PUT api/airport/5
        public bool Put(int id, [FromBody]string value)
        {
            return false;
        }

        // DELETE api/airport/5
        public bool Delete(int id)
        {
            return false;
        }
    }
}
