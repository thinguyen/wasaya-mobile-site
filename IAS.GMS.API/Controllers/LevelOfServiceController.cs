﻿using IAS.GMS.API.BookingService;
using IAS.GMS.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Mvc;

namespace IAS.GMS.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class LevelOfServiceController : ApiController
    {
        //
        // GET: /LevelOfServiceList/

        public Array Get()
        {
            var client = APIConnection.createBookingService();
            LevelOfServiceListResponse response = new LevelOfServiceListResponse();
            response = client.GetLevelOfServiceList();
            return response.LosList;
        }

    }
}
