﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using IAS.GMS.API.BookingService;
using IAS.GMS.API.ReservationInvoiceService;
using IAS.GMS.API.Models;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Web;
using System.Web.Http.Cors;

namespace IAS.GMS.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ReservationController : ApiController
    {
        // GET api/reservation
        /*public string Get()
        {
            return "value";
        }*/

        // GET api/reservation/5
        public ReservationInvoiceService.ReservationInvoice Get()
        {
            // Get type params
            HttpRequest httpRequest = HttpContext.Current.Request;

            if (httpRequest.QueryString != null)
            {
                // Get reservation number and options data 
                try
                {
                    //create client to call reservation invoice service
                    var client = APIConnection.createReservationInvoiceService();

                    //create reservation request with option
                    ReservationRequest request = new ReservationRequest();
                    request.ReservationNumber = Int32.Parse(httpRequest.QueryString["reservationNumber"]);
                    request.PaxLastName = httpRequest.QueryString["paxLastName"];
                    request.PaxFirstName = httpRequest.QueryString["paxFirstName"];

                    //create reservation response from service
                    ReservationResponse response = new ReservationResponse();

                    //get reservation to response
                    response = client.GetReservation(request);

                    return response.Invoice;
                }
                catch (Exception e)
                {
                    throw (e);
                }
            }

            return null;
        }

        public class BookingDataReceive
        {
            public List<BookingLeg> legs { get; set; }
            public List<BookingPassenger> passengers { get; set; }
            public BookingPayment payment { get; set; }
        }


        [HttpPost]
        // POST api/reservation
        public BookingResponse Post(BookingDataReceive data)
        {
            var client = APIConnection.createBookingService();

            //create booking query
            var query = new BookingQuery();

            //create legs data from client
            var _legs = data.legs.ToArray();

            //assign legs to booking query
            query.Legs = new BookingLeg[_legs.Length];

            //assign data from client to Legs Option
            for (var i = 0; i < _legs.Length; i++)
            {
                query.Legs[i] = new BookingLeg();

                //Adult fare
                query.Legs[i].AdultFares = new BookingFare();
                query.Legs[i].AdultFares.BookingClassCode = _legs[i].AdultFares.BookingClassCode;
                query.Legs[i].AdultFares.CurrencyCode = _legs[i].AdultFares.CurrencyCode;
                query.Legs[i].AdultFares.FareCode = _legs[i].AdultFares.FareCode;
                query.Legs[i].AdultFares.PaxCount = _legs[i].AdultFares.PaxCount;
                query.Legs[i].AdultFares.TotalCost = _legs[i].AdultFares.TotalCost;

                //child fare
                query.Legs[i].ChildFares = new BookingFare();
                query.Legs[i].ChildFares.BookingClassCode = _legs[i].ChildFares.BookingClassCode;
                query.Legs[i].ChildFares.CurrencyCode = _legs[i].ChildFares.CurrencyCode;
                query.Legs[i].ChildFares.FareCode = _legs[i].ChildFares.FareCode;
                query.Legs[i].ChildFares.PaxCount = _legs[i].ChildFares.PaxCount;
                query.Legs[i].ChildFares.TotalCost = _legs[i].ChildFares.TotalCost;

                //segment array
                var segments = _legs[i].Segments.ToArray();
                query.Legs[i].Segments = new BookingSegment[segments.Length];
                for (var j = 0; j < segments.Length; j++)
                {
                    //booking segment
                    query.Legs[i].Segments[j] = new BookingSegment();
                    query.Legs[i].Segments[j].ETDLocal = segments[j].ETDLocal;
                    query.Legs[i].Segments[j].ETALocal = segments[j].ETALocal;
                    query.Legs[i].Segments[j].FlightCode = segments[j].FlightCode;
                    query.Legs[i].Segments[j].DepartureAirportCode = segments[j].DepartureAirportCode;
                    query.Legs[i].Segments[j].ArrivalAirportCode = segments[j].ArrivalAirportCode;
                }
                query.Legs[i].PromoCodeID = _legs[i].PromoCodeID;
                query.Legs[i].PromoCodeSuffixID = _legs[i].PromoCodeSuffixID;
            }

            //create passengers from client
            var _passengers = data.passengers.ToArray();

            //assign passengers to booking query
            query.Passengers = new BookingPassenger[_passengers.Length];

            //assign data from client to passengers
            for (var i = 0; i < _passengers.Length; i++)
            {
                //passenger
                query.Passengers[i] = new BookingPassenger();


                BookingPassengerAgeCategory age_cat = new BookingPassengerAgeCategory();
                age_cat = (BookingPassengerAgeCategory)Enum.Parse(typeof(BookingPassengerAgeCategory), _passengers[i].AgeCategory.ToString());
                BookingPassengerGender passengerGender = new BookingPassengerGender();
                passengerGender = (BookingPassengerGender)Enum.Parse(typeof(BookingPassengerGender), _passengers[i].Gender.ToString());
                query.Passengers[i].Firstname = _passengers[i].Firstname;
                query.Passengers[i].Lastname = _passengers[i].Lastname;
                query.Passengers[i].DateOfBirth = _passengers[i].DateOfBirth;
                query.Passengers[i].AgeCategory = age_cat;
                query.Passengers[i].Gender = passengerGender;
                query.Passengers[i].ProfileUsername = _passengers[i].ProfileUsername;
                query.Passengers[i].ProfilePassword = _passengers[i].ProfilePassword;

                if (_passengers[i].AgeCategory.ToString() == "Adult")
                {
                    BookingPassengerTitle title = new BookingPassengerTitle();
                    title = (BookingPassengerTitle)Enum.Parse(typeof(BookingPassengerTitle), _passengers[i].Title.ToString());
                    query.Passengers[i].Title = title;
                    //infant
                    if (_passengers[i].Infants.Length > 0)
                    {
                        if (_passengers[i].Infants != null)
                        {
                            query.Passengers[i].Infants = new BookingService.Infant[1];
                            query.Passengers[i].Infants[0] = new BookingService.Infant();
                            query.Passengers[i].Infants[0].Gender = _passengers[i].Infants[0].Gender;
                            query.Passengers[i].Infants[0].DateOfBirth = _passengers[i].Infants[0].DateOfBirth;
                            query.Passengers[i].Infants[0].Firstname = _passengers[i].Infants[0].Firstname;
                            query.Passengers[i].Infants[0].Lastname = _passengers[i].Infants[0].Lastname;
                        }
                    }

                    //flightSSRs array
                    var flightSSRs = _passengers[i].FlightSSRs.ToArray();
                    query.Passengers[i].FlightSSRs = new BookingFlightSSR[flightSSRs.Length];
                    for (var j = 0; j < flightSSRs.Length; j++)
                    {
                        //booking flightSSRs
                        query.Passengers[i].FlightSSRs[j] = new BookingFlightSSR();
                        query.Passengers[i].FlightSSRs[j].ExtraAllocationIDCount = flightSSRs[j].ExtraAllocationIDCount;
                        query.Passengers[i].FlightSSRs[j].Quantity = flightSSRs[j].Quantity;
                        query.Passengers[i].FlightSSRs[j].FlightAllocID = flightSSRs[j].FlightAllocID;
                        query.Passengers[i].FlightSSRs[j].LegNumber = flightSSRs[j].LegNumber;
                        query.Passengers[i].FlightSSRs[j].ExtraAllocationIDs = flightSSRs[j].ExtraAllocationIDs;
                    }

                    //personal contact infomation
                    query.Passengers[i].PersonalContact = new BookingService.Contact();
                    query.Passengers[i].PersonalContact.Email = _passengers[i].PersonalContact.Email;
                    query.Passengers[i].PersonalContact.Mobile = _passengers[i].PersonalContact.Mobile;
                    query.Passengers[i].PersonalContact.Name = _passengers[i].PersonalContact.Name;
                }
            }

            //create payment data from client
            var _payment = data.payment;

            //assign payment to query booking

            query.Payment = new BookingPayment();
            query.Payment.Amount = _payment.Amount;
            query.Payment.CurrencyCode = _payment.CurrencyCode;
            BookingPaymentType bookPaymentType = new BookingPaymentType();
            bookPaymentType = (BookingPaymentType)Enum.Parse(typeof(BookingPaymentType), _payment.PaymentType.ToString());
            query.Payment.PaymentType = bookPaymentType;
            if (_payment.PaymentType.ToString() == "CreditCard")
            {
                query.Payment.CreditCardPayment = new BookingPaymentCreditCard();

                query.Payment.CreditCardPayment.Address = new BookingService.Address();
                query.Payment.CreditCardPayment.Address.Addr1 = _payment.CreditCardPayment.Address.Addr1; //payment["creditCardPayment"]["address"]["addr1"].Value<string>();
                query.Payment.CreditCardPayment.Address.City = _payment.CreditCardPayment.Address.City; //payment["creditCardPayment"]["address"]["city"].Value<string>();
                query.Payment.CreditCardPayment.Address.CountryCode = _payment.CreditCardPayment.Address.CountryCode;// payment["creditCardPayment"]["address"]["countryCode"].Value<string>();
                query.Payment.CreditCardPayment.Address.ProvinceAbbreviation = _payment.CreditCardPayment.Address.ProvinceAbbreviation;//payment["creditCardPayment"]["address"]["provinceAbbreviation"].Value<string>();
                query.Payment.CreditCardPayment.Address.PostalCode = _payment.CreditCardPayment.Address.PostalCode; //payment["creditCardPayment"]["address"]["postalCode"].Value<string>();

                query.Payment.CreditCardPayment.CVV = _payment.CreditCardPayment.CVV;//payment["creditCardPayment"]["cvv"].Value<string>();
                query.Payment.CreditCardPayment.Number = _payment.CreditCardPayment.Number; //payment["creditCardPayment"]["number"].Value<string>();

                query.Payment.CreditCardPayment.ExpiryMonth = _payment.CreditCardPayment.ExpiryMonth;// payment["creditCardPayment"]["expiryMonth"].Value<int>();
                query.Payment.CreditCardPayment.ExpireYear = _payment.CreditCardPayment.ExpireYear; //payment["creditCardPayment"]["expiryYear"].Value<int>();

                query.Payment.CreditCardPayment.Email = _payment.CreditCardPayment.Email;// payment["creditCardPayment"]["email"].Value<string>();
                query.Payment.CreditCardPayment.Firstname = _payment.CreditCardPayment.Firstname;// payment["creditCardPayment"]["firstName"].Value<string>();
                query.Payment.CreditCardPayment.Lastname = _payment.CreditCardPayment.Lastname;// payment["creditCardPayment"]["lastName"].Value<string>();
                query.Payment.CreditCardPayment.TypeCode = _payment.CreditCardPayment.TypeCode;
            }

            //call processing fee
            var feeRequest = new CalculateProcessingFeeRequest();
            feeRequest.Query = query;
            var feeResponse = client.CalculateProcessingFee(feeRequest);
            query.Payment = feeResponse.Query.Payment;
            //create booking request
            var request = new BookingRequest();
            request.Query = query;

            //call booking reservation
            var response = client.BookReservation(request);

            return response;
        }

        // PUT api/reservation/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/reservation/5
        public void Delete(int id)
        {
        }
    }
}
