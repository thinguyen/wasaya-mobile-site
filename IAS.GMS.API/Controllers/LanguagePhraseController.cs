﻿using IAS.GMS.API.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace IAS.GMS.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class LanguagePhraseController : ApiController
    {
       
        private HttpRequest request = HttpContext.Current.Request;
        DataAccessLayer da = new DataAccessLayer();
        //
        // GET: /LanguagePhrase/
        //Create Json LanguagePhrase
        public JArray Get()
        {
            var dt = da.GetDataSet("CALL sp_GetListLanguagePhrase").Tables[0];
            return da.ToJson(dt);
        }
        //public JArray Get(string languageCode)
        //{
        //    string strSQL = string.Format("sp_GetListLanguagePhraseByLanguageCode @LanguageCode='{0}'", languageCode);
        //    dt = da.GetDataSet(strSQL).Tables[0];
        //    return da.ToJson(dt);
        //}
        public JObject Get(string lang)
        {
            string strSQL = string.Format("CALL sp_GetListLanguagePhraseByLanguageCode('{0}')", lang);
            var table = da.GetDataSet(strSQL).Tables[0];
            var data = new JObject();
            foreach (DataRow row in table.Rows) {
                data.Add(row["PhraseName"].ToString(), row["PhraseValue"].ToString());
            }
            return data;
        }
    
        public class LanguagePhraseDataReceive
        {
            public int ID { get; set; }
            public int languageID { get; set; }
            public int phraseID { get; set; }
            public string phraseValue { get; set; }
            
        }
        [HttpPost]
        public bool Post(LanguagePhraseDataReceive data)
        {
            if (data.ID == 0)
            {
                string strSQL = string.Format("CALL sp_LanguagePhrase_Insert({0},{1},{2},N'{3}')", data.ID, data.languageID, data.phraseID, data.phraseValue);
                return da.ExecuteSQL(strSQL);
            }
            else
            {
                string strSQL = string.Format("CALL sp_LanguagePhrase_Update({0},{1},{2},N'{3}')", data.ID, data.languageID, data.phraseID, data.phraseValue);
                return da.ExecuteSQL(strSQL);
            }
        }
        public void Delete(int id)
        {
            string strSQL = string.Format("CALL sp_LanguagePhrase_Delete({0})", id);
            da.ExecuteSQL(strSQL);
        }
   

    }
}
