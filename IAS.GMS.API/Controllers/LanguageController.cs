﻿using IAS.GMS.API.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace IAS.GMS.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class LanguageController : ApiController
    {
        
        private HttpRequest request = HttpContext.Current.Request;
        DataAccessLayer da = new DataAccessLayer();
        DataTable dt = new DataTable();
     
        // GET: /Language/
        //Create Json ListUser
        public JArray Get()
        {
            dt = da.GetDataSet("CALL sp_GetListLanguage").Tables[0];
            return da.ToJson(dt);
        }
        public class LanguageDataReceive
        {
            public int languageID { get; set; }
            public string languageName { get; set; }
            public string languageCode { get; set; }
            public bool isDefault { get; set; }
        }
         [HttpPost]
        public bool Post(LanguageDataReceive data)
        {
            if (data.languageID == 0)
            {
                string strSQL = string.Format("CALL sp_Language_Insert ({0},N'{1}','{2}',{3})", data.languageID, data.languageName, data.languageCode, data.isDefault);
                return da.ExecuteSQL(strSQL);
            }
            else
            {
                string strSQL = string.Format("CALL sp_Language_Update ({0},N'{1}','{2}',{3})", data.languageID, data.languageName, data.languageCode, data.isDefault);
                return da.ExecuteSQL(strSQL);
            }
        }
         // DELETE api/language/5
         public void Delete(int id)
         {
             string strSQL = string.Format("CALL sp_Language_Delete({0})", id);
             da.ExecuteSQL(strSQL);
         }
        


    }
}
