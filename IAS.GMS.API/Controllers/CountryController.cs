﻿using IAS.GMS.API.BookingService;
using IAS.GMS.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace IAS.GMS.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CountryController : ApiController
    {
        // Get Country List from service
        private Array GetCountryList()
        {
            CountryListResponse response = new CountryListResponse();
            try
            {
                // Get Country data from service
                var client = APIConnection.createBookingService();
                response = client.GetCountryList(null);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return response.Countries;
        }

        // GET api/contry
        public Array Get()
        {
            return GetCountryList();
        }

        // GET api/contry/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/contry
        public void Post([FromBody]string value)
        {
        }

        // PUT api/contry/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/paymentmethod/5
        public void Delete(int id)
        {
        }

    }
}
