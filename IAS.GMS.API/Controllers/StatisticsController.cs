﻿using IAS.GMS.API.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace IAS.GMS.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class StatisticsController : ApiController
    {
        private HttpRequest request = HttpContext.Current.Request;
        DataAccessLayer da = new DataAccessLayer();
        DataTable dt = new DataTable();
        public JArray Get()
        {
            dt = da.GetDataSet("CALL sp_Report").Tables[0];
            return da.ToJson(dt);
        }

        public JArray Get(DateTime dateFrom, DateTime dateTo)
        {
            string strSQL = string.Format("CALL sp_Report01('{0}','{1}')", dateFrom.ToString("yyyy/MM/dd HH:mm:ss"), dateTo.ToString("yyyy/MM/dd HH:mm:ss"));
            dt = da.GetDataSet(strSQL).Tables[0];
            return da.ToJson(dt);
        }

        public class LanguagePhraseDataReceive
        {
            public int ID { get; set; }
            public DateTime createdDate { get; set; }
            public int Booking { get; set; }
            public int Checkin { get; set; }
            public int flightStatus { get; set; }
            public int Member { get; set; }
            public int Inforamtion { get; set; }
            public int Language { get; set; }
            public int completeBooking { get; set; }
            public string userID { get; set; }
        }

        [HttpPost]
        public bool Post(LanguagePhraseDataReceive data)
        {
            string strSQL = string.Format("CALL sp_Statistics_Insert('{0}',{1},{2},{3},{4},{5},{6},{7},'{8}')", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), data.Booking, data.Checkin, data.flightStatus, data.Member, data.Inforamtion, data.Language, data.completeBooking, data.userID);
            return da.ExecuteSQL(strSQL);
        }
        public void Delete(int id)
        {
            
        }
    }
}
