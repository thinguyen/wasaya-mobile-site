﻿using IAS.GMS.API.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;


namespace IAS.GMS.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ContactController : ApiController
    {
        private HttpRequest request = HttpContext.Current.Request;
        DataAccessLayer da = new DataAccessLayer();
        DataTable dt = new DataTable();
        //
        // GET: /InformationPage/
        //Create Json Contact
        public JArray Get()
        {
            dt = da.GetDataSet("CALL sp_GetListContact()").Tables[0];
            return da.ToJson(dt);
        }
        
        public JArray Get(string languageCode)
        {
            string strSQL = string.Format("CALL sp_GetListContactByLanguageCode('{0}')", languageCode);
            dt = da.GetDataSet(strSQL).Tables[0];
            return da.ToJson(dt);
        }
        public class ContactDataReceive
        {
            public int contactID { get; set; }
            public string contactTitle { get; set; }
            public string languageCode { get; set; }
            public string contactPhone { get; set; }
            public string contactAddress { get; set; }
            public string contactfor { get; set; }
            public string contactContent { get; set; }
            public string contactLat { get; set; }
            public string contactLong { get; set; }
            public DateTime createOn { get; set; }
            public DateTime updateOn { get; set; }
            public string createBy { get; set; }
            public string updateBy { get; set; }
        }
        [HttpPost]
        public bool Post(ContactDataReceive data)
        {
            if (data.contactID == 0)
            {
                
                string strSQL = string.Format("CALL sp_Contact_Insert({0}, '{1}',N'{2}',N'{3}',N'{4}',N'{5}',N'{6}','{7}','{8}','{9}','{10}')",
                                                data.contactID, data.contactTitle, data.languageCode, data.contactPhone, data.contactfor, data.contactAddress, data.contactContent, data.contactLat, data.contactLong, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), data.createBy);
                return da.ExecuteSQL(strSQL);
            }
            else
            {
                string strSQL = string.Format("CALL sp_Contact_Update({0}, '{1}',N'{2}',N'{3}',N'{4}',N'{5}',N'{6}','{7}','{8}','{9}',N'{10}')",
                                               data.contactID, data.contactTitle, data.languageCode, data.contactPhone, data.contactfor, data.contactAddress, data.contactContent, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), data.updateBy, data.contactLat, data.contactLong);
                return da.ExecuteSQL(strSQL);
            }
        }
        public void Delete(int id)
        {
            string strSQL = string.Format("CALL sp_Contact_Delete ({0})",id);
            da.ExecuteSQL(strSQL);
        }
        

    }
}
