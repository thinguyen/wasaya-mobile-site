﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using IAS.GMS.API.CheckInService;
using IAS.GMS.API.Models;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Web;
using System.Web.Http.Cors;

namespace IAS.GMS.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CheckInController : ApiController
    {
        // GET api/checkin
        public string Get()
        {
            return null;
        }

        // GET api/checkin/1
        public string Get(int id)
        {
            return null;
        }
        public class CheckInDataReceive
        {
            public int reservationNumber { get; set; }
            public int legNumber { get; set; }
            public List<ReservationPassenger> passengers { get; set; }
            public string languageCode { get; set; }
        }
        [HttpPost]
        // POST api/checkin
        public CheckInResponse Post(CheckInDataReceive data)
        {
            var client = APIConnection.createCheckInService();

            CheckInRequest request = new CheckInRequest();

            //ReservationNumber

            request.ReservationNumber = data.reservationNumber;

            //LegNumber
            request.LegNumber = data.legNumber;

            //Array paxlist
            var passengers = data.passengers.ToArray();

            //paxlist length
            request.PaxList = new CheckInPax[passengers.Length];

            //query paxlist
            for (var i = 0; i < passengers.Length; i++)
            {
                //SeatAssignmentPax of pax list
                request.PaxList[i] = new CheckInPax();
                request.PaxList[i].Lastname = passengers[i].Lastname;
                request.PaxList[i].Firstname = passengers[i].Firstname;
                request.PaxList[i].PassportNumber = passengers[i].Passport.Number;
            }

            CheckInResponse respone = new CheckInResponse();
            respone = client.CheckIn(request);

            //query paxlist

            //paxlist length
            for (var i = 0; i < passengers.Length; i++)
            {
                //create email request for boadingpass
                EmailBoardingPassRequest requestEmail = new EmailBoardingPassRequest();
                requestEmail.ReservationNumber = data.reservationNumber;
                requestEmail.LegNumber = data.legNumber;
                requestEmail.PaxGroupID = passengers[i].PaxGroupID;
                requestEmail.Email = passengers[i].HomeContact.Email;
                requestEmail.LanguageCode = data.languageCode;
                //send email
                client.EmailBoardingPass(requestEmail);
            }


            return respone;
        }
        // PUT api/checkin/1
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/checkin/1
        public void Delete(int id)
        {
        }
    }
}

