﻿using IAS.GMS.API.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace IAS.GMS.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class FareRuleController : ApiController
    {

        private HttpRequest request = HttpContext.Current.Request;
        DataAccessLayer da = new DataAccessLayer();
        DataTable dt = new DataTable();
        //
        // GET: /InformationPage/
        //Create Json Infomation Page
        public JArray Get()
        {
            dt = da.GetDataSet("CALL sp_GetFareRuleList()").Tables[0];
            return da.ToJson(dt);
        }
        public JArray Get(string languageCode)
        {
            string strSQL = string.Format("CALL sp_GetListInformationPageByLanguageCode('{0}')", languageCode);
            dt = da.GetDataSet(strSQL).Tables[0];
            return da.ToJson(dt);
        }
        public JArray Get(string infoSlug, string languageCode, string languageName)
        {
            string strSQL = string.Format("CALL sp_GetInformationPageByInfoSlug('{0}')", infoSlug);
            dt = da.GetDataSet(strSQL).Tables[0];
            return da.ToJson(dt);
        }
        public JArray Get(string languageCode, string infoSlug)
        {
            string strSQL = string.Format("CALL sp_GetListInformationPageByInfoSlug('{0}', '{1}')", infoSlug, languageCode);
            dt = da.GetDataSet(strSQL).Tables[0];
            return da.ToJson(dt);
        }
        public class InfomationPageDataReceive
        {
            public int infoID { get; set; }
            public string languageCode { get; set; }
            public string infoSlug { get; set; }
            public string infoTitle { get; set; }
            public string infoDescription { get; set; }
            public string infoContent { get; set; }
            public DateTime createOn { get; set; }
            public DateTime updateOn { get; set; }
            public string createBy { get; set; }
            public string updateBy { get; set; }
        }
        [HttpPost]
        public bool Post(InfomationPageDataReceive data)
        {
            bool result = false;
            if (data.infoID == 0)
            {
                if (data.languageCode == null)
                {
                    DataTable dt = da.GetDataSet("CALL sp_GetListLanguage").Tables[0];

                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            string langguageCode = dr["LanguageCode"].ToString();
                            string strSQL = string.Format("CALL sp_InformationPage_Insert ({0},N'{1}',N'{2}',N'{3}','{4}','{5}','{6}',N'{7}','{8}')",
                                                                          data.infoID, data.infoTitle, data.infoDescription, data.infoContent, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), data.createBy, data.infoSlug, langguageCode, "FARERULE");
                            result = da.ExecuteSQL(strSQL);
                        }
                    }
                }
                else
                {
                    string strSQL = string.Format("CALL sp_InformationPage_Insert ({0},N'{1}',N'{2}',N'{3}','{4}','{5}','{6}',N'{7}','{8}')",
                                                                          data.infoID, data.infoTitle, data.infoDescription, data.infoContent, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), data.createBy, data.infoSlug, data.languageCode, "FARERULE");
                    result = da.ExecuteSQL(strSQL);
                }

                return result;
            }
            else
            {
                string strSQL = string.Format("CALL sp_InformationPage_Update ({0},N'{1}',N'{2}',N'{3}','{4}','{5}','{6}',N'{7}')",
                                                data.infoID, data.infoTitle, data.infoDescription, data.infoContent, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), data.updateBy, data.infoSlug, data.languageCode);
                return da.ExecuteSQL(strSQL);
            }
        }
        public void Delete(int id)
        {
            string strSQL = string.Format("CALL sp_InformationPage_Delete ({0})", id);
            da.ExecuteSQL(strSQL);
        }


    }
}
