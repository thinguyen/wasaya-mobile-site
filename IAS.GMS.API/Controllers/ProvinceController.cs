﻿using IAS.GMS.API.BookingService;
using IAS.GMS.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace IAS.GMS.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ProvinceController : ApiController
    {

        // GET api/province
        public Array Get()
        {
            HttpRequest httpRequest = HttpContext.Current.Request;

            if (httpRequest.QueryString != null)
            {
                // Get Province data from service
                var client = APIConnection.createBookingService();
                ProvinceListRequest request = new ProvinceListRequest();
                request.CountryCode = httpRequest.QueryString["countryCode"];
                ProvinceListResponse response = new ProvinceListResponse();
                response = client.GetProvinceList(request);
                return response.Provinces;
            }
            else
            { 
                return  null;
            }
        }
        // POST api/province
        public void Post([FromBody]string value)
        {
        }

        // PUT api/province/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/province/5
        public void Delete(int id)
        {
        }
    }
}
