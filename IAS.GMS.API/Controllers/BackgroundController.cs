﻿using IAS.GMS.API.BookingService;
using IAS.GMS.API.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Mvc;

namespace IAS.GMS.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class BackgroundController : ApiController
    {
        DataAccessLayer da = new DataAccessLayer();
        DataTable dt = new DataTable();
        public JArray Get()
        {
            string strSQL = "SELECT ConfigCode as code, ConfigData as url, ConfigDescription as link FROM tbl_Configuration WHERE ConfigGroup='BACKGROUND';";
            dt = da.GetDataSet(strSQL).Tables[0];
            return da.ToJson(dt);
        }
    }
}
