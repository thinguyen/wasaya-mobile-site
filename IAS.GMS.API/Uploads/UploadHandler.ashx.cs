﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Cors;

namespace IAS.GMS.API.Uploads
{
    /// <summary>
    /// Summary description for UploadHandler
    /// </summary>
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UploadHandler : IHttpHandler
    {
        
        public void ProcessRequest(HttpContext context)
        {
            context.Response.AddHeader("Pragma", "no-cache");
            context.Response.AddHeader("Cache-Control", "private, no-cache");
            context.Response.AddHeader("Access-Control-Allow-Headers", "X-File-Name,X-File-Type,X-File-Size");
            context.Response.AddHeader("Access-Control-Allow-Origin", "*");

            HttpFileCollection files = context.Request.Files;
            for (int i = 0; i < files.Count; i++)
            {
                HttpPostedFile file = files[i];
                string fname = context.Server.MapPath("Uploads\\" + file.FileName);
                file.SaveAs(fname);
            }
         
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
