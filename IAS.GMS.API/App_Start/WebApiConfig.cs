﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace IAS.GMS.API
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            //enable coross origin resoucre sharing
            config.EnableCors();

            config.Routes.MapHttpRoute(
                name: "ActiontApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            // Remove XML media type support at all to make API return JSON
            var appXmlType = config.Formatters.XmlFormatter.SupportedMediaTypes.FirstOrDefault(t => t.MediaType == "application/xml");
            config.Formatters.XmlFormatter.SupportedMediaTypes.Remove(appXmlType);
        }
    }
}
