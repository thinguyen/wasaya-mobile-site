'use strict';

module.exports = function(grunt) {
	/* Grunt task configurations */
    grunt.initConfig ({
        /* Load project information */ 
        pkg: grunt.file.readJSON('package.json'),
        /* Merge css-js file task */
        concat: {
            /*css : {
				src:'',
				dest:'',
			},*/
            js	: {
                src: ['app/modules/*.js', 'app/modules/**/*.js'],
                dest: 'app/js/<%= pkg.name %>-<%= pkg.version %>.js'
            }
        },
        html2js: {
            all: {
                src: 'app/modules/**/*.html',
                dest: 'app/js/<%= pkg.name %>-<%= pkg.version %>.templates.js'
            },
            options: {
                base: 'app/modules/',
                module: 'gms-templates',
                singleModule: true
            }
        },
        /* Auto update when a file is changed */
        watch: {
            'js.all': {
                files: '<%= concat.js.src %>',
                tasks: ['concat:js']
            },
            'html.all': {
                files: '<%= html2js.all.src %>',
                tasks: ['html2js:all']
            }
        }
	});
	
	/* Load neccessary grunt npm tasks */ 
	require('jit-grunt')(grunt);
	
	/* Run grunt task */
	grunt.registerTask('build', ['concat', 'html2js', 'watch']);
	grunt.registerTask('default', ['concat', 'html2js', 'watch']);
}