﻿/// <reference path="../../../Scripts/typings/angularjs/angular.d.ts"/>
module UserProfile {
    function resetUserProfile(sessionStorage) {
        delete sessionStorage.isLogin;
        delete sessionStorage.userProfile;
        delete sessionStorage.setReservationNumberDetail;
    }
    //Home User
    class UserController {
        constructor($scope, $state, $location, $sessionStorage) {
            $scope.state = $state.current.name;
            if ($sessionStorage.isLogin != true) {
                $location.path('/login');
            }
            $scope.logout = function () {
                resetUserProfile($sessionStorage);
                $location.path('/login');
            };
        }
    }

    //Register
    class RegisterController {
        constructor($scope, $state, $location, $sessionStorage, $translate, $modal, $sce, FareRule, UserProfile, Province, Country, Statistics) {
            $scope.state = $state.current.name;
            //profile
            Statistics.save({
                Booking: 0,
                Checkin: 0,
                flightStatus: 0,
                Member: 1,
                Inforamtion: 0,
                Language: 0,
                completeBooking: 0,
            }, function (data) {
                })
            if ($sessionStorage.isLogin == true || $sessionStorage.isLogin != undefined) {
                $location.path('/user');
            }
            $scope.countryResult = Country.query();

            $scope.changeCountry = function () {
                $scope.provinceResult = Province.query({ countryCode: $scope.userCountry }, function () {
                    $scope.isProvince = true;
                });
            };
            var myInfomationModal = $modal({ scope: $scope, template: 'app/modal/information.html', show: false });
            $scope.closeInformation = function () {
                myInfomationModal.hide();
            };
            $scope.termsConditionsHelp = function () {
                FareRule.query({
                    languagecode: $sessionStorage.languageCode,
                    infoSlug: 'terms_conditions_register'
                }, function (data) {
                        $scope.informationTitle = data[0].InfoTitle;
                        $scope.informationContent = $sce.trustAsHtml(data[0].InfoContent);
                        myInfomationModal.show();
                    });
            }
            $scope.submitCreateAccount = function () {
                if ($scope.userEmail == undefined) {
                    //alert("Please enter your email address");
                    $translate(['DATA_MESSAGE_EMAIL_ADDRESS']).then(function (translation) {
                        alert(translation.DATA_MESSAGE_EMAIL_ADDRESS);
                    });
                    return;
                }
                if ($scope.userPassword == undefined) {
                    //alert("Please enter your password");
                    $translate(['DATA_MESSAGE_PASSWORD']).then(function (translation) {
                        alert(translation.DATA_MESSAGE_PASSWORD);
                    });
                    return;
                }
                if ($scope.userVerifyPassword == undefined) {
                    //alert("Please enter your verify password");
                    $translate(['DATA_YOUR_VERIFY_PASSWORD']).then(function (translation) {
                        alert(translation.DATA_YOUR_VERIFY_PASSWORD);
                    });
                    return;
                }
                if ($scope.userVerifyPassword != $scope.userPassword) {
                    //alert("These password don’t match. Try again?");
                    $translate(['DATA_MESSAGE_PASSWORD_DO_NOT_MATCH']).then(function (translation) {
                        alert(translation.DATA_MESSAGE_PASSWORD_DO_NOT_MATCH);
                    });
                    return;
                }
                if ($scope.userQuestion == undefined) {
                    //alert("Please enter a question");
                    $translate(['DATA_MESSAGE_QUESTION']).then(function (translation) {
                        alert(translation.DATA_MESSAGE_QUESTION);
                    });
                    return;
                }
                if ($scope.userAnswer == undefined) {
                    //alert("Please enter your answer");
                    $translate(['DATA_MESSAGE_YOUR_ANSWER']).then(function (translation) {
                        alert(translation.DATA_MESSAGE_YOUR_ANSWER);
                    });
                    return;
                }
                if ($scope.userTitle == undefined) {
                    //alert("Please select a title");
                    $translate(['DATA_MESSAGE_SELECT_TITLE']).then(function (translation) {
                        alert(translation.DATA_MESSAGE_SELECT_TITLE);
                    });
                    return;
                }
                if ($scope.userFirstName == undefined) {
                    //alert("Please enter your first name");
                    $translate(['DATA_MESSAGE_FIRST_NAME']).then(function (translation) {
                        alert(translation.DATA_MESSAGE_FIRST_NAME);
                    });
                    return;
                }
                if ($scope.userLastName == undefined) {
                    //alert("Please enter your last name");
                    $translate(['DATA_MESSAGE_LAST_NAME']).then(function (translation) {
                        alert(translation.DATA_MESSAGE_LAST_NAME);
                    });
                    return;
                }
                if ($scope.userBirthday == undefined) {
                    //alert("Please select your date of birth");
                    $translate(['DATA_MESSAGE_DATE_OF_BIRHT']).then(function (translation) {
                        alert(translation.DATA_MESSAGE_DATE_OF_BIRHT);
                    });
                    return;
                }
                if ($scope.userMobile == undefined) {
                    //alert("Please enter your phone number");
                    $translate(['DATA_MESSAGE_ENTER_PHONE_NUMBER']).then(function (translation) {
                        alert(translation.DATA_MESSAGE_ENTER_PHONE_NUMBER);
                    });
                    return;
                }
                if ($scope.userPasport == undefined) {
                    //alert("Please enter your passport number");
                    $translate(['DATA_MESSAGE_YOUR_PASSPORT_NUMBER']).then(function (translation) {
                        alert(translation.DATA_MESSAGE_YOUR_PASSPORT_NUMBER);
                    });
                    return;
                }
                if ($scope.userAddress == undefined) {
                    //alert("Please enter your address");
                    $translate(['DATA_MESSAGE_ENTER_YOUR_ADDRESS']).then(function (translation) {
                        alert(translation.DATA_MESSAGE_ENTER_YOUR_ADDRESS);
                    });
                    return;
                }
                if ($scope.userCountry == undefined) {
                    //alert("Please select your country");
                    $translate(['DATA_MESSAGE_YOUR_COUNTRY']).then(function (translation) {
                        alert(translation.DATA_MESSAGE_YOUR_COUNTRY);
                    });
                    return;
                }
                if ($scope.userCity == undefined) {
                    //alert("Please select your province/state");
                    $translate(['DATA_MESSAGE_YOUR_PROVINCE_STATE']).then(function (translation) {
                        alert(translation.DATA_MESSAGE_YOUR_PROVINCE_STATE);
                    });
                    return;
                }
                if ($scope.registerForm.$invalid) {
                    $scope.$broadcast('show-errors-check-validity');
                    //alert("Please fill in all information of user register form");
                    $translate(['DATA_MESSAGE_FILL_ALL_INFORMATION_OF_USER_REGISTER']).then(function (translation) {
                        alert(translation.DATA_MESSAGE_FILL_ALL_INFORMATION_OF_USER_REGISTER);
                    });
                    return;
                }

                var gender = ($scope.userTitle == "Mr") ? "Male" : "Female";
                var paxProfile = {
                    gender: gender,
                    isActive: true,
                    firstname: $scope.userFirstName,
                    lastname: $scope.userLastName,
                    email: $scope.userEmail,
                    dateOfBirth: $scope.userBirthday,
                    homeTel: $scope.userMobile,
                    passportNumber: $scope.userPasport,
                    addr1: $scope.userAddress,
                    countryCode: $scope.userCountry,
                    provinceCode: $scope.userCity
                };

                var data_profile = {
                    email: $scope.userEmail,
                    profileName: $scope.userEmail,
                    password: $scope.userPassword,
                    isActive: true,
                    securityQuestion: $scope.userQuestion,
                    securityAnswer: $scope.userAnswer,
                    paxProfile: paxProfile
                };
                var loadingModal = $modal({
                    scope: $scope,
                    template: 'app/modal/loading.html',
                    show: true,
                    backdrop: 'static'
                });
                $scope.reservationResponse = UserProfile.save({
                    isAddUser: true,
                    profile: data_profile
                }, function (data) {
                        if (data.OperationSucceeded) {
                            $sessionStorage.userProfile = data.UserProfile;
                            $sessionStorage.isLogin = true;
                            loadingModal.hide();
                            $location.path('/accountcreated');
                        } else {
                            if (data.OperationMessage == "Specified cast is not valid.") {
                                //alert("This email already exists. Try another.");
                                $translate(['DATA_MESSAGE_EMAIL_ALREADY_EXISTS']).then(function (translation) {
                                    alert(translation.DATA_MESSAGE_EMAIL_ALREADY_EXISTS);
                                });
                                loadingModal.hide();
                            }
                            else {
                                alert(data.OperationMessage);
                                loadingModal.hide();
                            }
                            return false;
                        }
                    });
            };
        }
    }
    //created account
    class AccountCreatedController {
        constructor($scope, $state, $location, $sessionStorage, $translate, $sce) {
            $scope.state = $state.current.name;
            $translate('DATA_WELCOME').then(function (translation) {
                $scope.headdingWellcom = $sce.trustAsHtml(translation);
            });
            $translate('DATA_MESSAGE_ENJOIN_OUR_SERVICES').then(function (translation) {
                $scope.headdingContent = $sce.trustAsHtml(translation);
            });
            $translate('DATA_MESSAGE_SENT_YOU_EMAIL_TO_VERIFY').then(function (translation) {
                $scope.contentNote = $sce.trustAsHtml(translation);
            });
            $scope.toHome = function () {
                $location.path('/user');
            };
        }
    }

    // Login
    class LoginController {
        constructor($scope, $state, $location, $translate, $sessionStorage, UserProfile) {
            $scope.state = $state.current.name;
            if ($sessionStorage.isLogin == true) {
                $location.path('/user');
            }
            $scope.login = function () {
                if ($scope.userName == undefined) {
                    //alert("Please enter your email address");
                    $translate(['DATA_MESSAGE_EMAIL_ADDRESS']).then(function (translation) {
                        alert(translation.DATA_MESSAGE_EMAIL_ADDRESS);
                    });
                    return;
                }
                if ($scope.userPassword == undefined) {
                    //alert("Please enter your password");
                    $translate(['DATA_MESSAGE_PASSWORD']).then(function (translation) {
                        alert(translation.DATA_MESSAGE_PASSWORD);
                    });
                    return;
                }
                if ($scope.registerForm.$invalid) {
                    $scope.$broadcast('show-errors-check-validity');
                    //alert("Please fill in all field");
                    $translate(['DATA_MESSAGE_FILL_ALL_FIELD']).then(function (translation) {
                        alert(translation.DATA_MESSAGE_FILL_ALL_FIELD);
                    });
                    return;
                }

                var userProfile = UserProfile.get({
                    profileEmail: $scope.userName,
                    profilePassword: $scope.userPassword
                }, function (data) {
                        if (data.OperationSucceeded) {
                            $sessionStorage.userProfile = data.UserProfile;
                            $sessionStorage.isLogin = true;
                            $location.path('/user');
                        } else {
                            //alert("The email or password you entered is incorrect");
                            $translate(['DATA_MESSAGE_EMAIL_OR_PASSWORD_INCORRECT']).then(function (translation) {
                                alert(translation.DATA_MESSAGE_EMAIL_OR_PASSWORD_INCORRECT);
                            });
                            return false;
                        }
                    });
            };
        }
    }

    // Forgot password
    class ForgotPasswordController {
        constructor($scope, $state, $location) {
            $scope.state = $state.current.name;

        }
    }

    // Change Password
    class ChangePasswordController {
        constructor($scope, $state, $location) {

        }
    }

    // Update Profile
    class UpdateProfileController {
        constructor($scope, $state, $location, $modal, $translate, $sessionStorage, Country, Province, UserProfile) {
            $scope.state = $state.current.name;
            if ($sessionStorage.isLogin) {
                var userProfile = $sessionStorage.userProfile.PaxProfile;
                var gender = "";
                switch (userProfile.Gender) {
                    case 0:
                        gender = "Mr";
                        break;
                    case 1:
                        gender = "Ms";
                        break;
                }
                $scope.isProvince = true;
                $scope.provinceResult = Province.query({ countryCode: userProfile.CountryCode }, function () {
                    $scope.isProvince = true;
                });
                $scope.userTitle = gender;
                $scope.userFirstName = userProfile.Firstname;
                $scope.userLastName = userProfile.Lastname;
                $scope.userEmail = userProfile.Email;
                $scope.userBirthday = userProfile.DateOfBirth;
                $scope.userMobile = userProfile.HomeTel;
                $scope.userPasport = userProfile.PassportNumber;
                $scope.userAddress = userProfile.Addr1;
                $scope.userCountry = userProfile.CountryCode;
                $scope.userCity = userProfile.ProvinceCode;

                //profile
                $scope.countryResult = Country.query();

                $scope.changeCountry = function () {
                    $scope.provinceResult = Province.query({ countryCode: $scope.userCountry }, function () {
                        $scope.isProvince = true;
                    });
                };
                $scope.submitUpdateProfile = function () {
                    if ($scope.userTitle == "") {
                        //alert("Please select a title");
                        $translate(['DATA_MESSAGE_SELECT_TITLE']).then(function (translation) {
                            alert(translation.DATA_MESSAGE_SELECT_TITLE);
                        });
                        return;
                    }
                    if ($scope.userFirstName == "") {
                        //alert("Please enter your first name");
                        $translate(['DATA_MESSAGE_FIRST_NAME']).then(function (translation) {
                            alert(translation.DATA_MESSAGE_FIRST_NAME);
                        });
                        return;
                    }
                    if ($scope.userLastName == "") {
                        //alert("Please enter your last name");
                        $translate(['DATA_MESSAGE_LAST_NAME']).then(function (translation) {
                            alert(translation.DATA_MESSAGE_LAST_NAME);
                        });
                        return;
                    }
                    if ($scope.userBirthday == "") {
                        //alert("Please select your date of birth");
                        $translate(['DATA_MESSAGE_DATE_OF_BIRHT']).then(function (translation) {
                            alert(translation.DATA_MESSAGE_DATE_OF_BIRHT);
                        });
                        return;
                    }
                    if ($scope.userMobile == "") {
                        //alert("Please enter your phone number");
                        $translate(['DATA_MESSAGE_ENTER_PHONE_NUMBER']).then(function (translation) {
                            alert(translation.DATA_MESSAGE_ENTER_PHONE_NUMBER);
                        });
                        return;
                    }
                    if ($scope.userPasport == "") {
                        //alert("Please enter your passport number");
                        $translate(['DATA_MESSAGE_YOUR_PASSPORT_NUMBER']).then(function (translation) {
                            alert(translation.DATA_MESSAGE_YOUR_PASSPORT_NUMBER);
                        });
                        return;
                    }
                    if ($scope.userAddress == "") {
                        //alert("Please enter your address");
                        $translate(['DATA_MESSAGE_ENTER_YOUR_ADDRESS']).then(function (translation) {
                            alert(translation.DATA_MESSAGE_ENTER_YOUR_ADDRESS);
                        });
                        return;
                    }
                    if ($scope.userCountry == "") {
                        //alert("Please select your country");
                        $translate(['DATA_MESSAGE_YOUR_COUNTRY']).then(function (translation) {
                            alert(translation.DATA_MESSAGE_YOUR_COUNTRY);
                        });
                        return;
                    }
                    if ($scope.userCity == "") {
                        //alert("Please select your province/state");
                        $translate(['DATA_MESSAGE_YOUR_PROVINCE_STATE']).then(function (translation) {
                            alert(translation.DATA_MESSAGE_YOUR_PROVINCE_STATE);
                        });
                        return;
                    }
                    if ($scope.registerForm.$invalid) {
                        $scope.$broadcast('show-errors-check-validity');
                        //alert("Please fill in all information of user register form");
                        $translate(['DATA_MESSAGE_FILL_ALL_INFORMATION_OF_USER_REGISTER']).then(function (translation) {
                            alert(translation.DATA_MESSAGE_FILL_ALL_INFORMATION_OF_USER_REGISTER);
                        });
                        return;
                    }

                    var gender = ($scope.userTitle == "Mr") ? "0" : "1";
                    var paxProfile = {
                        gender: gender,
                        firstname: $scope.userFirstName,
                        lastname: $scope.userLastName,
                        email: $scope.userEmail,
                        dateOfBirth: $scope.userBirthday,
                        homeTel: $scope.userMobile,
                        passportNumber: $scope.userPasport,
                        addr1: $scope.userAddress,
                        countryCode: $scope.userCountry,
                        provinceCode: $scope.userCity,
                        Addr2: $sessionStorage.userProfile.PaxProfile.Addr2,
                        AeroplanNumber: $sessionStorage.userProfile.PaxProfile.AeroplanNumber,
                        AgeCategory: $sessionStorage.userProfile.PaxProfile.AgeCategory,
                        BusExt: $sessionStorage.userProfile.PaxProfile.BusExt,
                        BusFax: $sessionStorage.userProfile.PaxProfile.BusFax,
                        BusTel: $sessionStorage.userProfile.PaxProfile.BusTel,
                        CategoryID: $sessionStorage.userProfile.PaxProfile.CategoryID,
                        City: $sessionStorage.userProfile.PaxProfile.City,
                        //CountryID: $sessionStorage.userProfile.PaxProfile.CountryID,
                        EmployeeNumber: $sessionStorage.userProfile.PaxProfile.EmployeeNumber,
                        GeneralNumber1: $sessionStorage.userProfile.PaxProfile.GeneralNumber1,
                        GeneralNumber2: $sessionStorage.userProfile.PaxProfile.GeneralNumber2,
                        HomeFax: $sessionStorage.userProfile.PaxProfile.HomeFax,
                        IsActive: $sessionStorage.userProfile.PaxProfile.IsActive,
                        LoyaltyPoints: $sessionStorage.userProfile.PaxProfile.LoyaltyPoints,
                        Middlename: $sessionStorage.userProfile.PaxProfile.Middlename,
                        Nationality: $sessionStorage.userProfile.PaxProfile.Nationality,
                        Notes: $sessionStorage.userProfile.PaxProfile.Notes,
                        PassportCountryCode: $sessionStorage.userProfile.PaxProfile.PassportCountryCode,
                        PassportExires: $sessionStorage.userProfile.PaxProfile.PassportExires,
                        PassportIssuedCity: $sessionStorage.userProfile.PaxProfile.PassportIssuedCity,
                        PassportIssuedDate: $sessionStorage.userProfile.PaxProfile.PassportIssuedDate,
                        PaxID: $sessionStorage.userProfile.PaxProfile.PaxID,
                        PostalCode: $sessionStorage.userProfile.PaxProfile.PostalCode,
                        PreBoard: $sessionStorage.userProfile.PaxProfile.PreBoard,
                        //ProvinceID: $sessionStorage.userProfile.PaxProfile.ProvinceID,
                        SpecialNeeds: $sessionStorage.userProfile.PaxProfile.SpecialNeeds,
                    };

                    var data_profile = {
                        email: $scope.userEmail,
                        profileName: $sessionStorage.userProfile.ProfileName,
                        password: $sessionStorage.userProfile.Password,
                        isActive: $sessionStorage.userProfile.IsActive,
                        iiVIP: $sessionStorage.userProfile.IsVIP,
                        departureAirportID: $sessionStorage.userProfile.DepartureAirportID,
                        arrivalAirportID: $sessionStorage.userProfile.ArrivalAirportID,
                        securityQuestion: $sessionStorage.userProfile.SecurityQuestion,
                        securityAnswer: $sessionStorage.userProfile.SecurityAnswer,
                        userProfileID: $sessionStorage.userProfile.UserProfileID,
                        paxProfile: paxProfile
                    };
                    var loadingModal = $modal({
                        scope: $scope,
                        template: 'app/modal/loading.html',
                        show: true,
                        backdrop: 'static'
                    });

                    $scope.reservationResponse = UserProfile.save({
                        isAddUser: false,
                        profile: data_profile
                    }, function (data) {
                            if (data.OperationSucceeded) {
                                $sessionStorage.userProfile = data.UserProfile;
                                loadingModal.hide();
                                $location.path('/user');
                            } else {
                                alert(data.OperationMessage);
                                return false;
                            }
                        });
                };
            }
        }
    }



    // Reservation History
    class ReservationHistoryController {
        constructor($scope, $state, $location, $sessionStorage, $modal, UserProfileReservation) {
            $scope.state = $state.current.name;
            var loadingModal = $modal({ scope: $scope, template: 'app/modal/loading.html', show: true });
            UserProfileReservation.get({ userProfileID: $sessionStorage.userProfile.UserProfileID },
                function (data) {
                    $scope.reservationHistoryResults = data.Results;
                    loadingModal.hide();
                });
            $scope.viewReservationDetail = function (reservationNumber) {
                $sessionStorage.setReservationNumberDetail = reservationNumber;
                $location.path('/reservationdetail');
            };
        }
    }

    // Reservation History Detail
    class ReservationHistoryDetailController {
        constructor($scope, $state, $location, $sessionStorage, $modal, $sce, ReservationInvoice, FareRule) {
            //set current name to state
            $scope.state = $state.current.name;
            if (!$sessionStorage.setReservationNumberDetail) {
                $location.path('/reservationhistory');
            }
            var myAirportModal = $modal({ scope: $scope, template: 'app/modal/airport.html', show: false });
            $scope.loadAirportDetail = function (airportCode, airportName) {
                $scope.airportCode = airportCode;
                $scope.airportName = airportName;
                myAirportModal.show();
            };
            $scope.closeAirport = function () {
                myAirportModal.hide();
            };

            var loadingModal = $modal({ scope: $scope, template: 'app/modal/loading.html', show: true });

            function caculatorSegment(legOption) {
                legOption.SegmentsTemp = [];
                angular.copy(legOption.Segments, legOption.SegmentsTemp);
                var airCraftIndex = 0;
                var countStop = 1;

                if (legOption.SegmentsTemp.length == 1) {
                    var segment = legOption.SegmentsTemp;
                    segment[0].TimeTransit = "";
                    segment[0].checkAirCraft = false;
                } else {
                    legOption.SegmentsTemp[0].TimeTransit = "";
                    legOption.SegmentsTemp[0].checkAirCraft = false;
                    for (var j = 1; j < legOption.SegmentsTemp.length; j++) {
                        var segment = legOption.SegmentsTemp;
                        if (segment[j].AircraftName == segment[airCraftIndex].AircraftName
                            && segment[j].Flight == segment[airCraftIndex].Flight) {
                            segment[j].checkAirCraft = true;
                            var timeeta = segment[j - 1].ArrivalLocal;
                            var timeetd = segment[j].DepartureLocal;
                            var totalMinutes = moment(timeetd).diff(timeeta, 'minutes');
                            var hours = Math.floor(totalMinutes / 60);
                            var minutes = totalMinutes % 60;
                            segment[j].TimeTransit = hours + 'h ' + minutes + 'm';
                            legOption.SegmentsTemp[0].ArrivalLocal = segment[j].ArrivalLocal;
                            legOption.SegmentsTemp[0].ArrivalAirportCode = segment[j].ArrivalAirportCode;
                            legOption.SegmentsTemp[0].ArrivalAirportName = segment[j].ArrivalAirportName;
                            segment[j].countStop = countStop;
                            countStop++;
                            airCraftIndex = j;
                        } else {
                            segment[j].checkAirCraft = false;
                            airCraftIndex = j;
                        }
                    }
                }
            }

            $scope.reservationInvoice = ReservationInvoice.get({
                reservationNumber: $sessionStorage.setReservationNumberDetail,
            }, function (data) {
                    loadingModal.hide();
                    try {
                        $scope.vaildResevation = true;
                        var reservationInvoiceResult = data;
                        var isCheckin = false;
                        var isPayed = false;
                        var countNoInfantNoCheckin = false;
                        for (var i = 0; i < reservationInvoiceResult.Legs.length; i++) {
                            var leg = reservationInvoiceResult.Legs[i];
                            var arrivalAirportName_ = "";
                            for (var j = 0; j < leg.Segments.length; j++) {
                                var segment = leg.Segments;
                                arrivalAirportName_ = segment[j].ArrivalAirportName;
                            }
                            leg.DepartureAirportName = leg.Segments[0].DepartureAirportName;
                            leg.ArrivalAirportName = arrivalAirportName_;

                            caculatorSegment(leg);

                            var now = moment();
                            var etdofLeg = moment(leg.Segments[0].DepartureLocal);
                            var totalHourToETD = -(moment(now).diff(etdofLeg, 'hour'));

                            var statusClass = "";
                            var status = "";
                            if (reservationInvoiceResult.ResStatus.IsCancelled == true) {
                                status = "Cancelled";
                                statusClass = "closed";
                            } else if (totalHourToETD <= 0) {
                                status = "Closed";
                                statusClass = "closed";
                            } else if (totalHourToETD > 65) {
                                status = "Waiting";
                                statusClass = "waiting";
                            } else {
                                var allChecked = true;
                                for (var k = 0; k < leg.Segments[0].PaxList.length; k++) {
                                    var pax = leg.Segments[0].PaxList[k];
                                    for (var m = 0; m < reservationInvoiceResult.Passengers.length; m++) {
                                        var passenger = reservationInvoiceResult.Passengers[m];
                                        if (pax.PaxGroupID == passenger.PaxGroupID) {
                                            if (pax.CheckinStatus == 'Not Checked-in' && passenger.Infants.length != 0) {
                                                countNoInfantNoCheckin = true;
                                                leg.countNoInfantNoCheckin = true;
                                            } else if (pax.CheckinStatus == 'Not Checked-in' && passenger.Infants.length == 0) {
                                                allChecked = false;
                                                leg.countNoInfantNoCheckin = false;
                                                break;
                                            }
                                        }
                                    }
                                }
                                if (allChecked == false || countNoInfantNoCheckin == true) {
                                    status = "Check In";
                                    statusClass = "checkin";
                                } else {
                                    status = "Checked In";
                                    statusClass = "checkedin";
                                }

                            }
                            leg.FlightNumberofLeg = i + 1;
                            leg.Status = status;
                            leg.StatusClass = statusClass;
                            if (statusClass == "checkin") {
                                isCheckin = true;
                            }
                        }
                    } catch (err) {
                        $scope.vaildResevation = false;
                    }
                    /*
                    ● Check In: If the difference of the ETD and the current time is in the allowed time for Check In.
                    ● Checked In: If all passengers have checked in before (not count passengers with infant).
                    ● Waiting: the time for check in is not ready yet.
                    ● Closed: the flight was departed.
                    */
                    $scope.isCheckin = isCheckin;
                    $scope.s2Legs = reservationInvoiceResult.Legs;
                    $scope.reservationInvoiceResult = reservationInvoiceResult;
                    $scope.checkinStep2DataTotal = reservationInvoiceResult;
                    $sessionStorage.flightstatusStep2DataTotal = reservationInvoiceResult;
                }, function (respone) {
                    loadingModal.hide();
                    $scope.vaildResevation = false;
                });
            var myInfomationModal_ = $modal({ scope: $scope, template: 'app/modal/information.html', show: false });
            $scope.loadFare = function (fareClass) {
                FareRule.query({
                    languagecode: $sessionStorage.languageCode,
                    infoSlug: fareClass
                }, function (data) {
                        if (data.length > 0) {
                            $scope.informationTitle = data[0].InfoTitle;
                            $scope.informationContent = $sce.trustAsHtml(data[0].InfoContent);
                        } else {
                            $scope.informationTitle = fareClass;
                            $scope.informationContent = $sce.trustAsHtml(fareClass);
                        }
                        $scope.classname = "fareclass";
                        myInfomationModal_.show();
                    });
            }
            $scope.closeInformation = function () {
                myInfomationModal_.hide();
            };
            $scope.goBack = function () {
                delete $sessionStorage.setReservationNumberDetail;
                $location.path('/reservationhistory');
            };
        }
    }

    function FareRuleFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/farerule");
    }
    function CountryFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/country");
    }

    function ProvinceFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/province");
    }

    function UserProfileFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/userprofile");
    }
    function UserProfileReservationFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/userprofilereservation");
    }
    function StatisticsFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/Statistics");
    }

    // Register controllers to angular 
    angular.module('gms')
        .controller('UserController', UserController)
        .controller('RegisterController', RegisterController)
        .controller('AccountCreatedController', AccountCreatedController)
        .controller('LoginController', LoginController)
        .controller('ForgotPasswordController', ForgotPasswordController)
        .controller('ChangePasswordController', ChangePasswordController)
        .controller('UpdateProfileController', UpdateProfileController)
        .controller('ReservationHistoryController', ReservationHistoryController)
        .controller('ReservationHistoryDetailController', ReservationHistoryDetailController)
        .factory('Statistics', StatisticsFactory)
        .factory('FareRule', FareRuleFactory)
        .factory('Country', CountryFactory)
        .factory('Province', ProvinceFactory)
        .factory('UserProfile', UserProfileFactory)
        .factory('UserProfileReservation', UserProfileReservationFactory);

}