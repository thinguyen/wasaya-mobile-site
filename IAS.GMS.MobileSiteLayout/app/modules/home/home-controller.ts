﻿/// <reference path="../../../Scripts/typings/angularjs/angular.d.ts"/>
module Home {
    // Home Controller
    class HomeController {
        constructor($scope, $state, $location, $translate, $filter, $sessionStorage, Language) {
            $scope.state = $state.current.name;
            if ($sessionStorage.defaultLanguage == undefined) {
                Language.query({}, function (data) {
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].IsDefault == true) {
                            $translate.use(data[i].LanguageCode);
                            $sessionStorage.defaultLanguage = data[i].LanguageCode;
                            $sessionStorage.languageCode = data[i].LanguageCode;
                        }
                    }
                });
            }
            //$translate.use('vi');
            $scope.isLogin = $sessionStorage.isLogin;
        }
    }

    function LanguageFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/language");
    }
    // Register controllers to angular module
    angular.module('gms')
        .controller('HomeController', HomeController)
        .factory('Language', LanguageFactory);
}