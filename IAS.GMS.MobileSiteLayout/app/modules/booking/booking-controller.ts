/// <reference path="../../../Scripts/typings/angularjs/angular.d.ts"/>
/// <reference path="../../../Scripts/typings/moment/moment.d.ts"/>

module Booking {
    function resetBooking(sessionStorage) {
        delete sessionStorage.step;
        delete sessionStorage.s1FromCode;
        delete sessionStorage.s1FromName;
        delete sessionStorage.step1;
        delete sessionStorage.s1ToCode;
        delete sessionStorage.s1ToName;
        delete sessionStorage.promoCode;
        delete sessionStorage.step2;
        delete sessionStorage.step3;
        delete sessionStorage.step4;
        delete sessionStorage.step4AddonDepart;
        delete sessionStorage.step4AddonArrival;
        delete sessionStorage.totalAmount;
        delete sessionStorage.step6;
        delete sessionStorage.step7;
    }
    // Infomation Controller
    function BookingFlightController($sessionStorage, $scope, $state, $location) {
        $scope.state = $state.current.name;
        var step = $sessionStorage.step;
        if (typeof (step) != "undefined" && step !== null) {
            // $location.path('/' + step);
            $location.path('/step1');
        } else {
            $location.path('/step1');
        }
    }
    function isFloat(n) {
        return n != "" && !isNaN(n) && Math.round(n) != n;
    }
    function Step1Controller($sessionStorage, $scope, $window, $state, $location, $timeout, $translate, $modal, AirportList) {
        //set current name to state
        $scope.state = $state.current.name;
        //$sessionStorage.$reset();
        resetBooking($sessionStorage);
        //set current step to session
        $sessionStorage.step = "step1";

        //set origin and destination null when start
        $scope.optionOrigin = null;
        $scope.optionDestination = null;
        //loading
        var loadingModal = $modal({
            scope: $scope,
            template: 'app/modal/loading.html',
            show: true,
            backdrop: 'static'
        });
        //load origin list from service
        AirportList.query(function (data) {
            $scope.originResult = data;
            loadingModal.hide();
        });

        //when origin change call function to load destination
        $scope.loadDestination = function () {
            if (typeof ($scope.optionOrigin) != "undefined" || $scope.optionOrigin != null) {
                loadingModal.show();
                //load destination from service with Code Origin
                AirportList.get({ id: $scope.optionOrigin }, function (data) {
                    loadingModal.hide();
                    //load destination list from service return
                    $scope.destinationResult = data.ArrivalAirports;
                    //set currency code and from code, from name
                    $scope.S1CurrencyCode = data.Currency.Abbreviation;
                    $sessionStorage.s1FromCode = data.DepartureAirport.Code;
                    $sessionStorage.s1FromName = data.DepartureAirport.Name;
                });
            }
        };

        //set default value for datestart, dateend, roundtrip, adults,children, infant
        $scope.today = new Date().toISOString().substr(0, 10);
        $scope.departDateCheck = new Date().toISOString().substr(0, 10);
        $scope.dateStart = new Date();
        $scope.dateEnd = new Date();
        $scope.check_roundtrip = 1;
        $scope.adultsValue = 1;
        $scope.childrenValue = 0;
        $scope.infantsValue = 0;

        //check if session of step1 is vaild
        if ($sessionStorage.step1) {
            //set date from session step 1 to field
            var step1Data = $sessionStorage.step1;
            $scope.tab = step1Data.s1RoundTrip;
            $scope.optionOrigin = null;
            $scope.optionDestination = null;
            $scope.dateStart = new Date(step1Data.s1DateStart);
            $scope.dateEnd = new Date(step1Data.s1DateReturn);
            $scope.adultsValue = step1Data.s1Adults;
            $scope.childrenValue = step1Data.s1Children;
            $scope.infantsValue = step1Data.s1Infants;
            $scope.promoCodeValue = step1Data.s1Promocode;
        }
        // set data into adults, children, infants
        $scope.AdultsRange = 9;// [{ name: "1", id: 1 }, { name: "2", id: 2 }, { name: "3", id: 3 }, { name: "4", id: 4 }, { name: "5", id: 5 }, { name: "6", id: 6 }, { name: "7", id: 7 }, { name: "8", id: 8 }, { name: "9", id: 9 }];
        $scope.ChildrenRange = 9;
        $scope.InfantsRange = 10;
        $scope.getNumber = function (range) {
            return new Array(range);
        }
         if ($scope.adultsValue == 1) {
            $scope.InfantsRange = 2;
            $scope.ChildrenRange = 9;
            $scope.childrenValue = 0;
            $scope.infantsValue = 0;
        }
        //function check quality when update adults
        $scope.checkAdults = function () {
            switch (parseInt($scope.adultsValue)) {
                case 1:
                    $scope.InfantsRange = 2;
                    $scope.ChildrenRange = 9;
                    $scope.childrenValue = 0;
                    $scope.infantsValue = 0;
                    break;
                case 2:
                    $scope.InfantsRange = 3;
                    $scope.ChildrenRange = 8;
                    $scope.childrenValue = 0;
                    $scope.infantsValue = 0;
                    break;
                case 3:
                    $scope.InfantsRange = 4;
                    $scope.ChildrenRange = 7;
                    $scope.childrenValue = 0;
                    $scope.infantsValue = 0;
                    break;
                case 4:
                    $scope.InfantsRange = 5;
                    $scope.ChildrenRange = 6;
                    $scope.childrenValue = 0;
                    $scope.infantsValue = 0;
                    break;
                case 5:
                    $scope.InfantsRange = 6;
                    $scope.ChildrenRange = 5;
                    $scope.childrenValue = 0;
                    $scope.infantsValue = 0;
                    break;
                case 6:
                    $scope.InfantsRange = 7;
                    $scope.ChildrenRange = 4;
                    $scope.childrenValue = 0;
                    $scope.infantsValue = 0;
                    break;
                case 7:
                    $scope.InfantsRange = 8;
                    $scope.ChildrenRange = 3;
                    $scope.childrenValue = 0;
                    $scope.infantsValue = 0;
                    break;
                case 8:
                    $scope.InfantsRange = 9;
                    $scope.ChildrenRange = 2;
                    $scope.childrenValue = 0;
                    $scope.infantsValue = 0;
                    break;
                case 9:
                    $scope.InfantsRange = 10;
                    $scope.ChildrenRange = 1;
                    $scope.childrenValue = 0;
                    $scope.infantsValue = 0;
                    break;
            }

        }

        //function check date start and date end when update date start
        $scope.updateDepartDate = function () {
            //check if date start larger date end
            if ($scope.dateEnd < $scope.dateStart) {
                $scope.dateEnd = new Date($scope.dateStart);
            }
            //$scope.departDateCheck = new Date().toISOString().substr(0, 10);
        };
        $scope.updateReturnDate = function () {
            //check if date start larger date end
            if ($scope.dateEnd < $scope.dateStart) {
                $scope.dateEnd = new Date($scope.dateStart);
            }
            //$scope.departDateCheck = new Date().toISOString().substr(0, 10);
        };

        //function button Find Flight click
        $scope.findFlightClick = function () {
            //check data of step1Form invalid
            if ($scope.step1Form.$invalid) {
                //broadcast field invalid
                $scope.$broadcast('show-errors-check-validity');
                if (typeof ($scope.optionOrigin) == "undefined" || $scope.optionOrigin === null) {
                    $translate(['DATA_MESSAGE_CHOOSE_ORIGIN']).then(function (translation) {
                        alert(translation.DATA_MESSAGE_CHOOSE_ORIGIN);
                    });
                }
                if (typeof ($scope.optionDestination) == "undefined" || $scope.optionDestination === null) {
                    $translate(['DATA_MESSAGE_CHOOSE_DESTINATION']).then(function (translation) {
                        alert(translation.DATA_MESSAGE_CHOOSE_DESTINATION);
                    });
                }
                return;
            }
            //check destination chose or not chose
            if ($scope.optionDestination) {
                //query list destination
                for (var i = 0; i < $scope.destinationResult.length; i++) {
                    //check value chose with destination code from list
                    if ($scope.optionDestination == $scope.destinationResult[i].Code) {
                        //set toCode and toName to session
                        $sessionStorage.s1ToCode = $scope.destinationResult[i].Code;
                        $sessionStorage.s1ToName = $scope.destinationResult[i].Name;
                        break;
                    }
                }
            } else {
                //notification chose destination
                //alert("Please choose Destination");
                $translate(['DATA_MESSAGE_CHOOSE_DESTINATION']).then(function (translation) {
                    alert(translation.DATA_MESSAGE_CHOOSE_DESTINATION);
                });
            }
            var roundtrip = 1;
            //check current tab !1 is Oneway (1: return, 2: one way)
            if ($scope.tab != 1) { roundtrip = 2; }
            //check if roundtrip equal 1 to set replace date end, else to set null
            if (roundtrip == 1)
                $scope.dateEnd = $scope.dateEnd;
            else
                $scope.dateEnd = "";

            //create object data with data from step 1
            var data = {
                s1RoundTrip: roundtrip,
                s1Origin: $scope.optionOrigin,
                s1Destination: $scope.optionDestination,
                s1DateStart: $scope.dateStart,
                s1DateReturn: $scope.dateEnd,
                s1Adults: $scope.adultsValue,
                s1Children: $scope.childrenValue,
                s1Infants: $scope.infantsValue,
                s1Promocode: $scope.promoCodeValue,
                s1CurrencyCode: $scope.S1CurrencyCode
            };
            function setDateTimeToCurrent(dateString) {
                var date = moment(dateString);
                var current = moment();
                date.hours(current.hours());
                date.minutes(current.minutes());
                date.seconds(current.seconds());
                return date.toISOString();
            }
            //set data step 1 to sesstion
            $sessionStorage.step1 = data;
            //redirect to step 2
            $location.path('/step2');
        };
    }

    function caculatorSurcharges(options) {
        for (var i = 0; i < options.length; i++) {
            var leg = options[i].Legs[0];
            var surcharges = leg.Surcharges;
            var sum = 0;
            for (var j = 0; j < surcharges.length; j++) {
                if (surcharges[j].ApplyToAdult) {
                    sum = sum + surcharges[j].Total;
                }
            }
            leg.surchargesTotalAdult = sum;
        }
    }
    function formatDate(date) {
        var d = new Date(date), month = '' + (d.getMonth() + 1), day = '' + d.getDate(), year = d.getFullYear();

        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;

        return [year, month, day].join('-');
    }
//Hide legs if SeatAllocation==0
    function HideLegs(outboundOptions, totalPassengers) {
        //console.log(outboundOptions);
        for (var i = 0; i < outboundOptions.length; i++) {
            var legs = outboundOptions[i].Legs[0];
            var isHideLegs = false;
            for (var k = 0; k < legs.SegmentOptions.length; k++) {
                var segments = legs.SegmentOptions[k];
                if (segments.SeatAllocation < totalPassengers) {
                    //console.log(k);
                    isHideLegs = true;
                }
            }
            if (isHideLegs == true) {
                outboundOptions.splice(i, 1);
                i = -1;
            }
        }
        //console.log(outboundOptions);
        return outboundOptions;
    }
    function sortCost(legs) {
        var fares = legs.FareOptions.Adultfares;
        for (var i = 0; i < fares.length - 1; i++) {
            for (var j = 1; j < fares.length - i; j++) {
                var fare1 = fares[j - 1].DiscountFare;
                var fare2 = fares[j].DiscountFare;
                if (fare1 > fare2) {
                    var temp = fares[j - 1];
                    fares[j - 1] = fares[j];
                    fares[j] = temp;
                }
            }
        }
        return legs;
    };
    function Step2Controller($sessionStorage, $scope, $window, $state, $location, $modal, $sce, $anchorScroll, $translate, TravelOption, FareRule, Statistics ) {
        $scope.state = $state.current.name;
        Statistics.save({
            Booking: 1,
            Checkin: 0,
            flightStatus: 0,
            Member:0 ,
            Inforamtion: 0,
            Language:0 ,
            completeBooking:0,
        }, function (data) {
            })
        var step1Data = $sessionStorage.step1;

        if (!step1Data && ($sessionStorage.step != "step1" || $sessionStorage.step != "step2")) {
            $location.path('/step1');
        }
     
        $sessionStorage.step = "step2";
        var totalPassengers = step1Data.s1Adults + step1Data.s1Children;
        //$scope.selectedDate_start = moment($scope.selectedDate_start).add('days', 1).toDate();
        //$scope.selectedDate_start = moment($scope.selectedDate_start).subtract('days', 1).toDate();
        $scope.valueFareCollapse = -1;
        $scope.isCodePromo = false;
        $scope.isFinding = true;
        $scope.findFlights = "Finding Flights...";
        $scope.step1Data = $sessionStorage.step1;
        $scope.s1FromCode = $sessionStorage.s1FromCode;
        $scope.s1FromName = $sessionStorage.s1FromName;
        $scope.s1ToCode = $sessionStorage.s1ToCode;
        $scope.s1ToName = $sessionStorage.s1ToName;
        var DateStart = formatDate(step1Data.s1DateStart) + " " + "00:00:00";
        //if (moment(DateStart).format("MMM Do YY") == moment(new Date).format("MMM Do YY")) {
        //    DateStart = step1Data.s1DateStart;
        //     DateStart = formatDate(DateStart) + " " + "00:00:00";
        //} else {
        //    DateStart = formatDate(DateStart) + " " + "00:00:00";
        //}
        var DateReturn = formatDate(step1Data.s1DateReturn) + " " + "00:00:00";
        //if (moment(DateReturn).format("MMM Do YY") == moment(new Date).format("MMM Do YY")) {
        //    DateReturn = step1Data.s1DateReturn;
        //    DateReturn = formatDate(DateReturn) + " " + "00:00:00";
        //} else {
        //    DateReturn = formatDate(DateReturn) + " " + "00:00:00";
        //}
        $scope.travelOptionResult = TravelOption.get({
            roundTrip: step1Data.s1RoundTrip,
            OutboundDate: DateStart,
            InboundDate: DateReturn,
            DaysBefore: 0,
            DaysAfter: 0,
            AdultCount: step1Data.s1Adults,
            ChildCount: step1Data.s1Children,
            InfantCount: step1Data.s1Infants,
            DepartureAirportCode: step1Data.s1Origin,
            ArrivalAirportCode: step1Data.s1Destination,
            CurrencyCode: step1Data.s1CurrencyCode,
            PromoCode: step1Data.s1Promocode
        }, function (data) {
                if (data.OutboundOptions != undefined) {
                    var outboundOptions = data.OutboundOptions;
                    for (var i = 0; i < outboundOptions.length; i++) {
                        var legs = outboundOptions[i].Legs[0];
                        //Sort cost
                        legs = sortCost(legs);
                        if (moment(legs.DepartureDate).format("MMM Do YY") != moment(step1Data.s1DateStart).format("MMM Do YY")) {
                            outboundOptions.splice(i, 1);
                            i = -1;
                        } else {
                            for (var j = 0; j < legs.SegmentOptions.length; j++) {
                                var segments = legs.SegmentOptions[j];
                                var DateNow = moment.utc(new Date).format();
                                var DepartureDate = moment.utc(segments.Flight.ETD).format();
                                if (DateNow >= DepartureDate) {
                                    outboundOptions.splice(i, 1);
                                    i = -1;
                                }
                            }
                        }
                    }
                    var inboundOptions = data.InboundOptions;
                    for (var i = 0; i < inboundOptions.length; i++) {
                        var legs = inboundOptions[i].Legs[0];
                        legs = sortCost(legs);
                        if (moment(legs.DepartureDate).format("MMM Do YY") != moment(step1Data.s1DateReturn).format("MMM Do YY")) {
                            inboundOptions.splice(i, 1);
                            i = -1;
                        } else {
                            for (var j = 0; j < legs.SegmentOptions.length; j++) {
                                var segments = legs.SegmentOptions[j];

                                var DateNow = moment.utc(new Date).format();
                                var ReturnDate = moment.utc(segments.Flight.ETA).format();
                                if (DateNow >= ReturnDate) {
                                    outboundOptions.splice(i, 1);
                                    i = -1;
                                }
                            }
                        }
                    }
                    HideLegs(outboundOptions, totalPassengers);
                    HideLegs(inboundOptions, totalPassengers);
                    caculatorSegment(outboundOptions);
                    caculatorSegment(inboundOptions);
                    caculatorSurcharges(outboundOptions);
                    caculatorSurcharges(inboundOptions);
                    $scope.travelOptionSelect.Selected = outboundOptions;
                    $scope.travelOptionPromoSelect = data.OutboundPromoCodeResults;
                    setPromoData(data);
                }
                $scope.isFinding = false;
                $scope.isCodePromo = true;
                $scope.findFlights = "No flights matched your search criteria";
            });
        $scope.travelOptionSelect = {};
        $scope.travelOptionPromoSelect = {};
        $scope.packagePriceSelect = Array(2);

        var segmentChoosen = [];
        var fareClass = "";
        $scope.isSelected = function (tabIndex, fareIndex, pricePackage, leg) {
            fareClass = pricePackage.FareClass;
            segmentChoosen = leg.SegmentOptions;
        };

        $scope.predate = function () {

            var today = new Date();

            var DateStart_temp = new Date(setDateTimeToCurrent($scope.step1Data.s1DateStart));
            var DateReturn_temp = new Date(setDateTimeToCurrent($scope.step1Data.s1DateReturn));

            var isLoadTravelOption = true;
            if (step1Data.s1RoundTrip == 1) {
                if ($scope.tab == 0) {
                    DateStart_temp = moment(DateStart_temp).subtract(1, 'days').toDate();
                    if (DateStart_temp < moment(today).subtract(1, 'minutes').toDate()) {
                        DateStart_temp = today;

                        //alert("Depart date must be greater than or equal today");
                        $translate(['DATA_MESSAGE_DEPART_DATE_MUST_GREATER_EQUAL_TODAY']).then(function (translation) {
                            alert(translation.DATA_MESSAGE_DEPART_DATE_MUST_GREATER_EQUAL_TODAY);
                        });
                        isLoadTravelOption = false;
                    }
                } else {
                    DateReturn_temp = moment(DateReturn_temp).subtract(1, 'days').toDate();
                    DateStart_temp = moment(DateStart_temp).subtract(1, 'minutes').toDate();
                    if (DateReturn_temp < moment(DateStart_temp).subtract(1, 'minutes').toDate()) {
                        DateReturn_temp = DateStart_temp;

                        //alert("Return date must be greater than depart date");
                        $translate(['DATA_MESSAGE_RETURN_DATE_MUST_GREATER_DEPART_DATE']).then(function (translation) {
                            alert(translation.DATA_MESSAGE_RETURN_DATE_MUST_GREATER_DEPART_DATE);
                        });
                        isLoadTravelOption = false;
                    }
                }
            } else {
                DateStart_temp = moment(DateStart_temp).subtract(1, 'days').toDate();
                if (DateStart_temp < moment(today).subtract(1, 'minutes').toDate()) {
                    DateStart_temp = today;

                    //alert("Depart date must be greater than or equal today");
                    $translate(['DATA_MESSAGE_DEPART_DATE_MUST_GREATER_EQUAL_TODAY']).then(function (translation) {
                        alert(translation.DATA_MESSAGE_DEPART_DATE_MUST_GREATER_EQUAL_TODAY);
                    });
                    isLoadTravelOption = false;
                }
                DateReturn_temp = null;
            }
            var DateStart_Pre = formatDate(DateStart_temp.toISOString()) + " " + "00:00:00";
            

            //if (moment(DateStart_Pre).format("MMM Do YY") == moment(new Date).format("MMM Do YY")) {
            //    DateStart_Pre = formatDate(DateStart_Pre) + " " + "00:00:00";
            //} else {
            //    DateStart_Pre = formatDate(DateStart_Pre) + " " + "00:00:00";
            //}

            if (DateReturn_temp != null) {
                var DateReturn_Pre = formatDate(DateReturn_temp.toISOString()) + " " + "00:00:00";
                //if (moment(DateReturn_Pre).format("MMM Do YY") == moment(new Date).format("MMM Do YY")) {
                //    DateStart_Pre = formatDate(DateStart_Pre) + " " + "00:00:00";
                //} else {
                //    DateReturn_Pre = formatDate(DateReturn_Pre) + " " + "00:00:00";
                //}
            }
            if (isLoadTravelOption) {
                $scope.isFinding = true;
                $scope.isCodePromo = false;
                $scope.findFlights = "Finding Flights...";
                var datas1 = {
                    s1RoundTrip: step1Data.s1RoundTrip,
                    s1Origin: step1Data.s1Origin,
                    s1Destination: step1Data.s1Destination,
                    s1DateStart: DateStart_temp,
                    s1DateReturn: DateReturn_temp,
                    s1Adults: step1Data.s1Adults,
                    s1Children: step1Data.s1Children,
                    s1Infants: step1Data.s1Infants,
                    s1Promocode: step1Data.s1Promocode,
                    s1CurrencyCode: step1Data.s1CurrencyCode
                };
                $sessionStorage.step1 = datas1;
                $scope.step1Data = $sessionStorage.step1;

                $scope.travelOptionResult = TravelOption.get({
                    roundTrip: step1Data.s1RoundTrip,
                    OutboundDate: DateStart_Pre,
                    InboundDate: DateReturn_Pre,
                    DaysBefore: 0,
                    DaysAfter: 0,
                    AdultCount: step1Data.s1Adults,
                    ChildCount: step1Data.s1Children,
                    InfantCount: step1Data.s1Infants,
                    DepartureAirportCode: step1Data.s1Origin,
                    ArrivalAirportCode: step1Data.s1Destination,
                    CurrencyCode: step1Data.s1CurrencyCode,
                    PromoCode: step1Data.s1Promocode
                }, function (data) {
                        if (data.OutboundOptions != undefined) {
                            var outboundOptions = data.OutboundOptions;
                            for (var i = 0; i < outboundOptions.length; i++) {
                                var legs = outboundOptions[i].Legs[0];
                                legs = sortCost(legs);
                                if (moment(legs.DepartureDate).format("MMM Do YY") != moment(DateStart_temp).format("MMM Do YY")) {
                                    outboundOptions.splice(i, 1);
                                    i = -1;
                                } else {
                                    for (var j = 0; j < legs.SegmentOptions.length; j++) {
                                        var segments = legs.SegmentOptions[j];
                                        var DateNow = moment.utc(new Date).format();
                                        var DepartureDate = moment.utc(segments.Flight.ETD).format();
                                        if (DateNow >= DepartureDate) {
                                            outboundOptions.splice(i, 1);
                                            i = -1;
                                        }
                                    }
                                }
                            }
                            var inboundOptions = data.InboundOptions;
                            for (var i = 0; i < inboundOptions.length; i++) {
                                var legs = inboundOptions[i].Legs[0];
                                legs = sortCost(legs);
                                if (moment(legs.DepartureDate).format("MMM Do YY") != moment(DateReturn_temp).format("MMM Do YY")) {
                                    inboundOptions.splice(i, 1);
                                    i = -1;
                                } else {
                                    for (var j = 0; j < legs.SegmentOptions.length; j++) {
                                        var segments = legs.SegmentOptions[j];
                                        var DateNow = moment.utc(new Date).format();
                                        var ReturnDate = moment.utc(segments.Flight.ETA).format();
                                        if (DateNow >= ReturnDate) {
                                            outboundOptions.splice(i, 1);
                                            i = -1;
                                        }
                                    }
                                }
                            }
                            HideLegs(outboundOptions, totalPassengers);
                            HideLegs(inboundOptions, totalPassengers);
                            caculatorSegment(outboundOptions);
                            caculatorSegment(inboundOptions);
                            caculatorSurcharges(outboundOptions);
                            caculatorSurcharges(inboundOptions);

                            if ($scope.tab == 0) $scope.travelOptionSelect.Selected = outboundOptions;
                            else $scope.travelOptionSelect.Selected = inboundOptions;

                            for (var i = 0; i < outboundOptions.length; i++) {
                                var legs = outboundOptions[i].Legs[0];
                                for (var k = 0; k <= legs.FareOptions.Adultfares.length - 1; k++) {
                                    var adult = legs.FareOptions.Adultfares[k];
                                    if (adult.FareClass == fareClass) {
                                        for (var j = 0; j < legs.SegmentOptions.length; j++) {
                                            if (segmentChoosen.length != legs.SegmentOptions.length) {
                                                legs.FareOptions.Adultfares[k].isChecked = false;
                                                $scope.isFinding = false;
                                                break;
                                            } else {
                                                var flights = legs.SegmentOptions[j].Flight;
                                                if (flights.ETDLocal != segmentChoosen[j].Flight.ETDLocal && flights.ETALocal != segmentChoosen[j].Flight.ETALocal) {
                                                    legs.FareOptions.Adultfares[k].isChecked = false;
                                                    $scope.isFinding = false;
                                                    break;
                                                } else {
                                                    legs.FareOptions.Adultfares[k].isChecked = true;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            setPromoData(data);
                        }
                        if ($scope.tab == 0) {
                            $scope.tab = 0;
                            $scope.travelOptionSelect.Selected = outboundOptions;
                            $scope.travelOptionPromoSelect = data.OutboundPromoCodeResults;
                        }
                        else {
                            $scope.tab = 1;
                            $scope.travelOptionSelect.Selected = inboundOptions;
                            $scope.travelOptionPromoSelect = data.InboundPromoCodeResults;
                        }
                        $scope.isFinding = false;
                        $scope.isCodePromo = true;
                        $scope.findFlights = "No flights matched your search criteria";

                    });
                $scope.travelOptionSelect = {};
                $scope.travelOptionPromoSelect = {};
            }
        };

        function caculatorSegment(outboundOptions) {
            for (var i = 0; i < outboundOptions.length; i++) {
                var outbound = outboundOptions[i];
                outbound.Legs[0].SegmentOptionsTemp = [];
                angular.copy(outbound.Legs[0].SegmentOptions, outbound.Legs[0].SegmentOptionsTemp);
                var airCraftIndex = 0;
                var countStop = 1;
                
                if (outbound.Legs[0].SegmentOptionsTemp.length == 1) {
                    var segment = outbound.Legs[0].SegmentOptionsTemp;
                    segment[0].Flight.TimeTransit = "";
                    segment[0].checkAirCraft = false;
                } else {
                    outbound.Legs[0].SegmentOptionsTemp[0].Flight.TimeTransit = "";
                    outbound.Legs[0].SegmentOptionsTemp[0].checkAirCraft = false;
                    for (var j = 1; j < outbound.Legs[0].SegmentOptionsTemp.length; j++) {
                        var segment = outbound.Legs[0].SegmentOptionsTemp;
                        if (segment[j].Flight.Aircraft.ModelName == segment[airCraftIndex].Flight.Aircraft.ModelName
                            && segment[j].Flight.Number == segment[airCraftIndex].Flight.Number) {
                            segment[j].checkAirCraft = true;
                            var timeeta = segment[j - 1].Flight.ETALocal;
                            var timeetd = segment[j].Flight.ETDLocal;
                            var totalMinutes = moment(timeetd).diff(timeeta, 'minutes');
                            var hours = Math.floor(totalMinutes / 60);
                            var minutes = totalMinutes % 60;
                            segment[j].Flight.TimeTransit = hours + 'h ' + minutes + 'm';
                            outbound.Legs[0].SegmentOptionsTemp[airCraftIndex].Flight.ETA = segment[j].Flight.ETA;
                            outbound.Legs[0].SegmentOptionsTemp[airCraftIndex].Flight.ETALocal = segment[j].Flight.ETALocal;
                            outbound.Legs[0].SegmentOptionsTemp[airCraftIndex].Flight.ArrivalAirport.Code = segment[j].Flight.ArrivalAirport.Code;
                            outbound.Legs[0].SegmentOptionsTemp[airCraftIndex].Flight.ArrivalAirport.Name = segment[j].Flight.ArrivalAirport.Name;
                            segment[j].countStop = countStop;
                            countStop++;
                        } else {
                            segment[j].checkAirCraft = false;
                            airCraftIndex = j;
                        }
                    }
                }
            }
        }

        $scope.nextdate = function () {
            var today = new Date();
            var DateStart_temp = new Date(setDateTimeToCurrent($scope.step1Data.s1DateStart));
            var DateReturn_temp = new Date(setDateTimeToCurrent($scope.step1Data.s1DateReturn));
            var isLoadTravelOption = true;
            if (step1Data.s1RoundTrip == 1) {
                if ($scope.tab == 0) {
                    DateStart_temp = moment(DateStart_temp).add(1, 'days').toDate();
                    if ($scope.step1Data.s1RoundTrip == 1)
                        if (DateStart_temp > moment(DateReturn_temp).add(1, 'minutes').toDate()) {
                            DateStart_temp = DateReturn_temp;
                            isLoadTravelOption = false;

                            //alert("Depart date must not be greater than return date");
                            $translate(['DATA_MESSAGE_DEPART_DATE_MUST_NOT_GREATER_EQUAL_TODAY']).then(function (translation) {
                                alert(translation.DATA_MESSAGE_DEPART_DATE_MUST_NOT_GREATER_EQUAL_TODAY);
                            });
                        }
                } else {
                    DateReturn_temp = moment(DateReturn_temp).add(1, 'days').toDate();
                }
            } else {
                DateStart_temp = moment(DateStart_temp).add(1, 'days').toDate();
                DateReturn_temp = null;
            }
            var DateStart_Next = formatDate(DateStart_temp.toISOString()) + " " + "00:00:00";
            //if (moment(DateStart_Next).format("MMM Do YY") == moment(new Date).format("MMM Do YY")) {
            //    DateStart_Next = formatDate(DateStart_Next) + " " + "00:00:00";
            //} else {
            //    DateStart_Next = formatDate(DateStart_Next) + " " + "00:00:00";
            //}
            if (DateReturn_temp != null) {
                var DateReturn_Next = formatDate(DateReturn_temp.toISOString()) + " " + "00:00:00";
                //if (moment(DateReturn_Next).format("MMM Do YY") == moment(new Date).format("MMM Do YY")) {
                //    DateReturn_Next = formatDate(DateReturn_Next) + " " + "00:00:00";
                //} else {
                //    DateReturn_Next = formatDate(DateReturn_Next) + " " + "00:00:00";
                //}
            }
            if (isLoadTravelOption) {
                $scope.isFinding = true;
                $scope.isCodePromo = false;
                $scope.findFlights = "Finding Flights...";
                var datas1 = {
                    s1RoundTrip: step1Data.s1RoundTrip,
                    s1Origin: step1Data.s1Origin,
                    s1Destination: step1Data.s1Destination,
                    s1DateStart: DateStart_temp,
                    s1DateReturn: DateReturn_temp,
                    s1Adults: step1Data.s1Adults,
                    s1Children: step1Data.s1Children,
                    s1Infants: step1Data.s1Infants,
                    s1Promocode: step1Data.s1Promocode,
                    s1CurrencyCode: step1Data.s1CurrencyCode
                };
                $sessionStorage.step1 = datas1;
                $scope.step1Data = $sessionStorage.step1;
                $scope.travelOptionResult = TravelOption.get({
                    roundTrip: step1Data.s1RoundTrip,
                    //OutboundDate: DateStart_temp,
                    //InboundDate: DateReturn_temp,
                    OutboundDate: DateStart_Next,
                    InboundDate: DateReturn_Next,
                    DaysBefore: 0,
                    DaysAfter: 0,
                    AdultCount: step1Data.s1Adults,
                    ChildCount: step1Data.s1Children,
                    InfantCount: step1Data.s1Infants,
                    DepartureAirportCode: step1Data.s1Origin,
                    ArrivalAirportCode: step1Data.s1Destination,
                    CurrencyCode: step1Data.s1CurrencyCode,
                    PromoCode: step1Data.s1Promocode
                }, function (data) {
                        if (data.OutboundOptions != undefined) {
                            var outboundOptions = data.OutboundOptions;
                            for (var i = 0; i < outboundOptions.length; i++) {
                                var legs = outboundOptions[i].Legs[0];
                                legs = sortCost(legs);
                                if (moment(legs.DepartureDate).format("MMM Do YY") != moment(DateStart_temp).format("MMM Do YY")) {
                                    outboundOptions.splice(i, 1);
                                    i = -1;
                                } else {
                                    for (var j = 0; j < legs.SegmentOptions.length; j++) {
                                        var segments = legs.SegmentOptions[j];
                                        var DateNow = moment.utc(new Date).format();
                                        var DepartureDate = moment.utc(segments.Flight.ETD).format();
                                        if (DateNow >= DepartureDate) {
                                            outboundOptions.splice(i, 1);
                                            i = -1;
                                        }
                                    }
                                }
                            }
                            var inboundOptions = data.InboundOptions;
                            for (var i = 0; i < inboundOptions.length; i++) {
                                var legs = inboundOptions[i].Legs[0];
                                legs = sortCost(legs);
                                if (moment(legs.DepartureDate).format("MMM Do YY") != moment(DateReturn_temp).format("MMM Do YY")) {
                                    inboundOptions.splice(i, 1);
                                    i = -1;
                                } else {
                                    for (var j = 0; j < legs.SegmentOptions.length; j++) {
                                        var segments = legs.SegmentOptions[j];
                                        var DateNow = moment.utc(new Date).format();
                                        var ReturnDate = moment.utc(segments.Flight.ETA).format();
                                        if (DateNow >= ReturnDate) {
                                            outboundOptions.splice(i, 1);
                                            i = -1;
                                        }
                                    }
                                }
                            }
                            HideLegs(outboundOptions, totalPassengers);
                            HideLegs(inboundOptions, totalPassengers);
                            caculatorSegment(outboundOptions);
                            caculatorSegment(inboundOptions);
                            caculatorSurcharges(outboundOptions);
                            caculatorSurcharges(inboundOptions);

                            if ($scope.tab == 0) $scope.travelOptionSelect.Selected = outboundOptions;
                            else $scope.travelOptionSelect.Selected = inboundOptions;

                            for (var i = 0; i < outboundOptions.length; i++) {
                                var legs = outboundOptions[i].Legs[0];
                                for (var k = 0; k <= legs.FareOptions.Adultfares.length - 1; k++) {
                                    var adult = legs.FareOptions.Adultfares[k];
                                    if (adult.FareClass == fareClass) {
                                        for (var j = 0; j < legs.SegmentOptions.length; j++) {
                                            if (segmentChoosen.length != legs.SegmentOptions.length) {
                                                legs.FareOptions.Adultfares[k].isChecked = false;
                                                $scope.isFinding = false;
                                                break;
                                            } else {
                                                var flights = legs.SegmentOptions[j].Flight;
                                                if (flights.ETDLocal != segmentChoosen[j].Flight.ETDLocal && flights.ETALocal != segmentChoosen[j].Flight.ETALocal) {
                                                    legs.FareOptions.Adultfares[k].isChecked = false;
                                                    $scope.isFinding = false;
                                                    break;
                                                } else {
                                                    legs.FareOptions.Adultfares[k].isChecked = true;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            setPromoData(data);
                        }
                        if ($scope.tab == 0) {
                            $scope.tab = 0;
                            $scope.travelOptionSelect.Selected = outboundOptions;
                            $scope.travelOptionPromoSelect = data.OutboundPromoCodeResults;
                        }
                        else {
                            $scope.tab = 1;
                            $scope.travelOptionSelect.Selected = inboundOptions;
                            $scope.travelOptionPromoSelect = data.InboundPromoCodeResults;
                        }
                        $scope.isFinding = false;
                        $scope.isCodePromo = true;
                        $scope.findFlights = "No flights matched your search criteria";
                    });
                $scope.travelOptionSelect = $scope.travelOptionPromoSelect = {};
            }
        };

        $scope.departTabClick = function () {
            $scope.tab = 0;
            $scope.travelOptionSelect.Selected = $scope.travelOptionResult.OutboundOptions;
            $scope.travelOptionPromoSelect = $scope.travelOptionResult.OutboundPromoCodeResults;
        };

        $scope.returnTabClick = function () {
            $scope.tab = 1;
            $scope.travelOptionSelect.Selected = $scope.travelOptionResult.InboundOptions;
            $scope.travelOptionPromoSelect = $scope.travelOptionResult.InboundPromoCodeResults;
        };
        function setDateTimeToCurrent(dateString) {
            var date = moment(dateString);
            var current = moment();
            date.hours(current.hours());
            date.minutes(current.minutes());
            date.seconds(current.seconds());
            return date.toISOString();
        }

        function setPromoData(data) {
            $sessionStorage.promoCode = [
                {
                    legNumber: 1,
                    promoDetail: data.OutboundPromoCodeResults
                },
                {
                    legNumber: 2,
                    promoDetail: data.InboundPromoCodeResults
                }
            ];
        }

        var myAirportModal = $modal({ scope: $scope, template: 'app/modal/airport.html', show: false });
        $scope.loadAirportDetail = function (airportCode, airportName) {
            $scope.airportCode = airportCode;
            $scope.airportName = airportName;
            myAirportModal.show();
        };
        $scope.closeAirport = function () {
            myAirportModal.hide();
        };

        var myInfomationModal = $modal({ scope: $scope, template: 'app/modal/information.html', show: false });
        $scope.loadFare = function (fareClass) {
            FareRule.query({
                languagecode: $sessionStorage.languageCode,
                infoSlug: fareClass
            }, function (data) {
                    if (data.length > 0) {
                        $scope.informationTitle = data[0].InfoTitle;
                        $scope.informationContent = $sce.trustAsHtml(data[0].InfoContent);
                    } else {
                        $scope.informationTitle = fareClass;
                        $scope.informationContent = $sce.trustAsHtml(fareClass);
                    }
                    $scope.classname = "fareclass";
                    myInfomationModal.show();
                });
        }
        $scope.closeInformation = function () {
            myInfomationModal.hide();
        };

        var myChargeSummaryModal = $modal({ scope: $scope, template: 'app/modal/chargeSummary.html', show: false });
        $scope.loadChargeSummary = function (surcharges, pricePackage) {
            $scope.fareName = pricePackage.Description + '(' + pricePackage.FareClass + ')';
            $scope.farePrice = pricePackage.DiscountFareTotal;
            $scope.Currency = pricePackage.Currency.Abbreviation;
            $scope.surcharges = surcharges;
            var sum = 0;
            for (var i = 0; i < surcharges.length; i++) {
                if (surcharges[i].ApplyToAdult) {
                    sum = sum + surcharges[i].Total;
                }
            }
            $scope.summaryCharge = pricePackage.DiscountFareTotal + sum;
            myChargeSummaryModal.show();
        }
        $scope.closeChargeSummaryModal = function () {
            myChargeSummaryModal.hide();
        };



        $scope.submitFlight = function () {
            if ($scope.step2Form.$invalid)
                return;
            if ($scope.packagePriceSelect[0]) {
                if (step1Data.s1RoundTrip == 1 && !$scope.packagePriceSelect[1]) {
                    if ($scope.tab == 1 && !$scope.packagePriceSelect[1])
                        //alert("Please choose Flights return");
                        $translate(['DATA_MESSAGE_CHOOSE_FLIGHTS_RETURN']).then(function (translation) {
                            alert(translation.DATA_MESSAGE_CHOOSE_FLIGHTS_RETURN);
                        });
                    $scope.tab = 1;
                    $scope.travelOptionSelect.Selected = $scope.travelOptionResult.InboundOptions;
                    $location.hash('primary');
                    $anchorScroll();
                } else {
                    if (step1Data.s1RoundTrip == 1) {
                        var departSegmentOptions = $scope.packagePriceSelect[0].leg.SegmentOptions;
                        var returnSegmentOptions = $scope.packagePriceSelect[1].leg.SegmentOptions;
                        var etaDepart = moment(departSegmentOptions[departSegmentOptions.length - 1].Flight.ETALocal);
                        var etdReturn = moment(returnSegmentOptions[returnSegmentOptions.length - 1].Flight.ETDLocal);
                        //console.log(etaDepart, etdReturn);
                        //console.log(etdReturn.diff(etaDepart));
                        if (etdReturn.diff(etaDepart) < 0) {
                            //alert("Depart date time must not be greater than return date time");
                            $translate(['DATA_MESSAGE_DEPART_DATE_GREATER_RETURN_DATE']).then(function (translation) {
                                alert(translation.DATA_MESSAGE_DEPART_DATE_GREATER_RETURN_DATE);
                            });
                            return;
                        }
                    }
                    $sessionStorage.step2 = $scope.packagePriceSelect;
                    $location.path('/step3');
                }
            }
            else {
                //alert("Please choose Flights depart");
                $translate(['DATA_MESSAGE_CHOOSE_FLIGHTS_DEPART']).then(function (translation) {
                    alert(translation.DATA_MESSAGE_CHOOSE_FLIGHTS_DEPART);
                });
            }

        };
    }


    function Step3Controller($sessionStorage, $scope, $state, $location, $translate, $timeout, $modal, Country, Province) {
        $scope.state = $state.current.name;
        if (!$sessionStorage.step2 && ($sessionStorage.step != "step2" || $sessionStorage.step != "step3")) {
            $location.path('/step2');
        }

        $sessionStorage.step = "step3";
        $timeout(function () { $scope.passengerCollapse = 0; });

        var loadingModal_country = $modal({
            scope: $scope,
            template: 'app/modal/loading.html',
            show: true,
            backdrop: 'static'
        });
        Country.query({}, function (data) {
            $scope.countryResult = data;
            loadingModal_country.hide();
        });
        $scope.isProvince = false;
        var loadingModal_province = $modal({
            scope: $scope,
            template: 'app/modal/loading.html',
            show: false,
            backdrop: 'static'
        });
        $scope.changeCountry = function () {
            loadingModal_province.show();
            $scope.provinceResult = Province.query({ countryCode: $scope.adults[0].passengerSelectCountry }, function () {
                loadingModal_province.hide();
                $scope.isProvince = true;
            });
        };
        if ($sessionStorage.step3) {
            $scope.adults = $sessionStorage.step3.adults;
            $scope.provinceResult = Province.query({ countryCode: $scope.adults[0].passengerSelectCountry }, function () {
                $scope.isProvince = true;
            });
            $scope.children = $sessionStorage.step3.children;
            $scope.infants = $sessionStorage.step3.infants;
        } else {
            var step1Data = $sessionStorage.step1;

            $scope.adults = Array(parseInt(step1Data.s1Adults));
            for (var i = 0; i < $scope.adults.length; i++)
                $scope.adults[i] = {};

            $scope.children = Array(parseInt(step1Data.s1Children));
            for (var i = 0; i < $scope.children.length; i++) {
                $scope.children[i] = {};
                $scope.children[i].gender = 'Male';
            }

            $scope.infants = Array(parseInt(step1Data.s1Infants));
            for (var i = 0; i < $scope.infants.length; i++) {
                $scope.infants[i] = {};
                $scope.infants[i].gender = 'M';
            }
        }
        $scope.submitPassenger = function () {
            if ($scope.step3Form.$invalid) {
                $scope.$broadcast('show-errors-check-validity');
                //alert("Please fill in all information of all passengers");
                $translate(['DATA_FILL_INFO_OF_PASSENGER']).then(function (translation) {
                    alert(translation.DATA_FILL_INFO_OF_PASSENGER);
                });
                return;
            }
            var data = {
                adults: $scope.adults,
                children: $scope.children,
                infants: $scope.infants
            };
            $sessionStorage.step3 = data;
            $location.path('/step4');
        };
    }


    function Step4Controller($sessionStorage, $scope, $state, $location, $window, $modal, $timeout, Addon) {
        $scope.state = $state.current.name;
        if (!$sessionStorage.step3 && ($sessionStorage.step != "step3" || $sessionStorage.step != "step4")) {
            $location.path('/step3');
        }
        $sessionStorage.step = "step4";

        function collapseFlight() {
            $timeout(function () {
                $scope.addonServiceCollapseFlight1 = 1;
                $scope.addonServiceCollapseFlight2 = 1;
            }, 300000);
        }
        var step1Data = $sessionStorage.step1;

        if ($sessionStorage.step4) {
            $scope.adults = $sessionStorage.step4.adults;
            $scope.children = $sessionStorage.step4.children;
            $scope.infants = $sessionStorage.step4.infants;
        } else {
            $scope.adults = $sessionStorage.step3.adults;
            $scope.children = $sessionStorage.step3.children;
            $scope.infants = $sessionStorage.step3.infants;
        }
        $scope.step1Data = $sessionStorage.step1;
        $scope.step2Data = $sessionStorage.step2;
        $scope.s1FromCode = $sessionStorage.s1FromCode;
        $scope.s1FromName = $sessionStorage.s1FromName;
        $scope.s1ToCode = $sessionStorage.s1ToCode;
        $scope.s1ToName = $sessionStorage.s1ToName;

        for (var i = 0; i < $scope.adults.length; i++) {
            $scope.adults[i].addons = [
                { mealQuality: {}, baggage: null },
                { mealQuality: {}, baggage: null },
            ];
        }
        for (var i = 0; i < $scope.children.length; i++) {
            $scope.children[i].addons = [
                { mealQuality: {}, baggage: {} },
                { mealQuality: {}, baggage: {} },
            ];
        }
        var loadingModal = $modal({
            scope: $scope,
            template: 'app/modal/loading.html',
            show: true,
            backdrop: 'static'
        });

        var addonDepResults = [];
        var addonArrResults = [];
        $scope.addonDepartMeal = [];
        $scope.addonDepartBaggage = [];
        $scope.addonArrivalMeal = [];
        $scope.addonArrivalBaggage = [];
        var indexDepSegment = 0;
        var indexArrSegment = 0;
        for (var i = 0; i < $scope.step2Data[0].leg.SegmentOptions.length; i++) {
            var segment = $scope.step2Data[0].leg.SegmentOptions[i];
            var addon = Addon.query({
                type: "flight",
                depDate: segment.Flight.ETDLocal,
                arrDate: segment.Flight.ETALocal,
                depAirport: segment.Flight.DepartureAirport.Code,
                arrAirport: segment.Flight.ArrivalAirport.Code,
                flight: segment.Flight.Number,
                currency: $sessionStorage.step1.s1CurrencyCode
            }, function (data) {
                    function checkMaxPerPax(allocation) {
                        return allocation.MaxQuatityPerPax != 0;
                    }
                    indexDepSegment++;
                    if (data.length != 0) {
                        for (var i = 0; i < data.length; i++) {
                            if (data[i].Name == "Baggage") {
                                data[i].Allocations = data[i].Allocations.filter(checkMaxPerPax);
                                if (data[i].Allocations.length != 0)
                                    $scope.addonDepartBaggage.push(data[i]);
                            }
                            if (data[i].Name == "Meal") {
                                data[i].Allocations = data[i].Allocations.filter(checkMaxPerPax);
                                if (data[i].Allocations.length != 0)
                                    $scope.addonDepartMeal.push(data[i]);
                            }
                        }
                        addonDepResults.push(data);
                    }
                    if ($scope.step2Data[0].leg.SegmentOptions.length == indexDepSegment) {
                        if ($scope.step2Data[1] != null) {
                            for (var j = 0; j < $scope.step2Data[1].leg.SegmentOptions.length; j++) {
                                var segment_arr = $scope.step2Data[1].leg.SegmentOptions[j];
                                var addon_arr = Addon.query({
                                    type: "flight",
                                    depDate: segment_arr.Flight.ETDLocal,
                                    arrDate: segment_arr.Flight.ETALocal,
                                    depAirport: segment_arr.Flight.DepartureAirport.Code,
                                    arrAirport: segment_arr.Flight.ArrivalAirport.Code,
                                    flight: segment_arr.Flight.Number,
                                    currency: $sessionStorage.step1.s1CurrencyCode
                                }, function (data) {
                                        indexArrSegment++;
                                        if (data.length != 0) {
                                            for (var k = 0; k < data.length; k++) {
                                                if (data[k].Name == "Baggage") {
                                                    data[k].Allocations = data[k].Allocations.filter(checkMaxPerPax);
                                                    if (data[k].Allocations.length != 0)
                                                        $scope.addonArrivalBaggage.push(data[k]);
                                                }
                                                if (data[k].Name == "Meal") {
                                                    data[k].Allocations = data[k].Allocations.filter(checkMaxPerPax);
                                                    if (data[k].Allocations.length != 0)
                                                        $scope.addonArrivalMeal.push(data[k]);
                                                }
                                            }
                                            addonArrResults.push(data);
                                        }
                                        if ($scope.step2Data[1].leg.SegmentOptions.length == indexArrSegment) {
                                            loadingModal.hide();
                                            collapseFlight();
                                        }
                                    });
                            }
                        } else {
                            loadingModal.hide();
                            collapseFlight();
                        }
                    }
                });
        }

        $scope.checkObjectIsEmpty = function isEmptyObject(obj) {
            var name;
            for (name in obj) {
                return false;
            }
            return true;
        }

        $scope.checkAddon = function (addonBaggage, addonMeal, index) {
            var hasBaggage = addonBaggage[index] && addonBaggage[index].Allocations
                && addonBaggage[index].Allocations.length != 0;

            var hasMeal = addonMeal[index] && addonMeal[index].Allocations
                && addonMeal[index].Allocations.length != 0;

            if (hasBaggage || hasMeal)
                return true;
            else
                return false;
        }
            $scope.checkBaggage = function (addonBaggage, index) {
            var hasBaggage = addonBaggage[index] && addonBaggage[index].Allocations
                && addonBaggage[index].Allocations.length != 0;
            if (hasBaggage)
                return true;
            else
                return false;
        }
            $scope.checkMeal = function (addonMeal, index) {
            var hasMeal = addonMeal[index] && addonMeal[index].Allocations
                && addonMeal[index].Allocations.length != 0;

            if (hasMeal)
                return true;
            else
                return false;
        }

            $scope.AddonData = {};
        $scope.submitAddon = function () {
            if ($scope.step4Form.$invalid)
                return;

            function setMealtoPassenger(passengers) {
                for (var i = 0; i < passengers.length; i++) {
                    var passenger = passengers[i];
                    for (var j = 0; j < passenger.addons.length; j++) {
                        var addon = passenger.addons[j];
                        var meal = [];
                        for (var mealID in addon.mealQuality) {
                            var numberMeal = addon.mealQuality[mealID].number;
                            for (var k = 0; k < $scope.addonDepartMeal.length; k++) {
                                var addonDepartMeal_ = $scope.addonDepartMeal[k];
                                for (var l = 0; l < addonDepartMeal_.Allocations.length; l++) {
                                    if (mealID == addonDepartMeal_.Allocations[l].ID) {
                                        var mealOption = addonDepartMeal_.Allocations[l];
                                        mealOption.numberMeal = numberMeal;
                                        meal.push(mealOption);
                                    }
                                }
                            }
                            for (var m = 0; m < $scope.addonArrivalMeal.length; m++) {
                                var addonArrivalMeal_ = $scope.addonArrivalMeal[m];
                                for (var n = 0; n < addonArrivalMeal_.Allocations.length; n++) {
                                    if (mealID == addonArrivalMeal_.Allocations[n].ID) {
                                        var mealOption = addonArrivalMeal_.Allocations[n];
                                        mealOption.numberMeal = numberMeal;
                                        meal.push(mealOption);
                                    }
                                }
                            }
                        }
                        addon.mealSelected = meal;
                    }
                }
            }
            setMealtoPassenger($scope.adults);
            setMealtoPassenger($scope.children);
            var data = {
                adults: $scope.adults,
                children: $scope.children,
                infants: $scope.infants
            };

            $sessionStorage.step4AddonDepart = addonDepResults;
            $sessionStorage.step4AddonArrival = addonArrResults;

            $sessionStorage.step4 = data;
            $location.path('/step5');
        };
    }


    function Step5Controller($sessionStorage, $scope, $state, $modal, $location, $sce, FareRule) {
        $scope.state = $state.current.name;
        if (!$sessionStorage.step4 && ($sessionStorage.step != "step4" || $sessionStorage.step != "step5")) {
            $location.path('/step4');
        }
        $sessionStorage.step = "step5";

        $scope.s1Data = $sessionStorage.step1;
        $scope.s2Data = $sessionStorage.step2;
        if (!$scope.s2Data[1])
            $scope.s2Data.splice(1, 1);
        $scope.s3Data = $sessionStorage.step3;
        $scope.s4Data = $sessionStorage.step4;
        $scope.addonDepart = $sessionStorage.step4AddonDepart;
        $scope.addonArrival = $sessionStorage.step4AddonArrival;
        $scope.costs = [];
        $scope.total = [];
        $scope.addons = [];
        $scope.totalAmount = 0;

        var myAirportModal = $modal({ scope: $scope, template: 'app/modal/airport.html', show: false });
        $scope.loadAirportDetail = function (airportCode, airportName) {
            $scope.airportCode = airportCode;
            $scope.airportName = airportName;
            myAirportModal.show();
        };
        $scope.closeAirport = function () {
            myAirportModal.hide();
        };
        var myInfomationModal = $modal({ scope: $scope, template: 'app/modal/information.html', show: false });
        $scope.loadFare = function (fareClass) {
            FareRule.query({
                languagecode: $sessionStorage.languageCode,
                infoSlug: fareClass
            }, function (data) {
                    if (data.length > 0) {
                        $scope.informationTitle = data[0].InfoTitle;
                        $scope.informationContent = $sce.trustAsHtml(data[0].InfoContent);
                    } else {
                        $scope.informationTitle = fareClass;
                        $scope.informationContent = $sce.trustAsHtml(fareClass);
                    }
                    $scope.classname = "fareclass";
                    myInfomationModal.show();
                });
        }
        $scope.closeInformation = function () {
            myInfomationModal.hide();
        };

        $scope.s2Data.forEach(function (depart, index) {
            var adultCosts = depart.selected.DiscountFareTotal * $scope.s4Data.adults.length;
            var childCosts = depart.selected.DiscountFareTotal * $scope.s4Data.children.length;
            $scope.costs.push({ adultCosts: adultCosts, childCosts: childCosts });

            var dataAddon;
            if (index == 0)
                dataAddon = $scope.addonDepart;
            else
                dataAddon = $scope.addonArrival;

            var selectedAddons_temp = [];
            var selectedAddons = [];
            var sum = 0;


            $scope.s4Data.adults.forEach(function (adult, adultIndex) {
                for (var g = 0; g < dataAddon.length; g++) {
                    var dataAddon_ = dataAddon[g];
                    for (var i = 0; i < dataAddon_.length; i++) {
                        for (var j = 0; j < dataAddon_[i].Allocations.length; j++) {
                            var allocation = dataAddon_[i].Allocations[j];
                            for (var k = 0; k < adult.addons.length; k++) {
                                var addon = adult.addons[k];
                                if (addon.baggage != null) {
                                    if (allocation.ID == addon.baggage.ID) {
                                        if (allocation.MaxQuatityPerPax >= 1)
                                            selectedAddons_temp.push({
                                                count: 1,
                                                name: addon.baggage.Description,
                                                amount: addon.baggage.ChargeAmount
                                            });
                                        $sessionStorage.step4.adults[adultIndex].addons[k].baggage.legNumber = index + 1;
                                    }
                                }
                            }
                            for (var l = 0; l < addon.mealSelected.length; l++) {
                                var meal = addon.mealSelected[l];
                                if (allocation.ID == meal.ID) {
                                    var quantity = 0;
                                    if (meal.MaxQuatityPerPax >= meal.numberMeal) {
                                        quantity = meal.numberMeal;
                                    } else {
                                        quantity = meal.MaxQuatityPerPax;
                                    }
                                    if (meal.MaxQuatityPerPax >= 1) {
                                        selectedAddons_temp.push({
                                            count: quantity,
                                            name: meal.Description,
                                            amount: meal.ChargeAmount
                                        });
                                        $sessionStorage.step4.adults[adultIndex].addons[k].mealSelected[l].legNumber
                                        = index + 1;
                                    }
                                }
                            }
                        }
                    }
                }
            });

            $scope.s4Data.children.forEach(function (child, childIndex) {
                for (var g = 0; g < dataAddon.length; g++) {
                    var dataAddon_ = dataAddon[g];
                    for (var i = 0; i < dataAddon_.length; i++) {
                        for (var j = 0; j < dataAddon_[i].Allocations.length; j++) {
                            var allocation = dataAddon_[i].Allocations[j];
                            for (var k = 0; k < child.addons.length; k++) {
                                var addon = child.addons[k];
                                if (allocation.ID == addon.baggage.ID) {
                                    if (allocation.MaxQuatityPerPax >= 1)
                                        selectedAddons_temp.push({
                                            count: 1,
                                            name: addon.baggage.Description,
                                            amount: addon.baggage.ChargeAmount
                                        });
                                    $sessionStorage.step4.adults[childIndex].addons[k].baggage.legNumber = index + 1;
                                }
                            }
                            for (var l = 0; l < addon.mealSelected.length; l++) {
                                var meal = addon.mealSelected[l];
                                if (allocation.ID == meal.ID) {
                                    var quantity = 0;
                                    if (meal.MaxQuatityPerPax >= meal.numberMeal) {
                                        quantity = meal.numberMeal;
                                    } else {
                                        quantity = meal.MaxQuatityPerPax;
                                    }
                                    if (meal.MaxQuatityPerPax >= 1) {
                                        selectedAddons_temp.push({
                                            count: quantity,
                                            name: meal.Description,
                                            amount: meal.ChargeAmount
                                        });
                                        $sessionStorage.step4.adults[childIndex].addons[k].mealSelected[l].legNumber
                                        = index + 1;
                                    }
                                }
                            }
                        }
                    }
                }
            });

            for (var i = 0; i < selectedAddons_temp.length; i++) {
                sum = sum + selectedAddons_temp[i].amount;
            }

            sum = sum + adultCosts + childCosts;

            var addonSet = {};

            selectedAddons_temp.forEach(function (item, index) {
                if (!addonSet[item.name]) {
                    addonSet[item.name] = { count: item.count, total: item.amount };
                } else {
                    addonSet[item.name].count += item.count;
                    addonSet[item.name].total += item.amount;
                }
            });

            $scope.total.push(sum);
            $scope.addons.push(addonSet);
        });



        var surcharges = 0;

        for (var i = 0; i < $scope.s2Data.length; i++) {
            var surcharge_ = [];
            for (var j = 0; j < $scope.s2Data[i].leg.Surcharges.length; j++) {
                var surcharges_index = $scope.s2Data[i].leg.Surcharges[j];
                var numberOfSurcharges = 0;
                var totalOfSurcharges = 0;
                if (surcharges_index.ApplyToAdult && surcharges_index.ApplyToChild) {
                    numberOfSurcharges = $scope.s4Data.adults.length + $scope.s4Data.children.length;
                    totalOfSurcharges = numberOfSurcharges * surcharges_index.Total;
                } else if (surcharges_index.ApplyToAdult) {
                    numberOfSurcharges = $scope.s4Data.adults.length;
                    totalOfSurcharges = numberOfSurcharges * surcharges_index.Total;
                } else if (surcharges_index.ApplyToChild) {
                    numberOfSurcharges = $scope.s4Data.children.length;
                    totalOfSurcharges = (numberOfSurcharges * surcharges_index.Total);
                }
                else if (surcharges_index.ApplyToInfant) {
                    numberOfSurcharges = $scope.s4Data.infants.length;
                    totalOfSurcharges = (numberOfSurcharges * surcharges_index.Total);
                }
                surcharge_.push({
                    numberOfSurcharges: numberOfSurcharges,
                    totalOfSurcharges: totalOfSurcharges,
                    descriptionSurcharges: surcharges_index.Description
                });

                surcharges += totalOfSurcharges;
            }
            var sumSurcharge = 0;
            surcharge_.forEach(function (item, index) {
                sumSurcharge = sumSurcharge + item.totalOfSurcharges;
            });
            $scope.s2Data[i].surchargeData = surcharge_;
            $scope.s2Data[i].surchargeData.sumSurcharge = sumSurcharge;
        }

        $scope.total.push(surcharges);
        for (var i = 0; i < $scope.total.length; i++)
            $scope.totalAmount = $scope.totalAmount + $scope.total[i];

        //*100 for TotalAmount to payment
        $sessionStorage.totalAmount = $scope.totalAmount*10;
        if ($scope.s4Data.children.length > 0) {
            $scope.isShowAddtional = true;
        }
        else
        {
            $scope.isShowAddtional = false;
        }
        $scope.payment = function () {
            if ($scope.step5Form.$invalid)
                return;
            $location.path('/step6');
        };
    }

    function reNewValueUndefine(valueUndefine, valueReplace) {
        if (typeof (valueUndefine) != "undefined" && valueUndefine !== null)
            return valueUndefine;
        else
            return valueReplace;

    }
    function Step6Controller($scope, $state, $locale, $location, $modal, $sce, $sessionStorage, FareRule, Country, Province, Payment, Reservation, Itinerary) {
        $scope.state = $state.current.name;
        if (!$sessionStorage.totalAmount && ($sessionStorage.step != "step5" || $sessionStorage.step != "step6")) {
            $location.path('/step5');
        }
        $sessionStorage.step = 'step6';

        $scope.currentYear = new Date().getFullYear();
        $scope.currentMonth = new Date().getMonth() + 1;
        $scope.months = $locale.DATETIME_FORMATS.MONTH;


        $scope.paymentMethodResult = Payment.query();
        $scope.countryResult = Country.query();
        $scope.isProvince = true;

        $scope.s3Data = $sessionStorage.step3;
        /*if ($sessionStorage.step6) {
            $scope.payMethod = $scope.payMedthod = $sessionStorage.step6.paymentMethod;
            $scope.payBilling = $sessionStorage.step6.paymentBilling;
            $scope.payPurchaser = $sessionStorage.step6.paymentPurchaser;
        } else {}*/

        var loadingModal_province = $modal({
            scope: $scope,
            template: 'app/modal/loading.html',
            show: false,
            backdrop: 'static'
        });
        $scope.changeCountry = function () {
            loadingModal_province.show();
            $scope.provinceResult = Province.query({ countryCode: $scope.payBilling.billingSelectCountry }, function () {
                loadingModal_province.hide();
                $scope.isProvince = true;
            });
        };
        var myInfomationModal = $modal({ scope: $scope, template: 'app/modal/information.html', show: false });
        $scope.closeInformation = function () {
            myInfomationModal.hide();
        };

        $scope.cvvHelp = function () {
            FareRule.query({
                languagecode: $sessionStorage.languageCode,
                infoSlug: 'CVV'
            }, function (data) {
                    $scope.informationTitle = data[0].InfoTitle;
                    $scope.informationContent = $sce.trustAsHtml(data[0].InfoContent);
                    myInfomationModal.show();
                });
        }
        $scope.termsConditionsHelp = function () {
            FareRule.query({
                languagecode: $sessionStorage.languageCode,
                infoSlug: 'terms_conditions'
            }, function (data) {
                    $scope.informationTitle = data[0].InfoTitle;
                    $scope.informationContent = $sce.trustAsHtml(data[0].InfoContent);
                    $scope.classname = "terms_conditions";
                    myInfomationModal.show();
                });
        }

        $scope.payMethod = $scope.payMedthod = {};
        $scope.payBilling = {};
        $scope.payPurchaser = {};
        $scope.doIfChecked = function (check) {
            if (check) {
                var adultPrimary = $scope.s3Data.adults[0];
                $scope.payBilling.billingFirstName = adultPrimary.firstName;
                $scope.payBilling.billingLastName = adultPrimary.lastName;
                $scope.payBilling.billingAddress = adultPrimary.passengerAddress;
                $scope.payBilling.billingMobile = adultPrimary.mobile;
                $scope.payBilling.billingSelectCountry = adultPrimary.passengerSelectCountry;
                $scope.provinceResult = Province.query({ countryCode: adultPrimary.passengerSelectCountry }, function () {
                    $scope.isProvince = true;
                });
                $scope.payBilling.billingCity = adultPrimary.billingCity;
            } else {
                $scope.payBilling.billingFirstName = "";
                $scope.payBilling.billingLastName = "";
                $scope.payBilling.billingAddress = "";
                $scope.payBilling.billingMobile = "";
                $scope.payBilling.billingSelectCountry = "";
                $scope.payBilling.billingCity = "";
                $scope.isProvince = false;
            }
        }

        $scope.payNow = function () {
            $sessionStorage.step6 = {
                paymentMethod: $scope.payMethod,
                paymentBilling: $scope.payBilling,
                paymentPurchaser: $scope.payPurchaser,
            };

            if ($scope.step6Form.$invalid) {
                $scope.$broadcast('show-errors-check-validity');
                return;
            }
            var payData = $sessionStorage.step2;
            var promoData = $sessionStorage.promoCode;
            var step1Data = $sessionStorage.step1;
            var data = $sessionStorage.step4;

            var legs = [];

            for (var i = 0; i < payData.length; i++) {
                var segments = [];
                payData[i].leg.SegmentOptions.forEach(function (segment) {
                    segments.push({
                        etdLocal: segment.Flight.ETDLocal,
                        etaLocal: segment.Flight.ETALocal,
                        flightCode: segment.Flight.Number,
                        departureAirportCode: segment.Flight.DepartureAirport.Code,
                        arrivalAirportCode: segment.Flight.ArrivalAirport.Code,
                    });
                });

                legs.push({
                    adultFares: {
                        bookingClassCode: payData[i].selected.FareCategory,
                        fareCode: payData[i].selected.FareClass,
                        currencyCode: payData[i].selected.Currency.Abbreviation,
                        paxCount: data.adults.length, //(data.adults.length) + (data.children.length),
                        totalCost: payData[i].selected.Totalfare
                    },
                    childFares: {
                        bookingClassCode: payData[i].selected.FareCategory,
                        fareCode: payData[i].selected.FareClass,
                        currencyCode: payData[i].selected.Currency.Abbreviation,
                        paxCount: data.children.length,
                        totalCost: payData[i].selected.Totalfare
                    },
                    segments: segments,
                    promoCodeID: promoData[i].promoDetail.ID,
                    promoCodeSuffixID: promoData[i].promoDetail.SuffixID
                });
            }

            var userProfileName = "";
            var userProfilePassword = "";
            if ($sessionStorage.isLogin) {
                userProfileName = $sessionStorage.userProfile.ProfileName;
                userProfilePassword = $sessionStorage.userProfile.Password;
            }

            var passengers = [];

            data.adults.forEach(function (adult, index) {
                var infant_ = [];
                if (data.infants && data.infants.length != 0 && data.infants[index] != null) {
                    infant_ = [{
                        gender: data.infants[index].gender,
                        dateOfBirth: data.infants[index].birthday,
                        firstname: data.infants[index].firstName,
                        lastname: data.infants[index].lastName,
                    }];
                }

                var title = adult.gender;
                var gender = (adult.gender == "Mr") ? "Male" : "Female";
                passengers.push({
                    title: title,
                    gender: gender,
                    firstName: adult.firstName,
                    lastName: adult.lastName,
                    dateOfBirth: adult.birthday,
                    email: adult.email,
                    mobile: adult.mobile,
                    profileUsername: userProfileName,
                    profilePassword: userProfilePassword,
                    ageCategory: 'Adult',
                    personalContact: {
                        mobile: data.adults[0].mobile,
                        email: data.adults[0].email,
                        name: data.adults[0].lastName
                    },
                    passport: {
                        number: reNewValueUndefine(adult.passportNumber, '12345'),
                        expiryDate: adult.passportExpiry
                    },
                    infants: infant_,
                    flightSSRs: createFlightSSRs(adult),
                });
            });



            data.children.forEach(function (child) {
                //var gender = (child.gender == "Mr") ? "Male" : "Female";
                passengers.push({
                    gender: child.gender,
                    firstName: child.firstName,
                    lastName: child.lastName,
                    dateOfBirth: child.birthday,
                    email: child.email,
                    mobile: child.mobile,
                    profileUsername: userProfileName,
                    profilePassword: userProfilePassword,
                    ageCategory: 'Child',
                    flightSSRs: createFlightSSRs(child),
                });
            });

            function createFlightSSRs(passenger) {
                var flightSSRs = [];

                for (var i = 0; i < passenger.addons.length; i++) {
                    var dataAddon = {};
                    if (passenger.addons[i].mealSelected != null) {
                        for (var j = 0; j < passenger.addons[i].mealSelected.length; j++) {
                            var meal = passenger.addons[i].mealSelected[j];
                            var quantity = 0;
                            if (meal.MaxQuatityPerPax >= meal.numberMeal) {
                                quantity = meal.numberMeal;
                            } else {
                                quantity = meal.MaxQuatityPerPax;
                            }
                            if (meal.MaxQuatityPerPax >= 1) {
                                dataAddon = {
                                    extraAllocationIDCount: meal.ExtraAllocationIDCount,
                                    quantity: meal.numberMeal,
                                    flightAllocID: meal.ID,
                                    legNumber: meal.legNumber,
                                    extraAllocationIDs: meal.ExtraAllocationIDs,
                                }
                            flightSSRs.push(dataAddon);
                            }
                        }
                    }
                    if (passenger.addons[i].baggage != null) {
                        if (passenger.addons[i].baggage.MaxQuatityPerPax >= 1) {
                            dataAddon = {
                                extraAllocationIDCount: passenger.addons[i].baggage.ExtraAllocationIDCount,
                                quantity: 1,
                                flightAllocID: passenger.addons[i].baggage.ID,
                                legNumber: passenger.addons[i].baggage.legNumber,
                                extraAllocationIDs: passenger.addons[i].baggage.ExtraAllocationIDs,
                            }
                        flightSSRs.push(dataAddon);
                        }
                    }
                }
                return flightSSRs;
            }

            var payment = {};
            if ($scope.payMedthod.paymentMethod != 'PL') {
                payment = {
                    amount: $sessionStorage.totalAmount,
                    currencyCode: step1Data.s1CurrencyCode,
                    paymentType: "CreditCard",
                    creditCardPayment: {
                        address: {
                            addr1: $scope.payBilling.billingAddress,
                            city: $scope.payBilling.billingCity,
                            countryCode: $scope.payBilling.billingSelectCountry,
                            provinceAbbreviation: $scope.payBilling.billingCity,
                            postalCode: 'NA',
                        },
                        expiryMonth: $scope.payMethod.month,
                        expireYear: $scope.payMethod.year,
                        cvv: $scope.payMethod.payCVV,
                        number: $scope.payMethod.payCardNumber,
                        firstName: $scope.payMedthod.payFirstName,
                        lastName: $scope.payMedthod.payLastName,
                        email: $scope.payPurchaser.purchaserEmail,
                        typeCode: $scope.payMedthod.paymentMethod,
                    }
                };
            } else {
                payment = {
                    amount: $sessionStorage.totalAmount,
                    currencyCode: step1Data.s1CurrencyCode,
                    paymentType: "Hold"
                    //,
                    //thirdPartyPayment: {
                    //    amount: $sessionStorage.totalAmount,
                    //    currencyCode: step1Data.s1CurrencyCode,
                    //    languageCode: 'EN',
                    //    typeCode:"PL"

                    //}
                };
            }

            var loadingModal = $modal({
                scope: $scope,
                template: 'app/modal/loading.html',
                show: true,
                backdrop: 'static'
            });

            $scope.reservationResponse = Reservation.save({
                legs: legs,
                passengers: passengers,
                payment: payment,
            }, function (data) {
                    $sessionStorage.step7 = data;
                    if (data.OperationSucceeded) {
                        var itinerary = Itinerary.get({
                            reservationNumber: data.ReservationInvoice.ReservationNumber,
                            languageCode: 'en',
                        }, function (boolreturn) {
                                loadingModal.hide();
                                $location.path('/step7');
                            });

                    } else {
                        alert(data.OperationMessage);
                        return false;
                    }
                });
        };
    }

    function Step7Controller($scope, $state, $location, $sessionStorage, $localStorage, Statistics) {
        $scope.state = $state.current.name;
        if (!$sessionStorage.step7 && ($sessionStorage.step != "step6" || $sessionStorage.step != "step7")) {
            $location.path('/step6');
        }
        $sessionStorage.step = "step7";
        $scope.s1Data = $sessionStorage.step1;
        $scope.s6Data = $sessionStorage.step6;

        if ($scope.s6Data.paymentMethod.paymentMethod == "PL") {
            $scope.paymentStatus = "Booking Successful";
            var dateDepart = moment($scope.s1Data.s1DateStart);
            var onHoldHours = dateDepart.diff(moment(), "hours");
            if (onHoldHours >= 24) {
                onHoldHours = 24;
            }
            var canceledHours = moment().add(onHoldHours, 'hours');
            $scope.onHoldHours = onHoldHours;
            $scope.cancelledDate = canceledHours.toDate();
        }
        else {

            $scope.paymentStatus = "Payment Successful";
            Statistics.save({
                Booking: 0,
                Checkin: 0,
                flightStatus: 0,
                Member: 0,
                Inforamtion: 0,
                Language: 0,
                completeBooking: 1,
            }, function (data) {
                })
        }

        $scope.totalAmount = $sessionStorage.step7.BookingResult.PaymentAmount;
        $scope.reservationNumber = $sessionStorage.step7.ReservationInvoice.ReservationNumber;
        //$sessionStorage.$reset();
        resetBooking($sessionStorage);
        $scope.toHome = function () {
            $location.path('/home');
        };
    }

    function FinishController($scope, $state, $location) {
        $scope.state = $state.current.name;
    }


    function AirportListFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/airport/:airportId", { airportId: '@id' });
    }

    function TravelOptionFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/travelOption");
    }

    function FareRuleFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/farerule");
    }

    function AddonFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/addon");
    }

    function CountryFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/country");
    }

    function ProvinceFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/province");
    }

    function PaymentFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/paymentmethod");
    }

    function ItineraryFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/itinerary");
    }

    function ReservationFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/reservation");
    }
    function StatisticsFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/Statistics");
    }
    //farerule? languageCode = EN & infoSlug = CVV
    // Register controllers to angular module

    angular.module('gms')
        .controller('BookingFlightController', BookingFlightController)
        .controller('Step1Controller', Step1Controller)
        .controller('Step2Controller', Step2Controller)
        .controller('Step3Controller', Step3Controller)
        .controller('Step4Controller', Step4Controller)
        .controller('Step5Controller', Step5Controller)
        .controller('Step6Controller', Step6Controller)
        .controller('Step7Controller', Step7Controller)
        .controller('FinishController', FinishController)

        .factory('AirportList', AirportListFactory)
        .factory('TravelOption', TravelOptionFactory)
        .factory('FareRule', FareRuleFactory)
        .factory('Addon', AddonFactory)
        .factory('Payment', PaymentFactory)
        .factory('Country', CountryFactory)
        .factory('Province', ProvinceFactory)
        .factory('Reservation', ReservationFactory)
        .factory('Statistics', StatisticsFactory)
        .factory('Itinerary', ItineraryFactory);
}