﻿/// <reference path="../../../Scripts/typings/angularjs/angular.d.ts"/>
module Language {
    // Language Controller
    class LanguageController {
        constructor($scope, $state, $location, $translate, $sessionStorage, Language, Statistics) {
            $scope.state = $state.current.name;
            Statistics.save({
                Booking: 0,
                Checkin: 0,
                flightStatus: 0,
                Member: 0,
                Inforamtion: 0,
                Language: 1,
                completeBooking: 0,
            }, function (data) {
                })
            Language.query({}, function (data) {
                $scope.languageList = data;
            });
            $scope.chooseLanguage = function (languageCode) {
                $translate.use(languageCode);
                $sessionStorage.languageCode = languageCode;
                $location.path('/home');
            };
        }
    }
    function LanguageFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/language");
    }
    function StatisticsFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/Statistics");
    }
    // Register controllers to angular module
    angular.module('gms')
        .controller('LanguageController', LanguageController)
        .factory('Statistics', StatisticsFactory)
        .factory('Language', LanguageFactory);
}