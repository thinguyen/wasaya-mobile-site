﻿/// <reference path="../../../Scripts/typings/angularjs/angular.d.ts"/>
module Information {
    // Infomation Controller
    class InformationController {
        constructor($scope, $state, $location, $sessionStorage, Infomation, Statistics) {
            $scope.state = $state.current.name;
            Statistics.save({
                Booking: 0,
                Checkin: 0,
                flightStatus: 0,
                Member: 0,
                Inforamtion: 1,
                Language: 0,
                completeBooking: 0,
            }, function (data) {
                })
            Infomation.query({ languagecode: $sessionStorage.languageCode }, function (data) {
                $scope.infomationList = data;
            });
            $scope.choosePage = function (infomationSlug) {
                $sessionStorage.pageSlug = infomationSlug;
                $location.path('/infomationdetail');
            };
        }
    }

    class InfomationDetailController {
        constructor($scope, $state, $location, $sessionStorage, $sce, Infomation) {
            $scope.state = $state.current.name;
            Infomation.query({
                languagecode: $sessionStorage.languageCode,
                infoSlug: $sessionStorage.pageSlug
            }, function (data) {
                    $scope.titleInfomation = data[0].InfoTitle;
                    $scope.contentInfomation = $sce.trustAsHtml(data[0].InfoContent);
                });
        }
    }
    class ContactusController {
        constructor($scope, $state, $location, $sessionStorage, $sce, contact) {
            $scope.state = $state.current.name;
            $scope.contactPhone = $sce.trustAsHtml(contact[0].ContactPhone);
            $scope.contactAddress = $sce.trustAsHtml(contact[0].ContactAddress);
            $scope.contactfor = $sce.trustAsHtml(contact[0].Contactfor);
            $scope.contactContent = $sce.trustAsHtml(contact[0].ContactContent);

            $scope.titleInfomation = contact[0].InfoTitle;
            $scope.contentInfomation = $sce.trustAsHtml(contact[0].InfoContent);

            $scope.map = {
                center: {
                    latitude: contact[0].ContactLat,
                    longitude: contact[0].ContactLong
                },
                zoom: 17
            };
            $scope.options = { scrollwheel: false };
            $scope.marker = {
                coords: {
                    latitude: contact[0].ContactLat,
                    longitude: contact[0].ContactLong
                },
                show: false,
                id: 0
            };

            $scope.title = contact[0].ContactTitle;


        }
    }
    function InfomationFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/informationpage");
    }
    function ContactFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/contact");
    }
    function StatisticsFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/Statistics");
    }
    // Register controllers to angular module
    angular.module('gms')
        .controller('InformationController', InformationController)
        .controller('InfomationDetailController', InfomationDetailController)
        .controller('ContactusController', ContactusController)
        .factory('Infomation', InfomationFactory)
        .factory('Statistics', StatisticsFactory)
        .factory('Contact', ContactFactory);
}