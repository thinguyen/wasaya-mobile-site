﻿/// <reference path="../../../Scripts/typings/angularjs/angular.d.ts"/>
module Core {
    // Core Controller
    export class CoreController {
        constructor($scope, $state, $location, $window, $translate, $sessionStorage, $anchorScroll) {
            //change header for homepage or page
            $scope.state = $state.current.name;
            if ($sessionStorage.languageCode) {
                $translate.use($sessionStorage.languageCode);
            }

            $scope.toogleDropdown = function () {
                $scope.dropdownOpenned = !$scope.dropdownOpenned;
            };
            $scope.$on('$stateChangeStart', function (event, toState) {
                $scope.state = toState.name;
                $anchorScroll();
                switch ($scope.state) {
                    case 'information':
                        $translate(['DATA_TITLE_INFORMATION']).then(function (translation) {
                            $scope.title_name = translation.DATA_TITLE_INFORMATION;
                        });
                        break;
                    case 'aboutus':
                        $translate(['DATA_TITLE_ABOUTUS']).then(function (translation) {
                            $scope.title_name = translation.DATA_TITLE_ABOUTUS;
                        });
                        //$scope.title_name = "About Intelisys";
                        break;
                    case 'contactus':
                        $translate(['DATA_TITLE_CONTACTUS']).then(function (translation) {
                            $scope.title_name = translation.DATA_TITLE_CONTACTUS;
                        });
                        //$scope.title_name = "Contact Intelisys";
                        break;
                    case 'language':
                        $translate(['DATA_TITLE_LANGUAGE']).then(function (translation) {
                            $scope.title_name = translation.DATA_TITLE_LANGUAGE;
                        });
                        //$scope.title_name = "Language";
                        break;
                    case 'step1':
                        $translate(['DATA_TITLE_SEARCH_FLIGHT']).then(function (translation) {
                            $scope.title_name = translation.DATA_TITLE_SEARCH_FLIGHT;
                        });
                        //$scope.title_name = "Search Flight";
                        break;
                    case 'step2':
                        $translate(['DATA_TITLE_SELECT_FLIGHT']).then(function (translation) {
                            $scope.title_name = translation.DATA_TITLE_SELECT_FLIGHT;
                        });
                        //$scope.title_name = "Select Flight";
                        break;
                    case 'step3':
                        $translate(['DATA_TITLE_PASSENGER_DETAILS']).then(function (translation) {
                            $scope.title_name = translation.DATA_TITLE_PASSENGER_DETAILS;
                        });
                        //$scope.title_name = "Passenger Details";
                        break;
                    case 'step4':
                        $translate(['DATA_TITLE_ADDITIONAL_SERVICES']).then(function (translation) {
                            $scope.title_name = translation.DATA_TITLE_ADDITIONAL_SERVICES;
                        });
                        //$scope.title_name = "Additional Services";
                        break;
                    case 'step5':
                        $translate(['DATA_TITLE_BOOKING_DETAIL']).then(function (translation) {
                            $scope.title_name = translation.DATA_TITLE_BOOKING_DETAIL;
                        });
                        //$scope.title_name = "Booking Detail";
                        break;
                    case 'step6':
                        $translate(['DATA_TITLE_PAYMENT_DETAIL']).then(function (translation) {
                            $scope.title_name = translation.DATA_TITLE_PAYMENT_DETAIL;
                        });
                        //$scope.title_name = "Payment Details";
                        break;
                    case 'step7':
                        $translate(['DATA_TITLE_CONFIRMED']).then(function (translation) {
                            $scope.title_name = translation.DATA_TITLE_CONFIRMED;
                        });
                        //$scope.title_name = "Confirmed";
                        break;
                    case 'finish':
                        $translate(['DATA_TITLE_FINISH']).then(function (translation) {
                            $scope.title_name = translation.DATA_TITLE_FINISH;
                        });
                        //$scope.title_name = "Confirmed";
                        break;
                    case 'checkin':
                        $translate(['DATA_TITLE_CHECK_IN']).then(function (translation) {
                            $scope.title_name = translation.DATA_TITLE_CHECK_IN;
                        });
                        //$scope.title_name = "Check In";
                        break;
                    case 'checkinstep2':
                        $translate(['DATA_TITLE_CHOOSE_YOUR_FLIGHT']).then(function (translation) {
                            $scope.title_name = translation.DATA_TITLE_CHOOSE_YOUR_FLIGHT;
                        });
                        //$scope.title_name = "Choose Your Flight";
                        break;
                    case 'checkinstep3':
                        $translate(['DATA_TITLE_PASSENGER_LIST']).then(function (translation) {
                            $scope.title_name = translation.DATA_TITLE_PASSENGER_LIST;
                        });
                        //$scope.title_name = "Passenger List";
                        break;
                    case 'checkinstep3_1':
                        $translate(['DATA_TITLE_SEAT_SELECTION']).then(function (translation) {
                            $scope.title_name = translation.DATA_TITLE_SEAT_SELECTION;
                        });
                        //$scope.title_name = "Seat Selection";
                        break;
                    case 'checkinstep3_2':
                        $translate(['DATA_TITLE_PASSENGER_LIST']).then(function (translation) {
                            $scope.title_name = translation.DATA_TITLE_PASSENGER_LIST;
                        });
                        //$scope.title_name = "Passenger List";
                        break;
                    case 'checkinstep4':
                        $translate(['DATA_TITLE_BOARDING_PASS']).then(function (translation) {
                            $scope.title_name = translation.DATA_TITLE_BOARDING_PASS;
                        });
                        //$scope.title_name = "Boarding Pass";
                        break;
                    case 'flightstatus':
                        $translate(['DATA_TITLE_FLIGHT_STATUS']).then(function (translation) {
                            $scope.title_name = translation.DATA_TITLE_FLIGHT_STATUS;
                        });
                        //$scope.title_name = "Flight Status";
                        break;
                    case 'flightstatus2':
                        $translate(['DATA_TITLE_FLIGHT_STATUS']).then(function (translation) {
                            $scope.title_name = translation.DATA_TITLE_FLIGHT_STATUS;
                        });
                        //$scope.title_name = "Flight Status";
                        break;
                    case 'flightstatus3':
                        $translate(['DATA_TITLE_VIEW_SEAT']).then(function (translation) {
                            $scope.title_name = translation.DATA_TITLE_VIEW_SEAT;
                        });
                        //$scope.title_name = "View Seat";
                        break;
                    case 'user':
                        $scope.title_name = $sessionStorage.userProfile.PaxProfile.Lastname;
                        break;
                    case 'login':
                        $translate(['DATA_TITLE_MEMBER_LOGIN']).then(function (translation) {
                            $scope.title_name = translation.DATA_TITLE_MEMBER_LOGIN;
                        });
                        //$scope.title_name = "Member Login";
                        break;
                    case 'register':
                        $translate(['DATA_TITLE_CREATE_ACCOUNT']).then(function (translation) {
                            $scope.title_name = translation.DATA_TITLE_CREATE_ACCOUNT;
                        });
                        //$scope.title_name = "Create Account";
                        break;
                    case 'changepassword':
                        $translate(['DATA_TITLE_CHANGE_PASSWORD']).then(function (translation) {
                            $scope.title_name = translation.DATA_TITLE_CHANGE_PASSWORD;
                        });
                        //$scope.title_name = "Change Password";
                        break;
                    case 'accountcreated':
                        $translate(['DATA_TITLE_ACCOUNT_CREATED']).then(function (translation) {
                            $scope.title_name = translation.DATA_TITLE_ACCOUNT_CREATED;
                        });
                        //$scope.title_name = "Account Created";
                        break;
                    case 'updateprofile':
                        $translate(['DATA_TITLE_UPDATE_PROFILE']).then(function (translation) {
                            $scope.title_name = translation.DATA_TITLE_UPDATE_PROFILE;
                        });
                        //$scope.title_name = "Update Profile";
                        break;
                    case 'reservationhistory':
                        $translate(['DATA_TITLE_RESERVATION_HISTORY']).then(function (translation) {
                            $scope.title_name = translation.DATA_TITLE_RESERVATION_HISTORY;
                        });
                        //$scope.title_name = "Reservation History";
                        break;
                    case 'reservationdetail':
                        $translate(['DATA_TITLE_RESERVATION_DETAIL']).then(function (translation) {
                            $scope.title_name = translation.DATA_TITLE_RESERVATION_DETAIL;
                        });
                        //$scope.title_name = "Reservation Detail";
                        break;
                    default:
                        $translate(['DATA_SEARCH_RESERVATION']).then(function (translation) {
                            $scope.title_name = translation.DATA_SEARCH_RESERVATION;
                        });
                       
                }
            });

        }
    }
    // Head Controller
    export class HeadController {
        constructor($scope, $state, $location) {
            $scope.favicon = "http://www.wasaya.com/favicon.ico";
            $scope.title = "Wasaya Airways Mobile Site";
            $scope.type = "cms";
            $scope.desc = "Wasaya Airways Mobile Site";
            $scope.url = "URL";
        }
    }
    export function toHomeController($scope, $window, $location) {
        $scope.toHome = function () {
            $location.path('/home');
        };
    }
    export function SliderController($scope) {
        var base = '';
        $scope.pictures = 'app/img/banner.jpg';
        //$scope.pictures = 'app/img/viaair/banner_img.jpg';
        //$scope.pictures = 'app/img/cma/banner_img.jpg';
        //$scope.pictures = 'app/img/pasco/banner_img_pasco.jpg';

        /*$scope.pictures = [
            { src: base + 'app/img/banner.png' },
            { src: base + 'app/img/banner.png' },
            { src: base + 'app/img/banner.png' }
        ];*/
        //viaair
        /*$scope.pictures = [
            { src: base + 'app/img/banner.png' },
            { src: base + 'app/img/banner.png' },
            { src: base + 'app/img/banner.png' }
        ];*/
        //cma
        /*$scope.pictures = [
            { src: base + 'app/img/cma/banner_img.jpg' },
            { src: base + 'app/img/cma/banner_img.jpg' },
            { src: base + 'app/img/cma/banner_img.jpg' }
        ];*/
        //pasco
        /*$scope.pictures = [
            { src: base + 'app/img/pasco/banner_img_pasco.jpg' },
            { src: base + 'app/img/pasco/banner_img_pasco.jpg' },
            { src: base + 'app/img/pasco/banner_img_pasco.jpg' }
        ];*/
    }

    // Register controllers to angular module
    angular.module('gms').controller('CoreController', CoreController)
        .controller('HeadController', HeadController)
        .controller('toHomeController', toHomeController)
        .controller('SliderController', SliderController);
}