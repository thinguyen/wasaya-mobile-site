﻿/// <reference path="../../../Scripts/typings/angularjs/angular.d.ts"/>
module FlightStatus {
    function resetFlightStatus(sessionStorage) {
        delete sessionStorage.flightstatusStep;
        delete sessionStorage.flightstatusStep1;
        delete sessionStorage.flightstatusStep2DataTotal;
    }
    class FlightStatusStep1Controller {
        constructor($sessionStorage, $scope, $state, $location, $translate, $modal, AirportList) {
            //set current name to state
            $scope.state = $state.current.name;
            //set current step to session
            $sessionStorage.flightstatusStep = "flightstatusstep1";
            //set origin and destination null when start
            $scope.optionOrigin = null;
            $scope.optionDestination = null;
            //loading
            var loadingModal = $modal({
                scope: $scope,
                template: 'app/modal/loading.html',
                show: true,
                backdrop: 'static'
            });
            //load origin list from service
            AirportList.query(function (data) {
                $scope.originResult = data;
                loadingModal.hide();
            });

            //when origin change call function to load destination
            $scope.loadDestination = function () {
                if (typeof ($scope.optionOrigin) != "undefined" || $scope.optionOrigin != null) {
                    loadingModal.show();
                    //load destination from service with Code Origin
                    AirportList.get({ id: $scope.optionOrigin }, function (data) {
                        loadingModal.hide();
                        //load destination list from service return
                        $scope.destinationResult = data.ArrivalAirports;
                        //set currency code and from code, from name
                        $scope.S1CurrencyCode = data.Currency.Abbreviation;
                        $sessionStorage.s1FromCode = data.DepartureAirport.Code;
                        $sessionStorage.s1FromName = data.DepartureAirport.Name;
                    });
                }
            };

            //set default value for datestart, dateend, roundtrip, adults,children, infant
            $scope.today = new Date().toISOString().substr(0, 10);
            $scope.departDateCheck = new Date().toISOString().substr(0, 10);
            $scope.dateStart = new Date();
            $scope.dateEnd = new Date();
            //check if session of step1 is vaild
            if ($sessionStorage.flightstatusStep1) {
                //set date from session step 1 to field
                var flightstatusStep1 = $sessionStorage.flightstatusStep1;

                $scope.reservationCodeValue = flightstatusStep1.s1ReservationCode;
                $scope.firstNameValue = flightstatusStep1.s1FirstName;
                $scope.lastNameValue = flightstatusStep1.s1LastName;

            }
            //function button Check In click on Step 1
            $scope.flightStatus = function () {
                //check data of step1Form invalid
                if ($scope.step1Form.$invalid) {
                    //broadcast field invalid
                    $scope.$broadcast('show-errors-check-validity');
                    return;
                }

                //create object data with data from step 1
                var data = {
                    s1Origin: $scope.optionOrigin,
                    s1Destination: $scope.optionDestination,
                    s1DateStart: $scope.dateStart,
                    s1FlightNumber: $scope.flightNumber
                  
                };
              
                //set data step 1 to sesstion
                $sessionStorage.flightstatusStep1 = data;
                //redirect to step 2
                $location.path('/flightstatus2');
            };
        }
    }
     //itemFlight.In_Local = moment(itemFlight.In_Local).format("HH:mm");
    //itemFlight.Off_Local = moment(itemFlight.Off_Local).format("hh:mm");

    //itemFlight.On_Local = moment(itemFlight.On_Local).format("HH:mm");
    //itemFlight.Out_Local = moment(itemFlight.Out_Local).format("HH:mm");

    //itemFlight.ScheduledETA_Local = moment(itemFlight.ScheduledETA_Local).format("HH:mm");
    //itemFlight.ScheduledETD_Local = moment(itemFlight.ScheduledETD_Local).format("HH:mm");
    //if (moment(itemFlight.ScheduledETD_Local).isBefore(itemFlight.ScheduledETA_Local, 'day')) {
    //    itemFlight.scheduleArrivalNextDay = moment(itemFlight.ScheduledETA_Local).format('MMM D');
    //}

    //if (moment(itemFlight.ScheduledETD_Local).isBefore(itemFlight.ETD_Local, 'day')) {
    //    itemFlight.actualDepartureNextDay = moment(itemFlight.ETD_Local).format('MMM D');
    //}

    //if (moment(itemFlight.ScheduledETD_Local).isBefore(itemFlight.ETA_Local, 'day')) {
    //    itemFlight.actualArrivalNextDay = moment(itemFlight.ETA_Local).format('MMM D');
    //}








    //Process flightstatus data
    function ProcessFlightStatusData(itemFlights, etd_StartRange, etd_EndRange) {
        for (var i = 0; i < itemFlights.length; i++) {
            var itemFlight = itemFlights[i];
            var currentTime = moment().utc().format('YYYY-MM-DDTHH:mm:ss');
            // get flight status: ontime, delayed, departed
            itemFlight.ETD_Zulu = moment(itemFlight.ETD_Zulu).format("YYYY-MM-DDTHH:mm:ss");
            itemFlight.ETA_Zulu = moment(itemFlight.ETA_Zulu).format("YYYY-MM-DDTHH:mm:ss");
            // check if ontime
            //console.log("current time:" + currentTime + "- Gio di:" + itemFlight.ETD_Zulu);
            //console.log(itemFlight.ArrivalDelay + itemFlight.DepartureDelay);

            if (moment(currentTime).isBefore(itemFlight.ETD_Zulu)) {
                //if (itemFlight.ArrivalDelay == 0 && itemFlight.DepartureDelay == 0 && etd_StartRange < moment(itemFlight.ETD_Local).format("HH:mm") && itemFlight.ETD_Local < moment(etd_EndRange).format("HH:mm")) {
                if (etd_StartRange < itemFlight.ETD_Local && itemFlight.ETD_Local < etd_EndRange) {
                    if (itemFlight.ArrivalDelay == 0 && itemFlight.DepartureDelay == 0) {
                        itemFlight.status_class = "ontime";
                    }
                    else {
                        itemFlight.status_class = "delayed";
                    }
                } else {
                    itemFlights.splice(i, 1);
                    i - 1;
                }
            } else {
                itemFlight.status_class = "departed";
            }
            itemFlight.ETD_Local = moment(itemFlight.ETD_Local).format("HH:mm");
            itemFlight.ETA_Local = moment(itemFlight.ETA_Local).format("HH:mm");
        }
        
        //console.log(itemFlights);
        return itemFlights;
                     
    }
    class FlightStatusStep2Controller {
        constructor($sessionStorage, $scope, $state, $location, $modal, $translate, FlightStatus, Statistics) {
            //set current name to state
            $scope.state = $state.current.name;
            Statistics.save({
                Booking: 0,
                Checkin: 0,
                flightStatus: 1,
                Member: 0,
                Inforamtion: 0,
                Language: 0,
                completeBooking: 0,
            }, function (data) {
                })
            if (!$sessionStorage.flightstatusStep1 && ($sessionStorage.flightstatusStep != "flightstatusstep1" || $sessionStorage.flightstatusStep != "flightstatusstep2")) {
                $location.path('/checkin');
            }
            //set current step to session
            $sessionStorage.checkinstep = "flightstatusstep2";
            var flightstatusStep1 = $sessionStorage.flightstatusStep1;
            var loadingModal = $modal({ scope: $scope, template: 'app/modal/loading.html', show: true });
            var etd_StartRange = moment(flightstatusStep1.s1DateStart).format("YYYY-MM-DDT00:00:00");
            var etd_EndRange = moment(flightstatusStep1.s1DateStart).format("YYYY-MM-DDT23:59:59");
            $scope.flightStatus = FlightStatus.get({
                arrivalAirportCode:flightstatusStep1.s1Origin,
                departureAirportCode:flightstatusStep1.s1Destination,
                etd_EndRange: etd_EndRange,
                etd_StartRange: etd_StartRange,
                flightNumber:flightstatusStep1.s1FlightNumber
            }, function (data) {
                loadingModal.hide();
                if (data.Items.length > 0) {
                    data.Items = ProcessFlightStatusData(data.Items, etd_StartRange, etd_EndRange);
                    $scope.itemFlights = data.Items;
                }
                else {
                    $translate(['DATA_MESSAGE_FLIGHT_NOT_FOUND']).then(function (translation) {
                        alert(translation.DATA_MESSAGE_FLIGHT_NOT_FOUND);
                    });
                }
                });

            $scope.toHome = function () {
                resetFlightStatus($sessionStorage);
                $location.path('/flightstatus');
            };
        }
    }
    class FlightStatusStep3Controller {
        constructor($sessionStorage, $scope, $state, $location, ReservationInvoice) {
            //set current name to state
            $scope.state = $state.current.name;
            if (!$sessionStorage.flightstatusStep2DataTotal && ($sessionStorage.flightstatusStep != "flightstatusstep2" || $sessionStorage.flightstatusStep != "flightstatusstep3")) {
                $location.path('/flightstatus2');
            }
            //set current step to session
            $sessionStorage.flightstatusStep = "flightstatusstep3";
            var flightstatusStep2 = $sessionStorage.flightstatusStep2DataTotal;
            $scope.flightstatusStep2 = flightstatusStep2;
            $scope.toHome = function () {
                resetFlightStatus($sessionStorage);
                $location.path('/flightstatus');
            };
        }
    }


    function FlightStatusFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/FlightStatus");
    }
    function StatisticsFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/Statistics");
    }
    // Register controllers to angular 
    angular.module('gms')
        .controller('FlightStatusStep1Controller', FlightStatusStep1Controller)
        .controller('FlightStatusStep2Controller', FlightStatusStep2Controller)
        .controller('FlightStatusStep3Controller', FlightStatusStep3Controller)
        .factory('Statistics', StatisticsFactory)
        .factory('FlightStatus', FlightStatusFactory);

}
