﻿/// <reference path="../../../Scripts/typings/angularjs/angular.d.ts"/>
module CheckIn {
    function resetCheckIn(sessionStorage) {
        delete sessionStorage.checkinstep;
        delete sessionStorage.checkinStep1;
        delete sessionStorage.checkinStep2DataTotal;
        delete sessionStorage.checkinStep2;
        delete sessionStorage.checkinStep3;
        delete sessionStorage.checkinStep3_1;
        delete sessionStorage.checkinStep3_2;
        delete sessionStorage.seats;
    }
    class CheckInStep1Controller {
        constructor($sessionStorage, $scope, $state, $location, $modal, AirportList, ReservationInvoice, $translate, Statistics) {
            //set current name to state
            $scope.state = $state.current.name;
            //set current step to session
            $sessionStorage.checkinstep = "checkinstep1";
            Statistics.save({
                Booking: 0,
                Checkin: 1,
                flightStatus: 0,
                Member: 0,
                Inforamtion: 0,
                Language: 0,
                completeBooking: 0,
            }, function (data) {
                })
            //check if session of step1 is vaild
            if ($sessionStorage.checkinStep1) {
                //set date from session step 1 to field
                var checkinStep1 = $sessionStorage.checkinStep1;

                $scope.reservationCodeValue = checkinStep1.s1ReservationCode;
                $scope.firstNameValue = checkinStep1.s1FirstName;
                $scope.lastNameValue = checkinStep1.s1LastName;
            }
            //function button Check In click on Step 1
            $scope.checkInClick = function () {
                //create object data with data from step 1
                var dataRes = {
                    s1ReservationCode: $scope.reservationCodeValue,
                    s1LastName: $scope.lastNameValue,
                    s1FirstName: $scope.firstNameValue
                };

                //check data of step1Form invalid
                if ($scope.step1Form.$invalid) {
                    //broadcast field invalid
                    $scope.$broadcast('show-errors-check-validity');
                    return;
                } else {
                    var loadingModal = $modal({
                        scope: $scope,
                        template: 'app/modal/loading.html',
                        show: true,
                        backdrop: 'static'
                    });
                    $scope.reservationInvoice = ReservationInvoice.get({
                        reservationNumber: $scope.reservationCodeValue,
                        paxLastName: $scope.lastNameValue,
                        paxFirstName: $scope.firstNameValue
                    }, function (data) {
                            loadingModal.hide();
                            var reservationInvoiceResult = data;
                            var countLegisCancelled = 0;
                            var countsegment = 0;
                            if (typeof (reservationInvoiceResult.Legs) == 'undefined' && reservationInvoiceResult.Legs == null) {
                                $translate(['DATA_MESSAGE_RESERVATION_NULL']).then(function (translation) {
                                    alert(translation.DATA_MESSAGE_RESERVATION_NULL);
                                });
                                return;
                            }
                            if (reservationInvoiceResult[0] == "n") {
                                //set data step 1 to sesstion
                                $sessionStorage.reservationInvoiceResult = data;
                                $sessionStorage.checkinStep1 = dataRes;

                                // redirect to step 2
                                $location.path('/checkinstep2');
                                $scope.vaildResevation = false;
                            } else {
                                for (var i = 0; i < reservationInvoiceResult.Legs.length; i++) {
                                    var leg = reservationInvoiceResult.Legs[i];
                                    for (var j = 0; j < leg.Segments.length; j++) {
                                        countsegment += 1;
                                        var segment = leg.Segments[j];
                                        if (segment.ResStatus == "Cancelled") {
                                            countLegisCancelled += 1;
                                        }
                                    }
                                }
                                if (countsegment == countLegisCancelled) {
                                    $translate(['DATA_MESSAGE_SEGMENT_CANCELLED']).then(function (translation) {
                                        alert(translation.DATA_MESSAGE_SEGMENT_CANCELLED);
                                    });
                                    return;
                                } else if (countsegment > countLegisCancelled) {
                                    //set data step 1 to sesstion
                                    $sessionStorage.reservationInvoiceResult = data;
                                    $sessionStorage.checkinStep1 = dataRes;

                                    // redirect to step 2
                                    $location.path('/checkinstep2');
                                }
                            }
                        });
                }
            };
        }
    }
    class CheckInStep2Controller {
        constructor($sessionStorage, $scope, $state, $location, $modal, $sce, FareRule, ReservationInvoice) {
            //set current name to state
            $scope.state = $state.current.name;
            if (!$sessionStorage.checkinStep1 && ($sessionStorage.checkinstep != "checkinstep1" || $sessionStorage.checkinstep != "checkinstep2")) {
                $location.path('/checkin');
            }
            //set current step to session
            $sessionStorage.checkinstep = "checkinstep2";

            var checkinStep1 = $sessionStorage.checkinStep1;
            var loadingModal = $modal({
                scope: $scope,
                template: 'app/modal/loading.html',
                show: true,
                backdrop: 'static'
            });

            function caculatorSegment(legOption) {
                legOption.SegmentsTemp = [];
                angular.copy(legOption.Segments, legOption.SegmentsTemp);
                var airCraftIndex = 0;
                var countStop = 1;

                if (legOption.SegmentsTemp.length == 1) {
                    var segment = legOption.SegmentsTemp;
                    segment[0].TimeTransit = "";
                    segment[0].checkAirCraft = false;
                } else {
                    legOption.SegmentsTemp[0].TimeTransit = "";
                    legOption.SegmentsTemp[0].checkAirCraft = false;
                    for (var j = 1; j < legOption.SegmentsTemp.length; j++) {
                        var segment = legOption.SegmentsTemp;
                        if (segment[j].AircraftName == segment[airCraftIndex].AircraftName
                            && segment[j].Flight == segment[airCraftIndex].Flight) {
                            segment[j].checkAirCraft = true;
                            var timeeta = segment[j - 1].ArrivalLocal;
                            var timeetd = segment[j].DepartureLocal;
                            var totalMinutes = moment(timeetd).diff(timeeta, 'minutes');
                            var hours = Math.floor(totalMinutes / 60);
                            var minutes = totalMinutes % 60;
                            segment[j].TimeTransit = hours + 'h ' + minutes + 'm';
                            legOption.SegmentsTemp[0].ArrivalLocal = segment[j].ArrivalLocal;
                            legOption.SegmentsTemp[0].ArrivalAirportCode = segment[j].ArrivalAirportCode;
                            legOption.SegmentsTemp[0].ArrivalAirportName = segment[j].ArrivalAirportName;
                            segment[j].countStop = countStop;
                            countStop++;
                            airCraftIndex = j;
                        } else {
                            segment[j].checkAirCraft = false;
                            airCraftIndex = j;
                        }
                    }
                }
            }
            $scope.reservationInvoice = ReservationInvoice.get({
                reservationNumber: checkinStep1.s1ReservationCode,
                paxLastName: checkinStep1.s1LastName,
                paxFirstName: checkinStep1.s1FirstName
            }, function (data) {
                    loadingModal.hide();
                    try {
                        $scope.vaildResevation = true;
                        //$sessionStorage.reservationInvoiceResult
                        var reservationInvoiceResult = data;
                        var isCheckin = false;
                        var isPayed = false;
                        var countNoInfantNoCheckin = false;
                        for (var i = 0; i < reservationInvoiceResult.Legs.length; i++) {
                            var leg = reservationInvoiceResult.Legs[i];
                            var arrivalAirportName_ = "";
                            for (var j = 0; j < leg.Segments.length; j++) {
                                var segment = leg.Segments;
                                arrivalAirportName_ = segment[j].ArrivalAirportName;
                            }
                            leg.DepartureAirportName = leg.Segments[0].DepartureAirportName;
                            leg.ArrivalAirportName = arrivalAirportName_;

                            caculatorSegment(leg);

                            var now = moment();
                            var etdofLeg = moment(leg.Segments[0].DepartureLocal);
                            var totalHourToETD = -(moment(now).diff(etdofLeg, 'hour'));

                            var statusClass = "";
                            var status = "";
                            if (reservationInvoiceResult.ResStatus.IsCancelled == true) {
                                status = "Cancelled";
                                statusClass = "closed";
                            } else if (totalHourToETD <= 0) {
                                status = "Closed";
                                statusClass = "closed";
                            } else if (totalHourToETD > 48) {
                                status = "Waiting";
                                statusClass = "waiting";
                            } else {
                                var allChecked = true;
                                for (var k = 0; k < leg.Segments[0].PaxList.length; k++) {
                                    var pax = leg.Segments[0].PaxList[k];
                                    for (var m = 0; m < reservationInvoiceResult.Passengers.length; m++) {
                                        var passenger = reservationInvoiceResult.Passengers[m];
                                        if (pax.PaxGroupID == passenger.PaxGroupID) {
                                            if (pax.CheckinStatus == 'Not Checked-in' && passenger.Infants.length != 0) {
                                                countNoInfantNoCheckin = true;
                                                leg.countNoInfantNoCheckin = true;
                                            } else if (pax.CheckinStatus == 'Not Checked-in' && passenger.Infants.length == 0) {
                                                allChecked = false;
                                                leg.countNoInfantNoCheckin = false;
                                                break;
                                            }
                                        }
                                    }
                                }
                                if (allChecked == false || countNoInfantNoCheckin == true) {
                                    status = "Check In";
                                    statusClass = "checkin";
                                } else {
                                    status = "Checked In";
                                    statusClass = "checkedin";
                                }

                            }
                            leg.FlightNumberofLeg = i + 1;
                            leg.Status = status;
                            leg.StatusClass = statusClass;
                            leg.allChecked = allChecked;
                            if (statusClass == "checkin") {
                                isCheckin = true;
                            }
                        }
                    } catch (err) {
                        $scope.vaildResevation = false;

                    }

                    /*
                    ● Check In: If the difference of the ETD and the current time is in the allowed time for Check In.
                    ● Checked In: If all passengers have checked in before (not count passengers with infant).
                    ● Waiting: the time for check in is not ready yet.
                    ● Closed: the flight was departed.
                    */
                    $scope.isCheckin = isCheckin;
                    $scope.s2Legs = reservationInvoiceResult.Legs;
                    $scope.checkinStep2DataTotal = reservationInvoiceResult;
                    $sessionStorage.checkinStep2DataTotal = reservationInvoiceResult;
                    $scope.checkcountNoInfantNoCheckinInLeg = function (Legs) {
                        for (var i = 0; i < Legs.length; i++) {
                            var Leg = Legs[i];
                            if (Leg.countNoInfantNoCheckin == false) {
                                return false;
                            }
                        }
                        return true;
                    };

                }, function (respone) {
                    loadingModal.hide();
                    $scope.vaildResevation = false;
                });

            var myAirportModal = $modal({ scope: $scope, template: 'app/modal/airport.html', show: false });
            $scope.loadAirportDetail = function (airportCode, airportName) {
                $scope.airportCode = airportCode;
                $scope.airportName = airportName;
                myAirportModal.show();
            };
            $scope.closeAirport = function () {
                myAirportModal.hide();
            };

            var myInfomationModal_ = $modal({ scope: $scope, template: 'app/modal/information.html', show: false });
            $scope.loadFare = function (fareClass) {
                FareRule.query({
                    languagecode: $sessionStorage.languageCode,
                    infoSlug: fareClass
                }, function (data) {
                        if (data.length > 0) {
                            $scope.informationTitle = data[0].InfoTitle;
                            $scope.informationContent = $sce.trustAsHtml(data[0].InfoContent);
                        } else {
                            $scope.informationTitle = fareClass;
                            $scope.informationContent = $sce.trustAsHtml(fareClass);
                        }
                        $scope.classname = "fareclass";
                        myInfomationModal_.show();
                    });
            }
            $scope.closeInformation = function () {
                myInfomationModal_.hide();
            };

            $scope.toPassengers = function () {
                var s2_legs = $scope.s2Legs;
                for (var i = 0; i < s2_legs.length; i++) {
                    var s2_Leg = s2_legs[i];
                    if (s2_Leg.StatusClass == "checkin" && s2_Leg.allChecked == false) {
                        $sessionStorage.checkinStep2 = s2_Leg;
                        $sessionStorage.checkinStep2.Passengers = $sessionStorage.checkinStep2DataTotal.Passengers;
                        break;
                    }
                }
                $location.path('/checkinstep3');
            };

            $scope.toback = function () {
                resetCheckIn($sessionStorage);
                $location.path('/checkin');
            };
        }
    }

    class CheckInStep3Controller {
        constructor($sessionStorage, $scope, $state, $location, ReservationInvoice) {
            //set current name to state
            $scope.state = $state.current.name;
            if (!$sessionStorage.checkinStep2 && ($sessionStorage.checkinstep != "checkinstep2" || $sessionStorage.checkinstep != "checkinstep3")) {
                $location.path('/checkinstep2');
            }
            //set current step to session
            $sessionStorage.checkinstep = "checkinstep3";
            var checkinStep2 = $sessionStorage.checkinStep2;
            $scope.checkinStep2 = checkinStep2;
            var isCheckin = false;
            for (var i = 0; i < checkinStep2.Passengers.length; i++) {
                var passenger = checkinStep2.Passengers[i];
                if (passenger.Infants.length == 0) {
                    isCheckin = true;
                    break;
                }
            }
            $scope.isCheckin = isCheckin;
            $scope.selectSeat = function () {
                $sessionStorage.checkinStep3 = [];
                for (var i = 0; i < checkinStep2.Passengers.length; i++) {
                    var passenger = checkinStep2.Passengers[i];
                    if (passenger.Infants.length == 0) {
                        $sessionStorage.checkinStep3.push(passenger);
                    }
                }
                $location.path('/checkinstep3_1');
            };
            $scope.toHome = function () {
                $location.path('/home');
            };
        }
    }
    class CheckInStep31Controller {
        constructor($sessionStorage, $scope, $state, $location, $modal, SeatMap, $translate) {
            //set current name to state
            $scope.state = $state.current.name;
            if (!$sessionStorage.checkinStep3 && ($sessionStorage.checkinstep != "checkinstep3" || $sessionStorage.checkinstep != "checkinstep3_1")) {
                $location.path('/checkinstep3');
            }
            //set current step to session
            $sessionStorage.checkinstep = "checkinstep3_1";
            var checkinStep1 = $sessionStorage.checkinStep1;
            var checkinStep2 = $sessionStorage.checkinStep2;
            var checkinStep3 = $sessionStorage.checkinStep3;
            for (var i = 0; i < checkinStep3.length; i++) {
                if (!(checkinStep3[i].RowNumber) && !(checkinStep3[i].SeatNumber)) {
                    checkinStep3[i].RowNumber = "";
                    checkinStep3[i].SeatNumber = "";
                }
            }
            $scope.checkinStep3Data = checkinStep3;
            var loadingModal = $modal({
                scope: $scope,
                template: 'app/modal/loading.html',
                show: true,
                backdrop: 'static'
            });
            $scope.reservationInvoice = SeatMap.get({
                reservationNumber: checkinStep1.s1ReservationCode,
                legNumber: checkinStep2.LegNumber
            }, function (data) {
                    loadingModal.hide();
                    var seats = data.Seats;
                    $sessionStorage.seats = seats;
                    $scope.seatmap = [];
                    seats.forEach(function (seat) {
                        if ($scope.seatmap[seat.RowNumber - 1] == null)
                            $scope.seatmap[seat.RowNumber - 1] = {};
                        seat.isSelected = false;
                        for (var i = 0; i < $scope.checkinStep3Data.length; i++) {
                            if (seat.RowNumber == $scope.checkinStep3Data[i].RowNumber
                                && seat.SeatNumber == $scope.checkinStep3Data[i].SeatNumber) {
                                seat.isSelected = true;
                            }
                        }
                        $scope.seatmap[seat.RowNumber - 1][seat.SeatNumber] = seat;
                    });
                console.log(data);
                console.log($scope.seatmap);
            });
           
            var myPassengerModal = $modal({ scope: $scope, template: 'app/modal/passenger.html', show: false });
            $scope.chooseSeat = function (seat) {
                if (seat.Availability == 10 || seat.Availability == 11) {
                    myPassengerModal.show();
                    $scope.chooseSeatInMap = seat;
                } else {
                    //alert("Seat not available");
                    $translate(['DATA_MESSAGE_SEAT_NOT_AVAILABLE']).then(function (translation) {
                        alert(translation.DATA_MESSAGE_SEAT_NOT_AVAILABLE);
                    });
                }
            };
            $scope.acceptSeat = function () {
                myPassengerModal.hide();
            };
            $scope.choosePassenger = function (passenger) {
                for (var i = 0; i < $scope.checkinStep3Data.length; i++) {
                    if (passenger.PaxGroupID == $scope.checkinStep3Data[i].PaxGroupID) {
                        $scope.checkinStep3Data[i].RowNumber = $scope.chooseSeatInMap.RowNumber;
                        $scope.checkinStep3Data[i].SeatNumber = $scope.chooseSeatInMap.SeatNumber;
                    } else {
                        if ($scope.chooseSeatInMap.RowNumber == $scope.checkinStep3Data[i].RowNumber
                            && $scope.chooseSeatInMap.SeatNumber == $scope.checkinStep3Data[i].SeatNumber) {
                            $scope.checkinStep3Data[i].RowNumber = "";
                            $scope.checkinStep3Data[i].SeatNumber = "";
                        }
                    }
                }
                var _seats = $sessionStorage.seats;
                $scope.seatmap = [];
                _seats.forEach(function (seat) {

                    if ($scope.seatmap[seat.RowNumber - 1] == null)
                        $scope.seatmap[seat.RowNumber - 1] = {};
                    seat.isSelected = false;
                    for (var i = 0; i < $scope.checkinStep3Data.length; i++) {
                        if (seat.RowNumber == $scope.checkinStep3Data[i].RowNumber
                            && seat.SeatNumber == $scope.checkinStep3Data[i].SeatNumber) {
                            seat.isSelected = true;
                        }
                    }

                    $scope.seatmap[seat.RowNumber - 1][seat.SeatNumber] = seat;
                });
                $sessionStorage.checkinStep3 = $scope.checkinStep3Data;
            }
            $scope.selectSeat = function () {
                var endS3 = $sessionStorage.checkinStep3;
                var isCheck = true;
                for (var i = 0; i < endS3.length; i++) {
                    if (endS3[i].RowNumber == "" && endS3[i].SeatNumber == "") {
                        isCheck = false;
                        break;
                    }
                }
                if (isCheck == false) {
                    //alert("Please choose seat for passenger");
                    $translate(['DATA_MESSAGE_CHOOSE_SEAT_FOR_PASSENGER']).then(function (translation) {
                        alert(translation.DATA_MESSAGE_CHOOSE_SEAT_FOR_PASSENGER);
                    });
                } else {
                    $sessionStorage.checkinStep3 = endS3;
                    $sessionStorage.checkinStep3_1 = true;
                    $location.path('/checkinstep3_2');
                }
            };
        }
    }
    class CheckInStep32Controller {
        constructor($sessionStorage, $scope, $state, $location, SeatMap) {
            //set current name to state
            $scope.state = $state.current.name;
            if (!$sessionStorage.checkinStep3_1 && ($sessionStorage.checkinstep != "checkinstep3_1" || $sessionStorage.checkinstep != "checkinstep3_2")) {
                $location.path('/checkinstep3_1');
            }
            //set current step to session
            $sessionStorage.checkinstep = "checkinstep3_2";
            var checkinStep2 = $sessionStorage.checkinStep2;
            var checkinStep3 = $sessionStorage.checkinStep3

            for (var i = 0; i < checkinStep2.Passengers.length; i++) {
                var passenger = checkinStep2.Passengers[i];
                for (var j = 0; j < checkinStep3.length; j++) {
                    if (passenger.PaxGroupID == checkinStep3[j].PaxGroupID) {
                        checkinStep2.Passengers[i] = checkinStep3[j];
                    }
                }
            }
            $scope.checkinStep2 = checkinStep2;
            $scope.confirmSeat = function () {
                var checkinStep1 = $sessionStorage.checkinStep1;
                var checkinStep2 = $sessionStorage.checkinStep2;
                var checkinStep3 = $sessionStorage.checkinStep3;
                var passengers = [];
                for (var i = 0; i < checkinStep3.length; i++) {
                    var pass = checkinStep3[i];
                    var data_pass = {
                        lastname: pass.Lastname,
                        firstName: pass.Firstname,
                        rowNumber: pass.RowNumber,
                        seatNumber: pass.SeatNumber
                    }
                    passengers.push(data_pass);
                }
                $scope.reservationResponse = SeatMap.save({
                    reservationNumber: parseInt(checkinStep1.s1ReservationCode),
                    legNumber: checkinStep2.LegNumber,
                    passengers: passengers
                }, function (data) {
                        if (data.OperationSucceeded) {
                            $sessionStorage.checkinStep3_2 = true;
                            $location.path('/checkinstep4');
                        } else {
                            alert(data.OperationMessage);
                            return false;
                        }
                    });
            };
        }
    }
    class CheckInStep4Controller {
        constructor($sessionStorage, $scope, $state, $location, $modal, CheckIn) {
            //set current name to state
            $scope.state = $state.current.name;
            if (!$sessionStorage.checkinStep3_2 && ($sessionStorage.checkinstep != "checkinstep3_2" || $sessionStorage.checkinstep != "checkinstep4")) {
                $location.path('/checkinstep3_2');
            }
            //set current step to session
            $sessionStorage.checkinstep = "checkinstep4";
            var checkinStep1 = $sessionStorage.checkinStep1;
            var checkinStep2 = $sessionStorage.checkinStep2;
            var checkinStep3 = $sessionStorage.checkinStep3;

            var myAirportModal = $modal({ scope: $scope, template: 'app/modal/airport.html', show: false });
            $scope.loadAirportDetail = function (airportCode, airportName) {
                $scope.airportCode = airportCode;
                $scope.airportName = airportName;
                myAirportModal.show();
            };
            $scope.closeAirport = function () {
                myAirportModal.hide();
            };

            $scope.DepartureAirportName = checkinStep2.Segments[0].DepartureAirportName;
            for (var i = 0; i < checkinStep2.Segments.length; i++) {
                $scope.ArrivalAirportName = checkinStep2.Segments[i].ArrivalAirportName;
            }
            $scope.checkinStep1 = checkinStep1;
            $scope.checkinStep2 = checkinStep2;
            $scope.checkinStep3 = checkinStep3;
            var loadingModal = $modal({
                scope: $scope,
                template: 'app/modal/loading.html',
                show: true,
                backdrop: 'static'
            });
            CheckIn.save({
                reservationNumber: parseInt(checkinStep1.s1ReservationCode),
                legNumber: checkinStep2.LegNumber,
                passengers: checkinStep3,
                languageCode: "VI"
            }, function (data) {
                    loadingModal.hide();
                    if (data.OperationSucceeded) {
                        $scope.checked = true;
                    } else {
                        alert(data.OperationMessage);
                    }
                });
            $scope.toHome = function () {
                resetCheckIn($sessionStorage);
                $location.path('/home');
            };
        }
    }

    function FareRuleFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/farerule");
    }
    function ReservationInvoiceFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/reservation");
    }
    function SeatMapFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/seatmap");
    }
    function CheckInFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/checkin");
    }
    function StatisticsFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/Statistics");
    }


    // Register controllers to angular 
    angular.module('gms')
        .controller('CheckInStep1Controller', CheckInStep1Controller)
        .controller('CheckInStep2Controller', CheckInStep2Controller)
        .controller('CheckInStep3Controller', CheckInStep3Controller)
        .controller('CheckInStep31Controller', CheckInStep31Controller)
        .controller('CheckInStep32Controller', CheckInStep32Controller)
        .controller('CheckInStep4Controller', CheckInStep4Controller)

        .factory('FareRule', FareRuleFactory)
        .factory('ReservationInvoice', ReservationInvoiceFactory)
        .factory('SeatMap', SeatMapFactory)
        .factory('Statistics', StatisticsFactory)
        .factory('CheckIn', CheckInFactory);

}
