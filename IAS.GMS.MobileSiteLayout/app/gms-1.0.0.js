/// <reference path="../../Scripts/typings/angularjs/angular.d.ts"/>
/// <reference path="../../Scripts/typings/accounting/accounting.d.ts"/>
/// <reference path="../../Scripts/typings/jquery/jquery.d.ts"/>
'use strict';
angular.module('gms', [
    'pascalprecht.translate', 'ngResource',
    'ngStorage',
    'ui.bootstrap.showErrors',
    'ui.router',
    'gms-templates',
    'uiGmapgoogle-maps',
    'mgcrea.ngStrap']);

angular.module('gms').config(function ($locationProvider) {
    $locationProvider.html5Mode(true);
});

angular.module('gms').config(function ($translateProvider) {
    $translateProvider.useUrlLoader(Configuration.Settings.serviceUrl + "/languagephrase");

    /*$translateProvider.useStaticFilesLoader({
    prefix: 'app/js/locale-',
    //prefix: 'http://192.168.1.80:7171/Scripts/locale-',
    suffix: '.js'
    });*/
    $translateProvider.preferredLanguage('en');
});
angular.module('gms').directive('numbersOnly', function () {
    return function (scope, element, attrs) {
        var keyCode = [8, 9, 37, 39, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 110];
        element.bind("keydown", function (event) {
            //console.log(event.which);
            if ($.inArray(event.which, keyCode) == -1) {
                scope.$apply(function () {
                    scope.$eval(attrs.numbersOnly);
                    event.preventDefault();
                });
                event.preventDefault();
            }
        });
    };
});

angular.module('gms').directive('startSlider', function () {
    return {
        restrict: 'A',
        replace: true,
        template: '<ul class="bxslider">' + '<li ng-repeat="picture in pictures">' + '<img ng-src="{{picture.src}}" />' + '</li>' + '</ul>',
        link: function (scope, elm) {
            setTimeout(function () {
                elm.bxSlider({
                    auto: true,
                    pause: 2000,
                    pager: false
                });
            }, 100);
        }
    };
});

var mapping = {
    "ALL": { "text": "Lek", "uniDec": "76, 101, 107", "uniHex": "4c, 65, 6b" }, "AFN": { "text": "؋", "uniDec": "1547", "uniHex": "60b" }, "ARS": { "text": "$", "uniDec": "36", "uniHex": "24" }, "AWG": { "text": "ƒ", "uniDec": "402", "uniHex": "192" }, "AUD": { "text": "$", "uniDec": "36", "uniHex": "24" }, "AZN": { "text": "ман", "uniDec": "1084, 1072, 1085", "uniHex": "43c, 430, 43d" }, "BSD": { "text": "$", "uniDec": "36", "uniHex": "24" },
    "BBD": { "text": "$", "uniDec": "36", "uniHex": "24" }, "BYR": { "text": "p.", "uniDec": "112, 46", "uniHex": "70, 2e" }, "BZD": { "text": "BZ$", "uniDec": "66, 90, 36", "uniHex": "42, 5a, 24" }, "BMD": { "text": "$", "uniDec": "36", "uniHex": "24" }, "BOB": { "text": "$b", "uniDec": "36, 98", "uniHex": "24, 62" }, "BAM": { "text": "KM", "uniDec": "75, 77", "uniHex": "4b, 4d" }, "BWP": { "text": "P", "uniDec": "80", "uniHex": "50" }, "BGN": { "text": "лв", "uniDec": "1083, 1074", "uniHex": "43b, 432" }, "BRL": { "text": "R$", "uniDec": "82, 36", "uniHex": "52, 24" }, "BND": { "text": "$", "uniDec": "36", "uniHex": "24" }, "KHR": { "text": "៛", "uniDec": "6107", "uniHex": "17db" }, "CAD": { "text": "$", "uniDec": "36", "uniHex": "24" }, "KYD": { "text": "$", "uniDec": "36", "uniHex": "24" }, "CLP": { "text": "$", "uniDec": "36", "uniHex": "24" }, "CNY": { "text": "¥", "uniDec": "165", "uniHex": "a5" }, "COP": { "text": "$", "uniDec": "36", "uniHex": "24" }, "CRC": { "text": "₡", "uniDec": "8353", "uniHex": "20a1" }, "HRK": { "text": "kn", "uniDec": "107, 110", "uniHex": "6b, 6e" }, "CUP": { "text": "₱", "uniDec": "8369", "uniHex": "20b1" }, "CZK": { "text": "Kč", "uniDec": "75, 269", "uniHex": "4b, 10d" }, "DKK": { "text": "kr", "uniDec": "107, 114", "uniHex": "6b, 72" }, "DOP": { "text": "RD$", "uniDec": "82, 68, 36", "uniHex": "52, 44, 24" }, "XCD": { "text": "$", "uniDec": "36", "uniHex": "24" }, "EGP": { "text": "£", "uniDec": "163", "uniHex": "a3" }, "SVC": { "text": "$", "uniDec": "36", "uniHex": "24" }, "EEK": { "text": "kr", "uniDec": "107, 114", "uniHex": "6b, 72" }, "EUR": { "text": "€", "uniDec": "8364", "uniHex": "20ac" }, "FKP": { "text": "£", "uniDec": "163", "uniHex": "a3" }, "FJD": { "text": "$", "uniDec": "36", "uniHex": "24" }, "GHC": { "text": "¢", "uniDec": "162", "uniHex": "a2" }, "GIP": { "text": "£", "uniDec": "163", "uniHex": "a3" }, "GTQ": { "text": "Q", "uniDec": "81", "uniHex": "51" }, "GGP": { "text": "£", "uniDec": "163", "uniHex": "a3" }, "GYD": { "text": "$", "uniDec": "36", "uniHex": "24" }, "HNL": { "text": "L", "uniDec": "76", "uniHex": "4c" }, "HKD": { "text": "$", "uniDec": "36", "uniHex": "24" }, "HUF": { "text": "Ft", "uniDec": "70, 116", "uniHex": "46, 74" }, "ISK": { "text": "kr", "uniDec": "107, 114", "uniHex": "6b, 72" }, "INR": { "text": "", "uniDec": "", "uniHex": "" }, "IDR": { "text": "Rp", "uniDec": "82, 112", "uniHex": "52, 70" }, "IRR": { "text": "﷼", "uniDec": "65020", "uniHex": "fdfc" }, "IMP": { "text": "£", "uniDec": "163", "uniHex": "a3" }, "ILS": { "text": "₪", "uniDec": "8362", "uniHex": "20aa" }, "JMD": { "text": "J$", "uniDec": "74, 36", "uniHex": "4a, 24" }, "JPY": { "text": "¥", "uniDec": "165", "uniHex": "a5" }, "JEP": { "text": "£", "uniDec": "163", "uniHex": "a3" }, "KZT": { "text": "лв", "uniDec": "1083, 1074", "uniHex": "43b, 432" }, "KPW": { "text": "₩", "uniDec": "8361", "uniHex": "20a9" }, "KRW": { "text": "₩", "uniDec": "8361", "uniHex": "20a9" }, "KGS": { "text": "лв", "uniDec": "1083, 1074", "uniHex": "43b, 432" }, "LAK": { "text": "₭", "uniDec": "8365", "uniHex": "20ad" }, "LVL": { "text": "Ls", "uniDec": "76, 115", "uniHex": "4c, 73" }, "LBP": { "text": "£", "uniDec": "163", "uniHex": "a3" }, "LRD": { "text": "$", "uniDec": "36", "uniHex": "24" }, "LTL": { "text": "Lt", "uniDec": "76, 116", "uniHex": "4c, 74" }, "MKD": { "text": "ден", "uniDec": "1076, 1077, 1085", "uniHex": "434, 435, 43d" }, "MYR": { "text": "RM", "uniDec": "82, 77", "uniHex": "52, 4d" }, "MUR": { "text": "₨", "uniDec": "8360", "uniHex": "20a8" }, "MXN": { "text": "$", "uniDec": "36", "uniHex": "24" }, "MNT": { "text": "₮", "uniDec": "8366", "uniHex": "20ae" }, "MZN": { "text": "MT", "uniDec": "77, 84", "uniHex": "4d, 54" }, "NAD": { "text": "$", "uniDec": "36", "uniHex": "24" }, "NPR": { "text": "₨", "uniDec": "8360", "uniHex": "20a8" }, "ANG": { "text": "ƒ", "uniDec": "402", "uniHex": "192" }, "NZD": { "text": "$", "uniDec": "36", "uniHex": "24" }, "NIO": { "text": "C$", "uniDec": "67, 36", "uniHex": "43, 24" }, "NGN": { "text": "₦", "uniDec": "8358", "uniHex": "20a6" }, "NOK": { "text": "kr", "uniDec": "107, 114", "uniHex": "6b, 72" }, "OMR": { "text": "﷼", "uniDec": "65020", "uniHex": "fdfc" }, "PKR": { "text": "₨", "uniDec": "8360", "uniHex": "20a8" }, "PAB": { "text": "B/.", "uniDec": "66, 47, 46", "uniHex": "42, 2f, 2e" }, "PYG": { "text": "Gs", "uniDec": "71, 115", "uniHex": "47, 73" }, "PEN": { "text": "S/.", "uniDec": "83, 47, 46", "uniHex": "53, 2f, 2e" }, "PHP": { "text": "₱", "uniDec": "8369", "uniHex": "20b1" }, "PLN": { "text": "zł", "uniDec": "122, 322", "uniHex": "7a, 142" }, "QAR": { "text": "﷼", "uniDec": "65020", "uniHex": "fdfc" }, "RON": { "text": "lei", "uniDec": "108, 101, 105", "uniHex": "6c, 65, 69" }, "RUB": { "text": "руб", "uniDec": "1088, 1091, 1073", "uniHex": "440, 443, 431" }, "SHP": { "text": "£", "uniDec": "163", "uniHex": "a3" }, "SAR": { "text": "﷼", "uniDec": "65020", "uniHex": "fdfc" }, "RSD": { "text": "Дин.", "uniDec": "1044, 1080, 1085, 46", "uniHex": "414, 438, 43d, 2e" }, "SCR": { "text": "₨", "uniDec": "8360", "uniHex": "20a8" }, "SGD": { "text": "$", "uniDec": "36", "uniHex": "24" }, "SBD": { "text": "$", "uniDec": "36", "uniHex": "24" }, "SOS": { "text": "S", "uniDec": "83", "uniHex": "53" }, "ZAR": { "text": "R", "uniDec": "82", "uniHex": "52" }, "LKR": { "text": "₨", "uniDec": "8360", "uniHex": "20a8" }, "SEK": { "text": "kr", "uniDec": "107, 114", "uniHex": "6b, 72" }, "CHF": { "text": "CHF", "uniDec": "67, 72, 70", "uniHex": "43, 48, 46" }, "SRD": { "text": "$", "uniDec": "36", "uniHex": "24" }, "SYP": { "text": "£", "uniDec": "163", "uniHex": "a3" }, "TWD": { "text": "NT$", "uniDec": "78, 84, 36", "uniHex": "4e, 54, 24" }, "THB": { "text": "฿", "uniDec": "3647", "uniHex": "e3f" }, "TTD": { "text": "TT$", "uniDec": "84, 84, 36", "uniHex": "54, 54, 24" }, "TRY": { "text": "", "uniDec": "", "uniHex": "" }, "TRL": { "text": "₤", "uniDec": "8356", "uniHex": "20a4" }, "TVD": { "text": "$", "uniDec": "36", "uniHex": "24" }, "UAH": { "text": "₴", "uniDec": "8372", "uniHex": "20b4" }, "GBP": { "text": "£", "uniDec": "163", "uniHex": "a3" }, "USD": { "text": "$", "uniDec": "36", "uniHex": "24" }, "UYU": { "text": "$U", "uniDec": "36, 85", "uniHex": "24, 55" }, "UZS": { "text": "лв", "uniDec": "1083, 1074", "uniHex": "43b, 432" }, "VEF": { "text": "Bs", "uniDec": "66, 115", "uniHex": "42, 73" }, "VND": { "text": "₫", "uniDec": "8363", "uniHex": "20ab" }, "YER": { "text": "﷼", "uniDec": "65020", "uniHex": "fdfc" }, "ZWD": { "text": "Z$", "uniDec": "90, 36", "uniHex": "5a, 24" }
};

angular.module("gms").filter('isoCurrency', function () {
    return function (value, iso, decimal, decimalSep, thousSep) {
        return accounting.formatMoney(value, mapping[iso].text || $, parseInt(decimal) || 2, thousSep || ",", decimalSep || ".");
    };
});
angular.module('gms').filter('range', function () {
    return function (input, total) {
        total = parseInt(total);
        for (var i = 0; i < total; i++)
            input.push(i);
        return input;
    };
});

angular.module('gms').config(function ($datepickerProvider) {
    angular.extend($datepickerProvider.defaults, {
        dateFormat: 'dd/MM/yyyy'
    });
});

angular.module('gms').config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider.state('core', {
        abstract: true,
        url: '',
        templateUrl: 'core/views/core.html',
        controller: 'CoreController'
    }).state('home', {
        url: '/home',
        views: {
            'main': {
                templateUrl: 'home/views/home.html',
                controller: 'HomeController'
            }
        },
        parent: 'core'
    }).state('information', {
        url: '/information',
        views: {
            'main': {
                templateUrl: 'information/views/index.html',
                controller: 'InformationController'
            }
        },
        parent: 'core'
    }).state('infomationdetail', {
        url: '/infomationdetail',
        views: {
            'main': {
                templateUrl: 'information/views/infomationdetail.html',
                controller: 'InfomationDetailController'
            }
        },
        parent: 'core'
    }).state('contactus', {
        url: '/contactus',
        views: {
            'main': {
                templateUrl: 'information/views/contactus.html',
                controller: 'ContactusController',
                resolve: {
                    contact: function (Contact, $sessionStorage) {
                        return Contact.query({ languageCode: $sessionStorage.languageCode }).$promise;
                    }
                }
            }
        },
        parent: 'core'
    }).state('language', {
        url: '/language',
        views: {
            'main': {
                templateUrl: 'language/views/index.html',
                controller: 'LanguageController'
            }
        },
        parent: 'core'
    }).state('bookingflight', {
        url: '/bookingflight',
        views: {
            'main': {
                templateUrl: 'booking/views/bookingflight.html',
                controller: 'BookingFlightController'
            }
        },
        parent: 'core'
    }).state('step1', {
        url: '/step1',
        views: {
            'main': {
                templateUrl: 'booking/views/step_1.html',
                controller: 'Step1Controller'
            }
        },
        parent: 'core'
    }).state('step2', {
        url: '/step2',
        views: {
            'main': {
                templateUrl: 'booking/views/step_2.html',
                controller: 'Step2Controller'
            }
        },
        parent: 'core'
    }).state('step3', {
        url: '/step3',
        views: {
            'main': {
                templateUrl: 'booking/views/step_3.html',
                controller: 'Step3Controller'
            }
        },
        parent: 'core'
    }).state('step4', {
        url: '/step4',
        views: {
            'main': {
                templateUrl: 'booking/views/step_4.html',
                controller: 'Step4Controller'
            }
        },
        parent: 'core'
    }).state('step5', {
        url: '/step5',
        views: {
            'main': {
                templateUrl: 'booking/views/step_5.html',
                controller: 'Step5Controller'
            }
        },
        parent: 'core'
    }).state('step6', {
        url: '/step6',
        views: {
            'main': {
                templateUrl: 'booking/views/step_6.html',
                controller: 'Step6Controller'
            }
        },
        parent: 'core'
    }).state('step7', {
        url: '/step7',
        views: {
            'main': {
                templateUrl: 'booking/views/step_7.html',
                controller: 'Step7Controller'
            }
        },
        parent: 'core'
    }).state('checkinstep1', {
        url: '/checkin',
        views: {
            'main': {
                templateUrl: 'checkin/views/step_1.html',
                controller: 'CheckInStep1Controller'
            }
        },
        parent: 'core'
    }).state('checkinstep2', {
        url: '/checkinstep2',
        views: {
            'main': {
                templateUrl: 'checkin/views/step_2.html',
                controller: 'CheckInStep2Controller'
            }
        },
        parent: 'core'
    }).state('checkinstep3', {
        url: '/checkinstep3',
        views: {
            'main': {
                templateUrl: 'checkin/views/step_3.html',
                controller: 'CheckInStep3Controller'
            }
        },
        parent: 'core'
    }).state('checkinstep3_1', {
        url: '/checkinstep3_1',
        views: {
            'main': {
                templateUrl: 'checkin/views/step_3_1.html',
                controller: 'CheckInStep31Controller'
            }
        },
        parent: 'core'
    }).state('checkinstep3_2', {
        url: '/checkinstep3_2',
        views: {
            'main': {
                templateUrl: 'checkin/views/step_3_2.html',
                controller: 'CheckInStep32Controller'
            }
        },
        parent: 'core'
    }).state('checkinstep4', {
        url: '/checkinstep4',
        views: {
            'main': {
                templateUrl: 'checkin/views/step_4.html',
                controller: 'CheckInStep4Controller'
            }
        },
        parent: 'core'
    }).state('flightstatus', {
        url: '/flightstatus',
        views: {
            'main': {
                templateUrl: 'flightstatus/views/step_1.html',
                controller: 'FlightStatusStep1Controller'
            }
        },
        parent: 'core'
    }).state('flightstatus2', {
        url: '/flightstatus2',
        views: {
            'main': {
                templateUrl: 'flightstatus/views/step_2.html',
                controller: 'FlightStatusStep2Controller'
            }
        },
        parent: 'core'
    }).state('flightstatus3', {
        url: '/flightstatus3',
        views: {
            'main': {
                templateUrl: 'flightstatus/views/step_3.html',
                controller: 'FlightStatusStep3Controller'
            }
        },
        parent: 'core'
    }).state('user', {
        url: '/user',
        views: {
            'main': {
                templateUrl: 'userprofile/views/homeuser.html',
                controller: 'UserController'
            }
        },
        parent: 'core'
    }).state('login', {
        url: '/login',
        views: {
            'main': {
                templateUrl: 'userprofile/views/login.html',
                controller: 'LoginController'
            }
        },
        parent: 'core'
    }).state('register', {
        url: '/register',
        views: {
            'main': {
                templateUrl: 'userprofile/views/register.html',
                controller: 'RegisterController'
            }
        },
        parent: 'core'
    }).state('accountcreated', {
        url: '/accountcreated',
        views: {
            'main': {
                templateUrl: 'userprofile/views/accountcreated.html',
                controller: 'AccountCreatedController'
            }
        },
        parent: 'core'
    }).state('forgotpassword', {
        url: '/forgotpassword',
        views: {
            'main': {
                templateUrl: 'userprofile/views/forgotpass.html',
                controller: 'ForgotPasswordController'
            }
        },
        parent: 'core'
    }).state('changepassword', {
        url: '/changepassword',
        views: {
            'main': {
                templateUrl: 'userprofile/views/changepass.html',
                controller: 'ChangePasswordController'
            }
        },
        parent: 'core'
    }).state('updateprofile', {
        url: '/updateprofile',
        views: {
            'main': {
                templateUrl: 'userprofile/views/updateprofile.html',
                controller: 'UpdateProfileController'
            }
        },
        parent: 'core'
    }).state('reservationhistory', {
        url: '/reservationhistory',
        views: {
            'main': {
                templateUrl: 'userprofile/views/reservationhistory.html',
                controller: 'ReservationHistoryController'
            }
        },
        parent: 'core'
    }).state('reservationdetail', {
        url: '/reservationdetail',
        views: {
            'main': {
                templateUrl: 'userprofile/views/reservationdetail.html',
                controller: 'ReservationHistoryDetailController'
            }
        },
        parent: 'core'
    });

    /* Return Home for any other urls */
    $urlRouterProvider.otherwise('/home');
});
//# sourceMappingURL=app.js.map

var Configuration;
(function (Configuration) {
    var Settings = (function () {
        function Settings() {
        }
        Settings.serviceUrl = "http://webee.vn:7070/Pasco/api";
        return Settings;
    })();
    Configuration.Settings = Settings;
})(Configuration || (Configuration = {}));
//# sourceMappingURL=config.js.map

/// <reference path="../../../Scripts/typings/angularjs/angular.d.ts"/>
/// <reference path="../../../Scripts/typings/moment/moment.d.ts"/>
var Booking;
(function (Booking) {
    function resetBooking(sessionStorage) {
        delete sessionStorage.step;
        delete sessionStorage.s1FromCode;
        delete sessionStorage.s1FromName;
        delete sessionStorage.step1;
        delete sessionStorage.s1ToCode;
        delete sessionStorage.s1ToName;
        delete sessionStorage.promoCode;
        delete sessionStorage.step2;
        delete sessionStorage.step3;
        delete sessionStorage.step4;
        delete sessionStorage.step4AddonDepart;
        delete sessionStorage.step4AddonArrival;
        delete sessionStorage.totalAmount;
        delete sessionStorage.step6;
        delete sessionStorage.step7;
    }

    // Infomation Controller
    function BookingFlightController($sessionStorage, $scope, $state, $location) {
        $scope.state = $state.current.name;
        var step = $sessionStorage.step;
        if (typeof (step) != "undefined" && step !== null) {
            // $location.path('/' + step);
            $location.path('/step1');
        } else {
            $location.path('/step1');
        }
    }
    function isFloat(n) {
        return n != "" && !isNaN(n) && Math.round(n) != n;
    }
    function Step1Controller($sessionStorage, $scope, $window, $state, $location, $timeout, $translate, $modal, AirportList) {
        //set current name to state
        $scope.state = $state.current.name;

        //$sessionStorage.$reset();
        resetBooking($sessionStorage);

        //set current step to session
        $sessionStorage.step = "step1";

        //set origin and destination null when start
        $scope.optionOrigin = null;
        $scope.optionDestination = null;

        //loading
        var loadingModal = $modal({
            scope: $scope,
            template: 'app/modal/loading.html',
            show: true,
            backdrop: 'static'
        });

        //load origin list from service
        AirportList.query(function (data) {
            $scope.originResult = data;
            loadingModal.hide();
        });

        //when origin change call function to load destination
        $scope.loadDestination = function () {
            if (typeof ($scope.optionOrigin) != "undefined" || $scope.optionOrigin != null) {
                loadingModal.show();

                //load destination from service with Code Origin
                AirportList.get({ id: $scope.optionOrigin }, function (data) {
                    loadingModal.hide();

                    //load destination list from service return
                    $scope.destinationResult = data.ArrivalAirports;

                    //set currency code and from code, from name
                    $scope.S1CurrencyCode = data.Currency.Abbreviation;
                    $sessionStorage.s1FromCode = data.DepartureAirport.Code;
                    $sessionStorage.s1FromName = data.DepartureAirport.Name;
                });
            }
        };

        //set default value for datestart, dateend, roundtrip, adults,children, infant
        $scope.today = new Date().toISOString().substr(0, 10);
        $scope.departDateCheck = new Date().toISOString().substr(0, 10);
        $scope.dateStart = new Date();
        $scope.dateEnd = new Date();
        $scope.check_roundtrip = 1;
        $scope.adultsValue = 1;
        $scope.childrenValue = 0;
        $scope.infantsValue = 0;

        //check if session of step1 is vaild
        if ($sessionStorage.step1) {
            //set date from session step 1 to field
            var step1Data = $sessionStorage.step1;
            $scope.tab = step1Data.s1RoundTrip;
            $scope.optionOrigin = null;
            $scope.optionDestination = null;
            $scope.dateStart = new Date(step1Data.s1DateStart);
            $scope.dateEnd = new Date(step1Data.s1DateReturn);
            $scope.adultsValue = step1Data.s1Adults;
            $scope.childrenValue = step1Data.s1Children;
            $scope.infantsValue = step1Data.s1Infants;
            $scope.promoCodeValue = step1Data.s1Promocode;
        }

        // set data into adults, children, infants
        $scope.AdultsRange = 9; // [{ name: "1", id: 1 }, { name: "2", id: 2 }, { name: "3", id: 3 }, { name: "4", id: 4 }, { name: "5", id: 5 }, { name: "6", id: 6 }, { name: "7", id: 7 }, { name: "8", id: 8 }, { name: "9", id: 9 }];
        $scope.ChildrenRange = 9;
        $scope.InfantsRange = 10;
        $scope.getNumber = function (range) {
            return new Array(range);
        };

        //function check quality when update adults
        $scope.checkAdults = function () {
            switch (parseInt($scope.adultsValue)) {
                case 1:
                    $scope.InfantsRange = 2;
                    $scope.ChildrenRange = 9;
                    $scope.childrenValue = 0;
                    $scope.infantsValue = 0;
                    break;
                case 2:
                    $scope.InfantsRange = 3;
                    $scope.ChildrenRange = 8;
                    $scope.childrenValue = 0;
                    $scope.infantsValue = 0;
                    break;
                case 3:
                    $scope.InfantsRange = 4;
                    $scope.ChildrenRange = 7;
                    $scope.childrenValue = 0;
                    $scope.infantsValue = 0;
                    break;
                case 4:
                    $scope.InfantsRange = 5;
                    $scope.ChildrenRange = 6;
                    $scope.childrenValue = 0;
                    $scope.infantsValue = 0;
                    break;
                case 5:
                    $scope.InfantsRange = 6;
                    $scope.ChildrenRange = 5;
                    $scope.childrenValue = 0;
                    $scope.infantsValue = 0;
                    break;
                case 6:
                    $scope.InfantsRange = 7;
                    $scope.ChildrenRange = 4;
                    $scope.childrenValue = 0;
                    $scope.infantsValue = 0;
                    break;
                case 7:
                    $scope.InfantsRange = 8;
                    $scope.ChildrenRange = 3;
                    $scope.childrenValue = 0;
                    $scope.infantsValue = 0;
                    break;
                case 8:
                    $scope.InfantsRange = 9;
                    $scope.ChildrenRange = 2;
                    $scope.childrenValue = 0;
                    $scope.infantsValue = 0;
                    break;
                case 9:
                    $scope.InfantsRange = 10;
                    $scope.ChildrenRange = 1;
                    $scope.childrenValue = 0;
                    $scope.infantsValue = 0;
                    break;
            }
        };

        //function check date start and date end when update date start
        $scope.updateDepartDate = function () {
            //check if date start larger date end
            if ($scope.dateEnd < $scope.dateStart) {
                $scope.dateEnd = new Date($scope.dateStart);
            }
            //$scope.departDateCheck = new Date().toISOString().substr(0, 10);
        };
        $scope.updateReturnDate = function () {
            //check if date start larger date end
            if ($scope.dateEnd < $scope.dateStart) {
                $scope.dateEnd = new Date($scope.dateStart);
            }
            //$scope.departDateCheck = new Date().toISOString().substr(0, 10);
        };

        //function button Find Flight click
        $scope.findFlightClick = function () {
            //check data of step1Form invalid
            if ($scope.step1Form.$invalid) {
                //broadcast field invalid
                $scope.$broadcast('show-errors-check-validity');
                if (typeof ($scope.optionOrigin) == "undefined" || $scope.optionOrigin === null) {
                    $translate(['DATA_MESSAGE_CHOOSE_ORIGIN']).then(function (translation) {
                        alert(translation.DATA_MESSAGE_CHOOSE_ORIGIN);
                    });
                }
                if (typeof ($scope.optionDestination) == "undefined" || $scope.optionDestination === null) {
                    $translate(['DATA_MESSAGE_CHOOSE_DESTINATION']).then(function (translation) {
                        alert(translation.DATA_MESSAGE_CHOOSE_DESTINATION);
                    });
                }
                return;
            }

            //check destination chose or not chose
            if ($scope.optionDestination) {
                for (var i = 0; i < $scope.destinationResult.length; i++) {
                    //check value chose with destination code from list
                    if ($scope.optionDestination == $scope.destinationResult[i].Code) {
                        //set toCode and toName to session
                        $sessionStorage.s1ToCode = $scope.destinationResult[i].Code;
                        $sessionStorage.s1ToName = $scope.destinationResult[i].Name;
                        break;
                    }
                }
            } else {
                //notification chose destination
                //alert("Please choose Destination");
                $translate(['DATA_MESSAGE_CHOOSE_DESTINATION']).then(function (translation) {
                    alert(translation.DATA_MESSAGE_CHOOSE_DESTINATION);
                });
            }
            var roundtrip = 1;

            //check current tab !1 is Oneway (1: return, 2: one way)
            if ($scope.tab != 1) {
                roundtrip = 2;
            }

            //check if roundtrip equal 1 to set replace date end, else to set null
            if (roundtrip == 1)
                $scope.dateEnd = $scope.dateEnd;
            else
                $scope.dateEnd = "";

            //create object data with data from step 1
            var data = {
                s1RoundTrip: roundtrip,
                s1Origin: $scope.optionOrigin,
                s1Destination: $scope.optionDestination,
                s1DateStart: $scope.dateStart,
                s1DateReturn: $scope.dateEnd,
                s1Adults: $scope.adultsValue,
                s1Children: $scope.childrenValue,
                s1Infants: $scope.infantsValue,
                s1Promocode: $scope.promoCodeValue,
                s1CurrencyCode: $scope.S1CurrencyCode
            };
            function setDateTimeToCurrent(dateString) {
                var date = moment(dateString);
                var current = moment();
                date.hours(current.hours());
                date.minutes(current.minutes());
                date.seconds(current.seconds());
                return date.toISOString();
            }

            //set data step 1 to sesstion
            $sessionStorage.step1 = data;

            //redirect to step 2
            $location.path('/step2');
        };
    }

    function caculatorSurcharges(options) {
        for (var i = 0; i < options.length; i++) {
            var leg = options[i].Legs[0];
            var surcharges = leg.Surcharges;
            var sum = 0;
            for (var j = 0; j < surcharges.length; j++) {
                if (surcharges[j].ApplyToAdult) {
                    sum = sum + surcharges[j].Total;
                }
            }
            leg.surchargesTotalAdult = sum;
        }
    }
    function formatDate(date) {
        var d = new Date(date), month = '' + (d.getMonth() + 1), day = '' + d.getDate(), year = d.getFullYear();

        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;

        return [year, month, day].join('-');
    }

    //Hide legs if SeatAllocation==0
    function HideLegs(outboundOptions, totalPassengers) {
        for (var i = 0; i < outboundOptions.length; i++) {
            var legs = outboundOptions[i].Legs[0];
            var isHideLegs = false;
            for (var k = 0; k < legs.SegmentOptions.length; k++) {
                var segments = legs.SegmentOptions[k];
                if (segments.SeatAllocation < totalPassengers) {
                    //console.log(k);
                    isHideLegs = true;
                }
            }
            if (isHideLegs == true) {
                outboundOptions.splice(i, 1);
                i = -1;
            }
        }

        //console.log(outboundOptions);
        return outboundOptions;
    }
    function Step2Controller($sessionStorage, $scope, $window, $state, $location, $modal, $sce, $anchorScroll, $translate, TravelOption, FareRule) {
        $scope.state = $state.current.name;
        var step1Data = $sessionStorage.step1;

        if (!step1Data && ($sessionStorage.step != "step1" || $sessionStorage.step != "step2")) {
            $location.path('/step1');
        }

        $sessionStorage.step = "step2";
        var totalPassengers = step1Data.s1Adults + step1Data.s1Children;

        //$scope.selectedDate_start = moment($scope.selectedDate_start).add('days', 1).toDate();
        //$scope.selectedDate_start = moment($scope.selectedDate_start).subtract('days', 1).toDate();
        $scope.valueFareCollapse = -1;
        $scope.isCodePromo = false;
        $scope.isFinding = true;
        $scope.findFlights = "Finding Flights...";
        $scope.step1Data = $sessionStorage.step1;
        $scope.s1FromCode = $sessionStorage.s1FromCode;
        $scope.s1FromName = $sessionStorage.s1FromName;
        $scope.s1ToCode = $sessionStorage.s1ToCode;
        $scope.s1ToName = $sessionStorage.s1ToName;
        var DateStart = step1Data.s1DateStart;
        if (moment(DateStart).format("MMM Do YY") == moment(new Date).format("MMM Do YY")) {
            DateStart = step1Data.s1DateStart;
            DateStart = formatDate(DateStart) + " " + "00:00:00";
        } else {
            DateStart = formatDate(DateStart) + " " + "00:00:00";
        }
        var DateReturn = step1Data.s1DateReturn;
        if (moment(DateReturn).format("MMM Do YY") == moment(new Date).format("MMM Do YY")) {
            DateReturn = step1Data.s1DateReturn;
            DateReturn = formatDate(DateReturn) + " " + "00:00:00";
        } else {
            DateReturn = formatDate(DateReturn) + " " + "00:00:00";
        }
        $scope.travelOptionResult = TravelOption.get({
            roundTrip: step1Data.s1RoundTrip,
            OutboundDate: DateStart,
            InboundDate: DateReturn,
            DaysBefore: 0,
            DaysAfter: 0,
            AdultCount: step1Data.s1Adults,
            ChildCount: step1Data.s1Children,
            InfantCount: step1Data.s1Infants,
            DepartureAirportCode: step1Data.s1Origin,
            ArrivalAirportCode: step1Data.s1Destination,
            CurrencyCode: step1Data.s1CurrencyCode,
            PromoCode: step1Data.s1Promocode
        }, function (data) {
            if (data.OutboundOptions != undefined) {
                var outboundOptions = data.OutboundOptions;
                for (var i = 0; i < outboundOptions.length; i++) {
                    var legs = outboundOptions[i].Legs[0];
                    if (moment(legs.DepartureDate).format("MMM Do YY") != moment(step1Data.s1DateStart).format("MMM Do YY")) {
                        outboundOptions.splice(i, 1);
                        i = -1;
                    } else {
                        for (var j = 0; j < legs.SegmentOptions.length; j++) {
                            var segments = legs.SegmentOptions[j];
                            var DateNow = moment.utc(new Date).format();
                            var DepartureDate = moment.utc(segments.Flight.ETD).format();
                            if (DateNow >= DepartureDate) {
                                outboundOptions.splice(i, 1);
                                i = -1;
                            }
                        }
                    }
                }
                var inboundOptions = data.InboundOptions;
                for (var i = 0; i < inboundOptions.length; i++) {
                    var legs = inboundOptions[i].Legs[0];
                    if (moment(legs.DepartureDate).format("MMM Do YY") != moment(step1Data.s1DateReturn).format("MMM Do YY")) {
                        inboundOptions.splice(i, 1);
                        i = -1;
                    } else {
                        for (var j = 0; j < legs.SegmentOptions.length; j++) {
                            var segments = legs.SegmentOptions[j];

                            var DateNow = moment.utc(new Date).format();
                            var ReturnDate = moment.utc(segments.Flight.ETA).format();
                            if (DateNow >= ReturnDate) {
                                outboundOptions.splice(i, 1);
                                i = -1;
                            }
                        }
                    }
                }
                HideLegs(outboundOptions, totalPassengers);
                HideLegs(inboundOptions, totalPassengers);
                caculatorSegment(outboundOptions);
                caculatorSegment(inboundOptions);
                caculatorSurcharges(outboundOptions);
                caculatorSurcharges(inboundOptions);
                $scope.travelOptionSelect.Selected = outboundOptions;
                $scope.travelOptionPromoSelect = data.OutboundPromoCodeResults;
                setPromoData(data);
            }
            $scope.isFinding = false;
            $scope.isCodePromo = true;
            $scope.findFlights = "No flights matched your search criteria";
        });
        $scope.travelOptionSelect = {};
        $scope.travelOptionPromoSelect = {};
        $scope.packagePriceSelect = Array(2);

        var segmentChoosen = [];
        var fareClass = "";
        $scope.isSelected = function (tabIndex, fareIndex, pricePackage, leg) {
            fareClass = pricePackage.FareClass;
            segmentChoosen = leg.SegmentOptions;
        };

        $scope.predate = function () {
            var today = new Date();

            var DateStart_temp = new Date(setDateTimeToCurrent($scope.step1Data.s1DateStart));
            var DateReturn_temp = new Date(setDateTimeToCurrent($scope.step1Data.s1DateReturn));

            var isLoadTravelOption = true;
            if (step1Data.s1RoundTrip == 1) {
                if ($scope.tab == 0) {
                    DateStart_temp = moment(DateStart_temp).subtract(1, 'days').toDate();
                    if (DateStart_temp < moment(today).subtract(1, 'minutes').toDate()) {
                        DateStart_temp = today;

                        //alert("Depart date must be greater than or equal today");
                        $translate(['DATA_MESSAGE_DEPART_DATE_MUST_GREATER_EQUAL_TODAY']).then(function (translation) {
                            alert(translation.DATA_MESSAGE_DEPART_DATE_MUST_GREATER_EQUAL_TODAY);
                        });
                        isLoadTravelOption = false;
                    }
                } else {
                    DateReturn_temp = moment(DateReturn_temp).subtract(1, 'days').toDate();
                    DateStart_temp = moment(DateStart_temp).subtract(1, 'minutes').toDate();
                    if (DateReturn_temp < moment(DateStart_temp).subtract(1, 'minutes').toDate()) {
                        DateReturn_temp = DateStart_temp;

                        //alert("Return date must be greater than depart date");
                        $translate(['DATA_MESSAGE_RETURN_DATE_MUST_GREATER_DEPART_DATE']).then(function (translation) {
                            alert(translation.DATA_MESSAGE_RETURN_DATE_MUST_GREATER_DEPART_DATE);
                        });
                        isLoadTravelOption = false;
                    }
                }
            } else {
                DateStart_temp = moment(DateStart_temp).subtract(1, 'days').toDate();
                if (DateStart_temp < moment(today).subtract(1, 'minutes').toDate()) {
                    DateStart_temp = today;

                    //alert("Depart date must be greater than or equal today");
                    $translate(['DATA_MESSAGE_DEPART_DATE_MUST_GREATER_EQUAL_TODAY']).then(function (translation) {
                        alert(translation.DATA_MESSAGE_DEPART_DATE_MUST_GREATER_EQUAL_TODAY);
                    });
                    isLoadTravelOption = false;
                }
                DateReturn_temp = null;
            }
            var DateStart_Pre = DateStart_temp.toISOString();

            if (moment(DateStart_Pre).format("MMM Do YY") == moment(new Date).format("MMM Do YY")) {
                DateStart_Pre = formatDate(DateStart_Pre) + " " + "00:00:00";
            } else {
                DateStart_Pre = formatDate(DateStart_Pre) + " " + "00:00:00";
            }

            if (DateReturn_temp != null) {
                var DateReturn_Pre = DateReturn_temp.toISOString();
                if (moment(DateReturn_Pre).format("MMM Do YY") == moment(new Date).format("MMM Do YY")) {
                    DateStart_Pre = formatDate(DateStart_Pre) + " " + "00:00:00";
                } else {
                    DateReturn_Pre = formatDate(DateReturn_Pre) + " " + "00:00:00";
                }
            }
            if (isLoadTravelOption) {
                $scope.isFinding = true;
                $scope.isCodePromo = false;
                $scope.findFlights = "Finding Flights...";
                var datas1 = {
                    s1RoundTrip: step1Data.s1RoundTrip,
                    s1Origin: step1Data.s1Origin,
                    s1Destination: step1Data.s1Destination,
                    s1DateStart: DateStart_temp,
                    s1DateReturn: DateReturn_temp,
                    s1Adults: step1Data.s1Adults,
                    s1Children: step1Data.s1Children,
                    s1Infants: step1Data.s1Infants,
                    s1Promocode: step1Data.s1Promocode,
                    s1CurrencyCode: step1Data.s1CurrencyCode
                };
                $sessionStorage.step1 = datas1;
                $scope.step1Data = $sessionStorage.step1;

                $scope.travelOptionResult = TravelOption.get({
                    roundTrip: step1Data.s1RoundTrip,
                    OutboundDate: DateStart_Pre,
                    InboundDate: DateReturn_Pre,
                    DaysBefore: 0,
                    DaysAfter: 0,
                    AdultCount: step1Data.s1Adults,
                    ChildCount: step1Data.s1Children,
                    InfantCount: step1Data.s1Infants,
                    DepartureAirportCode: step1Data.s1Origin,
                    ArrivalAirportCode: step1Data.s1Destination,
                    CurrencyCode: step1Data.s1CurrencyCode,
                    PromoCode: step1Data.s1Promocode
                }, function (data) {
                    if (data.OutboundOptions != undefined) {
                        var outboundOptions = data.OutboundOptions;
                        for (var i = 0; i < outboundOptions.length; i++) {
                            var legs = outboundOptions[i].Legs[0];
                            if (moment(legs.DepartureDate).format("MMM Do YY") != moment(DateStart_temp).format("MMM Do YY")) {
                                outboundOptions.splice(i, 1);
                                i = -1;
                            } else {
                                for (var j = 0; j < legs.SegmentOptions.length; j++) {
                                    var segments = legs.SegmentOptions[j];
                                    var DateNow = moment.utc(new Date).format();
                                    var DepartureDate = moment.utc(segments.Flight.ETD).format();
                                    if (DateNow >= DepartureDate) {
                                        outboundOptions.splice(i, 1);
                                        i = -1;
                                    }
                                }
                            }
                        }
                        var inboundOptions = data.InboundOptions;
                        for (var i = 0; i < inboundOptions.length; i++) {
                            var legs = inboundOptions[i].Legs[0];
                            if (moment(legs.DepartureDate).format("MMM Do YY") != moment(DateReturn_temp).format("MMM Do YY")) {
                                inboundOptions.splice(i, 1);
                                i = -1;
                            } else {
                                for (var j = 0; j < legs.SegmentOptions.length; j++) {
                                    var segments = legs.SegmentOptions[j];
                                    var DateNow = moment.utc(new Date).format();
                                    var ReturnDate = moment.utc(segments.Flight.ETA).format();
                                    if (DateNow >= ReturnDate) {
                                        outboundOptions.splice(i, 1);
                                        i = -1;
                                    }
                                }
                            }
                        }
                        HideLegs(outboundOptions, totalPassengers);
                        HideLegs(inboundOptions, totalPassengers);
                        caculatorSegment(outboundOptions);
                        caculatorSegment(inboundOptions);
                        caculatorSurcharges(outboundOptions);
                        caculatorSurcharges(inboundOptions);

                        if ($scope.tab == 0)
                            $scope.travelOptionSelect.Selected = outboundOptions;
                        else
                            $scope.travelOptionSelect.Selected = inboundOptions;

                        for (var i = 0; i < outboundOptions.length; i++) {
                            var legs = outboundOptions[i].Legs[0];
                            for (var k = 0; k <= legs.FareOptions.Adultfares.length - 1; k++) {
                                var adult = legs.FareOptions.Adultfares[k];
                                if (adult.FareClass == fareClass) {
                                    for (var j = 0; j < legs.SegmentOptions.length; j++) {
                                        if (segmentChoosen.length != legs.SegmentOptions.length) {
                                            legs.FareOptions.Adultfares[k].isChecked = false;
                                            $scope.isFinding = false;
                                            break;
                                        } else {
                                            var flights = legs.SegmentOptions[j].Flight;
                                            if (flights.ETDLocal != segmentChoosen[j].Flight.ETDLocal && flights.ETALocal != segmentChoosen[j].Flight.ETALocal) {
                                                legs.FareOptions.Adultfares[k].isChecked = false;
                                                $scope.isFinding = false;
                                                break;
                                            } else {
                                                legs.FareOptions.Adultfares[k].isChecked = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        setPromoData(data);
                    }
                    if ($scope.tab == 0) {
                        $scope.tab = 0;
                        $scope.travelOptionSelect.Selected = outboundOptions;
                        $scope.travelOptionPromoSelect = data.OutboundPromoCodeResults;
                    } else {
                        $scope.tab = 1;
                        $scope.travelOptionSelect.Selected = inboundOptions;
                        $scope.travelOptionPromoSelect = data.InboundPromoCodeResults;
                    }
                    $scope.isFinding = false;
                    $scope.isCodePromo = true;
                    $scope.findFlights = "No flights matched your search criteria";
                });
                $scope.travelOptionSelect = {};
                $scope.travelOptionPromoSelect = {};
            }
        };

        function caculatorSegment(outboundOptions) {
            for (var i = 0; i < outboundOptions.length; i++) {
                var outbound = outboundOptions[i];
                outbound.Legs[0].SegmentOptionsTemp = [];
                angular.copy(outbound.Legs[0].SegmentOptions, outbound.Legs[0].SegmentOptionsTemp);
                var airCraftIndex = 0;
                var countStop = 1;

                if (outbound.Legs[0].SegmentOptionsTemp.length == 1) {
                    var segment = outbound.Legs[0].SegmentOptionsTemp;
                    segment[0].Flight.TimeTransit = "";
                    segment[0].checkAirCraft = false;
                } else {
                    outbound.Legs[0].SegmentOptionsTemp[0].Flight.TimeTransit = "";
                    outbound.Legs[0].SegmentOptionsTemp[0].checkAirCraft = false;
                    for (var j = 1; j < outbound.Legs[0].SegmentOptionsTemp.length; j++) {
                        var segment = outbound.Legs[0].SegmentOptionsTemp;
                        if (segment[j].Flight.Aircraft.ModelName == segment[airCraftIndex].Flight.Aircraft.ModelName && segment[j].Flight.Number == segment[airCraftIndex].Flight.Number) {
                            segment[j].checkAirCraft = true;
                            var timeeta = segment[j - 1].Flight.ETALocal;
                            var timeetd = segment[j].Flight.ETDLocal;
                            var totalMinutes = moment(timeetd).diff(timeeta, 'minutes');
                            var hours = Math.floor(totalMinutes / 60);
                            var minutes = totalMinutes % 60;
                            segment[j].Flight.TimeTransit = hours + 'h ' + minutes + 'm';
                            outbound.Legs[0].SegmentOptionsTemp[airCraftIndex].Flight.ETA = segment[j].Flight.ETA;
                            outbound.Legs[0].SegmentOptionsTemp[airCraftIndex].Flight.ETALocal = segment[j].Flight.ETALocal;
                            outbound.Legs[0].SegmentOptionsTemp[airCraftIndex].Flight.ArrivalAirport.Code = segment[j].Flight.ArrivalAirport.Code;
                            outbound.Legs[0].SegmentOptionsTemp[airCraftIndex].Flight.ArrivalAirport.Name = segment[j].Flight.ArrivalAirport.Name;
                            segment[j].countStop = countStop;
                            countStop++;
                        } else {
                            segment[j].checkAirCraft = false;
                            airCraftIndex = j;
                        }
                    }
                }
            }
        }

        $scope.nextdate = function () {
            var today = new Date();
            var DateStart_temp = new Date(setDateTimeToCurrent($scope.step1Data.s1DateStart));
            var DateReturn_temp = new Date(setDateTimeToCurrent($scope.step1Data.s1DateReturn));
            var isLoadTravelOption = true;
            if (step1Data.s1RoundTrip == 1) {
                if ($scope.tab == 0) {
                    DateStart_temp = moment(DateStart_temp).add(1, 'days').toDate();
                    if ($scope.step1Data.s1RoundTrip == 1)
                        if (DateStart_temp > moment(DateReturn_temp).add(1, 'minutes').toDate()) {
                            DateStart_temp = DateReturn_temp;
                            isLoadTravelOption = false;

                            //alert("Depart date must not be greater than return date");
                            $translate(['DATA_MESSAGE_DEPART_DATE_MUST_NOT_GREATER_EQUAL_TODAY']).then(function (translation) {
                                alert(translation.DATA_MESSAGE_DEPART_DATE_MUST_NOT_GREATER_EQUAL_TODAY);
                            });
                        }
                } else {
                    DateReturn_temp = moment(DateReturn_temp).add(1, 'days').toDate();
                }
            } else {
                DateStart_temp = moment(DateStart_temp).add(1, 'days').toDate();
                DateReturn_temp = null;
            }
            var DateStart_Next = DateStart_temp.toISOString();
            if (moment(DateStart_Next).format("MMM Do YY") == moment(new Date).format("MMM Do YY")) {
                DateStart_Next = formatDate(DateStart_Next) + " " + "00:00:00";
            } else {
                DateStart_Next = formatDate(DateStart_Next) + " " + "00:00:00";
            }
            if (DateReturn_temp != null) {
                var DateReturn_Next = DateReturn_temp.toISOString();
                if (moment(DateReturn_Next).format("MMM Do YY") == moment(new Date).format("MMM Do YY")) {
                    DateReturn_Next = formatDate(DateReturn_Next) + " " + "00:00:00";
                } else {
                    DateReturn_Next = formatDate(DateReturn_Next) + " " + "00:00:00";
                }
            }
            if (isLoadTravelOption) {
                $scope.isFinding = true;
                $scope.isCodePromo = false;
                $scope.findFlights = "Finding Flights...";
                var datas1 = {
                    s1RoundTrip: step1Data.s1RoundTrip,
                    s1Origin: step1Data.s1Origin,
                    s1Destination: step1Data.s1Destination,
                    s1DateStart: DateStart_temp,
                    s1DateReturn: DateReturn_temp,
                    s1Adults: step1Data.s1Adults,
                    s1Children: step1Data.s1Children,
                    s1Infants: step1Data.s1Infants,
                    s1Promocode: step1Data.s1Promocode,
                    s1CurrencyCode: step1Data.s1CurrencyCode
                };
                $sessionStorage.step1 = datas1;
                $scope.step1Data = $sessionStorage.step1;
                $scope.travelOptionResult = TravelOption.get({
                    roundTrip: step1Data.s1RoundTrip,
                    //OutboundDate: DateStart_temp,
                    //InboundDate: DateReturn_temp,
                    OutboundDate: DateStart_Next,
                    InboundDate: DateReturn_Next,
                    DaysBefore: 0,
                    DaysAfter: 0,
                    AdultCount: step1Data.s1Adults,
                    ChildCount: step1Data.s1Children,
                    InfantCount: step1Data.s1Infants,
                    DepartureAirportCode: step1Data.s1Origin,
                    ArrivalAirportCode: step1Data.s1Destination,
                    CurrencyCode: step1Data.s1CurrencyCode,
                    PromoCode: step1Data.s1Promocode
                }, function (data) {
                    if (data.OutboundOptions != undefined) {
                        var outboundOptions = data.OutboundOptions;
                        for (var i = 0; i < outboundOptions.length; i++) {
                            var legs = outboundOptions[i].Legs[0];
                            if (moment(legs.DepartureDate).format("MMM Do YY") != moment(DateStart_temp).format("MMM Do YY")) {
                                outboundOptions.splice(i, 1);
                                i = -1;
                            } else {
                                for (var j = 0; j < legs.SegmentOptions.length; j++) {
                                    var segments = legs.SegmentOptions[j];
                                    var DateNow = moment.utc(new Date).format();
                                    var DepartureDate = moment.utc(segments.Flight.ETD).format();
                                    if (DateNow >= DepartureDate) {
                                        outboundOptions.splice(i, 1);
                                        i = -1;
                                    }
                                }
                            }
                        }
                        var inboundOptions = data.InboundOptions;
                        for (var i = 0; i < inboundOptions.length; i++) {
                            var legs = inboundOptions[i].Legs[0];
                            if (moment(legs.DepartureDate).format("MMM Do YY") != moment(DateReturn_temp).format("MMM Do YY")) {
                                inboundOptions.splice(i, 1);
                                i = -1;
                            } else {
                                for (var j = 0; j < legs.SegmentOptions.length; j++) {
                                    var segments = legs.SegmentOptions[j];
                                    var DateNow = moment.utc(new Date).format();
                                    var ReturnDate = moment.utc(segments.Flight.ETA).format();
                                    if (DateNow >= ReturnDate) {
                                        outboundOptions.splice(i, 1);
                                        i = -1;
                                    }
                                }
                            }
                        }
                        HideLegs(outboundOptions, totalPassengers);
                        HideLegs(inboundOptions, totalPassengers);
                        caculatorSegment(outboundOptions);
                        caculatorSegment(inboundOptions);
                        caculatorSurcharges(outboundOptions);
                        caculatorSurcharges(inboundOptions);

                        if ($scope.tab == 0)
                            $scope.travelOptionSelect.Selected = outboundOptions;
                        else
                            $scope.travelOptionSelect.Selected = inboundOptions;

                        for (var i = 0; i < outboundOptions.length; i++) {
                            var legs = outboundOptions[i].Legs[0];
                            for (var k = 0; k <= legs.FareOptions.Adultfares.length - 1; k++) {
                                var adult = legs.FareOptions.Adultfares[k];
                                if (adult.FareClass == fareClass) {
                                    for (var j = 0; j < legs.SegmentOptions.length; j++) {
                                        if (segmentChoosen.length != legs.SegmentOptions.length) {
                                            legs.FareOptions.Adultfares[k].isChecked = false;
                                            $scope.isFinding = false;
                                            break;
                                        } else {
                                            var flights = legs.SegmentOptions[j].Flight;
                                            if (flights.ETDLocal != segmentChoosen[j].Flight.ETDLocal && flights.ETALocal != segmentChoosen[j].Flight.ETALocal) {
                                                legs.FareOptions.Adultfares[k].isChecked = false;
                                                $scope.isFinding = false;
                                                break;
                                            } else {
                                                legs.FareOptions.Adultfares[k].isChecked = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        setPromoData(data);
                    }
                    if ($scope.tab == 0) {
                        $scope.tab = 0;
                        $scope.travelOptionSelect.Selected = outboundOptions;
                        $scope.travelOptionPromoSelect = data.OutboundPromoCodeResults;
                    } else {
                        $scope.tab = 1;
                        $scope.travelOptionSelect.Selected = inboundOptions;
                        $scope.travelOptionPromoSelect = data.InboundPromoCodeResults;
                    }
                    $scope.isFinding = false;
                    $scope.isCodePromo = true;
                    $scope.findFlights = "No flights matched your search criteria";
                });
                $scope.travelOptionSelect = $scope.travelOptionPromoSelect = {};
            }
        };

        $scope.departTabClick = function () {
            $scope.tab = 0;
            $scope.travelOptionSelect.Selected = $scope.travelOptionResult.OutboundOptions;
            $scope.travelOptionPromoSelect = $scope.travelOptionResult.OutboundPromoCodeResults;
        };

        $scope.returnTabClick = function () {
            $scope.tab = 1;
            $scope.travelOptionSelect.Selected = $scope.travelOptionResult.InboundOptions;
            $scope.travelOptionPromoSelect = $scope.travelOptionResult.InboundPromoCodeResults;
        };
        function setDateTimeToCurrent(dateString) {
            var date = moment(dateString);
            var current = moment();
            date.hours(current.hours());
            date.minutes(current.minutes());
            date.seconds(current.seconds());
            return date.toISOString();
        }

        function setPromoData(data) {
            $sessionStorage.promoCode = [
                {
                    legNumber: 1,
                    promoDetail: data.OutboundPromoCodeResults
                },
                {
                    legNumber: 2,
                    promoDetail: data.InboundPromoCodeResults
                }
            ];
        }

        var myAirportModal = $modal({ scope: $scope, template: 'app/modal/airport.html', show: false });
        $scope.loadAirportDetail = function (airportCode, airportName) {
            $scope.airportCode = airportCode;
            $scope.airportName = airportName;
            myAirportModal.show();
        };
        $scope.closeAirport = function () {
            myAirportModal.hide();
        };

        var myInfomationModal = $modal({ scope: $scope, template: 'app/modal/information.html', show: false });
        $scope.loadFare = function (fareClass) {
            FareRule.query({
                languagecode: $sessionStorage.languageCode,
                infoSlug: fareClass
            }, function (data) {
                if (data.length > 0) {
                    $scope.informationTitle = data[0].InfoTitle;
                    $scope.informationContent = $sce.trustAsHtml(data[0].InfoContent);
                } else {
                    $scope.informationTitle = fareClass;
                    $scope.informationContent = $sce.trustAsHtml(fareClass);
                }
                $scope.classname = "fareclass";
                myInfomationModal.show();
            });
        };
        $scope.closeInformation = function () {
            myInfomationModal.hide();
        };

        var myChargeSummaryModal = $modal({ scope: $scope, template: 'app/modal/chargeSummary.html', show: false });
        $scope.loadChargeSummary = function (surcharges, pricePackage) {
            $scope.fareName = pricePackage.Description + '(' + pricePackage.FareClass + ')';
            $scope.farePrice = pricePackage.DiscountFareTotal;
            $scope.Currency = pricePackage.Currency.Abbreviation;
            $scope.surcharges = surcharges;
            var sum = 0;
            for (var i = 0; i < surcharges.length; i++) {
                if (surcharges[i].ApplyToAdult) {
                    sum = sum + surcharges[i].Total;
                }
            }
            $scope.summaryCharge = pricePackage.DiscountFareTotal + sum;
            myChargeSummaryModal.show();
        };
        $scope.closeChargeSummaryModal = function () {
            myChargeSummaryModal.hide();
        };

        $scope.submitFlight = function () {
            if ($scope.step2Form.$invalid)
                return;
            if ($scope.packagePriceSelect[0]) {
                if (step1Data.s1RoundTrip == 1 && !$scope.packagePriceSelect[1]) {
                    if ($scope.tab == 1 && !$scope.packagePriceSelect[1])
                        //alert("Please choose Flights return");
                        $translate(['DATA_MESSAGE_CHOOSE_FLIGHTS_RETURN']).then(function (translation) {
                            alert(translation.DATA_MESSAGE_CHOOSE_FLIGHTS_RETURN);
                        });
                    $scope.tab = 1;
                    $scope.travelOptionSelect.Selected = $scope.travelOptionResult.InboundOptions;
                    $location.hash('primary');
                    $anchorScroll();
                } else {
                    if (step1Data.s1RoundTrip == 1) {
                        var departSegmentOptions = $scope.packagePriceSelect[0].leg.SegmentOptions;
                        var returnSegmentOptions = $scope.packagePriceSelect[1].leg.SegmentOptions;
                        var etaDepart = moment(departSegmentOptions[departSegmentOptions.length - 1].Flight.ETALocal);
                        var etdReturn = moment(returnSegmentOptions[returnSegmentOptions.length - 1].Flight.ETDLocal);

                        //console.log(etaDepart, etdReturn);
                        //console.log(etdReturn.diff(etaDepart));
                        if (etdReturn.diff(etaDepart) < 0) {
                            //alert("Depart date time must not be greater than return date time");
                            $translate(['DATA_MESSAGE_DEPART_DATE_GREATER_RETURN_DATE']).then(function (translation) {
                                alert(translation.DATA_MESSAGE_DEPART_DATE_GREATER_RETURN_DATE);
                            });
                            return;
                        }
                    }
                    $sessionStorage.step2 = $scope.packagePriceSelect;
                    $location.path('/step3');
                }
            } else {
                //alert("Please choose Flights depart");
                $translate(['DATA_MESSAGE_CHOOSE_FLIGHTS_DEPART']).then(function (translation) {
                    alert(translation.DATA_MESSAGE_CHOOSE_FLIGHTS_DEPART);
                });
            }
        };
    }

    function Step3Controller($sessionStorage, $scope, $state, $location, $translate, $timeout, $modal, Country, Province) {
        $scope.state = $state.current.name;
        if (!$sessionStorage.step2 && ($sessionStorage.step != "step2" || $sessionStorage.step != "step3")) {
            $location.path('/step2');
        }

        $sessionStorage.step = "step3";
        $timeout(function () {
            $scope.passengerCollapse = 0;
        });

        var loadingModal_country = $modal({
            scope: $scope,
            template: 'app/modal/loading.html',
            show: true,
            backdrop: 'static'
        });
        Country.query({}, function (data) {
            $scope.countryResult = data;
            loadingModal_country.hide();
        });
        $scope.isProvince = false;
        var loadingModal_province = $modal({
            scope: $scope,
            template: 'app/modal/loading.html',
            show: false,
            backdrop: 'static'
        });
        $scope.changeCountry = function () {
            loadingModal_province.show();
            $scope.provinceResult = Province.query({ countryCode: $scope.adults[0].passengerSelectCountry }, function () {
                loadingModal_province.hide();
                $scope.isProvince = true;
            });
        };
        if ($sessionStorage.step3) {
            $scope.adults = $sessionStorage.step3.adults;
            $scope.provinceResult = Province.query({ countryCode: $scope.adults[0].passengerSelectCountry }, function () {
                $scope.isProvince = true;
            });
            $scope.children = $sessionStorage.step3.children;
            $scope.infants = $sessionStorage.step3.infants;
        } else {
            var step1Data = $sessionStorage.step1;

            $scope.adults = Array(parseInt(step1Data.s1Adults));
            for (var i = 0; i < $scope.adults.length; i++)
                $scope.adults[i] = {};

            $scope.children = Array(parseInt(step1Data.s1Children));
            for (var i = 0; i < $scope.children.length; i++) {
                $scope.children[i] = {};
                $scope.children[i].gender = 'Male';
            }

            $scope.infants = Array(parseInt(step1Data.s1Infants));
            for (var i = 0; i < $scope.infants.length; i++) {
                $scope.infants[i] = {};
                $scope.infants[i].gender = 'M';
            }
        }
        $scope.submitPassenger = function () {
            if ($scope.step3Form.$invalid) {
                $scope.$broadcast('show-errors-check-validity');

                //alert("Please fill in all information of all passengers");
                $translate(['DATA_FILL_INFO_OF_PASSENGER']).then(function (translation) {
                    alert(translation.DATA_FILL_INFO_OF_PASSENGER);
                });
                return;
            }
            var data = {
                adults: $scope.adults,
                children: $scope.children,
                infants: $scope.infants
            };
            $sessionStorage.step3 = data;
            $location.path('/step4');
        };
    }

    function Step4Controller($sessionStorage, $scope, $state, $location, $window, $modal, $timeout, Addon) {
        $scope.state = $state.current.name;
        if (!$sessionStorage.step3 && ($sessionStorage.step != "step3" || $sessionStorage.step != "step4")) {
            $location.path('/step3');
        }
        $sessionStorage.step = "step4";

        function collapseFlight() {
            $timeout(function () {
                $scope.addonServiceCollapseFlight1 = 1;
                $scope.addonServiceCollapseFlight2 = 1;
            }, 300000);
        }
        var step1Data = $sessionStorage.step1;

        if ($sessionStorage.step4) {
            $scope.adults = $sessionStorage.step4.adults;
            $scope.children = $sessionStorage.step4.children;
            $scope.infants = $sessionStorage.step4.infants;
        } else {
            $scope.adults = $sessionStorage.step3.adults;
            $scope.children = $sessionStorage.step3.children;
            $scope.infants = $sessionStorage.step3.infants;
        }
        $scope.step1Data = $sessionStorage.step1;
        $scope.step2Data = $sessionStorage.step2;
        $scope.s1FromCode = $sessionStorage.s1FromCode;
        $scope.s1FromName = $sessionStorage.s1FromName;
        $scope.s1ToCode = $sessionStorage.s1ToCode;
        $scope.s1ToName = $sessionStorage.s1ToName;

        for (var i = 0; i < $scope.adults.length; i++) {
            $scope.adults[i].addons = [
                { mealQuality: {}, baggage: null },
                { mealQuality: {}, baggage: null }
            ];
        }
        for (var i = 0; i < $scope.children.length; i++) {
            $scope.children[i].addons = [
                { mealQuality: {}, baggage: {} },
                { mealQuality: {}, baggage: {} }
            ];
        }
        var loadingModal = $modal({
            scope: $scope,
            template: 'app/modal/loading.html',
            show: true,
            backdrop: 'static'
        });

        var addonDepResults = [];
        var addonArrResults = [];
        $scope.addonDepartMeal = [];
        $scope.addonDepartBaggage = [];
        $scope.addonArrivalMeal = [];
        $scope.addonArrivalBaggage = [];
        var indexDepSegment = 0;
        var indexArrSegment = 0;
        for (var i = 0; i < $scope.step2Data[0].leg.SegmentOptions.length; i++) {
            var segment = $scope.step2Data[0].leg.SegmentOptions[i];
            var addon = Addon.query({
                type: "flight",
                depDate: segment.Flight.ETDLocal,
                arrDate: segment.Flight.ETALocal,
                depAirport: segment.Flight.DepartureAirport.Code,
                arrAirport: segment.Flight.ArrivalAirport.Code,
                flight: segment.Flight.Number,
                currency: $sessionStorage.step1.s1CurrencyCode
            }, function (data) {
                function checkMaxPerPax(allocation) {
                    return allocation.MaxQuatityPerPax != 0;
                }
                indexDepSegment++;
                if (data.length != 0) {
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].Name == "Baggage") {
                            data[i].Allocations = data[i].Allocations.filter(checkMaxPerPax);
                            if (data[i].Allocations.length != 0)
                                $scope.addonDepartBaggage.push(data[i]);
                        }
                        if (data[i].Name == "Meal") {
                            data[i].Allocations = data[i].Allocations.filter(checkMaxPerPax);
                            if (data[i].Allocations.length != 0)
                                $scope.addonDepartMeal.push(data[i]);
                        }
                    }
                    addonDepResults.push(data);
                }
                if ($scope.step2Data[0].leg.SegmentOptions.length == indexDepSegment) {
                    if ($scope.step2Data[1] != null) {
                        for (var j = 0; j < $scope.step2Data[1].leg.SegmentOptions.length; j++) {
                            var segment_arr = $scope.step2Data[1].leg.SegmentOptions[j];
                            var addon_arr = Addon.query({
                                type: "flight",
                                depDate: segment_arr.Flight.ETDLocal,
                                arrDate: segment_arr.Flight.ETALocal,
                                depAirport: segment_arr.Flight.DepartureAirport.Code,
                                arrAirport: segment_arr.Flight.ArrivalAirport.Code,
                                flight: segment_arr.Flight.Number,
                                currency: $sessionStorage.step1.s1CurrencyCode
                            }, function (data) {
                                indexArrSegment++;
                                if (data.length != 0) {
                                    for (var k = 0; k < data.length; k++) {
                                        if (data[k].Name == "Baggage") {
                                            data[k].Allocations = data[k].Allocations.filter(checkMaxPerPax);
                                            if (data[k].Allocations.length != 0)
                                                $scope.addonArrivalBaggage.push(data[k]);
                                        }
                                        if (data[k].Name == "Meal") {
                                            data[k].Allocations = data[k].Allocations.filter(checkMaxPerPax);
                                            if (data[k].Allocations.length != 0)
                                                $scope.addonArrivalMeal.push(data[k]);
                                        }
                                    }
                                    addonArrResults.push(data);
                                }
                                if ($scope.step2Data[1].leg.SegmentOptions.length == indexArrSegment) {
                                    loadingModal.hide();
                                    collapseFlight();
                                }
                            });
                        }
                    } else {
                        loadingModal.hide();
                        collapseFlight();
                    }
                }
            });
        }

        $scope.checkObjectIsEmpty = function isEmptyObject(obj) {
            var name;
            for (name in obj) {
                return false;
            }
            return true;
        };

        $scope.checkAddon = function (addonBaggage, addonMeal, index) {
            var hasBaggage = addonBaggage[index] && addonBaggage[index].Allocations && addonBaggage[index].Allocations.length != 0;

            var hasMeal = addonMeal[index] && addonMeal[index].Allocations && addonMeal[index].Allocations.length != 0;

            if (hasBaggage || hasMeal)
                return true;
            else
                return false;
        };
        $scope.checkBaggage = function (addonBaggage, index) {
            var hasBaggage = addonBaggage[index] && addonBaggage[index].Allocations && addonBaggage[index].Allocations.length != 0;
            if (hasBaggage)
                return true;
            else
                return false;
        };
        $scope.checkMeal = function (addonMeal, index) {
            var hasMeal = addonMeal[index] && addonMeal[index].Allocations && addonMeal[index].Allocations.length != 0;

            if (hasMeal)
                return true;
            else
                return false;
        };

        $scope.AddonData = {};
        $scope.submitAddon = function () {
            if ($scope.step4Form.$invalid)
                return;

            function setMealtoPassenger(passengers) {
                for (var i = 0; i < passengers.length; i++) {
                    var passenger = passengers[i];
                    for (var j = 0; j < passenger.addons.length; j++) {
                        var addon = passenger.addons[j];
                        var meal = [];
                        for (var mealID in addon.mealQuality) {
                            var numberMeal = addon.mealQuality[mealID].number;
                            for (var k = 0; k < $scope.addonDepartMeal.length; k++) {
                                var addonDepartMeal_ = $scope.addonDepartMeal[k];
                                for (var l = 0; l < addonDepartMeal_.Allocations.length; l++) {
                                    if (mealID == addonDepartMeal_.Allocations[l].ID) {
                                        var mealOption = addonDepartMeal_.Allocations[l];
                                        mealOption.numberMeal = numberMeal;
                                        meal.push(mealOption);
                                    }
                                }
                            }
                            for (var m = 0; m < $scope.addonArrivalMeal.length; m++) {
                                var addonArrivalMeal_ = $scope.addonArrivalMeal[m];
                                for (var n = 0; n < addonArrivalMeal_.Allocations.length; n++) {
                                    if (mealID == addonArrivalMeal_.Allocations[n].ID) {
                                        var mealOption = addonArrivalMeal_.Allocations[n];
                                        mealOption.numberMeal = numberMeal;
                                        meal.push(mealOption);
                                    }
                                }
                            }
                        }
                        addon.mealSelected = meal;
                    }
                }
            }
            setMealtoPassenger($scope.adults);
            setMealtoPassenger($scope.children);
            var data = {
                adults: $scope.adults,
                children: $scope.children,
                infants: $scope.infants
            };

            $sessionStorage.step4AddonDepart = addonDepResults;
            $sessionStorage.step4AddonArrival = addonArrResults;

            $sessionStorage.step4 = data;
            $location.path('/step5');
        };
    }

    function Step5Controller($sessionStorage, $scope, $state, $modal, $location, $sce, FareRule) {
        $scope.state = $state.current.name;
        if (!$sessionStorage.step4 && ($sessionStorage.step != "step4" || $sessionStorage.step != "step5")) {
            $location.path('/step4');
        }
        $sessionStorage.step = "step5";

        $scope.s1Data = $sessionStorage.step1;
        $scope.s2Data = $sessionStorage.step2;
        if (!$scope.s2Data[1])
            $scope.s2Data.splice(1, 1);
        $scope.s3Data = $sessionStorage.step3;
        $scope.s4Data = $sessionStorage.step4;
        $scope.addonDepart = $sessionStorage.step4AddonDepart;
        $scope.addonArrival = $sessionStorage.step4AddonArrival;
        $scope.costs = [];
        $scope.total = [];
        $scope.addons = [];
        $scope.totalAmount = 0;

        var myAirportModal = $modal({ scope: $scope, template: 'app/modal/airport.html', show: false });
        $scope.loadAirportDetail = function (airportCode, airportName) {
            $scope.airportCode = airportCode;
            $scope.airportName = airportName;
            myAirportModal.show();
        };
        $scope.closeAirport = function () {
            myAirportModal.hide();
        };
        var myInfomationModal = $modal({ scope: $scope, template: 'app/modal/information.html', show: false });
        $scope.loadFare = function (fareClass) {
            FareRule.query({
                languagecode: $sessionStorage.languageCode,
                infoSlug: fareClass
            }, function (data) {
                if (data.length > 0) {
                    $scope.informationTitle = data[0].InfoTitle;
                    $scope.informationContent = $sce.trustAsHtml(data[0].InfoContent);
                } else {
                    $scope.informationTitle = fareClass;
                    $scope.informationContent = $sce.trustAsHtml(fareClass);
                }
                $scope.classname = "fareclass";
                myInfomationModal.show();
            });
        };
        $scope.closeInformation = function () {
            myInfomationModal.hide();
        };

        $scope.s2Data.forEach(function (depart, index) {
            var adultCosts = depart.selected.DiscountFareTotal * $scope.s4Data.adults.length;
            var childCosts = depart.selected.DiscountFareTotal * $scope.s4Data.children.length;
            $scope.costs.push({ adultCosts: adultCosts, childCosts: childCosts });

            var dataAddon;
            if (index == 0)
                dataAddon = $scope.addonDepart;
            else
                dataAddon = $scope.addonArrival;

            var selectedAddons_temp = [];
            var selectedAddons = [];
            var sum = 0;

            $scope.s4Data.adults.forEach(function (adult, adultIndex) {
                for (var g = 0; g < dataAddon.length; g++) {
                    var dataAddon_ = dataAddon[g];
                    for (var i = 0; i < dataAddon_.length; i++) {
                        for (var j = 0; j < dataAddon_[i].Allocations.length; j++) {
                            var allocation = dataAddon_[i].Allocations[j];
                            for (var k = 0; k < adult.addons.length; k++) {
                                var addon = adult.addons[k];
                                if (addon.baggage != null) {
                                    if (allocation.ID == addon.baggage.ID) {
                                        if (allocation.MaxQuatityPerPax >= 1)
                                            selectedAddons_temp.push({
                                                count: 1,
                                                name: addon.baggage.Description,
                                                amount: addon.baggage.ChargeAmount
                                            });
                                        $sessionStorage.step4.adults[adultIndex].addons[k].baggage.legNumber = index + 1;
                                    }
                                }
                            }
                            for (var l = 0; l < addon.mealSelected.length; l++) {
                                var meal = addon.mealSelected[l];
                                if (allocation.ID == meal.ID) {
                                    var quantity = 0;
                                    if (meal.MaxQuatityPerPax >= meal.numberMeal) {
                                        quantity = meal.numberMeal;
                                    } else {
                                        quantity = meal.MaxQuatityPerPax;
                                    }
                                    if (meal.MaxQuatityPerPax >= 1) {
                                        selectedAddons_temp.push({
                                            count: quantity,
                                            name: meal.Description,
                                            amount: meal.ChargeAmount
                                        });
                                        $sessionStorage.step4.adults[adultIndex].addons[k].mealSelected[l].legNumber = index + 1;
                                    }
                                }
                            }
                        }
                    }
                }
            });

            $scope.s4Data.children.forEach(function (child, childIndex) {
                for (var g = 0; g < dataAddon.length; g++) {
                    var dataAddon_ = dataAddon[g];
                    for (var i = 0; i < dataAddon_.length; i++) {
                        for (var j = 0; j < dataAddon_[i].Allocations.length; j++) {
                            var allocation = dataAddon_[i].Allocations[j];
                            for (var k = 0; k < child.addons.length; k++) {
                                var addon = child.addons[k];
                                if (allocation.ID == addon.baggage.ID) {
                                    if (allocation.MaxQuatityPerPax >= 1)
                                        selectedAddons_temp.push({
                                            count: 1,
                                            name: addon.baggage.Description,
                                            amount: addon.baggage.ChargeAmount
                                        });
                                    $sessionStorage.step4.adults[childIndex].addons[k].baggage.legNumber = index + 1;
                                }
                            }
                            for (var l = 0; l < addon.mealSelected.length; l++) {
                                var meal = addon.mealSelected[l];
                                if (allocation.ID == meal.ID) {
                                    var quantity = 0;
                                    if (meal.MaxQuatityPerPax >= meal.numberMeal) {
                                        quantity = meal.numberMeal;
                                    } else {
                                        quantity = meal.MaxQuatityPerPax;
                                    }
                                    if (meal.MaxQuatityPerPax >= 1) {
                                        selectedAddons_temp.push({
                                            count: quantity,
                                            name: meal.Description,
                                            amount: meal.ChargeAmount
                                        });
                                        $sessionStorage.step4.adults[childIndex].addons[k].mealSelected[l].legNumber = index + 1;
                                    }
                                }
                            }
                        }
                    }
                }
            });

            for (var i = 0; i < selectedAddons_temp.length; i++) {
                sum = sum + selectedAddons_temp[i].amount;
            }

            sum = sum + adultCosts + childCosts;

            var addonSet = {};

            selectedAddons_temp.forEach(function (item, index) {
                if (!addonSet[item.name]) {
                    addonSet[item.name] = { count: item.count, total: item.amount };
                } else {
                    addonSet[item.name].count += item.count;
                    addonSet[item.name].total += item.amount;
                }
            });

            $scope.total.push(sum);
            $scope.addons.push(addonSet);
        });

        var surcharges = 0;

        for (var i = 0; i < $scope.s2Data.length; i++) {
            var surcharge_ = [];
            for (var j = 0; j < $scope.s2Data[i].leg.Surcharges.length; j++) {
                var surcharges_index = $scope.s2Data[i].leg.Surcharges[j];
                var numberOfSurcharges = 0;
                var totalOfSurcharges = 0;
                if (surcharges_index.ApplyToAdult && surcharges_index.ApplyToChild) {
                    numberOfSurcharges = $scope.s4Data.adults.length + $scope.s4Data.children.length;
                    totalOfSurcharges = numberOfSurcharges * surcharges_index.Total;
                } else if (surcharges_index.ApplyToAdult) {
                    numberOfSurcharges = $scope.s4Data.adults.length;
                    totalOfSurcharges = numberOfSurcharges * surcharges_index.Total;
                } else if (surcharges_index.ApplyToChild) {
                    numberOfSurcharges = $scope.s4Data.children.length;
                    totalOfSurcharges = (numberOfSurcharges * surcharges_index.Total);
                }
                surcharge_.push({
                    numberOfSurcharges: numberOfSurcharges,
                    totalOfSurcharges: totalOfSurcharges,
                    descriptionSurcharges: surcharges_index.Description
                });

                surcharges += totalOfSurcharges;
            }
            var sumSurcharge = 0;
            surcharge_.forEach(function (item, index) {
                sumSurcharge = sumSurcharge + item.totalOfSurcharges;
            });
            $scope.s2Data[i].surchargeData = surcharge_;
            $scope.s2Data[i].surchargeData.sumSurcharge = sumSurcharge;
        }

        $scope.total.push(surcharges);
        for (var i = 0; i < $scope.total.length; i++)
            $scope.totalAmount = $scope.totalAmount + $scope.total[i];

        $sessionStorage.totalAmount = $scope.totalAmount * 100;
        if ($scope.s4Data.children.length > 0) {
            $scope.isShowAddtional = true;
        } else {
            $scope.isShowAddtional = false;
        }
        $scope.payment = function () {
            if ($scope.step5Form.$invalid)
                return;
            $location.path('/step6');
        };
    }

    function reNewValueUndefine(valueUndefine, valueReplace) {
        if (typeof (valueUndefine) != "undefined" && valueUndefine !== null)
            return valueUndefine;
        else
            return valueReplace;
    }
    function Step6Controller($scope, $state, $locale, $location, $modal, $sce, $sessionStorage, FareRule, Country, Province, Payment, Reservation, Itinerary) {
        $scope.state = $state.current.name;
        if (!$sessionStorage.totalAmount && ($sessionStorage.step != "step5" || $sessionStorage.step != "step6")) {
            $location.path('/step5');
        }
        $sessionStorage.step = 'step6';

        $scope.currentYear = new Date().getFullYear();
        $scope.currentMonth = new Date().getMonth() + 1;
        $scope.months = $locale.DATETIME_FORMATS.MONTH;

        $scope.paymentMethodResult = Payment.query();
        $scope.countryResult = Country.query();
        $scope.isProvince = true;

        $scope.s3Data = $sessionStorage.step3;

        /*if ($sessionStorage.step6) {
        $scope.payMethod = $scope.payMedthod = $sessionStorage.step6.paymentMethod;
        $scope.payBilling = $sessionStorage.step6.paymentBilling;
        $scope.payPurchaser = $sessionStorage.step6.paymentPurchaser;
        } else {}*/
        var loadingModal_province = $modal({
            scope: $scope,
            template: 'app/modal/loading.html',
            show: false,
            backdrop: 'static'
        });
        $scope.changeCountry = function () {
            loadingModal_province.show();
            $scope.provinceResult = Province.query({ countryCode: $scope.payBilling.billingSelectCountry }, function () {
                loadingModal_province.hide();
                $scope.isProvince = true;
            });
        };
        var myInfomationModal = $modal({ scope: $scope, template: 'app/modal/information.html', show: false });
        $scope.closeInformation = function () {
            myInfomationModal.hide();
        };

        $scope.cvvHelp = function () {
            FareRule.query({
                languagecode: $sessionStorage.languageCode,
                infoSlug: 'CVV'
            }, function (data) {
                $scope.informationTitle = data[0].InfoTitle;
                $scope.informationContent = $sce.trustAsHtml(data[0].InfoContent);
                myInfomationModal.show();
            });
        };
        $scope.termsConditionsHelp = function () {
            FareRule.query({
                languagecode: $sessionStorage.languageCode,
                infoSlug: 'terms_conditions'
            }, function (data) {
                $scope.informationTitle = data[0].InfoTitle;
                $scope.informationContent = $sce.trustAsHtml(data[0].InfoContent);
                $scope.classname = "terms_conditions";
                myInfomationModal.show();
            });
        };

        $scope.payMethod = $scope.payMedthod = {};
        $scope.payBilling = {};
        $scope.payPurchaser = {};
        $scope.doIfChecked = function (check) {
            if (check) {
                var adultPrimary = $scope.s3Data.adults[0];
                $scope.payBilling.billingFirstName = adultPrimary.firstName;
                $scope.payBilling.billingLastName = adultPrimary.lastName;
                $scope.payBilling.billingAddress = adultPrimary.passengerAddress;
                $scope.payBilling.billingMobile = adultPrimary.mobile;
                $scope.payBilling.billingSelectCountry = adultPrimary.passengerSelectCountry;
                $scope.provinceResult = Province.query({ countryCode: adultPrimary.passengerSelectCountry }, function () {
                    $scope.isProvince = true;
                });
                $scope.payBilling.billingCity = adultPrimary.billingCity;
            } else {
                $scope.payBilling.billingFirstName = "";
                $scope.payBilling.billingLastName = "";
                $scope.payBilling.billingAddress = "";
                $scope.payBilling.billingMobile = "";
                $scope.payBilling.billingSelectCountry = "";
                $scope.payBilling.billingCity = "";
                $scope.isProvince = false;
            }
        };

        $scope.payNow = function () {
            $sessionStorage.step6 = {
                paymentMethod: $scope.payMethod,
                paymentBilling: $scope.payBilling,
                paymentPurchaser: $scope.payPurchaser
            };

            if ($scope.step6Form.$invalid) {
                $scope.$broadcast('show-errors-check-validity');
                return;
            }
            var payData = $sessionStorage.step2;
            var promoData = $sessionStorage.promoCode;
            var step1Data = $sessionStorage.step1;
            var data = $sessionStorage.step4;

            var legs = [];

            for (var i = 0; i < payData.length; i++) {
                var segments = [];
                payData[i].leg.SegmentOptions.forEach(function (segment) {
                    segments.push({
                        etdLocal: segment.Flight.ETDLocal,
                        etaLocal: segment.Flight.ETALocal,
                        flightCode: segment.Flight.Number,
                        departureAirportCode: segment.Flight.DepartureAirport.Code,
                        arrivalAirportCode: segment.Flight.ArrivalAirport.Code
                    });
                });

                legs.push({
                    adultFares: {
                        bookingClassCode: payData[i].selected.FareCategory,
                        fareCode: payData[i].selected.FareClass,
                        currencyCode: payData[i].selected.Currency.Abbreviation,
                        paxCount: data.adults.length,
                        totalCost: payData[i].selected.Totalfare
                    },
                    childFares: {
                        bookingClassCode: payData[i].selected.FareCategory,
                        fareCode: payData[i].selected.FareClass,
                        currencyCode: payData[i].selected.Currency.Abbreviation,
                        paxCount: data.children.length,
                        totalCost: payData[i].selected.Totalfare
                    },
                    segments: segments,
                    promoCodeID: promoData[i].promoDetail.ID,
                    promoCodeSuffixID: promoData[i].promoDetail.SuffixID
                });
            }

            var userProfileName = "";
            var userProfilePassword = "";
            if ($sessionStorage.isLogin) {
                userProfileName = $sessionStorage.userProfile.ProfileName;
                userProfilePassword = $sessionStorage.userProfile.Password;
            }

            var passengers = [];

            data.adults.forEach(function (adult, index) {
                var infant_ = [];
                if (data.infants && data.infants.length != 0 && data.infants[index] != null) {
                    infant_ = [{
                            gender: data.infants[index].gender,
                            dateOfBirth: data.infants[index].birthday,
                            firstname: data.infants[index].firstName,
                            lastname: data.infants[index].lastName
                        }];
                }

                var title = adult.gender;
                var gender = (adult.gender == "Mr") ? "Male" : "Female";
                passengers.push({
                    title: title,
                    gender: gender,
                    firstName: adult.firstName,
                    lastName: adult.lastName,
                    dateOfBirth: adult.birthday,
                    email: adult.email,
                    mobile: adult.mobile,
                    profileUsername: userProfileName,
                    profilePassword: userProfilePassword,
                    ageCategory: 'Adult',
                    personalContact: {
                        mobile: data.adults[0].mobile,
                        email: data.adults[0].email,
                        name: data.adults[0].lastName
                    },
                    passport: {
                        number: reNewValueUndefine(adult.passportNumber, '12345'),
                        expiryDate: adult.passportExpiry
                    },
                    infants: infant_,
                    flightSSRs: createFlightSSRs(adult)
                });
            });

            data.children.forEach(function (child) {
                //var gender = (child.gender == "Mr") ? "Male" : "Female";
                passengers.push({
                    gender: child.gender,
                    firstName: child.firstName,
                    lastName: child.lastName,
                    dateOfBirth: child.birthday,
                    email: child.email,
                    mobile: child.mobile,
                    profileUsername: userProfileName,
                    profilePassword: userProfilePassword,
                    ageCategory: 'Child',
                    flightSSRs: createFlightSSRs(child)
                });
            });

            function createFlightSSRs(passenger) {
                var flightSSRs = [];

                for (var i = 0; i < passenger.addons.length; i++) {
                    var dataAddon = {};
                    if (passenger.addons[i].mealSelected != null) {
                        for (var j = 0; j < passenger.addons[i].mealSelected.length; j++) {
                            var meal = passenger.addons[i].mealSelected[j];
                            var quantity = 0;
                            if (meal.MaxQuatityPerPax >= meal.numberMeal) {
                                quantity = meal.numberMeal;
                            } else {
                                quantity = meal.MaxQuatityPerPax;
                            }
                            if (meal.MaxQuatityPerPax >= 1) {
                                dataAddon = {
                                    extraAllocationIDCount: meal.ExtraAllocationIDCount,
                                    quantity: meal.numberMeal,
                                    flightAllocID: meal.ID,
                                    legNumber: meal.legNumber,
                                    extraAllocationIDs: meal.ExtraAllocationIDs
                                };
                                flightSSRs.push(dataAddon);
                            }
                        }
                    }
                    if (passenger.addons[i].baggage != null) {
                        if (passenger.addons[i].baggage.MaxQuatityPerPax >= 1) {
                            dataAddon = {
                                extraAllocationIDCount: passenger.addons[i].baggage.ExtraAllocationIDCount,
                                quantity: 1,
                                flightAllocID: passenger.addons[i].baggage.ID,
                                legNumber: passenger.addons[i].baggage.legNumber,
                                extraAllocationIDs: passenger.addons[i].baggage.ExtraAllocationIDs
                            };
                            flightSSRs.push(dataAddon);
                        }
                    }
                }
                return flightSSRs;
            }

            var payment = {};
            if ($scope.payMedthod.paymentMethod != 'PL') {
                payment = {
                    amount: $sessionStorage.totalAmount,
                    currencyCode: step1Data.s1CurrencyCode,
                    paymentType: "CreditCard",
                    creditCardPayment: {
                        address: {
                            addr1: $scope.payBilling.billingAddress,
                            city: $scope.payBilling.billingCity,
                            countryCode: $scope.payBilling.billingSelectCountry,
                            provinceAbbreviation: $scope.payBilling.billingCity,
                            postalCode: 'NA'
                        },
                        expiryMonth: $scope.payMethod.month,
                        expireYear: $scope.payMethod.year,
                        cvv: $scope.payMethod.payCVV,
                        number: $scope.payMethod.payCardNumber,
                        firstName: $scope.payMedthod.payFirstName,
                        lastName: $scope.payMedthod.payLastName,
                        email: $scope.payPurchaser.purchaserEmail,
                        typeCode: $scope.payMedthod.paymentMethod
                    }
                };
            } else {
                payment = {
                    amount: $sessionStorage.totalAmount,
                    currencyCode: step1Data.s1CurrencyCode,
                    paymentType: "Hold"
                };
            }

            var loadingModal = $modal({
                scope: $scope,
                template: 'app/modal/loading.html',
                show: true,
                backdrop: 'static'
            });

            $scope.reservationResponse = Reservation.save({
                legs: legs,
                passengers: passengers,
                payment: payment
            }, function (data) {
                $sessionStorage.step7 = data;
                if (data.OperationSucceeded) {
                    var itinerary = Itinerary.get({
                        reservationNumber: data.ReservationInvoice.ReservationNumber,
                        languageCode: 'en'
                    }, function (boolreturn) {
                        loadingModal.hide();
                        $location.path('/step7');
                    });
                } else {
                    alert(data.OperationMessage);
                    return false;
                }
            });
        };
    }

    function Step7Controller($scope, $state, $location, $sessionStorage, $localStorage) {
        $scope.state = $state.current.name;
        if (!$sessionStorage.step7 && ($sessionStorage.step != "step6" || $sessionStorage.step != "step7")) {
            $location.path('/step6');
        }
        $sessionStorage.step = "step7";
        $scope.s1Data = $sessionStorage.step1;
        $scope.s6Data = $sessionStorage.step6;

        if ($scope.s6Data.paymentMethod.paymentMethod == "PL") {
            $scope.paymentStatus = "Booking Successful";
            var dateDepart = moment($scope.s1Data.s1DateStart);
            var onHoldHours = dateDepart.diff(moment(), "hours");
            if (onHoldHours >= 24) {
                onHoldHours = 24;
            }
            var canceledHours = moment().add(onHoldHours, 'hours');
            $scope.onHoldHours = onHoldHours;
            $scope.cancelledDate = canceledHours.toDate();
        } else
            $scope.paymentStatus = "Payment Successful";

        $scope.totalAmount = $sessionStorage.step7.BookingResult.PaymentAmount;
        $scope.reservationNumber = $sessionStorage.step7.ReservationInvoice.ReservationNumber;

        //$sessionStorage.$reset();
        resetBooking($sessionStorage);
        $scope.toHome = function () {
            $location.path('/home');
        };
    }

    function FinishController($scope, $state, $location) {
        $scope.state = $state.current.name;
    }

    function AirportListFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/airport/:airportId", { airportId: '@id' });
    }

    function TravelOptionFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/travelOption");
    }

    function FareRuleFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/farerule");
    }

    function AddonFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/addon");
    }

    function CountryFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/country");
    }

    function ProvinceFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/province");
    }

    function PaymentFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/paymentmethod");
    }

    function ItineraryFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/itinerary");
    }

    function ReservationFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/reservation");
    }

    //farerule? languageCode = EN & infoSlug = CVV
    // Register controllers to angular module
    angular.module('gms').controller('BookingFlightController', BookingFlightController).controller('Step1Controller', Step1Controller).controller('Step2Controller', Step2Controller).controller('Step3Controller', Step3Controller).controller('Step4Controller', Step4Controller).controller('Step5Controller', Step5Controller).controller('Step6Controller', Step6Controller).controller('Step7Controller', Step7Controller).controller('FinishController', FinishController).factory('AirportList', AirportListFactory).factory('TravelOption', TravelOptionFactory).factory('FareRule', FareRuleFactory).factory('Addon', AddonFactory).factory('Payment', PaymentFactory).factory('Country', CountryFactory).factory('Province', ProvinceFactory).factory('Reservation', ReservationFactory).factory('Itinerary', ItineraryFactory);
})(Booking || (Booking = {}));
//# sourceMappingURL=booking-controller.js.map

/// <reference path="../../../Scripts/typings/angularjs/angular.d.ts"/>
var CheckIn;
(function (_CheckIn) {
    function resetCheckIn(sessionStorage) {
        delete sessionStorage.checkinstep;
        delete sessionStorage.checkinStep1;
        delete sessionStorage.checkinStep2DataTotal;
        delete sessionStorage.checkinStep2;
        delete sessionStorage.checkinStep3;
        delete sessionStorage.checkinStep3_1;
        delete sessionStorage.checkinStep3_2;
        delete sessionStorage.seats;
    }
    var CheckInStep1Controller = (function () {
        function CheckInStep1Controller($sessionStorage, $scope, $state, $location, $modal, AirportList, ReservationInvoice, $translate) {
            //set current name to state
            $scope.state = $state.current.name;

            //set current step to session
            $sessionStorage.checkinstep = "checkinstep1";

            //check if session of step1 is vaild
            if ($sessionStorage.checkinStep1) {
                //set date from session step 1 to field
                var checkinStep1 = $sessionStorage.checkinStep1;

                $scope.reservationCodeValue = checkinStep1.s1ReservationCode;
                $scope.firstNameValue = checkinStep1.s1FirstName;
                $scope.lastNameValue = checkinStep1.s1LastName;
            }

            //function button Check In click on Step 1
            $scope.checkInClick = function () {
                //create object data with data from step 1
                var dataRes = {
                    s1ReservationCode: $scope.reservationCodeValue,
                    s1LastName: $scope.lastNameValue,
                    s1FirstName: $scope.firstNameValue
                };

                //check data of step1Form invalid
                if ($scope.step1Form.$invalid) {
                    //broadcast field invalid
                    $scope.$broadcast('show-errors-check-validity');
                    return;
                } else {
                    var loadingModal = $modal({
                        scope: $scope,
                        template: 'app/modal/loading.html',
                        show: true,
                        backdrop: 'static'
                    });
                    $scope.reservationInvoice = ReservationInvoice.get({
                        reservationNumber: $scope.reservationCodeValue,
                        paxLastName: $scope.lastNameValue,
                        paxFirstName: $scope.firstNameValue
                    }, function (data) {
                        loadingModal.hide();
                        var reservationInvoiceResult = data;
                        var countLegisCancelled = 0;
                        var countsegment = 0;
                        if (typeof (reservationInvoiceResult.Legs) == 'undefined' && reservationInvoiceResult.Legs == null) {
                            $translate(['DATA_MESSAGE_RESERVATION_NULL']).then(function (translation) {
                                alert(translation.DATA_MESSAGE_RESERVATION_NULL);
                            });
                            return;
                        }
                        if (reservationInvoiceResult[0] == "n") {
                            //set data step 1 to sesstion
                            $sessionStorage.reservationInvoiceResult = data;
                            $sessionStorage.checkinStep1 = dataRes;

                            // redirect to step 2
                            $location.path('/checkinstep2');
                            $scope.vaildResevation = false;
                        } else {
                            for (var i = 0; i < reservationInvoiceResult.Legs.length; i++) {
                                var leg = reservationInvoiceResult.Legs[i];
                                for (var j = 0; j < leg.Segments.length; j++) {
                                    countsegment += 1;
                                    var segment = leg.Segments[j];
                                    if (segment.ResStatus == "Cancelled") {
                                        countLegisCancelled += 1;
                                    }
                                }
                            }
                            if (countsegment == countLegisCancelled) {
                                $translate(['DATA_MESSAGE_SEGMENT_CANCELLED']).then(function (translation) {
                                    alert(translation.DATA_MESSAGE_SEGMENT_CANCELLED);
                                });
                                return;
                            } else if (countsegment > countLegisCancelled) {
                                //set data step 1 to sesstion
                                $sessionStorage.reservationInvoiceResult = data;
                                $sessionStorage.checkinStep1 = dataRes;

                                // redirect to step 2
                                $location.path('/checkinstep2');
                            }
                        }
                    });
                }
            };
        }
        return CheckInStep1Controller;
    })();
    var CheckInStep2Controller = (function () {
        function CheckInStep2Controller($sessionStorage, $scope, $state, $location, $modal, $sce, FareRule, ReservationInvoice) {
            //set current name to state
            $scope.state = $state.current.name;
            if (!$sessionStorage.checkinStep1 && ($sessionStorage.checkinstep != "checkinstep1" || $sessionStorage.checkinstep != "checkinstep2")) {
                $location.path('/checkin');
            }

            //set current step to session
            $sessionStorage.checkinstep = "checkinstep2";

            var checkinStep1 = $sessionStorage.checkinStep1;
            var loadingModal = $modal({
                scope: $scope,
                template: 'app/modal/loading.html',
                show: true,
                backdrop: 'static'
            });

            function caculatorSegment(legOption) {
                legOption.SegmentsTemp = [];
                angular.copy(legOption.Segments, legOption.SegmentsTemp);
                var airCraftIndex = 0;
                var countStop = 1;

                if (legOption.SegmentsTemp.length == 1) {
                    var segment = legOption.SegmentsTemp;
                    segment[0].TimeTransit = "";
                    segment[0].checkAirCraft = false;
                } else {
                    legOption.SegmentsTemp[0].TimeTransit = "";
                    legOption.SegmentsTemp[0].checkAirCraft = false;
                    for (var j = 1; j < legOption.SegmentsTemp.length; j++) {
                        var segment = legOption.SegmentsTemp;
                        if (segment[j].AircraftName == segment[airCraftIndex].AircraftName && segment[j].Flight == segment[airCraftIndex].Flight) {
                            segment[j].checkAirCraft = true;
                            var timeeta = segment[j - 1].ArrivalLocal;
                            var timeetd = segment[j].DepartureLocal;
                            var totalMinutes = moment(timeetd).diff(timeeta, 'minutes');
                            var hours = Math.floor(totalMinutes / 60);
                            var minutes = totalMinutes % 60;
                            segment[j].TimeTransit = hours + 'h ' + minutes + 'm';
                            legOption.SegmentsTemp[0].ArrivalLocal = segment[j].ArrivalLocal;
                            legOption.SegmentsTemp[0].ArrivalAirportCode = segment[j].ArrivalAirportCode;
                            legOption.SegmentsTemp[0].ArrivalAirportName = segment[j].ArrivalAirportName;
                            segment[j].countStop = countStop;
                            countStop++;
                            airCraftIndex = j;
                        } else {
                            segment[j].checkAirCraft = false;
                            airCraftIndex = j;
                        }
                    }
                }
            }
            $scope.reservationInvoice = ReservationInvoice.get({
                reservationNumber: checkinStep1.s1ReservationCode,
                paxLastName: checkinStep1.s1LastName,
                paxFirstName: checkinStep1.s1FirstName
            }, function (data) {
                loadingModal.hide();
                try  {
                    $scope.vaildResevation = true;

                    //$sessionStorage.reservationInvoiceResult
                    var reservationInvoiceResult = data;
                    var isCheckin = false;
                    var isPayed = false;
                    var countNoInfantNoCheckin = false;
                    for (var i = 0; i < reservationInvoiceResult.Legs.length; i++) {
                        var leg = reservationInvoiceResult.Legs[i];
                        var arrivalAirportName_ = "";
                        for (var j = 0; j < leg.Segments.length; j++) {
                            var segment = leg.Segments;
                            arrivalAirportName_ = segment[j].ArrivalAirportName;
                        }
                        leg.DepartureAirportName = leg.Segments[0].DepartureAirportName;
                        leg.ArrivalAirportName = arrivalAirportName_;

                        caculatorSegment(leg);

                        var now = moment();
                        var etdofLeg = moment(leg.Segments[0].DepartureLocal);
                        var totalHourToETD = -(moment(now).diff(etdofLeg, 'hour'));

                        var statusClass = "";
                        var status = "";
                        if (reservationInvoiceResult.ResStatus.IsCancelled == true) {
                            status = "Cancelled";
                            statusClass = "closed";
                        } else if (totalHourToETD <= 0) {
                            status = "Closed";
                            statusClass = "closed";
                        } else if (totalHourToETD > 48) {
                            status = "Waiting";
                            statusClass = "waiting";
                        } else {
                            var allChecked = true;
                            for (var k = 0; k < leg.Segments[0].PaxList.length; k++) {
                                var pax = leg.Segments[0].PaxList[k];
                                for (var m = 0; m < reservationInvoiceResult.Passengers.length; m++) {
                                    var passenger = reservationInvoiceResult.Passengers[m];
                                    if (pax.PaxGroupID == passenger.PaxGroupID) {
                                        if (pax.CheckinStatus == 'Not Checked-in' && passenger.Infants.length != 0) {
                                            countNoInfantNoCheckin = true;
                                            leg.countNoInfantNoCheckin = true;
                                        } else if (pax.CheckinStatus == 'Not Checked-in' && passenger.Infants.length == 0) {
                                            allChecked = false;
                                            leg.countNoInfantNoCheckin = false;
                                            break;
                                        }
                                    }
                                }
                            }
                            if (allChecked == false || countNoInfantNoCheckin == true) {
                                status = "Check In";
                                statusClass = "checkin";
                            } else {
                                status = "Checked In";
                                statusClass = "checkedin";
                            }
                        }
                        leg.FlightNumberofLeg = i + 1;
                        leg.Status = status;
                        leg.StatusClass = statusClass;
                        leg.allChecked = allChecked;
                        if (statusClass == "checkin") {
                            isCheckin = true;
                        }
                    }
                } catch (err) {
                    $scope.vaildResevation = false;
                }

                /*
                ● Check In: If the difference of the ETD and the current time is in the allowed time for Check In.
                ● Checked In: If all passengers have checked in before (not count passengers with infant).
                ● Waiting: the time for check in is not ready yet.
                ● Closed: the flight was departed.
                */
                $scope.isCheckin = isCheckin;
                $scope.s2Legs = reservationInvoiceResult.Legs;
                $scope.checkinStep2DataTotal = reservationInvoiceResult;
                $sessionStorage.checkinStep2DataTotal = reservationInvoiceResult;
                $scope.checkcountNoInfantNoCheckinInLeg = function (Legs) {
                    for (var i = 0; i < Legs.length; i++) {
                        var Leg = Legs[i];
                        if (Leg.countNoInfantNoCheckin == false) {
                            return false;
                        }
                    }
                    return true;
                };
            }, function (respone) {
                loadingModal.hide();
                $scope.vaildResevation = false;
            });

            var myAirportModal = $modal({ scope: $scope, template: 'app/modal/airport.html', show: false });
            $scope.loadAirportDetail = function (airportCode, airportName) {
                $scope.airportCode = airportCode;
                $scope.airportName = airportName;
                myAirportModal.show();
            };
            $scope.closeAirport = function () {
                myAirportModal.hide();
            };

            var myInfomationModal_ = $modal({ scope: $scope, template: 'app/modal/information.html', show: false });
            $scope.loadFare = function (fareClass) {
                FareRule.query({
                    languagecode: $sessionStorage.languageCode,
                    infoSlug: fareClass
                }, function (data) {
                    if (data.length > 0) {
                        $scope.informationTitle = data[0].InfoTitle;
                        $scope.informationContent = $sce.trustAsHtml(data[0].InfoContent);
                    } else {
                        $scope.informationTitle = fareClass;
                        $scope.informationContent = $sce.trustAsHtml(fareClass);
                    }
                    $scope.classname = "fareclass";
                    myInfomationModal_.show();
                });
            };
            $scope.closeInformation = function () {
                myInfomationModal_.hide();
            };

            $scope.toPassengers = function () {
                var s2_legs = $scope.s2Legs;
                for (var i = 0; i < s2_legs.length; i++) {
                    var s2_Leg = s2_legs[i];
                    if (s2_Leg.StatusClass == "checkin" && s2_Leg.allChecked == false) {
                        $sessionStorage.checkinStep2 = s2_Leg;
                        $sessionStorage.checkinStep2.Passengers = $sessionStorage.checkinStep2DataTotal.Passengers;
                        break;
                    }
                }
                $location.path('/checkinstep3');
            };

            $scope.toback = function () {
                resetCheckIn($sessionStorage);
                $location.path('/checkin');
            };
        }
        return CheckInStep2Controller;
    })();

    var CheckInStep3Controller = (function () {
        function CheckInStep3Controller($sessionStorage, $scope, $state, $location, ReservationInvoice) {
            //set current name to state
            $scope.state = $state.current.name;
            if (!$sessionStorage.checkinStep2 && ($sessionStorage.checkinstep != "checkinstep2" || $sessionStorage.checkinstep != "checkinstep3")) {
                $location.path('/checkinstep2');
            }

            //set current step to session
            $sessionStorage.checkinstep = "checkinstep3";
            var checkinStep2 = $sessionStorage.checkinStep2;
            $scope.checkinStep2 = checkinStep2;
            var isCheckin = false;
            for (var i = 0; i < checkinStep2.Passengers.length; i++) {
                var passenger = checkinStep2.Passengers[i];
                if (passenger.Infants.length == 0) {
                    isCheckin = true;
                    break;
                }
            }
            $scope.isCheckin = isCheckin;
            $scope.selectSeat = function () {
                $sessionStorage.checkinStep3 = [];
                for (var i = 0; i < checkinStep2.Passengers.length; i++) {
                    var passenger = checkinStep2.Passengers[i];
                    if (passenger.Infants.length == 0) {
                        $sessionStorage.checkinStep3.push(passenger);
                    }
                }
                $location.path('/checkinstep3_1');
            };
            $scope.toHome = function () {
                $location.path('/home');
            };
        }
        return CheckInStep3Controller;
    })();
    var CheckInStep31Controller = (function () {
        function CheckInStep31Controller($sessionStorage, $scope, $state, $location, $modal, SeatMap, $translate) {
            //set current name to state
            $scope.state = $state.current.name;
            if (!$sessionStorage.checkinStep3 && ($sessionStorage.checkinstep != "checkinstep3" || $sessionStorage.checkinstep != "checkinstep3_1")) {
                $location.path('/checkinstep3');
            }

            //set current step to session
            $sessionStorage.checkinstep = "checkinstep3_1";
            var checkinStep1 = $sessionStorage.checkinStep1;
            var checkinStep2 = $sessionStorage.checkinStep2;
            var checkinStep3 = $sessionStorage.checkinStep3;
            for (var i = 0; i < checkinStep3.length; i++) {
                if (!(checkinStep3[i].RowNumber) && !(checkinStep3[i].SeatNumber)) {
                    checkinStep3[i].RowNumber = "";
                    checkinStep3[i].SeatNumber = "";
                }
            }
            $scope.checkinStep3Data = checkinStep3;
            var loadingModal = $modal({
                scope: $scope,
                template: 'app/modal/loading.html',
                show: true,
                backdrop: 'static'
            });
            $scope.reservationInvoice = SeatMap.get({
                reservationNumber: checkinStep1.s1ReservationCode,
                legNumber: checkinStep2.LegNumber
            }, function (data) {
                loadingModal.hide();
                var seats = data.Seats;
                $sessionStorage.seats = seats;
                $scope.seatmap = [];
                seats.forEach(function (seat) {
                    if ($scope.seatmap[seat.RowNumber - 1] == null)
                        $scope.seatmap[seat.RowNumber - 1] = {};
                    seat.isSelected = false;
                    for (var i = 0; i < $scope.checkinStep3Data.length; i++) {
                        if (seat.RowNumber == $scope.checkinStep3Data[i].RowNumber && seat.SeatNumber == $scope.checkinStep3Data[i].SeatNumber) {
                            seat.isSelected = true;
                        }
                    }
                    $scope.seatmap[seat.RowNumber - 1][seat.SeatNumber] = seat;
                });
            });
            var myPassengerModal = $modal({ scope: $scope, template: 'app/modal/passenger.html', show: false });
            $scope.chooseSeat = function (seat) {
                if (seat.Availability == 10 || seat.Availability == 11) {
                    myPassengerModal.show();
                    $scope.chooseSeatInMap = seat;
                } else {
                    //alert("Seat not available");
                    $translate(['DATA_MESSAGE_SEAT_NOT_AVAILABLE']).then(function (translation) {
                        alert(translation.DATA_MESSAGE_SEAT_NOT_AVAILABLE);
                    });
                }
            };
            $scope.acceptSeat = function () {
                myPassengerModal.hide();
            };
            $scope.choosePassenger = function (passenger) {
                for (var i = 0; i < $scope.checkinStep3Data.length; i++) {
                    if (passenger.PaxGroupID == $scope.checkinStep3Data[i].PaxGroupID) {
                        $scope.checkinStep3Data[i].RowNumber = $scope.chooseSeatInMap.RowNumber;
                        $scope.checkinStep3Data[i].SeatNumber = $scope.chooseSeatInMap.SeatNumber;
                    } else {
                        if ($scope.chooseSeatInMap.RowNumber == $scope.checkinStep3Data[i].RowNumber && $scope.chooseSeatInMap.SeatNumber == $scope.checkinStep3Data[i].SeatNumber) {
                            $scope.checkinStep3Data[i].RowNumber = "";
                            $scope.checkinStep3Data[i].SeatNumber = "";
                        }
                    }
                }
                var _seats = $sessionStorage.seats;
                $scope.seatmap = [];
                _seats.forEach(function (seat) {
                    if ($scope.seatmap[seat.RowNumber - 1] == null)
                        $scope.seatmap[seat.RowNumber - 1] = {};
                    seat.isSelected = false;
                    for (var i = 0; i < $scope.checkinStep3Data.length; i++) {
                        if (seat.RowNumber == $scope.checkinStep3Data[i].RowNumber && seat.SeatNumber == $scope.checkinStep3Data[i].SeatNumber) {
                            seat.isSelected = true;
                        }
                    }

                    $scope.seatmap[seat.RowNumber - 1][seat.SeatNumber] = seat;
                });
                $sessionStorage.checkinStep3 = $scope.checkinStep3Data;
            };
            $scope.selectSeat = function () {
                var endS3 = $sessionStorage.checkinStep3;
                var isCheck = true;
                for (var i = 0; i < endS3.length; i++) {
                    if (endS3[i].RowNumber == "" && endS3[i].SeatNumber == "") {
                        isCheck = false;
                        break;
                    }
                }
                if (isCheck == false) {
                    //alert("Please choose seat for passenger");
                    $translate(['DATA_MESSAGE_CHOOSE_SEAT_FOR_PASSENGER']).then(function (translation) {
                        alert(translation.DATA_MESSAGE_CHOOSE_SEAT_FOR_PASSENGER);
                    });
                } else {
                    $sessionStorage.checkinStep3 = endS3;
                    $sessionStorage.checkinStep3_1 = true;
                    $location.path('/checkinstep3_2');
                }
            };
        }
        return CheckInStep31Controller;
    })();
    var CheckInStep32Controller = (function () {
        function CheckInStep32Controller($sessionStorage, $scope, $state, $location, SeatMap) {
            //set current name to state
            $scope.state = $state.current.name;
            if (!$sessionStorage.checkinStep3_1 && ($sessionStorage.checkinstep != "checkinstep3_1" || $sessionStorage.checkinstep != "checkinstep3_2")) {
                $location.path('/checkinstep3_1');
            }

            //set current step to session
            $sessionStorage.checkinstep = "checkinstep3_2";
            var checkinStep2 = $sessionStorage.checkinStep2;
            var checkinStep3 = $sessionStorage.checkinStep3;

            for (var i = 0; i < checkinStep2.Passengers.length; i++) {
                var passenger = checkinStep2.Passengers[i];
                for (var j = 0; j < checkinStep3.length; j++) {
                    if (passenger.PaxGroupID == checkinStep3[j].PaxGroupID) {
                        checkinStep2.Passengers[i] = checkinStep3[j];
                    }
                }
            }
            $scope.checkinStep2 = checkinStep2;
            $scope.confirmSeat = function () {
                var checkinStep1 = $sessionStorage.checkinStep1;
                var checkinStep2 = $sessionStorage.checkinStep2;
                var checkinStep3 = $sessionStorage.checkinStep3;
                var passengers = [];
                for (var i = 0; i < checkinStep3.length; i++) {
                    var pass = checkinStep3[i];
                    var data_pass = {
                        lastname: pass.Lastname,
                        firstName: pass.Firstname,
                        rowNumber: pass.RowNumber,
                        seatNumber: pass.SeatNumber
                    };
                    passengers.push(data_pass);
                }
                $scope.reservationResponse = SeatMap.save({
                    reservationNumber: parseInt(checkinStep1.s1ReservationCode),
                    legNumber: checkinStep2.LegNumber,
                    passengers: passengers
                }, function (data) {
                    if (data.OperationSucceeded) {
                        $sessionStorage.checkinStep3_2 = true;
                        $location.path('/checkinstep4');
                    } else {
                        alert(data.OperationMessage);
                        return false;
                    }
                });
            };
        }
        return CheckInStep32Controller;
    })();
    var CheckInStep4Controller = (function () {
        function CheckInStep4Controller($sessionStorage, $scope, $state, $location, $modal, CheckIn) {
            //set current name to state
            $scope.state = $state.current.name;
            if (!$sessionStorage.checkinStep3_2 && ($sessionStorage.checkinstep != "checkinstep3_2" || $sessionStorage.checkinstep != "checkinstep4")) {
                $location.path('/checkinstep3_2');
            }

            //set current step to session
            $sessionStorage.checkinstep = "checkinstep4";
            var checkinStep1 = $sessionStorage.checkinStep1;
            var checkinStep2 = $sessionStorage.checkinStep2;
            var checkinStep3 = $sessionStorage.checkinStep3;

            var myAirportModal = $modal({ scope: $scope, template: 'app/modal/airport.html', show: false });
            $scope.loadAirportDetail = function (airportCode, airportName) {
                $scope.airportCode = airportCode;
                $scope.airportName = airportName;
                myAirportModal.show();
            };
            $scope.closeAirport = function () {
                myAirportModal.hide();
            };

            $scope.DepartureAirportName = checkinStep2.Segments[0].DepartureAirportName;
            for (var i = 0; i < checkinStep2.Segments.length; i++) {
                $scope.ArrivalAirportName = checkinStep2.Segments[i].ArrivalAirportName;
            }
            $scope.checkinStep1 = checkinStep1;
            $scope.checkinStep2 = checkinStep2;
            $scope.checkinStep3 = checkinStep3;
            var loadingModal = $modal({
                scope: $scope,
                template: 'app/modal/loading.html',
                show: true,
                backdrop: 'static'
            });
            CheckIn.save({
                reservationNumber: parseInt(checkinStep1.s1ReservationCode),
                legNumber: checkinStep2.LegNumber,
                passengers: checkinStep3,
                languageCode: "VI"
            }, function (data) {
                loadingModal.hide();
                if (data.OperationSucceeded) {
                    $scope.checked = true;
                } else {
                    alert(data.OperationMessage);
                }
            });
            $scope.toHome = function () {
                resetCheckIn($sessionStorage);
                $location.path('/home');
            };
        }
        return CheckInStep4Controller;
    })();

    function FareRuleFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/farerule");
    }
    function ReservationInvoiceFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/reservation");
    }
    function SeatMapFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/seatmap");
    }
    function CheckInFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/checkin");
    }

    // Register controllers to angular
    angular.module('gms').controller('CheckInStep1Controller', CheckInStep1Controller).controller('CheckInStep2Controller', CheckInStep2Controller).controller('CheckInStep3Controller', CheckInStep3Controller).controller('CheckInStep31Controller', CheckInStep31Controller).controller('CheckInStep32Controller', CheckInStep32Controller).controller('CheckInStep4Controller', CheckInStep4Controller).factory('FareRule', FareRuleFactory).factory('ReservationInvoice', ReservationInvoiceFactory).factory('SeatMap', SeatMapFactory).factory('CheckIn', CheckInFactory);
})(CheckIn || (CheckIn = {}));
//# sourceMappingURL=checkin-controller.js.map

/// <reference path="../../../Scripts/typings/angularjs/angular.d.ts"/>
var Core;
(function (Core) {
    // Core Controller
    var CoreController = (function () {
        function CoreController($scope, $state, $location, $window, $translate, $sessionStorage, $anchorScroll) {
            //change header for homepage or page
            $scope.state = $state.current.name;
            if ($sessionStorage.languageCode) {
                $translate.use($sessionStorage.languageCode);
            }

            $scope.toogleDropdown = function () {
                $scope.dropdownOpenned = !$scope.dropdownOpenned;
            };
            $scope.$on('$stateChangeStart', function (event, toState) {
                $scope.state = toState.name;
                $anchorScroll();
                switch ($scope.state) {
                    case 'information':
                        $scope.title_name = "Information";
                        break;
                    case 'aboutus':
                        $scope.title_name = "About Intelisys";
                        break;
                    case 'contactus':
                        $scope.title_name = "Contact Intelisys";
                        break;
                    case 'language':
                        $scope.title_name = "Language";
                        break;
                    case 'step1':
                        $scope.title_name = "Search Flight";
                        break;
                    case 'step2':
                        $scope.title_name = "Select Flight";
                        break;
                    case 'step3':
                        $scope.title_name = "Passenger Details";
                        break;
                    case 'step4':
                        $scope.title_name = "Additional Services";
                        break;
                    case 'step5':
                        $scope.title_name = "Booking Detail";
                        break;
                    case 'step6':
                        $scope.title_name = "Payment Details";
                        break;
                    case 'step7':
                        $scope.title_name = "Confirmed";
                        break;
                    case 'finish':
                        $scope.title_name = "Confirmed";
                        break;
                    case 'checkin':
                        $scope.title_name = "Check In";
                        break;
                    case 'checkinstep2':
                        $scope.title_name = "Choose Your Flight";
                        break;
                    case 'checkinstep3':
                        $scope.title_name = "Passenger List";
                        break;
                    case 'checkinstep3_1':
                        $scope.title_name = "Seat Selection";
                        break;
                    case 'checkinstep3_2':
                        $scope.title_name = "Passenger List";
                        break;
                    case 'checkinstep4':
                        $scope.title_name = "Boarding Pass";
                        break;
                    case 'flightstatus':
                        $scope.title_name = "Flight Status";
                        break;
                    case 'flightstatus2':
                        $scope.title_name = "Flight Status";
                        break;
                    case 'flightstatus3':
                        $scope.title_name = "View Seat";
                        break;
                    case 'user':
                        $scope.title_name = $sessionStorage.userProfile.PaxProfile.Lastname;
                        break;
                    case 'login':
                        $scope.title_name = "Member Login";
                        break;
                    case 'register':
                        $scope.title_name = "Create Account";
                        break;
                    case 'changepassword':
                        $scope.title_name = "Change Password";
                        break;
                    case 'accountcreated':
                        $scope.title_name = "Account Created";
                        break;
                    case 'updateprofile':
                        $scope.title_name = "Update Profile";
                        break;
                    case 'reservationhistory':
                        $scope.title_name = "Reservation History";
                        break;
                    case 'reservationdetail':
                        $scope.title_name = "Reservation Detail";
                        break;
                    default:
                        $scope.title_name = "Intelisys";
                }
            });
        }
        return CoreController;
    })();
    Core.CoreController = CoreController;

    // Head Controller
    var HeadController = (function () {
        function HeadController($scope, $state, $location) {
            $scope.favicon = "http://exe.com.vn/wp-content/themes/exe/images/favicon.ico";
            $scope.title = "Generic Mobile Site";
            $scope.type = "cms";
            $scope.desc = "Generic Mobile Site";
            $scope.url = "URL";
        }
        return HeadController;
    })();
    Core.HeadController = HeadController;
    function toHomeController($scope, $window, $location) {
        $scope.toHome = function () {
            $location.path('/home');
        };
    }
    Core.toHomeController = toHomeController;
    function SliderController($scope) {
        var base = '';
        $scope.pictures = 'app/img/banner.png';
        //$scope.pictures = 'app/img/viaair/banner_img.jpg';
        //$scope.pictures = 'app/img/cma/banner_img.jpg';
        //$scope.pictures = 'app/img/pasco/banner_img_pasco.jpg';
        /*$scope.pictures = [
        { src: base + 'app/img/banner.png' },
        { src: base + 'app/img/banner.png' },
        { src: base + 'app/img/banner.png' }
        ];*/
        //viaair
        /*$scope.pictures = [
        { src: base + 'app/img/banner.png' },
        { src: base + 'app/img/banner.png' },
        { src: base + 'app/img/banner.png' }
        ];*/
        //cma
        /*$scope.pictures = [
        { src: base + 'app/img/cma/banner_img.jpg' },
        { src: base + 'app/img/cma/banner_img.jpg' },
        { src: base + 'app/img/cma/banner_img.jpg' }
        ];*/
        //pasco
        /*$scope.pictures = [
        { src: base + 'app/img/pasco/banner_img_pasco.jpg' },
        { src: base + 'app/img/pasco/banner_img_pasco.jpg' },
        { src: base + 'app/img/pasco/banner_img_pasco.jpg' }
        ];*/
    }
    Core.SliderController = SliderController;

    // Register controllers to angular module
    angular.module('gms').controller('CoreController', CoreController).controller('HeadController', HeadController).controller('toHomeController', toHomeController).controller('SliderController', SliderController);
})(Core || (Core = {}));
//# sourceMappingURL=core-controller.js.map

/// <reference path="../../../Scripts/typings/angularjs/angular.d.ts"/>
var FlightStatus;
(function (FlightStatus) {
    function resetFlightStatus(sessionStorage) {
        delete sessionStorage.flightstatusStep;
        delete sessionStorage.flightstatusStep1;
        delete sessionStorage.flightstatusStep2DataTotal;
    }
    var FlightStatusStep1Controller = (function () {
        function FlightStatusStep1Controller($sessionStorage, $scope, $state, $location) {
            //set current name to state
            $scope.state = $state.current.name;

            //set current step to session
            $sessionStorage.flightstatusStep = "flightstatusstep1";

            //check if session of step1 is vaild
            if ($sessionStorage.flightstatusStep1) {
                //set date from session step 1 to field
                var flightstatusStep1 = $sessionStorage.flightstatusStep1;

                $scope.reservationCodeValue = flightstatusStep1.s1ReservationCode;
                $scope.firstNameValue = flightstatusStep1.s1FirstName;
                $scope.lastNameValue = flightstatusStep1.s1LastName;
            }

            //function button Check In click on Step 1
            $scope.flightStatus = function () {
                //check data of step1Form invalid
                if ($scope.step1Form.$invalid) {
                    //broadcast field invalid
                    $scope.$broadcast('show-errors-check-validity');
                    return;
                }

                //create object data with data from step 1
                var data = {
                    s1ReservationCode: $scope.reservationCodeValue,
                    s1LastName: $scope.lastNameValue,
                    s1FirstName: $scope.firstNameValue
                };

                //set data step 1 to sesstion
                $sessionStorage.flightstatusStep1 = data;

                //redirect to step 2
                $location.path('/flightstatus2');
            };
        }
        return FlightStatusStep1Controller;
    })();
    var FlightStatusStep2Controller = (function () {
        function FlightStatusStep2Controller($sessionStorage, $scope, $state, $location, $modal, ReservationInvoice) {
            //set current name to state
            $scope.state = $state.current.name;
            if (!$sessionStorage.flightstatusStep1 && ($sessionStorage.flightstatusStep != "flightstatusstep1" || $sessionStorage.flightstatusStep != "flightstatusstep2")) {
                $location.path('/checkin');
            }

            //set current step to session
            $sessionStorage.checkinstep = "flightstatusstep2";
            var flightstatusStep1 = $sessionStorage.flightstatusStep1;
            var loadingModal = $modal({ scope: $scope, template: 'app/modal/loading.html', show: true });

            function caculatorSegment(legOption) {
                legOption.SegmentsTemp = [];
                angular.copy(legOption.Segments, legOption.SegmentsTemp);
                var airCraftIndex = 0;
                var countStop = 1;

                if (legOption.SegmentsTemp.length == 1) {
                    var segment = legOption.SegmentsTemp;
                    segment[0].TimeTransit = "";
                    segment[0].checkAirCraft = false;
                } else {
                    legOption.SegmentsTemp[0].TimeTransit = "";
                    legOption.SegmentsTemp[0].checkAirCraft = false;
                    for (var j = 1; j < legOption.SegmentsTemp.length; j++) {
                        var segment = legOption.SegmentsTemp;
                        if (segment[j].AircraftName == segment[airCraftIndex].AircraftName && segment[j].Flight == segment[airCraftIndex].Flight) {
                            segment[j].checkAirCraft = true;
                            var timeeta = segment[j - 1].ArrivalLocal;
                            var timeetd = segment[j].DepartureLocal;
                            var totalMinutes = moment(timeetd).diff(timeeta, 'minutes');
                            var hours = Math.floor(totalMinutes / 60);
                            var minutes = totalMinutes % 60;
                            segment[j].TimeTransit = hours + 'h ' + minutes + 'm';
                            legOption.SegmentsTemp[0].ArrivalLocal = segment[j].ArrivalLocal;
                            legOption.SegmentsTemp[0].ArrivalAirportCode = segment[j].ArrivalAirportCode;
                            legOption.SegmentsTemp[0].ArrivalAirportName = segment[j].ArrivalAirportName;
                            segment[j].countStop = countStop;
                            countStop++;
                            airCraftIndex = j;
                        } else {
                            segment[j].checkAirCraft = false;
                            airCraftIndex = j;
                        }
                    }
                }
            }

            $scope.reservationInvoice = ReservationInvoice.get({
                reservationNumber: flightstatusStep1.s1ReservationCode,
                paxLastName: flightstatusStep1.s1LastName,
                paxFirstName: flightstatusStep1.s1FirstName
            }, function (data) {
                loadingModal.hide();
                try  {
                    $scope.vaildResevation = true;
                    var reservationInvoiceResult = data;
                    var isCheckin = false;
                    var isPayed = false;
                    var countNoInfantNoCheckin = false;
                    for (var i = 0; i < reservationInvoiceResult.Legs.length; i++) {
                        var leg = reservationInvoiceResult.Legs[i];
                        var arrivalAirportName_ = "";
                        for (var j = 0; j < leg.Segments.length; j++) {
                            var segment = leg.Segments;
                            arrivalAirportName_ = segment[j].ArrivalAirportName;
                        }
                        leg.DepartureAirportName = leg.Segments[0].DepartureAirportName;
                        leg.ArrivalAirportName = arrivalAirportName_;

                        caculatorSegment(leg);

                        var now = moment();
                        var etdofLeg = moment(leg.Segments[0].DepartureLocal);
                        var totalHourToETD = -(moment(now).diff(etdofLeg, 'hour'));

                        var statusClass = "";
                        var status = "";
                        if (reservationInvoiceResult.ResStatus.IsCancelled == true) {
                            status = "Cancelled";
                            statusClass = "closed";
                        } else if (totalHourToETD <= 0) {
                            status = "Closed";
                            statusClass = "closed";
                        } else if (totalHourToETD > 65) {
                            status = "Waiting";
                            statusClass = "waiting";
                        } else {
                            var allChecked = true;
                            for (var k = 0; k < leg.Segments[0].PaxList.length; k++) {
                                var pax = leg.Segments[0].PaxList[k];
                                for (var m = 0; m < reservationInvoiceResult.Passengers.length; m++) {
                                    var passenger = reservationInvoiceResult.Passengers[m];
                                    if (pax.PaxGroupID == passenger.PaxGroupID) {
                                        if (pax.CheckinStatus == 'Not Checked-in' && passenger.Infants.length != 0) {
                                            countNoInfantNoCheckin = true;
                                            leg.countNoInfantNoCheckin = true;
                                        } else if (pax.CheckinStatus == 'Not Checked-in' && passenger.Infants.length == 0) {
                                            allChecked = false;
                                            leg.countNoInfantNoCheckin = false;
                                            break;
                                        }
                                    }
                                }
                            }
                            if (allChecked == false || countNoInfantNoCheckin == true) {
                                status = "Check In";
                                statusClass = "checkin";
                            } else {
                                status = "Checked In";
                                statusClass = "checkedin";
                            }
                        }
                        leg.FlightNumberofLeg = i + 1;
                        leg.Status = status;
                        leg.StatusClass = statusClass;
                        if (statusClass == "checkin") {
                            isCheckin = true;
                        }
                    }
                } catch (err) {
                    $scope.vaildResevation = false;
                }

                /*
                ● Check In: If the difference of the ETD and the current time is in the allowed time for Check In.
                ● Checked In: If all passengers have checked in before (not count passengers with infant).
                ● Waiting: the time for check in is not ready yet.
                ● Closed: the flight was departed.
                */
                $scope.isCheckin = isCheckin;
                $scope.s2Legs = reservationInvoiceResult.Legs;
                $scope.reservationInvoiceResult = reservationInvoiceResult;
                $scope.checkinStep2DataTotal = reservationInvoiceResult;
                $sessionStorage.flightstatusStep2DataTotal = reservationInvoiceResult;
            }, function (respone) {
                loadingModal.hide();
                $scope.vaildResevation = false;
            });

            var myAirportModal = $modal({ scope: $scope, template: 'app/modal/airport.html', show: false });
            $scope.loadAirportDetail = function (airportCode, airportName) {
                $scope.airportCode = airportCode;
                $scope.airportName = airportName;
                myAirportModal.show();
            };
            $scope.closeAirport = function () {
                myAirportModal.hide();
            };

            $scope.viewPaxList = function () {
                $location.path('/flightstatus3');
            };
        }
        return FlightStatusStep2Controller;
    })();
    var FlightStatusStep3Controller = (function () {
        function FlightStatusStep3Controller($sessionStorage, $scope, $state, $location, ReservationInvoice) {
            //set current name to state
            $scope.state = $state.current.name;
            if (!$sessionStorage.flightstatusStep2DataTotal && ($sessionStorage.flightstatusStep != "flightstatusstep2" || $sessionStorage.flightstatusStep != "flightstatusstep3")) {
                $location.path('/flightstatus2');
            }

            //set current step to session
            $sessionStorage.flightstatusStep = "flightstatusstep3";
            var flightstatusStep2 = $sessionStorage.flightstatusStep2DataTotal;
            $scope.flightstatusStep2 = flightstatusStep2;
            $scope.toHome = function () {
                resetFlightStatus($sessionStorage);
                $location.path('/flightstatus');
            };
        }
        return FlightStatusStep3Controller;
    })();

    function ReservationInvoiceFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/reservation");
    }

    // Register controllers to angular
    angular.module('gms').controller('FlightStatusStep1Controller', FlightStatusStep1Controller).controller('FlightStatusStep2Controller', FlightStatusStep2Controller).controller('FlightStatusStep3Controller', FlightStatusStep3Controller).factory('ReservationInvoice', ReservationInvoiceFactory);
})(FlightStatus || (FlightStatus = {}));
//# sourceMappingURL=flightstatus-controller.js.map

/// <reference path="../../../Scripts/typings/angularjs/angular.d.ts"/>
var Home;
(function (Home) {
    // Home Controller
    var HomeController = (function () {
        function HomeController($scope, $state, $location, $translate, $filter, $sessionStorage, Language) {
            $scope.state = $state.current.name;
            if ($sessionStorage.defaultLanguage == undefined) {
                Language.query({}, function (data) {
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].IsDefault == true) {
                            $translate.use(data[i].LanguageCode);
                            $sessionStorage.defaultLanguage = data[i].LanguageCode;
                            $sessionStorage.languageCode = data[i].LanguageCode;
                        }
                    }
                });
            }

            //$translate.use('vi');
            $scope.isLogin = $sessionStorage.isLogin;
        }
        return HomeController;
    })();

    function LanguageFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/language");
    }

    // Register controllers to angular module
    angular.module('gms').controller('HomeController', HomeController).factory('Language', LanguageFactory);
})(Home || (Home = {}));
//# sourceMappingURL=home-controller.js.map

/// <reference path="../../../Scripts/typings/angularjs/angular.d.ts"/>
var Information;
(function (Information) {
    // Infomation Controller
    var InformationController = (function () {
        function InformationController($scope, $state, $location, $sessionStorage, Infomation) {
            $scope.state = $state.current.name;
            Infomation.query({ languagecode: $sessionStorage.languageCode }, function (data) {
                $scope.infomationList = data;
            });
            $scope.choosePage = function (infomationSlug) {
                $sessionStorage.pageSlug = infomationSlug;
                $location.path('/infomationdetail');
            };
        }
        return InformationController;
    })();

    var InfomationDetailController = (function () {
        function InfomationDetailController($scope, $state, $location, $sessionStorage, $sce, Infomation) {
            $scope.state = $state.current.name;
            Infomation.query({
                languagecode: $sessionStorage.languageCode,
                infoSlug: $sessionStorage.pageSlug
            }, function (data) {
                $scope.titleInfomation = data[0].InfoTitle;
                $scope.contentInfomation = $sce.trustAsHtml(data[0].InfoContent);
            });
        }
        return InfomationDetailController;
    })();
    var ContactusController = (function () {
        function ContactusController($scope, $state, $location, $sessionStorage, $sce, contact) {
            $scope.state = $state.current.name;
            $scope.contactPhone = $sce.trustAsHtml(contact[0].ContactPhone);
            $scope.contactAddress = $sce.trustAsHtml(contact[0].ContactAddress);
            $scope.contactfor = $sce.trustAsHtml(contact[0].Contactfor);
            $scope.contactContent = $sce.trustAsHtml(contact[0].ContactContent);

            $scope.titleInfomation = contact[0].InfoTitle;
            $scope.contentInfomation = $sce.trustAsHtml(contact[0].InfoContent);

            $scope.map = {
                center: {
                    latitude: contact[0].ContactLat,
                    longitude: contact[0].ContactLong
                },
                zoom: 17
            };
            $scope.options = { scrollwheel: false };
            $scope.marker = {
                coords: {
                    latitude: contact[0].ContactLat,
                    longitude: contact[0].ContactLong
                },
                show: false,
                id: 0
            };

            $scope.title = contact[0].ContactTitle;
        }
        return ContactusController;
    })();
    function InfomationFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/informationpage");
    }
    function ContactFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/contact");
    }

    // Register controllers to angular module
    angular.module('gms').controller('InformationController', InformationController).controller('InfomationDetailController', InfomationDetailController).controller('ContactusController', ContactusController).factory('Infomation', InfomationFactory).factory('Contact', ContactFactory);
})(Information || (Information = {}));
//# sourceMappingURL=information-controller.js.map

/// <reference path="../../../Scripts/typings/angularjs/angular.d.ts"/>
var Language;
(function (_Language) {
    // Language Controller
    var LanguageController = (function () {
        function LanguageController($scope, $state, $location, $translate, $sessionStorage, Language) {
            $scope.state = $state.current.name;
            Language.query({}, function (data) {
                $scope.languageList = data;
            });
            $scope.chooseLanguage = function (languageCode) {
                $translate.use(languageCode);
                $sessionStorage.languageCode = languageCode;
                $location.path('/home');
            };
        }
        return LanguageController;
    })();
    function LanguageFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/language");
    }

    // Register controllers to angular module
    angular.module('gms').controller('LanguageController', LanguageController).factory('Language', LanguageFactory);
})(Language || (Language = {}));
//# sourceMappingURL=language-controller.js.map

/// <reference path="../../../Scripts/typings/angularjs/angular.d.ts"/>
var UserProfile;
(function (_UserProfile) {
    function resetUserProfile(sessionStorage) {
        delete sessionStorage.isLogin;
        delete sessionStorage.userProfile;
        delete sessionStorage.setReservationNumberDetail;
    }

    //Home User
    var UserController = (function () {
        function UserController($scope, $state, $location, $sessionStorage) {
            $scope.state = $state.current.name;
            if ($sessionStorage.isLogin != true) {
                $location.path('/login');
            }
            $scope.logout = function () {
                resetUserProfile($sessionStorage);
                $location.path('/login');
            };
        }
        return UserController;
    })();

    //Register
    var RegisterController = (function () {
        function RegisterController($scope, $state, $location, $sessionStorage, $translate, $modal, $sce, FareRule, UserProfile, Province, Country) {
            $scope.state = $state.current.name;

            //profile
            if ($sessionStorage.isLogin == true || $sessionStorage.isLogin != undefined) {
                $location.path('/user');
            }
            $scope.countryResult = Country.query();

            $scope.changeCountry = function () {
                $scope.provinceResult = Province.query({ countryCode: $scope.userCountry }, function () {
                    $scope.isProvince = true;
                });
            };
            var myInfomationModal = $modal({ scope: $scope, template: 'app/modal/information.html', show: false });
            $scope.closeInformation = function () {
                myInfomationModal.hide();
            };
            $scope.termsConditionsHelp = function () {
                FareRule.query({
                    languagecode: $sessionStorage.languageCode,
                    infoSlug: 'terms_conditions_register'
                }, function (data) {
                    $scope.informationTitle = data[0].InfoTitle;
                    $scope.informationContent = $sce.trustAsHtml(data[0].InfoContent);
                    myInfomationModal.show();
                });
            };
            $scope.submitCreateAccount = function () {
                if ($scope.userEmail == undefined) {
                    //alert("Please enter your email address");
                    $translate(['DATA_MESSAGE_EMAIL_ADDRESS']).then(function (translation) {
                        alert(translation.DATA_MESSAGE_EMAIL_ADDRESS);
                    });
                    return;
                }
                if ($scope.userPassword == undefined) {
                    //alert("Please enter your password");
                    $translate(['DATA_MESSAGE_PASSWORD']).then(function (translation) {
                        alert(translation.DATA_MESSAGE_PASSWORD);
                    });
                    return;
                }
                if ($scope.userVerifyPassword == undefined) {
                    //alert("Please enter your verify password");
                    $translate(['DATA_YOUR_VERIFY_PASSWORD']).then(function (translation) {
                        alert(translation.DATA_YOUR_VERIFY_PASSWORD);
                    });
                    return;
                }
                if ($scope.userVerifyPassword != $scope.userPassword) {
                    //alert("These password don’t match. Try again?");
                    $translate(['DATA_MESSAGE_PASSWORD_DO_NOT_MATCH']).then(function (translation) {
                        alert(translation.DATA_MESSAGE_PASSWORD_DO_NOT_MATCH);
                    });
                    return;
                }
                if ($scope.userQuestion == undefined) {
                    //alert("Please enter a question");
                    $translate(['DATA_MESSAGE_QUESTION']).then(function (translation) {
                        alert(translation.DATA_MESSAGE_QUESTION);
                    });
                    return;
                }
                if ($scope.userAnswer == undefined) {
                    //alert("Please enter your answer");
                    $translate(['DATA_MESSAGE_YOUR_ANSWER']).then(function (translation) {
                        alert(translation.DATA_MESSAGE_YOUR_ANSWER);
                    });
                    return;
                }
                if ($scope.userTitle == undefined) {
                    //alert("Please select a title");
                    $translate(['DATA_MESSAGE_SELECT_TITLE']).then(function (translation) {
                        alert(translation.DATA_MESSAGE_SELECT_TITLE);
                    });
                    return;
                }
                if ($scope.userFirstName == undefined) {
                    //alert("Please enter your first name");
                    $translate(['DATA_MESSAGE_FIRST_NAME']).then(function (translation) {
                        alert(translation.DATA_MESSAGE_FIRST_NAME);
                    });
                    return;
                }
                if ($scope.userLastName == undefined) {
                    //alert("Please enter your last name");
                    $translate(['DATA_MESSAGE_LAST_NAME']).then(function (translation) {
                        alert(translation.DATA_MESSAGE_LAST_NAME);
                    });
                    return;
                }
                if ($scope.userBirthday == undefined) {
                    //alert("Please select your date of birth");
                    $translate(['DATA_MESSAGE_DATE_OF_BIRHT']).then(function (translation) {
                        alert(translation.DATA_MESSAGE_DATE_OF_BIRHT);
                    });
                    return;
                }
                if ($scope.userMobile == undefined) {
                    //alert("Please enter your phone number");
                    $translate(['DATA_MESSAGE_ENTER_PHONE_NUMBER']).then(function (translation) {
                        alert(translation.DATA_MESSAGE_ENTER_PHONE_NUMBER);
                    });
                    return;
                }
                if ($scope.userPasport == undefined) {
                    //alert("Please enter your passport number");
                    $translate(['DATA_MESSAGE_YOUR_PASSPORT_NUMBER']).then(function (translation) {
                        alert(translation.DATA_MESSAGE_YOUR_PASSPORT_NUMBER);
                    });
                    return;
                }
                if ($scope.userAddress == undefined) {
                    //alert("Please enter your address");
                    $translate(['DATA_MESSAGE_ENTER_YOUR_ADDRESS']).then(function (translation) {
                        alert(translation.DATA_MESSAGE_ENTER_YOUR_ADDRESS);
                    });
                    return;
                }
                if ($scope.userCountry == undefined) {
                    //alert("Please select your country");
                    $translate(['DATA_MESSAGE_YOUR_COUNTRY']).then(function (translation) {
                        alert(translation.DATA_MESSAGE_YOUR_COUNTRY);
                    });
                    return;
                }
                if ($scope.userCity == undefined) {
                    //alert("Please select your province/state");
                    $translate(['DATA_MESSAGE_YOUR_PROVINCE_STATE']).then(function (translation) {
                        alert(translation.DATA_MESSAGE_YOUR_PROVINCE_STATE);
                    });
                    return;
                }
                if ($scope.registerForm.$invalid) {
                    $scope.$broadcast('show-errors-check-validity');

                    //alert("Please fill in all information of user register form");
                    $translate(['DATA_MESSAGE_FILL_ALL_INFORMATION_OF_USER_REGISTER']).then(function (translation) {
                        alert(translation.DATA_MESSAGE_FILL_ALL_INFORMATION_OF_USER_REGISTER);
                    });
                    return;
                }

                var gender = ($scope.userTitle == "Mr") ? "Male" : "Female";
                var paxProfile = {
                    gender: gender,
                    isActive: true,
                    firstname: $scope.userFirstName,
                    lastname: $scope.userLastName,
                    email: $scope.userEmail,
                    dateOfBirth: $scope.userBirthday,
                    homeTel: $scope.userMobile,
                    passportNumber: $scope.userPasport,
                    addr1: $scope.userAddress,
                    countryCode: $scope.userCountry,
                    provinceCode: $scope.userCity
                };

                var data_profile = {
                    email: $scope.userEmail,
                    profileName: $scope.userEmail,
                    password: $scope.userPassword,
                    isActive: true,
                    securityQuestion: $scope.userQuestion,
                    securityAnswer: $scope.userAnswer,
                    paxProfile: paxProfile
                };
                var loadingModal = $modal({
                    scope: $scope,
                    template: 'app/modal/loading.html',
                    show: true,
                    backdrop: 'static'
                });
                $scope.reservationResponse = UserProfile.save({
                    isAddUser: true,
                    profile: data_profile
                }, function (data) {
                    if (data.OperationSucceeded) {
                        $sessionStorage.userProfile = data.UserProfile;
                        $sessionStorage.isLogin = true;
                        loadingModal.hide();
                        $location.path('/accountcreated');
                    } else {
                        if (data.OperationMessage == "Specified cast is not valid.") {
                            //alert("This email already exists. Try another.");
                            $translate(['DATA_MESSAGE_EMAIL_ALREADY_EXISTS']).then(function (translation) {
                                alert(translation.DATA_MESSAGE_EMAIL_ALREADY_EXISTS);
                            });
                            loadingModal.hide();
                        } else {
                            alert(data.OperationMessage);
                            loadingModal.hide();
                        }
                        return false;
                    }
                });
            };
        }
        return RegisterController;
    })();

    //created account
    var AccountCreatedController = (function () {
        function AccountCreatedController($scope, $state, $location, $sessionStorage, $translate, $sce) {
            $scope.state = $state.current.name;
            $translate('DATA_WELCOME').then(function (translation) {
                $scope.headdingWellcom = $sce.trustAsHtml(translation);
            });
            $translate('DATA_MESSAGE_ENJOIN_OUR_SERVICES').then(function (translation) {
                $scope.headdingContent = $sce.trustAsHtml(translation);
            });
            $translate('DATA_MESSAGE_SENT_YOU_EMAIL_TO_VERIFY').then(function (translation) {
                $scope.contentNote = $sce.trustAsHtml(translation);
            });
            $scope.toHome = function () {
                $location.path('/user');
            };
        }
        return AccountCreatedController;
    })();

    // Login
    var LoginController = (function () {
        function LoginController($scope, $state, $location, $translate, $sessionStorage, UserProfile) {
            $scope.state = $state.current.name;
            if ($sessionStorage.isLogin == true) {
                $location.path('/user');
            }
            $scope.login = function () {
                if ($scope.userName == undefined) {
                    //alert("Please enter your email address");
                    $translate(['DATA_MESSAGE_EMAIL_ADDRESS']).then(function (translation) {
                        alert(translation.DATA_MESSAGE_EMAIL_ADDRESS);
                    });
                    return;
                }
                if ($scope.userPassword == undefined) {
                    //alert("Please enter your password");
                    $translate(['DATA_MESSAGE_PASSWORD']).then(function (translation) {
                        alert(translation.DATA_MESSAGE_PASSWORD);
                    });
                    return;
                }
                if ($scope.registerForm.$invalid) {
                    $scope.$broadcast('show-errors-check-validity');

                    //alert("Please fill in all field");
                    $translate(['DATA_MESSAGE_FILL_ALL_FIELD']).then(function (translation) {
                        alert(translation.DATA_MESSAGE_FILL_ALL_FIELD);
                    });
                    return;
                }

                var userProfile = UserProfile.get({
                    profileEmail: $scope.userName,
                    profilePassword: $scope.userPassword
                }, function (data) {
                    if (data.OperationSucceeded) {
                        $sessionStorage.userProfile = data.UserProfile;
                        $sessionStorage.isLogin = true;
                        $location.path('/user');
                    } else {
                        //alert("The email or password you entered is incorrect");
                        $translate(['DATA_MESSAGE_EMAIL_OR_PASSWORD_INCORRECT']).then(function (translation) {
                            alert(translation.DATA_MESSAGE_EMAIL_OR_PASSWORD_INCORRECT);
                        });
                        return false;
                    }
                });
            };
        }
        return LoginController;
    })();

    // Forgot password
    var ForgotPasswordController = (function () {
        function ForgotPasswordController($scope, $state, $location) {
            $scope.state = $state.current.name;
        }
        return ForgotPasswordController;
    })();

    // Change Password
    var ChangePasswordController = (function () {
        function ChangePasswordController($scope, $state, $location) {
        }
        return ChangePasswordController;
    })();

    // Update Profile
    var UpdateProfileController = (function () {
        function UpdateProfileController($scope, $state, $location, $modal, $translate, $sessionStorage, Country, Province, UserProfile) {
            $scope.state = $state.current.name;
            if ($sessionStorage.isLogin) {
                var userProfile = $sessionStorage.userProfile.PaxProfile;
                var gender = "";
                switch (userProfile.Gender) {
                    case 0:
                        gender = "Mr";
                        break;
                    case 1:
                        gender = "Ms";
                        break;
                }
                $scope.isProvince = true;
                $scope.provinceResult = Province.query({ countryCode: userProfile.CountryCode }, function () {
                    $scope.isProvince = true;
                });
                $scope.userTitle = gender;
                $scope.userFirstName = userProfile.Firstname;
                $scope.userLastName = userProfile.Lastname;
                $scope.userEmail = userProfile.Email;
                $scope.userBirthday = userProfile.DateOfBirth;
                $scope.userMobile = userProfile.HomeTel;
                $scope.userPasport = userProfile.PassportNumber;
                $scope.userAddress = userProfile.Addr1;
                $scope.userCountry = userProfile.CountryCode;
                $scope.userCity = userProfile.ProvinceCode;

                //profile
                $scope.countryResult = Country.query();

                $scope.changeCountry = function () {
                    $scope.provinceResult = Province.query({ countryCode: $scope.userCountry }, function () {
                        $scope.isProvince = true;
                    });
                };
                $scope.submitUpdateProfile = function () {
                    if ($scope.userTitle == "") {
                        //alert("Please select a title");
                        $translate(['DATA_MESSAGE_SELECT_TITLE']).then(function (translation) {
                            alert(translation.DATA_MESSAGE_SELECT_TITLE);
                        });
                        return;
                    }
                    if ($scope.userFirstName == "") {
                        //alert("Please enter your first name");
                        $translate(['DATA_MESSAGE_FIRST_NAME']).then(function (translation) {
                            alert(translation.DATA_MESSAGE_FIRST_NAME);
                        });
                        return;
                    }
                    if ($scope.userLastName == "") {
                        //alert("Please enter your last name");
                        $translate(['DATA_MESSAGE_LAST_NAME']).then(function (translation) {
                            alert(translation.DATA_MESSAGE_LAST_NAME);
                        });
                        return;
                    }
                    if ($scope.userBirthday == "") {
                        //alert("Please select your date of birth");
                        $translate(['DATA_MESSAGE_DATE_OF_BIRHT']).then(function (translation) {
                            alert(translation.DATA_MESSAGE_DATE_OF_BIRHT);
                        });
                        return;
                    }
                    if ($scope.userMobile == "") {
                        //alert("Please enter your phone number");
                        $translate(['DATA_MESSAGE_ENTER_PHONE_NUMBER']).then(function (translation) {
                            alert(translation.DATA_MESSAGE_ENTER_PHONE_NUMBER);
                        });
                        return;
                    }
                    if ($scope.userPasport == "") {
                        //alert("Please enter your passport number");
                        $translate(['DATA_MESSAGE_YOUR_PASSPORT_NUMBER']).then(function (translation) {
                            alert(translation.DATA_MESSAGE_YOUR_PASSPORT_NUMBER);
                        });
                        return;
                    }
                    if ($scope.userAddress == "") {
                        //alert("Please enter your address");
                        $translate(['DATA_MESSAGE_ENTER_YOUR_ADDRESS']).then(function (translation) {
                            alert(translation.DATA_MESSAGE_ENTER_YOUR_ADDRESS);
                        });
                        return;
                    }
                    if ($scope.userCountry == "") {
                        //alert("Please select your country");
                        $translate(['DATA_MESSAGE_YOUR_COUNTRY']).then(function (translation) {
                            alert(translation.DATA_MESSAGE_YOUR_COUNTRY);
                        });
                        return;
                    }
                    if ($scope.userCity == "") {
                        //alert("Please select your province/state");
                        $translate(['DATA_MESSAGE_YOUR_PROVINCE_STATE']).then(function (translation) {
                            alert(translation.DATA_MESSAGE_YOUR_PROVINCE_STATE);
                        });
                        return;
                    }
                    if ($scope.registerForm.$invalid) {
                        $scope.$broadcast('show-errors-check-validity');

                        //alert("Please fill in all information of user register form");
                        $translate(['DATA_MESSAGE_FILL_ALL_INFORMATION_OF_USER_REGISTER']).then(function (translation) {
                            alert(translation.DATA_MESSAGE_FILL_ALL_INFORMATION_OF_USER_REGISTER);
                        });
                        return;
                    }

                    var gender = ($scope.userTitle == "Mr") ? "0" : "1";
                    var paxProfile = {
                        gender: gender,
                        firstname: $scope.userFirstName,
                        lastname: $scope.userLastName,
                        email: $scope.userEmail,
                        dateOfBirth: $scope.userBirthday,
                        homeTel: $scope.userMobile,
                        passportNumber: $scope.userPasport,
                        addr1: $scope.userAddress,
                        countryCode: $scope.userCountry,
                        provinceCode: $scope.userCity,
                        Addr2: $sessionStorage.userProfile.PaxProfile.Addr2,
                        AeroplanNumber: $sessionStorage.userProfile.PaxProfile.AeroplanNumber,
                        AgeCategory: $sessionStorage.userProfile.PaxProfile.AgeCategory,
                        BusExt: $sessionStorage.userProfile.PaxProfile.BusExt,
                        BusFax: $sessionStorage.userProfile.PaxProfile.BusFax,
                        BusTel: $sessionStorage.userProfile.PaxProfile.BusTel,
                        CategoryID: $sessionStorage.userProfile.PaxProfile.CategoryID,
                        City: $sessionStorage.userProfile.PaxProfile.City,
                        //CountryID: $sessionStorage.userProfile.PaxProfile.CountryID,
                        EmployeeNumber: $sessionStorage.userProfile.PaxProfile.EmployeeNumber,
                        GeneralNumber1: $sessionStorage.userProfile.PaxProfile.GeneralNumber1,
                        GeneralNumber2: $sessionStorage.userProfile.PaxProfile.GeneralNumber2,
                        HomeFax: $sessionStorage.userProfile.PaxProfile.HomeFax,
                        IsActive: $sessionStorage.userProfile.PaxProfile.IsActive,
                        LoyaltyPoints: $sessionStorage.userProfile.PaxProfile.LoyaltyPoints,
                        Middlename: $sessionStorage.userProfile.PaxProfile.Middlename,
                        Nationality: $sessionStorage.userProfile.PaxProfile.Nationality,
                        Notes: $sessionStorage.userProfile.PaxProfile.Notes,
                        PassportCountryCode: $sessionStorage.userProfile.PaxProfile.PassportCountryCode,
                        PassportExires: $sessionStorage.userProfile.PaxProfile.PassportExires,
                        PassportIssuedCity: $sessionStorage.userProfile.PaxProfile.PassportIssuedCity,
                        PassportIssuedDate: $sessionStorage.userProfile.PaxProfile.PassportIssuedDate,
                        PaxID: $sessionStorage.userProfile.PaxProfile.PaxID,
                        PostalCode: $sessionStorage.userProfile.PaxProfile.PostalCode,
                        PreBoard: $sessionStorage.userProfile.PaxProfile.PreBoard,
                        //ProvinceID: $sessionStorage.userProfile.PaxProfile.ProvinceID,
                        SpecialNeeds: $sessionStorage.userProfile.PaxProfile.SpecialNeeds
                    };

                    var data_profile = {
                        email: $scope.userEmail,
                        profileName: $sessionStorage.userProfile.ProfileName,
                        password: $sessionStorage.userProfile.Password,
                        isActive: $sessionStorage.userProfile.IsActive,
                        iiVIP: $sessionStorage.userProfile.IsVIP,
                        departureAirportID: $sessionStorage.userProfile.DepartureAirportID,
                        arrivalAirportID: $sessionStorage.userProfile.ArrivalAirportID,
                        securityQuestion: $sessionStorage.userProfile.SecurityQuestion,
                        securityAnswer: $sessionStorage.userProfile.SecurityAnswer,
                        userProfileID: $sessionStorage.userProfile.UserProfileID,
                        paxProfile: paxProfile
                    };
                    var loadingModal = $modal({
                        scope: $scope,
                        template: 'app/modal/loading.html',
                        show: true,
                        backdrop: 'static'
                    });

                    $scope.reservationResponse = UserProfile.save({
                        isAddUser: false,
                        profile: data_profile
                    }, function (data) {
                        if (data.OperationSucceeded) {
                            $sessionStorage.userProfile = data.UserProfile;
                            loadingModal.hide();
                            $location.path('/user');
                        } else {
                            alert(data.OperationMessage);
                            return false;
                        }
                    });
                };
            }
        }
        return UpdateProfileController;
    })();

    // Reservation History
    var ReservationHistoryController = (function () {
        function ReservationHistoryController($scope, $state, $location, $sessionStorage, $modal, UserProfileReservation) {
            $scope.state = $state.current.name;
            var loadingModal = $modal({ scope: $scope, template: 'app/modal/loading.html', show: true });
            UserProfileReservation.get({ userProfileID: $sessionStorage.userProfile.UserProfileID }, function (data) {
                $scope.reservationHistoryResults = data.Results;
                loadingModal.hide();
            });
            $scope.viewReservationDetail = function (reservationNumber) {
                $sessionStorage.setReservationNumberDetail = reservationNumber;
                $location.path('/reservationdetail');
            };
        }
        return ReservationHistoryController;
    })();

    // Reservation History Detail
    var ReservationHistoryDetailController = (function () {
        function ReservationHistoryDetailController($scope, $state, $location, $sessionStorage, $modal, $sce, ReservationInvoice, FareRule) {
            //set current name to state
            $scope.state = $state.current.name;
            if (!$sessionStorage.setReservationNumberDetail) {
                $location.path('/reservationhistory');
            }
            var myAirportModal = $modal({ scope: $scope, template: 'app/modal/airport.html', show: false });
            $scope.loadAirportDetail = function (airportCode, airportName) {
                $scope.airportCode = airportCode;
                $scope.airportName = airportName;
                myAirportModal.show();
            };
            $scope.closeAirport = function () {
                myAirportModal.hide();
            };

            var loadingModal = $modal({ scope: $scope, template: 'app/modal/loading.html', show: true });

            function caculatorSegment(legOption) {
                legOption.SegmentsTemp = [];
                angular.copy(legOption.Segments, legOption.SegmentsTemp);
                var airCraftIndex = 0;
                var countStop = 1;

                if (legOption.SegmentsTemp.length == 1) {
                    var segment = legOption.SegmentsTemp;
                    segment[0].TimeTransit = "";
                    segment[0].checkAirCraft = false;
                } else {
                    legOption.SegmentsTemp[0].TimeTransit = "";
                    legOption.SegmentsTemp[0].checkAirCraft = false;
                    for (var j = 1; j < legOption.SegmentsTemp.length; j++) {
                        var segment = legOption.SegmentsTemp;
                        if (segment[j].AircraftName == segment[airCraftIndex].AircraftName && segment[j].Flight == segment[airCraftIndex].Flight) {
                            segment[j].checkAirCraft = true;
                            var timeeta = segment[j - 1].ArrivalLocal;
                            var timeetd = segment[j].DepartureLocal;
                            var totalMinutes = moment(timeetd).diff(timeeta, 'minutes');
                            var hours = Math.floor(totalMinutes / 60);
                            var minutes = totalMinutes % 60;
                            segment[j].TimeTransit = hours + 'h ' + minutes + 'm';
                            legOption.SegmentsTemp[0].ArrivalLocal = segment[j].ArrivalLocal;
                            legOption.SegmentsTemp[0].ArrivalAirportCode = segment[j].ArrivalAirportCode;
                            legOption.SegmentsTemp[0].ArrivalAirportName = segment[j].ArrivalAirportName;
                            segment[j].countStop = countStop;
                            countStop++;
                            airCraftIndex = j;
                        } else {
                            segment[j].checkAirCraft = false;
                            airCraftIndex = j;
                        }
                    }
                }
            }

            $scope.reservationInvoice = ReservationInvoice.get({
                reservationNumber: $sessionStorage.setReservationNumberDetail
            }, function (data) {
                loadingModal.hide();
                try  {
                    $scope.vaildResevation = true;
                    var reservationInvoiceResult = data;
                    var isCheckin = false;
                    var isPayed = false;
                    var countNoInfantNoCheckin = false;
                    for (var i = 0; i < reservationInvoiceResult.Legs.length; i++) {
                        var leg = reservationInvoiceResult.Legs[i];
                        var arrivalAirportName_ = "";
                        for (var j = 0; j < leg.Segments.length; j++) {
                            var segment = leg.Segments;
                            arrivalAirportName_ = segment[j].ArrivalAirportName;
                        }
                        leg.DepartureAirportName = leg.Segments[0].DepartureAirportName;
                        leg.ArrivalAirportName = arrivalAirportName_;

                        caculatorSegment(leg);

                        var now = moment();
                        var etdofLeg = moment(leg.Segments[0].DepartureLocal);
                        var totalHourToETD = -(moment(now).diff(etdofLeg, 'hour'));

                        var statusClass = "";
                        var status = "";
                        if (reservationInvoiceResult.ResStatus.IsCancelled == true) {
                            status = "Cancelled";
                            statusClass = "closed";
                        } else if (totalHourToETD <= 0) {
                            status = "Closed";
                            statusClass = "closed";
                        } else if (totalHourToETD > 65) {
                            status = "Waiting";
                            statusClass = "waiting";
                        } else {
                            var allChecked = true;
                            for (var k = 0; k < leg.Segments[0].PaxList.length; k++) {
                                var pax = leg.Segments[0].PaxList[k];
                                for (var m = 0; m < reservationInvoiceResult.Passengers.length; m++) {
                                    var passenger = reservationInvoiceResult.Passengers[m];
                                    if (pax.PaxGroupID == passenger.PaxGroupID) {
                                        if (pax.CheckinStatus == 'Not Checked-in' && passenger.Infants.length != 0) {
                                            countNoInfantNoCheckin = true;
                                            leg.countNoInfantNoCheckin = true;
                                        } else if (pax.CheckinStatus == 'Not Checked-in' && passenger.Infants.length == 0) {
                                            allChecked = false;
                                            leg.countNoInfantNoCheckin = false;
                                            break;
                                        }
                                    }
                                }
                            }
                            if (allChecked == false || countNoInfantNoCheckin == true) {
                                status = "Check In";
                                statusClass = "checkin";
                            } else {
                                status = "Checked In";
                                statusClass = "checkedin";
                            }
                        }
                        leg.FlightNumberofLeg = i + 1;
                        leg.Status = status;
                        leg.StatusClass = statusClass;
                        if (statusClass == "checkin") {
                            isCheckin = true;
                        }
                    }
                } catch (err) {
                    $scope.vaildResevation = false;
                }

                /*
                ● Check In: If the difference of the ETD and the current time is in the allowed time for Check In.
                ● Checked In: If all passengers have checked in before (not count passengers with infant).
                ● Waiting: the time for check in is not ready yet.
                ● Closed: the flight was departed.
                */
                $scope.isCheckin = isCheckin;
                $scope.s2Legs = reservationInvoiceResult.Legs;
                $scope.reservationInvoiceResult = reservationInvoiceResult;
                $scope.checkinStep2DataTotal = reservationInvoiceResult;
                $sessionStorage.flightstatusStep2DataTotal = reservationInvoiceResult;
            }, function (respone) {
                loadingModal.hide();
                $scope.vaildResevation = false;
            });
            var myInfomationModal_ = $modal({ scope: $scope, template: 'app/modal/information.html', show: false });
            $scope.loadFare = function (fareClass) {
                FareRule.query({
                    languagecode: $sessionStorage.languageCode,
                    infoSlug: fareClass
                }, function (data) {
                    if (data.length > 0) {
                        $scope.informationTitle = data[0].InfoTitle;
                        $scope.informationContent = $sce.trustAsHtml(data[0].InfoContent);
                    } else {
                        $scope.informationTitle = fareClass;
                        $scope.informationContent = $sce.trustAsHtml(fareClass);
                    }
                    $scope.classname = "fareclass";
                    myInfomationModal_.show();
                });
            };
            $scope.closeInformation = function () {
                myInfomationModal_.hide();
            };
            $scope.goBack = function () {
                delete $sessionStorage.setReservationNumberDetail;
                $location.path('/reservationhistory');
            };
        }
        return ReservationHistoryDetailController;
    })();

    function FareRuleFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/farerule");
    }
    function CountryFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/country");
    }

    function ProvinceFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/province");
    }

    function UserProfileFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/userprofile");
    }
    function UserProfileReservationFactory($resource) {
        return $resource(Configuration.Settings.serviceUrl + "/userprofilereservation");
    }

    // Register controllers to angular
    angular.module('gms').controller('UserController', UserController).controller('RegisterController', RegisterController).controller('AccountCreatedController', AccountCreatedController).controller('LoginController', LoginController).controller('ForgotPasswordController', ForgotPasswordController).controller('ChangePasswordController', ChangePasswordController).controller('UpdateProfileController', UpdateProfileController).controller('ReservationHistoryController', ReservationHistoryController).controller('ReservationHistoryDetailController', ReservationHistoryDetailController).factory('FareRule', FareRuleFactory).factory('Country', CountryFactory).factory('Province', ProvinceFactory).factory('UserProfile', UserProfileFactory).factory('UserProfileReservation', UserProfileReservationFactory);
})(UserProfile || (UserProfile = {}));
//# sourceMappingURL=userprofile-controller.js.map
