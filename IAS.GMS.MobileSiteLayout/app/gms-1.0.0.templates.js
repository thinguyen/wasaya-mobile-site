angular.module('gms-templates', []).run(['$templateCache', function($templateCache) {
  $templateCache.put("booking/views/bookingflight.html",
    "<!DOCTYPE html>\n" +
    "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
    "<head>\n" +
    "    <title></title>\n" +
    "</head>\n" +
    "<body>\n" +
    "\n" +
    "</body>\n" +
    "</html>\n" +
    "");
  $templateCache.put("booking/views/finish.html",
    "Information Booking\n" +
    "<a href=\"home\">home</a>");
  $templateCache.put("booking/views/step_1.html",
    "<div class=\"step_status\"><span class=\"step step1\"></span></div>\n" +
    "<form name=\"step1Form\">\n" +
    "    <div class=\"full-width cf\">\n" +
    "        <ul ng-init=\"tab = 1\" class=\"step1_tabs step cf\">\n" +
    "            <li ng-class=\"{active:tab===1}\" class=\"left\">\n" +
    "                <a href ng-click=\"tab = 1\">Return</a>\n" +
    "            </li>\n" +
    "            <li ng-class=\"{active:tab===2}\" class=\"right\">\n" +
    "                <a href ng-click=\"tab = 2\">One Way</a>\n" +
    "            </li>\n" +
    "        </ul>\n" +
    "    </div>\n" +
    "    <div>\n" +
    "        <div class=\"select_location\">\n" +
    "            <div class=\"form-group cf\" show-errors>\n" +
    "                <label for=\"select_origin\" class=\"col-sm-2 control-label\">{{ 'DATA_BOOK_ORIGIN' | translate }}</label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <select class=\"form-control\" name=\"optionOrigin\" ng-change=\"loadDestination()\" ng-model=\"optionOrigin\" ng-options=\"origin.Code as (origin.Name + ' (' + origin.Code + ')') for origin in originResult\" required>\n" +
    "                        <option value=\"\">{{ 'DATA_SELECT_OGRIGIN' | translate }}</option>\n" +
    "                    </select>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group cf\" show-errors>\n" +
    "                <label for=\"select_destination\" class=\"col-sm-2 control-label\">{{ 'DATA_BOOK_DESTINATION' | translate }}</label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <select class=\"form-control\" name=\"optionDestination\" ng-model=\"optionDestination\" ng-options=\"destination.Code as (destination.Name + ' (' + destination.Code + ')') for destination in destinationResult\" required>\n" +
    "                        <option value=\"\">{{ 'DATA_SELECT_DESTINATION' | translate }}</option>\n" +
    "                    </select>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"select_date_start\">\n" +
    "            <div class=\"form-group cf\" show-errors>\n" +
    "                <label class=\"col-sm-2 control-label\">{{ 'DATA_BOOK_DEPARTDATE' | translate }}</label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <input type=\"text\" class=\"form-control\" ng-change=\"updateDepartDate()\" data-min-date=\"today\" name=\"dateStart\" data-autoclose=\"1\" ng-model=\"dateStart\" bs-datepicker required>\n" +
    "                    <!--<input type=\"date\" class=\"form-control\" ng-change=\"updateDepartDate()\" min=\"{{today}}\" name=\"dateStart\" ng-model=\"dateStart\" required>-->\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"select_date_end\" ng-show=\"tab === 1\">\n" +
    "            <div class=\"form-group cf\" show-errors>\n" +
    "                <label class=\"col-sm-2 control-label\">{{ 'DATA_BOOK_RETURNDATE' | translate }}</label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <input type=\"text\" class=\"form-control\" data-min-date=\"{{dateStart}}\" name=\"dateEnd\" data-autoclose=\"1\" ng-model=\"dateEnd\" bs-datepicker required>\n" +
    "                    <!--<input type=\"date\" class=\"form-control\" ng-change=\"updateReturnDate()\" min=\"{{departDateCheck}}\" name=\"dateEnd\" ng-model=\"dateEnd\" required>-->\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"select_person\">\n" +
    "            <div class=\"form-group cf\" show-errors>\n" +
    "                <label class=\"col-sm-2 control-label\">{{ 'DATA_BOOK_ADULT' | translate }}</label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <!--<input type=\"number\" pattern=\"\\d*\" ngmaxlength=\"1\" class=\"form-control\" name=\"adultsValue\" ng-model=\"adultsValue\" ng-change=\"checkAdults()\" name=\"select_adults\" required numbers-only>-->\n" +
    "                    <select class=\"form-control\" name=\"select_adults\" ng-model=\"adultsValue\" ng-change=\"checkAdults()\" required>\n" +
    "                        <option ng-repeat=\"index in getNumber(AdultsRange) track by $index\">{{$index+1}}</option>\n" +
    "                    </select>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group cf\" show-errors>\n" +
    "                <label class=\"col-sm-2 control-label\">{{ 'DATA_BOOK_CHILD' | translate }}</label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <!--<input type=\"number\" pattern=\"\\d*\" ngmaxlength=\"1\" class=\"form-control\" name=\"childrenValue\" ng-model=\"childrenValue\" ng-change=\"checkChildren()\" name=\"select_children\" required numbers-only>-->\n" +
    "                    <select class=\"form-control\" name=\"select_children\" ng-model=\"childrenValue\" ng-change=\"checkChildren()\" required>\n" +
    "                        <option ng-repeat=\"index in getNumber(ChildrenRange) track by $index\">{{$index}}</option>\n" +
    "                    </select>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group cf\" show-errors>\n" +
    "                <label class=\"col-sm-2 control-label\">{{ 'DATA_BOOK_INFANT' | translate }}</label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <!--<input type=\"number\" pattern=\"\\d*\" ngmaxlength=\"1\" class=\"form-control\" name=\"infantsValue\" ng-model=\"infantsValue\" ng-change=\"checkInfants()\" name=\"select_infants\" required numbers-only>-->\n" +
    "                    <select class=\"form-control\" name=\"select_infants\" ng-model=\"infantsValue\" ng-change=\"checkInfants()\" required>\n" +
    "                        <option ng-repeat=\"index in getNumber(InfantsRange) track by $index\">{{$index}}</option>\n" +
    "                    </select>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"select_promotion\">\n" +
    "            <div class=\"form-group cf\">\n" +
    "                <label class=\"col-sm-2 control-label\">{{ 'DATA_BOOK_PROMO' | translate }}</label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"promoCodeValue\" ng-model=\"promoCodeValue\">\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"check_round_trip\" ng-show=\"tab === 1\">\n" +
    "        <input type=\"hidden\" class=\"\" ng-model=\"S1CurrencyCode\">\n" +
    "    </div>\n" +
    "    <div class=\"button_next_step\">\n" +
    "        <button ng-click=\"findFlightClick()\">{{ 'BUTTON_FIND_FLIGHT' | translate }}</button>\n" +
    "    </div>\n" +
    "</form>\n" +
    "");
  $templateCache.put("booking/views/step_2.html",
    "<div class=\"step_status\"><span class=\"step step2\"></span></div>\n" +
    "<form name=\"step2Form\">\n" +
    "    <div class=\"full-width cf\">\n" +
    "        <ul ng-init=\"tab = 0\" class=\"step2_tabs step cf\">\n" +
    "            <li ng-class=\"{active:tab===0}\" class=\"left\">\n" +
    "                <a ng-click=\"departTabClick()\">{{ 'DATA_DEPART' | translate }}</a>\n" +
    "            </li>\n" +
    "            <li ng-class=\"{active:tab===1}\" ng-show=\"step1Data.s1RoundTrip == 1\" class=\"right\">\n" +
    "                <a ng-click=\"returnTabClick()\">{{ 'DATA_RETURN' | translate }}</a>\n" +
    "            </li>\n" +
    "        </ul>\n" +
    "    </div>\n" +
    "    <div>\n" +
    "        <div class=\"wrap_fly_selection\">\n" +
    "            <div class=\"flightname\">\n" +
    "                <p ng-show=\"tab == 0\">\n" +
    "                    {{ 'DATA_FLIGHT' | translate }} 1: {{s1FromName}} {{ 'DATA_TO' | translate }} {{s1ToName}} - {{step1Data.s1DateStart | date:'longDate'}}\n" +
    "                </p>\n" +
    "                <p ng-show=\"tab == 1\">\n" +
    "                    {{ 'DATA_FLIGHT' | translate }} 2: {{s1ToName}} {{ 'DATA_TO' | translate }} {{s1FromName}} - {{step1Data.s1DateReturn | date:'longDate'}}\n" +
    "                </p>\n" +
    "            </div>\n" +
    "            <div class=\"flight_from_to\" ng-show=\"tab == 0\">\n" +
    "                <span ng-model=\"step2From\">{{s1FromName}} ({{s1FromCode}})</span>\n" +
    "                <span class=\"depart\"></span>\n" +
    "                <span>{{s1ToName}} ({{s1ToCode}})</span>\n" +
    "            </div>\n" +
    "            <div class=\"flight_from_to\" ng-show=\"tab == 1\">\n" +
    "                <span ng-model=\"step2From\">{{s1ToName}} ({{s1ToCode}})</span>\n" +
    "                <span class=\"depart\"></span>\n" +
    "                <span>{{s1FromName}} ({{s1FromCode}})</span>\n" +
    "            </div>\n" +
    "            <div class=\"calendar\">\n" +
    "                <button ng-click=\"predate()\" class=\"pre_date\"></button>\n" +
    "                <span class=\"dateselected\" ng-show=\"tab == 0\">{{step1Data.s1DateStart | date:'longDate'}}</span>\n" +
    "                <span class=\"dateselected\" ng-show=\"tab == 1\">{{step1Data.s1DateReturn | date:'longDate'}}</span>\n" +
    "                <button ng-click=\"nextdate()\" class=\"next_date\"></button>\n" +
    "            </div>\n" +
    "            <div class=\"no_promo_code\" ng-if=\"step1Data.s1Promocode != null && isCodePromo \" ng-class=\"{'has_promocode':travelOptionPromoSelect.IsValid}\">\n" +
    "                <p ng-show=\"travelOptionPromoSelect.IsValid == false\">{{ 'DATA_MESSAGE_PROMO_CODE_INVALID' | translate }}</p>\n" +
    "                <p ng-show=\"travelOptionPromoSelect.IsValid\">{{ 'DATA_PROMO_CODE' | translate }}: {{travelOptionPromoSelect.PromoCode}}</p>\n" +
    "                <p ng-show=\"travelOptionPromoSelect.IsValid\" class=\"promocode_detail\"><span></span>{{travelOptionPromoSelect.Description}}</p>\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"nofare\" ng-if=\"!travelOptionSelect.Selected.length\">\n" +
    "                <p>\n" +
    "                    <img src=\"app/img/loading.gif\" ng-if=\"isFinding\" />{{findFlights}}\n" +
    "                </p>\n" +
    "            </div>\n" +
    "            <div class=\"valuefare\" ng-model=\"valueFareCollapse\" bs-collapse>\n" +
    "                <div class=\"flight_fly\" ng-repeat=\"travelOption in travelOptionSelect.Selected\">\n" +
    "                    <div class=\"summary_flight\" bs-collapse-toggle>\n" +
    "                        <span class=\"icon_expan icon_close_expan\"></span>\n" +
    "                        <span class=\"price_from\">\n" +
    "                            <span class=\"price_detail\">{{ 'DATA_DEPART' | translate }}: {{travelOption.Legs[0].SegmentOptions[0].Flight.ETDLocal | date:'HH:mm'}} \n" +
    "                            -\n" +
    "                             {{travelOption.Legs[0].SegmentOptions[travelOption.Legs[0].SegmentOptions.length - 1].Flight.ETALocal | date:'HH:mm'}}</span>\n" +
    "                            <span ng-class=\"{'has_promotion_code_tag': travelOption.Legs[0].FareOptions.Adultfares[0].Totalfare > travelOption.Legs[0].FareOptions.Adultfares[0].DiscountFareTotal}\"></span>\n" +
    "                        </span>\n" +
    "                        <span class=\"depart_time\">{{ 'DATA_FROM' | translate}} {{travelOption.Legs[0].FareOptions.Adultfares[0].DiscountFareTotal + travelOption.Legs[0].surchargesTotalAdult | isoCurrency:travelOption.Legs[0].FareOptions.Adultfares[0].Currency.Abbreviation:fractionSize}}</span>\n" +
    "                    </div>\n" +
    "                    <!--begin tick-->\n" +
    "                    <div class=\"expan_ticked_info\" bs-collapse-target>\n" +
    "                        <div class=\"detail_flight\">\n" +
    "                            <div class=\"indirect_icon_ticked\" ng-class=\"{'direct_icon_ticked': travelOption.Legs[0].SegmentOptions.length == 1}\"></div>\n" +
    "                            <div class=\"transitgroup cf\">\n" +
    "                                <div class=\"transit cf\" ng-repeat=\"segmentOption in travelOption.Legs[0].SegmentOptionsTemp\">\n" +
    "                                    <div ng-show=\"segmentOption.checkAirCraft == false\" class=\"segment_view cf\">\n" +
    "                                        <div class=\"time_to_time\">\n" +
    "                                            <p class=\"timesheet\">\n" +
    "                                                <span class=\"time_from\">{{segmentOption.Flight.ETDLocal | date:'HH:mm'}}</span>\n" +
    "                                                <span class=\"fly_to_icon\"></span>\n" +
    "                                                <span class=\"time_to\">{{segmentOption.Flight.ETALocal | date:'HH:mm'}}</span>\n" +
    "                                            </p>\n" +
    "                                            <p class=\"flight_name\">\n" +
    "                                                <span class=\"name\">{{segmentOption.Flight.Number}} - {{segmentOption.Flight.Aircraft.ModelName}}</span>\n" +
    "                                            </p>\n" +
    "                                        </div>\n" +
    "                                        <div class=\"airport_status\">\n" +
    "                                            <p class=\"airport_from\" ng-click=\"loadAirportDetail(segmentOption.Flight.DepartureAirport.Code, segmentOption.Flight.DepartureAirport.Name)\">{{segmentOption.Flight.DepartureAirport.Code}}</p>\n" +
    "                                            <p class=\"airport_to\" ng-click=\"loadAirportDetail(segmentOption.Flight.ArrivalAirport.Code, segmentOption.Flight.ArrivalAirport.Name)\">{{segmentOption.Flight.ArrivalAirport.Code}}</p>\n" +
    "                                        </div>\n" +
    "                                    </div>\n" +
    "                                    <div ng-show=\"segmentOption.checkAirCraft\" class=\"segment_stop cf\">\n" +
    "                                        <div>\n" +
    "                                            <span class=\"stop_lable\">{{ 'DATA_STOP' | translate }} {{segmentOption.countStop}}: </span>\n" +
    "                                            <span class=\"stop_value airport_from\" ng-click=\"loadAirportDetail(segmentOption.Flight.DepartureAirport.Code, segmentOption.Flight.DepartureAirport.Name)\">{{segmentOption.Flight.DepartureAirport.Code}}</span>\n" +
    "                                            <span class=\"stop_lable\">- {{ 'DATA_CONNECTION' | translate }}: </span>\n" +
    "                                            <span class=\"stop_value\">{{segmentOption.Flight.TimeTransit }}</span>\n" +
    "                                        </div>\n" +
    "                                    </div>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <div class=\"slecttion_package_price\">\n" +
    "                            <div class=\"eco_price cf\" ng-click=\"packagePriceSelect[tab].selected = pricePackage; isSelected(tab, $index, pricePackage, travelOption.Legs[0]); packagePriceSelect[tab].leg=travelOption.Legs[0]\" ng-repeat=\"pricePackage in travelOption.Legs[0].FareOptions.Adultfares\">\n" +
    "                                <span class=\"select_tion uncheck\" ng-class=\"{'check': packagePriceSelect[tab].selected==pricePackage || pricePackage.isChecked}\"></span>\n" +
    "                                <span class=\"package_name\" ng-click=\"loadFare(pricePackage.FareClass)\">{{pricePackage.FareClass}}</span>\n" +
    "                                <span class=\"package_price\" ng-click=\"loadChargeSummary(travelOption.Legs[0].Surcharges, pricePackage)\">{{pricePackage.DiscountFareTotal + travelOption.Legs[0].surchargesTotalAdult | isoCurrency:pricePackage.Currency.Abbreviation:fractionSize}}</span>\n" +
    "                            </div>\n" +
    "                            <!--<div class=\"bus_price cf soldout\">\n" +
    "                                <span class=\"select_tion uncheck\"></span>\n" +
    "                                <span class=\"package_name\">{{ 'DATA_BUSSINESS' | translate }}</span>\n" +
    "                                <span class=\"package_price\">{{ 'DATA_SOLDOUT' | translate }}</span>\n" +
    "                            </div>-->\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <!--begin tick-->\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"button_next_step\" ng-show=\"isFinding == false\">\n" +
    "        <button ng-click=\"submitFlight()\">{{ 'BUTTON_COUNTINUE' | translate }}</button>\n" +
    "    </div>\n" +
    "</form>\n" +
    "");
  $templateCache.put("booking/views/step_3.html",
    "<div class=\"step_status\"><span class=\"step step3\"></span></div>\n" +
    "<form class=\"form-horizontal infomation_passenger\" name=\"step3Form\" ng-model=\"passengerCollapse\" bs-collapse>\n" +
    "    <div class=\"passenger\" ng-repeat=\"passenger in adults\">\n" +
    "        <p class=\"passenger_name\" bs-collapse-toggle>\n" +
    "            {{passenger.firstName ? passenger.firstName + ' ' + passenger.lastName : 'Passenger ' + ($index + 1) }}\n" +
    "            <span class=\"pass_infant\" ng-if=\"$index < infants.length\">+ {{infants[$index].firstName ? infants[$index].firstName + ' ' + infants[$index].lastName : 'Infant' }}\n" +
    "            </span>\n" +
    "            <span class=\"icon_plus_passenger\" ng-class=\"{'has-completed': passenger.firstName && passenger.lastName && passenger.birthday && passenger.mobile && passenger.email && passenger.passengerAddress && passenger.passengerSelectCountry && passenger.billingCity}\" ng-if=\"$index == 0\"></span>\n" +
    "            <span class=\"icon_plus_passenger\" ng-class=\"{'has-completed': passenger.firstName && passenger.lastName}\" ng-if=\"$index > 0\"></span>\n" +
    "        </p>\n" +
    "        <div class=\"infomation_input_passenger panel-collapse\" bs-collapse-target>\n" +
    "            <div ng-if=\"$index == 0\">\n" +
    "                <div class=\"flightname\">\n" +
    "                    <p>{{ 'DATA_PASSENGER_DETAIL' | translate }}</p>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label for=\"passengerGender\" class=\"col-sm-2 control-label\">{{ 'DATA_TITLE' | translate }}</label>\n" +
    "                    <div class=\"col-sm-10\">\n" +
    "                        <select class=\"form-control\" name=\"passengerGender\" ng-model=\"passenger.gender\" ng-init=\"passenger.gender = 'Mr'\">\n" +
    "                            <!--<option value=\"\">{{ 'DATA_CHOOSE_TITLE' | translate }}</option>-->\n" +
    "                            <option>Mr</option>\n" +
    "                            <option>Ms</option>\n" +
    "                            <option>Mrs</option>\n" +
    "                        </select>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\" show-errors>\n" +
    "                    <label class=\"col-sm-2 control-label\">{{ 'DATA_FIRST_NAME' | translate }}<span style=\"color: red\">*</span></label>\n" +
    "                    <div class=\"col-sm-10\">\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"passengerFirstName\" ng-model=\"passenger.firstName\" required>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\" show-errors>\n" +
    "                    <label class=\"col-sm-2 control-label\">{{ 'DATA_LAST_NAME' | translate }}<span style=\"color: red\">*</span></label>\n" +
    "                    <div class=\"col-sm-10\">\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"passengerLastName\" ng-model=\"passenger.lastName\" required>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label class=\"col-sm-2 control-label\">{{ 'DATA_PASSPORT_NUMBER' | translate }}</label>\n" +
    "                    <div class=\"col-sm-10\">\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"passportNumber\" ng-model=\"passenger.passportNumber\">\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label class=\"col-sm-2 control-label\">{{ 'DATA_PASSPORT_EXPRIRY_DATE' | translate }}</label>\n" +
    "                    <div class=\"col-sm-10\">\n" +
    "                        <input type=\"text\" class=\"form-control\" data-min-date=\"today\" data-autoclose=\"1\" data-use-native=\"true\" name=\"passportExpiry\" ng-model=\"passenger.passportExpiry\" bs-datepicker>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\" show-errors>\n" +
    "                    <label class=\"col-sm-2 control-label\">{{ 'DATA_DOB' | translate }}<span style=\"color: red\">*</span></label>\n" +
    "                    <div class=\"col-sm-10\">\n" +
    "                        <input type=\"text\" class=\"form-control\" data-max-date=\"today\" data-autoclose=\"1\" data-use-native=\"true\" name=\"passengerBirthday\" ng-model=\"passenger.birthday\" bs-datepicker required>\n" +
    "                        <!--<input type=\"date\" class=\"form-control\" name=\"passengerBirthday\" ng-model=\"passenger.birthday\" required>-->\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\" show-errors>\n" +
    "                    <label class=\"col-sm-2 control-label\">{{ 'DATA_MOBILE' | translate }}<span style=\"color: red\">*</span></label>\n" +
    "                    <div class=\"col-sm-10\">\n" +
    "                        <input type=\"tel\" class=\"form-control\" name=\"passengerMobile\" ng-model=\"passenger.mobile\" pattern=\"[0-9-+() ]+\" title=\"Phone number invalid\" required>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\" show-errors>\n" +
    "                    <label class=\"col-sm-2 control-label\">{{ 'DATA_EMAIL' | translate }}<span style=\"color: red\">*</span></label>\n" +
    "                    <div class=\"col-sm-10\">\n" +
    "                        <input type=\"email\" class=\"form-control\" name=\"passengerEmail\" ng-model=\"passenger.email\" required>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group cf\" show-errors>\n" +
    "                    <label class=\"col-sm-2 control-label\">{{ 'DATA_ADDRESS' | translate }}<span style=\"color: red\">*</span></label>\n" +
    "                    <div class=\"col-sm-10\">\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"passengerAddress\" ng-model=\"passenger.passengerAddress\" required>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group cf\" show-errors>\n" +
    "                    <label for=\"select_origin\" class=\"col-sm-2 control-label\">{{ 'DATA_COUNTRY' | translate }}<span style=\"color: red\">*</span></label>\n" +
    "                    <div class=\"col-sm-10\">\n" +
    "                        <select class=\"form-control\" ng-change=\"changeCountry()\" name=\"select_payment_country_passenger\" ng-model=\"passenger.passengerSelectCountry\" ng-options=\"country.Code as country.Name for country in countryResult\" required>\n" +
    "                            <option value=\"\">{{ 'DATA_SELECT' | translate }}</option>\n" +
    "                        </select>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group cf\" show-errors>\n" +
    "                    <label class=\"col-sm-2 control-label\">{{ 'DATA_TOWN_CITY' | translate }}<span style=\"color: red\">*</span></label>\n" +
    "                    <div class=\"col-sm-10\">\n" +
    "                        <select class=\"form-control\" name=\"passengerCity\" ng-model=\"passenger.billingCity\" required ng-options=\"province.Code as province.Name for province in provinceResult\" required>\n" +
    "                            <option value=\"\">{{ 'DATA_SELECT' | translate }}</option>\n" +
    "                        </select>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "            <div ng-if=\"$index > 0\">\n" +
    "                <div class=\"flightname\">\n" +
    "                    <p>{{ 'DATA_PASSENGER_DETAIL' | translate }}</p>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label for=\"passengerGender\" class=\"col-sm-2 control-label\">{{ 'DATA_TITLE' | translate }}</label>\n" +
    "                    <div class=\"col-sm-10\">\n" +
    "                        <select class=\"form-control\" name=\"passengerGender\" ng-model=\"passenger.gender\" ng-init=\"passenger.gender = 'Mr'\">\n" +
    "                            <!--<option value=\"\">{{ 'DATA_CHOOSE_TITLE' | translate }}</option>-->\n" +
    "                            <option>Mr</option>\n" +
    "                            <option>Ms</option>\n" +
    "                            <option>Mrs</option>\n" +
    "                        </select>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\" show-errors>\n" +
    "                    <label class=\"col-sm-2 control-label\">{{ 'DATA_FIRST_NAME' | translate }}<span style=\"color: red\">*</span></label>\n" +
    "                    <div class=\"col-sm-10\">\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"passengerFirstName\" ng-model=\"passenger.firstName\" required>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\" show-errors>\n" +
    "                    <label class=\"col-sm-2 control-label\">{{ 'DATA_LAST_NAME' | translate }}<span style=\"color: red\">*</span></label>\n" +
    "                    <div class=\"col-sm-10\">\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"passengerLastName\" ng-model=\"passenger.lastName\" required>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label class=\"col-sm-2 control-label\">{{ 'DATA_PASSPORT_NUMBER' | translate }}</label>\n" +
    "                    <div class=\"col-sm-10\">\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"passportNumber\" ng-model=\"passenger.passportNumber\">\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label class=\"col-sm-2 control-label\">{{ 'DATA_PASSPORT_EXPRIRY_DATE' | translate }}</label>\n" +
    "                    <div class=\"col-sm-10\">\n" +
    "                        <input type=\"text\" class=\"form-control\" data-min-date=\"today\" data-autoclose=\"1\" data-use-native=\"true\" name=\"passportExpiry\" ng-model=\"passenger.passportExpiry\" bs-datepicker>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label class=\"col-sm-2 control-label\">{{ 'DATA_DOB' | translate }}</label>\n" +
    "                    <div class=\"col-sm-10\">\n" +
    "                        <input type=\"text\" class=\"form-control\" data-max-date=\"today\" data-autoclose=\"1\" data-use-native=\"true\" name=\"passengerBirthday\" ng-model=\"passenger.birthday\" bs-datepicker>\n" +
    "                        <!--<input type=\"date\" class=\"form-control\" name=\"passengerBirthday\" ng-model=\"passenger.birthday\" required>-->\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label class=\"col-sm-2 control-label\">{{ 'DATA_MOBILE' | translate }}</label>\n" +
    "                    <div class=\"col-sm-10\">\n" +
    "                        <input type=\"tel\" class=\"form-control\" name=\"passengerMobile\" ng-model=\"passenger.mobile\" pattern=\"[0-9-+() ]+\" title=\"Phone number invalid\">\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label class=\"col-sm-2 control-label\">{{ 'DATA_EMAIL' | translate }}</label>\n" +
    "                    <div class=\"col-sm-10\">\n" +
    "                        <input type=\"email\" class=\"form-control\" name=\"passengerEmail\" ng-model=\"passenger.email\">\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"flightname\" ng-if=\"$index < infants.length\">\n" +
    "                <p>{{ 'DATA_INFANT_PASSENGER' | translate }}</p>\n" +
    "            </div>\n" +
    "            <div class=\"form-group\" ng-if=\"$index < infants.length\">\n" +
    "                <label for=\"select_origin\" class=\"col-sm-2 control-label\">{{ 'DATA_GENDER' | translate }}</label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <p class=\"form-control\" style=\"border: none; padding: 14px 0px;box-shadow: none;\">{{ 'DATA_BOOK_GENDER_INFANT' | translate }}</p>\n" +
    "                    <!--<select class=\"form-control\" name=\"infantTitle\" ng-model=\"infants[$index].gender\" required>\n" +
    "                        <option value=\"\">{{ 'DATA_SELECT_GENDER' | translate }}</option>\n" +
    "                        <option value=\"M\">{{ 'DATA_MALE' | translate }}</option>\n" +
    "                        <option value=\"F\">{{ 'DATA_FEMALE' | translate }}</option>\n" +
    "                    </select>-->\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group\" ng-if=\"$index < infants.length\" show-errors>\n" +
    "                <label class=\"col-sm-2 control-label\">{{ 'DATA_FIRST_NAME' | translate }}<span style=\"color: red\">*</span></label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"infantFirstName\" ng-model=\"infants[$index].firstName\" required>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group\" ng-if=\"$index < infants.length\" show-errors>\n" +
    "                <label class=\"col-sm-2 control-label\">{{ 'DATA_LAST_NAME' | translate }}<span style=\"color: red\">*</span></label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"infantLastName\" ng-model=\"infants[$index].lastName\" required>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group\" ng-if=\"$index < infants.length\">\n" +
    "                <label class=\"col-sm-2 control-label\">{{ 'DATA_DOB' | translate }}</label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <input type=\"text\" class=\"form-control\" data-max-date=\"today\" data-use-native=\"true\" data-autoclose=\"1\" name=\"infantBirthday\" ng-model=\"infants[$index].birthday\" bs-datepicker>\n" +
    "                    <!--<input type=\"date\" class=\"form-control\" name=\"infantBirthday\" ng-model=\"infants[$index].birthday\" >-->\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"passenger\" ng-repeat=\"child in children\">\n" +
    "        <p class=\"passenger_name\" bs-collapse-toggle>\n" +
    "            {{child.firstName ? child.firstName + ' ' + child.lastName : 'Children ' + ($index + 1) }}\n" +
    "            <span class=\"icon_plus_passenger\" ng-class=\"{'has-completed': child.gender && child.firstName && child.lastName}\"></span>\n" +
    "        </p>\n" +
    "        <div class=\"infomation_input_passenger panel-collapse\" bs-collapse-target>\n" +
    "            <div class=\"flightname\">\n" +
    "                <p>{{ 'DATA_CHILDREN_DETAILS' | translate }}</p>\n" +
    "            </div>\n" +
    "            <div class=\"form-group\">\n" +
    "                <label for=\"select_origin\" class=\"col-sm-2 control-label\">{{ 'DATA_TITLE' | translate }}</label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <p class=\"form-control\" style=\"border: none; padding: 14px 0px;box-shadow: none;\">{{ 'DATA_BOOK_GENDER_CHILD' | translate }}</p>\n" +
    "                    <!--<select class=\"form-control\" name=\"childGender\" ng-model=\"child.gender\" required>\n" +
    "                        <option value=\"\">{{ 'DATA_CHOOSE_TITLE' | translate }}</option>\n" +
    "                        <option>Mr</option>\n" +
    "                        <option>Ms</option>\n" +
    "                        <option>Mrs</option>\n" +
    "                    </select>-->\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group\" show-errors>\n" +
    "                <label class=\"col-sm-2 control-label\">{{ 'DATA_FIRST_NAME' | translate }}<span style=\"color: red\">*</span></label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"childFirstName\" ng-model=\"child.firstName\" required>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group\" show-errors>\n" +
    "                <label class=\"col-sm-2 control-label\">{{ 'DATA_LAST_NAME' | translate }}<span style=\"color: red\">*</span></label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"childLastName\" ng-model=\"child.lastName\" required>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group\">\n" +
    "                <label class=\"col-sm-2 control-label\">{{ 'DATA_DOB' | translate }}</label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <input type=\"text\" class=\"form-control\" data-max-date=\"today\" data-use-native=\"true\" data-autoclose=\"1\" name=\"childBirthday\" ng-model=\"child.birthday\" bs-datepicker>\n" +
    "                    <!--<input type=\"date\" class=\"form-control\" name=\"childBirthday\" ng-model=\"child.birthday\">-->\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group\">\n" +
    "                <label class=\"col-sm-2 control-label\">{{ 'DATA_MOBILE' | translate }}</label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <input type=\"tel\" class=\"form-control\" name=\"childMobile\" ng-model=\"child.mobile\" pattern=\"[0-9-+() ]+\" title=\"Phone number invalid\">\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group\">\n" +
    "                <label class=\"col-sm-2 control-label\">{{ 'DATA_EMAIL' | translate }}</label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <input type=\"email\" class=\"form-control\" name=\"childEmail\" ng-model=\"child.email\">\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"button_next_step\">\n" +
    "        <button ng-click=\"submitPassenger()\">{{ 'BUTTON_COUNTINUE' | translate }}</button>\n" +
    "    </div>\n" +
    "</form>\n" +
    "\n" +
    "");
  $templateCache.put("booking/views/step_4.html",
    "<div class=\"step_status\"><span class=\"step step4\"></span></div>\n" +
    "<form name=\"step4Form\">\n" +
    "    <div class=\"flight_addon infomation_passenger\">\n" +
    "        <div class=\"flightname\">\n" +
    "            <p>{{ 'DATA_FLIGHT' | translate }} 1: {{s1FromName}} to {{s1ToName}} - {{step1Data.s1DateStart | date:'longDate'}}</p>\n" +
    "        </div>\n" +
    "        <div class=\"addon_service_for_flight addon_service_for_flight1\" ng-model=\"addonServiceCollapseFlight1\" bs-collapse ng-repeat=\"(sIndex, segment) in step2Data[0].leg.SegmentOptions\">\n" +
    "            <div class=\"flightname\">\n" +
    "                <p>{{ 'DATA_SEGMENT' | translate }} {{$index + 1}}: {{segment.Flight.DepartureAirport.Name}} to {{segment.Flight.ArrivalAirport.Name}} - {{segment.Flight.ETD | date:'longDate'}}</p>\n" +
    "            </div>\n" +
    "            <div class=\"passenger\" ng-repeat=\"passenger in adults\">\n" +
    "                <p class=\"passenger_name\" bs-collapse-toggle>\n" +
    "                    {{passenger.firstName ? passenger.firstName + ' ' + passenger.lastName : 'Passenger ' + ($index + 1) }}\n" +
    "                    <span class=\"pass_infant\" ng-if=\"$index < infants.length\">+ {{infants[$index].firstName ? infants[$index].firstName + ' ' + infants[$index].lastName : 'Infant' }}</span>\n" +
    "                    <span class=\"icon_plus_passenger\" ng-class=\"{'has-completed': passenger.addons[sIndex].baggage}\"></span>\n" +
    "                </p>\n" +
    "                <div class=\"infomation_input_passenger\" bs-collapse-target>\n" +
    "                    <div class=\"flightname\" ng-if=\"checkAddon(addonDepartBaggage, addonDepartMeal, sIndex) == false\">\n" +
    "                        <p style=\"color: red;\">{{ 'DATA_MESSAGE_ADDITIONAL_SERVICE' | translate }}</p>\n" +
    "                    </div>\n" +
    "                    <div class=\"flightname\" ng-if=\"checkBaggage(addonDepartBaggage, sIndex)\">\n" +
    "                        <p>{{ 'DATA_BAGGAGE_SERVICES' | translate }}</p>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group cf\" ng-if=\"checkBaggage(addonDepartBaggage, sIndex)\">\n" +
    "                        <label for=\"select_origin\" class=\"col-sm-2 control-label\">{{ 'DATA_BAGGAGE' | translate }}</label>\n" +
    "                        <div class=\"col-sm-10\">\n" +
    "                            <select class=\"form-control\" ng-model=\"passenger.addons[sIndex].baggage\" ng-options=\"depBaggage as depBaggage.Name for depBaggage in addonDepartBaggage[sIndex].Allocations\">\n" +
    "                                <option value=\"\">{{ 'DATA_NO_THANKS' | translate }}</option>\n" +
    "                            </select>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"flightname\" ng-if=\"checkMeal(addonDepartMeal, sIndex)\">\n" +
    "                        <p>{{ 'DATA_MEALS_SERVICES' | translate }}</p>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group cf\" ng-repeat=\"meal in addonDepartMeal[sIndex].Allocations\">\n" +
    "                        <label for=\"select_origin\" class=\"col-sm-2 control-label\">{{meal.Name}}</label>\n" +
    "                        <div class=\"col-sm-10\">\n" +
    "                            <input type=\"number\" ngmaxlength=\"1\" class=\"form-control\" ng-model=\"passenger.addons[sIndex].mealQuality[meal.ID].number\" numbers-only>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"passenger\" ng-repeat=\"child in children\">\n" +
    "                <p class=\"passenger_name\" bs-collapse-toggle>\n" +
    "                    {{child.firstName ? child.firstName + ' ' + child.lastName : 'Children ' + ($index + 1) }}\n" +
    "                    <span class=\"icon_plus_passenger\" ng-class=\"{'has-completed': checkObjectIsEmpty(child.addons[sIndex].baggage) == false}\"></span>\n" +
    "                </p>\n" +
    "                <div class=\"infomation_input_passenger panel-collapse\" bs-collapse-target>\n" +
    "                    <div class=\"flightname\" ng-if=\"checkAddon(addonDepartBaggage, addonDepartMeal, sIndex) == false\">\n" +
    "                        <p style=\"color: red;\">{{ 'DATA_MESSAGE_ADDITIONAL_SERVICE' | translate }}</p>\n" +
    "                    </div>\n" +
    "                    <div class=\"flightname\" ng-if=\"checkBaggage(addonDepartBaggage, sIndex)\">\n" +
    "                        <p>{{ 'DATA_BAGGAGE_SERVICES' | translate }}</p>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group cf\" ng-if=\"checkBaggage(addonDepartBaggage, sIndex)\">\n" +
    "                        <label for=\"select_origin\" class=\"col-sm-2 control-label\">{{ 'DATA_BAGGAGE' | translate }}</label>\n" +
    "                        <div class=\"col-sm-10\">\n" +
    "                            <select class=\"form-control\" ng-model=\"child.addons[sIndex].baggage\" ng-options=\"depBaggage as depBaggage.Name for depBaggage in addonDepartBaggage[sIndex].Allocations\">\n" +
    "                                <option value=\"\">{{ 'DATA_NO_THANKS' | translate }}</option>\n" +
    "                            </select>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"flightname\" ng-if=\"checkMeal(addonDepartMeal, sIndex)\">\n" +
    "                        <p>{{ 'DATA_MEALS_SERVICES' | translate }}</p>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group cf\" ng-repeat=\"meal in addonDepartMeal[sIndex].Allocations\">\n" +
    "                        <label for=\"select_origin\" class=\"col-sm-2 control-label\">{{meal.Name}}</label>\n" +
    "                        <div class=\"col-sm-10\">\n" +
    "                            <input type=\"number\" ngmaxlength=\"1\" class=\"form-control\" ng-model=\"child.addons[sIndex].mealQuality[meal.ID].number\" numbers-only>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"flightname\" ng-if=\"step1Data.s1RoundTrip == 1\">\n" +
    "            <p>{{ 'DATA_FLIGHT' | translate }} 2: {{s1ToName}} to {{s1FromName}} - {{step1Data.s1DateReturn | date:'longDate'}}</p>\n" +
    "        </div>\n" +
    "        <div class=\"addon_service_for_flight addon_service_for_flight1\" ng-if=\"step1Data.s1RoundTrip == 1\" ng-model=\"addonServiceCollapseFlight2\" bs-collapse ng-repeat=\"(sIndex, segment) in step2Data[1].leg.SegmentOptions\">\n" +
    "            <div class=\"flightname\">\n" +
    "                <p>{{ 'DATA_SEGMENT' | translate }} {{$index + 1}}: {{segment.Flight.DepartureAirport.Name}} to {{segment.Flight.ArrivalAirport.Name}} - {{segment.Flight.ETD | date:'longDate'}}</p>\n" +
    "            </div>\n" +
    "            <div class=\"passenger\" ng-repeat=\"passenger in adults\">\n" +
    "                <p class=\"passenger_name\" bs-collapse-toggle>\n" +
    "                    {{passenger.firstName ? passenger.firstName + ' ' + passenger.lastName : 'Passenger ' + ($index + 1) }}\n" +
    "            <span class=\"pass_infant\" ng-if=\"$index < infants.length\">+ {{infants[$index].firstName ? infants[$index].firstName + ' ' + infants[$index].lastName : 'Infant' }}\n" +
    "            </span>\n" +
    "                    <span class=\"icon_plus_passenger\" ng-class=\"{'has-completed': passenger.addons[step2Data[0].leg.SegmentOptions.length + sIndex].baggage}\"></span>\n" +
    "                </p>\n" +
    "                <div class=\"infomation_input_passenger panel-collapse\" bs-collapse-target>\n" +
    "                    <div class=\"flightname\" ng-if=\"checkAddon(addonArrivalBaggage, addonArrivalMeal, sIndex) == false\">\n" +
    "                        <p style=\"color: red;\">{{ 'DATA_MESSAGE_ADDITIONAL_SERVICE' | translate }}</p>\n" +
    "                    </div>\n" +
    "                    <div class=\"flightname\" ng-if=\"checkBaggage(addonArrivalBaggage, sIndex)\">\n" +
    "                        <p>{{ 'DATA_BAGGAGE_SERVICES' | translate }}</p>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group cf\" ng-if=\"checkBaggage(addonArrivalBaggage, sIndex)\">\n" +
    "                        <label for=\"select_origin\" class=\"col-sm-2 control-label\">{{ 'DATA_BAGGAGE' | translate }}</label>\n" +
    "                        <div class=\"col-sm-10\">\n" +
    "                            <select class=\"form-control\" ng-model=\"passenger.addons[step2Data[0].leg.SegmentOptions.length + sIndex].baggage\" ng-options=\"arrivalBaggage as arrivalBaggage.Name for arrivalBaggage in addonArrivalBaggage[sIndex].Allocations\">\n" +
    "                                <option value=\"\">{{ 'DATA_NO_THANKS' | translate }}</option>\n" +
    "                            </select>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"flightname\" ng-if=\"checkMeal(addonArrivalMeal, sIndex)\">\n" +
    "                        <p>{{ 'DATA_MEALS_SERVICES' | translate }}</p>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group cf\" ng-repeat=\"meal in addonArrivalMeal[sIndex].Allocations\">\n" +
    "                        <label for=\"select_origin\" class=\"col-sm-2 control-label\">{{meal.Name}}</label>\n" +
    "                        <div class=\"col-sm-10\">\n" +
    "                            <input type=\"number\" ngmaxlength=\"1\" class=\"form-control\" ng-model=\"passenger.addons[step2Data[0].leg.SegmentOptions.length + sIndex].mealQuality[meal.ID].number\" numbers-only>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"passenger\" ng-repeat=\"child in children\">\n" +
    "                <p class=\"passenger_name\" bs-collapse-toggle>\n" +
    "                    {{child.firstName ? child.firstName + ' ' + child.lastName : 'Children ' + ($index + 1) }}\n" +
    "                    <span class=\"icon_plus_passenger\" ng-class=\"{'has-completed': checkObjectIsEmpty(child.addons[step2Data[0].leg.SegmentOptions.length + sIndex].baggage) == false }\"></span>\n" +
    "                </p>\n" +
    "                <div class=\"infomation_input_passenger panel-collapse\" bs-collapse-target>\n" +
    "                    <div class=\"flightname\" ng-if=\"checkAddon(addonArrivalBaggage, addonArrivalMeal, sIndex) == false\">\n" +
    "                        <p style=\"color: red;\">{{ 'DATA_MESSAGE_ADDITIONAL_SERVICE' | translate }}</p>\n" +
    "                    </div>\n" +
    "                    <div class=\"flightname\" ng-if=\"checkBaggage(addonArrivalBaggage, sIndex)\">\n" +
    "                        <p>{{ 'DATA_BAGGAGE_SERVICES' | translate }}</p>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group cf\" ng-if=\"checkBaggage(addonArrivalBaggage, sIndex)\">\n" +
    "                        <label for=\"select_origin\" class=\"col-sm-2 control-label\">{{ 'DATA_BAGGAGE' | translate }}</label>\n" +
    "                        <div class=\"col-sm-10\">\n" +
    "                            <select class=\"form-control\" ng-model=\"child.addons[step2Data[0].leg.SegmentOptions.length + sIndex].baggage\" ng-options=\"arrivalBaggage as arrivalBaggage.Name for arrivalBaggage in addonArrivalBaggage[sIndex].Allocations\">\n" +
    "                                <option value=\"\">{{ 'DATA_NO_THANKS' | translate }}</option>\n" +
    "                            </select>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"flightname\" ng-if=\"checkMeal(addonArrivalMeal, sIndex)\">\n" +
    "                        <p>{{ 'DATA_MEALS_SERVICES' | translate }}</p>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group cf\" ng-repeat=\"meal in addonArrivalMeal[sIndex].Allocations\">\n" +
    "                        <label for=\"select_origin\" class=\"col-sm-2 control-label\">{{meal.Name}}</label>\n" +
    "                        <div class=\"col-sm-10\">\n" +
    "                            <input type=\"number\" ngmaxlength=\"1\" class=\"form-control\" ng-model=\"child.addons[step2Data[0].leg.SegmentOptions.length + sIndex].mealQuality[meal.ID].number\" numbers-only>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"button_next_step\">\n" +
    "        <button ng-click=\"submitAddon()\">{{ 'BUTTON_COUNTINUE' | translate }}</button>\n" +
    "    </div>\n" +
    "</form>\n" +
    "");
  $templateCache.put("booking/views/step_5.html",
    "<div class=\"step_status\"><span class=\"step step5\"></span></div>\n" +
    "<form name=\"step5Form\">\n" +
    "    <div class=\"infomation_booking\">\n" +
    "        <div ng-repeat=\"s2dataFare in s2Data\">\n" +
    "            <div class=\"flightname\">\n" +
    "                <p>{{ 'DATA_FLIGHT' | translate }} {{$index + 1}}: {{s2dataFare.leg.SegmentOptions[0].Flight.DepartureAirport.Name}} {{ 'DATA_TO' | translate }} {{s2dataFare.leg.SegmentOptions[s2dataFare.leg.SegmentOptions.length - 1].Flight.ArrivalAirport.Name}} - {{s2dataFare.leg.DepartureDate  | date:'longDate'}}</p>\n" +
    "            </div>\n" +
    "            <div class=\"valuefare\">\n" +
    "                <div class=\"flight_fly\">\n" +
    "                    <!--<div class=\"summary_flight\">\n" +
    "                        <span class=\"icon_expan icon_close_expan\"></span>\n" +
    "                        <span class=\"price_from\">{{ 'DATA_DEPART' | translate }}: {{s2dataFare.leg.SegmentOptions[0].Flight.ETDLocal | date:'HH:mm'}} - {{s2dataFare.leg.SegmentOptions[0].Flight.ETALocal | date:'HH:mm'}}</span>-->\n" +
    "                    <!--<span class=\"depart_time\">{{ 'DATA_FROM' | translate }}: <span class=\"price_detail\">{{s2dataFare.selected.DiscountFareTotal | isoCurrency:s2dataFare.selected.Currency.Abbreviation:fractionSize}}</span>-->\n" +
    "                    <!--</div>\n" +
    "                -->\n" +
    "                    <div class=\"expan_ticked_info\">\n" +
    "                        <div class=\"detail_flight\">\n" +
    "                            <div class=\"indirect_icon_ticked\" ng-class=\"{'direct_icon_ticked': s2dataFare.leg.SegmentOptions.length == 1}\"></div>\n" +
    "                            <div class=\"transitgroup cf\">\n" +
    "                                <div class=\"transit cf\" ng-repeat=\"segmentOption in s2dataFare.leg.SegmentOptionsTemp\">\n" +
    "                                    <div ng-show=\"segmentOption.checkAirCraft == false\" class=\"segment_view cf\">\n" +
    "                                        <div class=\"time_to_time\">\n" +
    "                                            <p class=\"timesheet\">\n" +
    "                                                <span class=\"time_from\">{{segmentOption.Flight.ETDLocal | date:'HH:mm'}}</span>\n" +
    "                                                <span class=\"fly_to_icon\"></span>\n" +
    "                                                <span class=\"time_to\">{{segmentOption.Flight.ETALocal | date:'HH:mm'}}</span>\n" +
    "                                            </p>\n" +
    "                                            <p class=\"flight_name\">\n" +
    "                                                <span class=\"name\">{{segmentOption.Flight.Number}} - {{segmentOption.Flight.Aircraft.ModelName}}</span>\n" +
    "                                            </p>\n" +
    "                                        </div>\n" +
    "                                        <div class=\"airport_status\">\n" +
    "                                            <p class=\"airport_from\" ng-click=\"loadAirportDetail(segmentOption.Flight.DepartureAirport.Code, segmentOption.Flight.DepartureAirport.Name)\">{{segmentOption.Flight.DepartureAirport.Code}}</p>\n" +
    "                                            <p class=\"airport_to\" ng-click=\"loadAirportDetail(segmentOption.Flight.ArrivalAirport.Code, segmentOption.Flight.ArrivalAirport.Name)\">{{segmentOption.Flight.ArrivalAirport.Code}}</p>\n" +
    "                                        </div>\n" +
    "                                    </div>\n" +
    "                                    <div ng-show=\"segmentOption.checkAirCraft\" class=\"segment_stop cf\">\n" +
    "                                        <div>\n" +
    "                                            <span class=\"stop_lable\">{{ 'DATA_STOP' | translate }} {{segmentOption.countStop}}: </span>\n" +
    "                                            <span class=\"stop_value airport_from\" ng-click=\"loadAirportDetail(segmentOption.Flight.DepartureAirport.Code, segmentOption.Flight.DepartureAirport.Name)\">{{segmentOption.Flight.DepartureAirport.Code}}</span>\n" +
    "                                            <span class=\"stop_lable\">- {{ 'DATA_CONNECTION' | translate }}: </span>\n" +
    "                                            <span class=\"stop_value\">{{segmentOption.Flight.TimeTransit }}</span>\n" +
    "                                        </div>\n" +
    "                                    </div>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <div class=\"slecttion_package_price\">\n" +
    "                            <div class=\"pre_price\">\n" +
    "                                <span class=\"select_tion check\"></span>\n" +
    "                                <span class=\"package_name\" ng-click=\"loadFare(s2dataFare.selected.FareClass)\">{{s2dataFare.selected.FareClass}}</span>\n" +
    "                                <span class=\"package_price\" style=\"text-decoration: none;\">{{s2dataFare.selected.DiscountFareTotal + s2dataFare.leg.surchargesTotalAdult | isoCurrency:s2dataFare.selected.Currency.Abbreviation:fractionSize}}</span>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "            </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"total\">\n" +
    "            <p>{{ 'DATA_TOTAL' | translate }}</p>\n" +
    "        </div>\n" +
    "        <div class=\"flyght_airport\" ng-repeat=\"s2dataTotal in s2Data\">\n" +
    "            <div class=\"flightname\">\n" +
    "                <p>{{ 'DATA_FLIGHT' | translate }} {{$index + 1}}: {{s2dataTotal.leg.SegmentOptions[0].Flight.DepartureAirport.Name}} {{ 'DATA_TO' | translate }} {{s2dataTotal.leg.SegmentOptions[s2dataTotal.leg.SegmentOptions.length - 1].Flight.ArrivalAirport.Name}} - {{s2dataTotal.leg.DepartureDate  | date:'longDate'}}</p>\n" +
    "            </div>\n" +
    "            <div class=\"info_passenger\">\n" +
    "                <p>{{s3Data.adults.length}} x Adult<span class=\"price\">{{costs[$index].adultCosts + (s3Data.adults.length * s2dataTotal.leg.surchargesTotalAdult) | isoCurrency:s2dataTotal.selected.Currency.Abbreviation:fractionSize}}</span></p>\n" +
    "                <p ng-show=\"s3Data.children.length > 0\">{{s3Data.children.length}} x Child<span class=\"price\">{{costs[$index].childCosts | isoCurrency:s2dataTotal.selected.Currency.Abbreviation:fractionSize}}</span></p>\n" +
    "                <p ng-show=\"s3Data.infants.length > 0\">{{s3Data.infants.length}} x Infant<span class=\"price\">{{ 0 | isoCurrency:s2dataTotal.selected.Currency.Abbreviation:fractionSize}}</span></p>\n" +
    "            </div>\n" +
    "            <div ng-show=\"isShowAddtional == true\" class=\"additional\">\n" +
    "                <p>{{ 'DATA_ADDITIONAL_COSTS' | translate }}</p>\n" +
    "            </div>\n" +
    "            <div class=\"info_passenger\">\n" +
    "                <p ng-repeat=\"(name,value) in addons[$index]\">\n" +
    "                    {{value.count}} x {{name}}<span class=\"price\">{{value.total | isoCurrency:s2dataTotal.selected.Currency.Abbreviation:fractionSize}}</span>\n" +
    "                </p>\n" +
    "            </div>\n" +
    "            <!--<div>\n" +
    "                <div class=\"info_passenger\">\n" +
    "                    <div ng-repeat=\"surcharge in s2dataTotal.leg.Surcharges\">\n" +
    "                        <p ng-if=\"surcharge.ApplyToAdult\">\n" +
    "                            {{s3Data.adults.length}} x {{surcharge.Description}} ({{ 'DATA_BOOK_ADULT' | translate }})<span class=\"price\">{{s3Data.adults.length * surcharge.Total | isoCurrency:s2dataTotal.selected.Currency.Abbreviation:fractionSize}}</span>\n" +
    "                        </p>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>-->\n" +
    "            <div ng-show=\"s3Data.children.length > 0\">\n" +
    "                <div class=\"info_passenger\">\n" +
    "                    <div ng-repeat=\"surcharge in s2dataTotal.leg.Surcharges\">\n" +
    "                        <p ng-if=\"surcharge.ApplyToChild\">\n" +
    "                            {{s3Data.children.length}} x {{surcharge.Description}} ({{ 'DATA_BOOK_CHILD' | translate }})<span class=\"price\">{{s3Data.children.length * surcharge.Total | isoCurrency:s2dataTotal.selected.Currency.Abbreviation:fractionSize}}</span>\n" +
    "                        </p>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div ng-show=\"s3Data.infants.length > 0\">\n" +
    "                <div class=\"info_passenger\">\n" +
    "                    <div ng-repeat=\"surcharge in s2dataTotal.leg.Surcharges\">\n" +
    "                        <p ng-if=\"surcharge.ApplyToInfant\">\n" +
    "                            {{s3Data.infants.length}} x {{surcharge.Description}} ({{ 'DATA_BOOK_INFANT' | translate }})<span class=\"price\">{{s3Data.infants.length * surcharge.Total | isoCurrency:s2dataTotal.selected.Currency.Abbreviation:fractionSize}}</span>\n" +
    "                        </p>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"total_price\">\n" +
    "                <p>{{ 'DATA_TOTAL' | translate }}: <span class=\"price\">{{total[$index]+ s2dataTotal.surchargeData.sumSurcharge | isoCurrency:s2dataTotal.selected.Currency.Abbreviation:fractionSize}}</span></p>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"total_amount\">\n" +
    "            <p>{{ 'DATA_TOTAL_AMOUNT' | translate }} <span class=\"price\">{{totalAmount | isoCurrency:s1Data.s1CurrencyCode:fractionSize}}</span></p>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"button_next_step\">\n" +
    "        <button ng-click=\"payment()\">{{ 'BUTTON_PAYMENT' | translate }}</button>\n" +
    "    </div>\n" +
    "</form>\n" +
    "");
  $templateCache.put("booking/views/step_6.html",
    "<div class=\"step_status\"><span class=\"step step6\"></span></div>\n" +
    "<form name=\"step6Form\">\n" +
    "    <div class=\"payment_infomation\">\n" +
    "        <div class=\"payment_detail\">\n" +
    "            <div class=\"flightname\">\n" +
    "                <p>{{ 'DATA_PAYMENT_DETAIL' | translate }}</p>\n" +
    "            </div>\n" +
    "            <div class=\"form-group cf\" show-errors>\n" +
    "                <label for=\"select_origin\" class=\"col-sm-2 control-label\">{{ 'DATA_PAYMENT_METHOD' | translate }}</label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <select class=\"form-control\" name=\"select_payment_method\" ng-model=\"payMedthod.paymentMethod\" ng-options=\"payment.Code as payment.Description for payment in paymentMethodResult\" required>\n" +
    "                        <option value=\"\">{{ 'DATA_SELECT' | translate }}</option>\n" +
    "                    </select>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"extend_payment_selected\" ng-if=\"payMedthod.paymentMethod && payMedthod.paymentMethod != 'PL'\">\n" +
    "                <div class=\"form-group cf\" show-errors>\n" +
    "                    <label class=\"col-sm-2 control-label\">{{ 'DATA_CARD_NUMBER' | translate }}</label>\n" +
    "                    <div class=\"col-sm-10\">\n" +
    "                        <input type=\"tel\"\n" +
    "                            class=\"form-control\"\n" +
    "                            name=\"payCardNumber\"\n" +
    "                            ng-model=\"payMedthod.payCardNumber\"\n" +
    "                            required\n" +
    "                            data-credit-card-type\n" +
    "                            data-ng-pattern=\"/^[0-9]+$/\"\n" +
    "                            data-ng-minlength=\"15\"\n" +
    "                            maxlength=\"16\"\n" +
    "                            ng-maxlength=\"16\"\n" +
    "                            placeholder=\"Card Number\">\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group cf\" show-errors>\n" +
    "                    <label class=\"col-sm-2 control-label\" style=\"text-decoration: underline;\" ng-click=\"cvvHelp()\">{{ 'DATA_CVV' | translate }}</label>\n" +
    "                    <div class=\"col-sm-10\">\n" +
    "                        <input type=\"tel\"\n" +
    "                            class=\"form-control\"\n" +
    "                            name=\"payCVV\"\n" +
    "                            ng-model=\"payMedthod.payCVV\"\n" +
    "                            required\n" +
    "                            placeholder=\"CVV\"\n" +
    "                            data-ng-pattern=\"/^[0-9]+$/\"\n" +
    "                            data-ng-minlength=\"3\"\n" +
    "                            maxlength=\"4\"\n" +
    "                            ng-maxlength=\"4\">\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group cf\" show-errors>\n" +
    "                    <label class=\"col-sm-2 control-label\">{{ 'DATA_EXPIRED_MONTH' | translate }} </label>\n" +
    "                    <div class=\"col-sm-10\">\n" +
    "                        <select class=\"form-control\" ng-model=\"payMedthod.month\" name=\"month\" data-card-expiration required>\n" +
    "                            <option ng-repeat=\"month in months\" value=\"{{$index+1}}\">\n" +
    "                            {{$index+1}} - {{month}}</li>\n" +
    "                        </select>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group cf\" show-errors>\n" +
    "                    <label class=\"col-sm-2 control-label\">{{ 'DATA_EXPIRED_YEAR' | translate }}</label>\n" +
    "                    <div class=\"col-sm-10\">\n" +
    "                        <select class=\"form-control\" ng-model=\"payMedthod.year\" name=\"year\" required>\n" +
    "                            <option ng-repeat=\"year in [] | range:13\" value=\"{{$index + currentYear}}\">\n" +
    "                            {{$index + currentYear}}</li>\n" +
    "                        </select>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group cf\" show-errors>\n" +
    "                    <label for=\"select_origin\" class=\"col-sm-2 control-label\">Name on Credit Card:</label>\n" +
    "                    <!--<div class=\"col-sm-10\">\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"payName\" ng-model=\"payMedthod.payName\" required>\n" +
    "                    </div>-->\n" +
    "                </div>\n" +
    "\n" +
    "                <div class=\"form-group cf\" show-errors>\n" +
    "                     \n" +
    "                <label for=\"select_origin\" class=\"col-sm-2 control-label\">First name</label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"payFirstName\" ng-model=\"payMedthod.payFirstName\" required>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group cf\" show-errors>\n" +
    "                <label for=\"select_origin\" class=\"col-sm-2 control-label\">Last name</label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"payLastName\" ng-model=\"payMedthod.payLastName\" required>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"billing_address\">\n" +
    "            <div class=\"flightname\">\n" +
    "                <p>{{ 'DATA_BILLING_ADDRESS' | translate }}</p>\n" +
    "            </div>\n" +
    "            <div class=\"form-group checkbox accept_payment\">\n" +
    "                <label data-placement=\"bottom-left\" data-type=\"info\" data-animation=\"am-fade-and-scale\" bs-tooltip=\"tooltip\">\n" +
    "                    <input type=\"checkbox\" name=\"checked_payment\" ng-model=\"tooltip.checked\" ng-init='tooltip.checked=false' ng-click='doIfChecked(tooltip.checked)'>{{ 'DATA_USE_PRIMARY_PAX' | translate }}\n" +
    "                </label>\n" +
    "            </div>\n" +
    "            <div class=\"form-group cf\" show-errors>\n" +
    "                <label for=\"select_origin\" class=\"col-sm-2 control-label\">First name</label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"billingFirstName\" ng-model=\"payBilling.billingFirstName\" required>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group cf\" show-errors>\n" +
    "                <label for=\"select_origin\" class=\"col-sm-2 control-label\">Last name</label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"billingLastName\" ng-model=\"payBilling.billingLastName\" required>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group cf\" show-errors>\n" +
    "                <label class=\"col-sm-2 control-label\">{{ 'DATA_ADDRESS' | translate }}</label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"billingAddress\" ng-model=\"payBilling.billingAddress\" required>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group cf\" show-errors>\n" +
    "                <label for=\"select_origin\" class=\"col-sm-2 control-label\">{{ 'DATA_COUNTRY' | translate }}</label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <select class=\"form-control\" ng-change=\"changeCountry()\" name=\"select_payment_country_billing\" ng-model=\"payBilling.billingSelectCountry\" ng-options=\"country.Code as country.Name for country in countryResult\" required>\n" +
    "                        <option value=\"\">{{ 'DATA_SELECT' | translate }}</option>\n" +
    "                    </select>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group cf\" show-errors ng-if=\"isProvince\">\n" +
    "                <label class=\"col-sm-2 control-label\">{{ 'DATA_TOWN_CITY' | translate }}</label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <select class=\"form-control\" name=\"billingCity\" ng-model=\"payBilling.billingCity\" required ng-options=\"province.Code as province.Name for province in provinceResult\" required>\n" +
    "                        <option value=\"\">{{ 'DATA_SELECT' | translate }}</option>\n" +
    "                    </select>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group cf\" show-errors>\n" +
    "                <label class=\"col-sm-2 control-label\">{{ 'DATA_MOBILE' | translate }}</label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <input type=\"tel\" class=\"form-control\" name=\"billingMobile\" ng-model=\"payBilling.billingMobile\" pattern=\"[0-9-+() ]+\" title=\"Phone number invalid\" required>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"purchaser_infomation\">\n" +
    "            <div class=\"flightname\">\n" +
    "                <p>{{ 'DATA_PURCHASER_INFO' | translate }}</p>\n" +
    "            </div>\n" +
    "            <div class=\"form-group cf\" show-errors>\n" +
    "                <label class=\"col-sm-2 control-label\">{{ 'DATA_EMAIL_ADDRESS' | translate }}</label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <input type=\"email\" class=\"form-control\" name=\"purchaserEmail\" ng-model=\"payPurchaser.purchaserEmail\" required>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group cf\" show-errors>\n" +
    "                <label class=\"col-sm-2 control-label\">{{ 'DATA_PHONE_NUMBER' | translate }}</label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <input type=\"tel\" class=\"form-control\" name=\"purchaserPhone\" ng-model=\"payPurchaser.purchaserPhone\" pattern=\"[0-9-+() ]+\" title=\"Phone number invalid\" required>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group checkbox accept_payment\" show-errors>\n" +
    "                <label data-placement=\"bottom-left\" data-type=\"info\" data-animation=\"am-fade-and-scale\" bs-tooltip=\"tooltip\">\n" +
    "                    <input type=\"checkbox\" name=\"checked_payment\" ng-model=\"tooltip.checked\" required>\n" +
    "                    {{ 'DATA_READ_UNDERSTOOD_ACCEPTED' | translate }} <a ng-click=\"termsConditionsHelp()\">{{ 'DATA_TERMS_CONDITIONS' | translate }}</a> {{ 'DATA_OF_IAS' | translate }} ...\n" +
    "                </label>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"button_next_step\">\n" +
    "        <button ng-click=\"payNow()\">{{ 'BUTTON_PAY_NOW' | translate }}</button>\n" +
    "    </div>\n" +
    "</form>\n" +
    "");
  $templateCache.put("booking/views/step_7.html",
    "<div class=\"step_status\"><span class=\"confirm step7\"></span></div>\n" +
    "<div class=\"booking_finish_infomation\">\n" +
    "    <div class=\"payment_status\">\n" +
    "        <img src=\"app/img/flight_confirm.png\" />\n" +
    "        <p>{{paymentStatus}}</p>\n" +
    "    </div>\n" +
    "    <div class=\"booking_number\">\n" +
    "        <p>{{ 'DATA_BOOKING_NUMBER' | translate }} <span class=\"booking_number_detail\">{{reservationNumber}}</span></p>\n" +
    "    </div>\n" +
    "    <div class=\"booking_total\">\n" +
    "        <p>{{ 'DATA_TOTAL_PAID' | translate }} <span class=\"booking_total_paid\">{{totalAmount | isoCurrency:s1Data.s1CurrencyCode:fractionSize}}</span></p>\n" +
    "    </div>\n" +
    "    <div class=\"booking_finish_info\" ng-if=\"s6Data.paymentMethod.paymentMethod == 'PLHUNGNGUYEN'\">\n" +
    "        {{ 'DATA_MESSAGE_CHECK_YOUR_EMAIL' | translate }}\n" +
    "    </div>\n" +
    "    <div class=\"booking_finish_info\" ng-if=\"s6Data.paymentMethod.paymentMethod == 'PL'\">\n" +
    "        {{ 'DATA_MESSAGE_ON_HOLD_SUCCESSFUL_1' | translate }} <span>{{onHoldHours}} {{ 'DATA_HOURS' | translate }}</span>. \n" +
    "        {{ 'DATA_MESSAGE_LEFT_UNPAID' | translate }} <span>{{cancelledDate | date:'EEEE, MMMM dd, yyyy - HH:mm (Z)'}}</span>. {{ 'DATA_MESSAGE_ON_HOLD_SUCCESSFUL_3' | translate }}.\n" +
    "    </div>\n" +
    "    <div class=\"booking_finish_info\">\n" +
    "        {{ 'DATA_THANK_YOU' | translate }}\n" +
    "    </div>\n" +
    "</div>\n" +
    "</div>\n" +
    "<div class=\"button_next_step\">\n" +
    "    <button ng-click=\"toHome()\">{{ 'BUTTON_HOME' | translate }}</button>\n" +
    "</div>\n" +
    "");
  $templateCache.put("checkin/views/passenger.html",
    "<div class=\"modal\" tabindex=\"-1\" role=\"dialog\">\n" +
    "    <div class=\"modal-dialog\">\n" +
    "        <div class=\"modal-content\">\n" +
    "            <div class=\"modal-header\">\n" +
    "                <h4 class=\"modal-title\">Seat selection</h4>\n" +
    "            </div>\n" +
    "            <div class=\"modal-body\">\n" +
    "                <div class=\"wrap_infomation_checkin\">\n" +
    "                    <div class=\"infomation_passenger checkin_passenger\">\n" +
    "                        <div class=\"passenger\" ng-repeat=\"passenger in checkinStep3Data\">\n" +
    "                            <p class=\"passenger_name\" ng-click=\"choosePassenger(passenger)\">\n" +
    "                                {{passenger.Firstname ? passenger.Firstname + ' ' + passenger.Lastname : 'Passenger ' + ($index + 1) }}\n" +
    "                                <span class=\"pass_infant\" ng-if=\"passenger.Infants.length > 0\">+ Infant</span>\n" +
    "                                <span class=\"has-seat\" ng-if=\"passenger.RowNumber && passenger.SeatNumber\">{{passenger.RowNumber + passenger.SeatNumber}}</span>\n" +
    "                            </p>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"modal-footer\">\n" +
    "                <button type=\"button\" class=\"btn btn-default\" ng-click=\"acceptSeat()\">Accept</button>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
  $templateCache.put("checkin/views/step_1.html",
    "<div class=\"checkin_step_status\"><span class=\"step step1\"></span></div>\n" +
    "<form name=\"step1Form\">\n" +
    "    <div class=\"wrap_infomation_checkin\">\n" +
    "        <div class=\"flightname\">\n" +
    "            <p>\n" +
    "                {{ 'DATA_CHECK_IN_ONLINE_NOT_AVAILABLE' | translate }}:\n" +
    "            </p>\n" +
    "        </div>\n" +
    "        <div class=\"infomation\">\n" +
    "            <p>{{ 'DATA_PASSENGER_INFANT' | translate }}.</p>\n" +
    "            <p>{{ 'DATA_PASSENGER_SPECIAL_NEEDS' | translate }}.</p>\n" +
    "            <p>{{ 'DATA_PASSENGER_BOOKING' | translate }}.</p>\n" +
    "        </div>\n" +
    "        <div class=\"note_infomation\">\n" +
    "            <p>{{ 'DATA_CHECK_IN_ONLINE_AVAILABLE' | translate }} <span>48 {{ 'DATA_HOURS' | translate }}</span> {{ 'DATA_CHECK_IN_ONLINE_AVAILABLE_1' | translate }} <span>4 {{ 'DATA_HOURS' | translate }}</span> {{ 'DATA_CHECK_IN_ONLINE_AVAILABLE_2' | translate }}.</p>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div>\n" +
    "        <div class=\"flightname\">\n" +
    "            <p>\n" +
    "                {{ 'DATA_RESERVATION_INFO' | translate }}\n" +
    "            </p>\n" +
    "        </div>\n" +
    "        <div>\n" +
    "            <div class=\"select_reservation_code\">\n" +
    "                <div class=\"form-group cf\"  show-errors>\n" +
    "                    <label class=\"col-sm-2 control-label\">{{ 'DATA_RESERVATION_CODE' | translate }}</label>\n" +
    "                    <div class=\"col-sm-10\">\n" +
    "                        <input type=\"tel\" \n" +
    "                            class=\"form-control\" \n" +
    "                            name=\"reservationCodeValue\" \n" +
    "                            ng-model=\"reservationCodeValue\" \n" +
    "                            data-ng-pattern=\"/^[0-9]+$/\"\n" +
    "                            data-ng-minlength=\"1\"\n" +
    "                            maxlength=\"9\"\n" +
    "                            ng-maxlength=\"9\"\n" +
    "                            numbers-only\n" +
    "                            required>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"select_firstname\">\n" +
    "                <div class=\"form-group cf\" show-errors>\n" +
    "                    <label class=\"col-sm-2 control-label\">{{ 'DATA_FIRST_NAME' | translate }}</label>\n" +
    "                    <div class=\"col-sm-10\">\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"firstNameValue\" ng-model=\"firstNameValue\" required>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"select_lastname\">\n" +
    "                <div class=\"form-group cf\" show-errors>\n" +
    "                    <label class=\"col-sm-2 control-label\">{{ 'DATA_LAST_NAME' | translate }}</label>\n" +
    "                    <div class=\"col-sm-10\">\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"lastNameValue\" ng-model=\"lastNameValue\" required>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"button_next_step\">\n" +
    "        <button ng-click=\"checkInClick()\">{{ 'BUTTON_CHECK_IN' | translate }}</button>\n" +
    "    </div>\n" +
    "</form>\n" +
    "");
  $templateCache.put("checkin/views/step_2.html",
    "<div class=\"checkin_step_status\" ng-if=\"vaildResevation  == true\"><span class=\"step step2\"></span></div>\n" +
    "<form name=\"step2Form\">\n" +
    "    <div class=\"infomation_booking\" ng-if=\"vaildResevation  == true\">\n" +
    "        <div ng-repeat=\"s2Leg in s2Legs\">\n" +
    "            <div class=\"flightname\">\n" +
    "                <p>{{ 'DATA_FLIGHT' | translate }} {{$index + 1}}: {{s2Leg.DepartureAirportName}} to {{s2Leg.ArrivalAirportName}} - {{s2Leg.Segments[0].DepartureLocal  | date:'longDate'}}</p>\n" +
    "            </div>\n" +
    "            <div class=\"valuefare\">\n" +
    "                <div class=\"flight_fly\">\n" +
    "                    <div class=\"expan_ticked_info\">\n" +
    "                        <div class=\"detail_flight\">\n" +
    "                            <div class=\"indirect_icon_ticked\" ng-class=\"{'direct_icon_ticked': s2Leg.Segments.length == 1}\"></div>\n" +
    "                            <div class=\"transitgroup cf\">\n" +
    "                                <div class=\"transit cf\" ng-repeat=\"segmentOption in s2Leg.SegmentsTemp\">\n" +
    "                                    <div ng-show=\"segmentOption.checkAirCraft == false\" class=\"segment_view cf\">\n" +
    "                                        <div class=\"time_to_time\">\n" +
    "                                            <p class=\"timesheet\">\n" +
    "                                                <span class=\"time_from\">{{segmentOption.DepartureLocal | date:'HH:mm'}}</span>\n" +
    "                                                <span class=\"fly_to_icon\"></span>\n" +
    "                                                <span class=\"time_to\">{{segmentOption.ArrivalLocal | date:'HH:mm'}}</span>\n" +
    "                                            </p>\n" +
    "                                            <p class=\"flight_name\">\n" +
    "                                                <span class=\"name\">{{segmentOption.Flight}} - {{segmentOption.AircraftName}}</span>\n" +
    "                                            </p>\n" +
    "                                        </div>\n" +
    "                                        <div class=\"airport_status\">\n" +
    "                                            <p class=\"airport_from\" ng-click=\"loadAirportDetail(segmentOption.DepartureAirportCode, segmentOption.DepartureAirportName)\">{{segmentOption.DepartureAirportCode}}</p>\n" +
    "                                            <p class=\"airport_to\" ng-click=\"loadAirportDetail(segmentOption.ArrivalAirportCode, segmentOption.ArrivalAirportName)\">{{segmentOption.ArrivalAirportCode}}</p>\n" +
    "                                        </div>\n" +
    "                                    </div>\n" +
    "                                    <div ng-show=\"segmentOption.checkAirCraft\" class=\"segment_stop cf\">\n" +
    "                                        <div>\n" +
    "                                            <span class=\"stop_lable\">{{ 'DATA_STOP' | translate }} {{segmentOption.countStop}}: </span>\n" +
    "                                            <span class=\"stop_value airport_from\" ng-click=\"loadAirportDetail(segmentOption.DepartureAirportCode, segmentOption.DepartureAirportName)\">{{segmentOption.DepartureAirportCode}}</span>\n" +
    "                                            <span class=\"stop_lable\">- {{ 'DATA_CONNECTION' | translate }}: </span>\n" +
    "                                            <span class=\"stop_value\">{{segmentOption.TimeTransit }}</span>\n" +
    "                                        </div>\n" +
    "                                    </div>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <div class=\"slecttion_package_price check_in_step2_view\">\n" +
    "                            <div class=\"pre_price {{s2Leg.StatusClass}}\">\n" +
    "                                <span class=\"select_tion check\"></span>\n" +
    "                                <span class=\"package_name\" ng-click=\"loadFare(s2Leg.Segments[0].PaxList[0].FareIdent)\" ng-if=\"s2Leg.Segments[0].PaxList[0].FareIdent != ''\">{{s2Leg.Segments[0].PaxList[0].FareIdent}}</span>\n" +
    "                                <span class=\"package_name\" ng-if=\"s2Leg.Segments[0].PaxList[0].FareIdent == '' \" style=\"text-decoration: none;\">-</span>\n" +
    "                                <span class=\"package_name package_name_status _{{s2Leg.StatusClass}}\">{{s2Leg.Status}}</span>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <p class=\"with_infant_no_check_in\" ng-if=\"s2Leg.countNoInfantNoCheckin == true\">{{ 'DATA_MESSAGE_PASSENGER_WITH_INFANT_NOT_CHECKIN_ONLINE' | translate }} </p>\n" +
    "                </div>\n" +
    "\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <p class=\"not_payment_detect\" ng-if=\"checkinStep2DataTotal.ResStatus.Balance > 0\">{{ 'DATA_MESSAGE_RESERVATION_BALANCE_OWING' | translate }} {{checkinStep2DataTotal.ResStatus.Balance | isoCurrency:checkinStep2DataTotal.CurrencyInfo.Abbreviation:fractionSize}}. {{ 'DATA_MESSAGE_UNABLE_CHECKIN_BALANCE_PAID' | translate }}.</p>\n" +
    "    <div class=\"check_in_no_flight\" ng-if=\"vaildResevation  == false\">\n" +
    "        <div class=\"no_flight\"></div>\n" +
    "        <h2>{{ 'DATA_MESSAGE_NOT_FLIGHT' | translate }}</h2>\n" +
    "        <p>{{ 'DATA_MESSAGE_CHECK_RESERVATION' | translate }}</p>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"button_next_step\" ng-if=\"(isCheckin == true && checkinStep2DataTotal.ResStatus.Balance <= 0) && (checkcountNoInfantNoCheckinInLeg(s2Legs) == false)\">\n" +
    "        <button ng-click=\"toPassengers()\">{{ 'BUTTON_COUNTINUE' | translate }}</button>\n" +
    "    </div>\n" +
    "    <div class=\"button_next_step\" ng-if=\"checkcountNoInfantNoCheckinInLeg(s2Legs) || vaildResevation == false || isCheckin == false || checkinStep2DataTotal.ResStatus.Balance > 0\">\n" +
    "        <button ng-click=\"toback()\">{{ 'BUTTON_BACK' | translate }}</button>\n" +
    "    </div>\n" +
    "</form>\n" +
    "");
  $templateCache.put("checkin/views/step_3.html",
    "<div class=\"checkin_step_status\"><span class=\"step step3\"></span></div>\n" +
    "<form name=\"step1Form\">\n" +
    "    <div class=\"wrap_infomation_checkin\">\n" +
    "        <div class=\"flightname\">\n" +
    "            <p>{{ 'DATA_FLIGHT' | translate }} {{checkinStep2.FlightNumberofLeg}}: {{checkinStep2.Segments[0].DepartureAirportName}} to {{checkinStep2.Segments[0].ArrivalAirportName}} - {{checkinStep2.Segments[0].DepartureLocal  | date:'longDate'}}</p>\n" +
    "        </div>\n" +
    "        <div class=\"infomation_passenger checkin_passenger\">\n" +
    "            <div class=\"passenger\" ng-repeat=\"passenger in checkinStep2.Passengers\">\n" +
    "                <p class=\"passenger_name\">\n" +
    "                    {{passenger.Firstname ? passenger.Firstname + ' ' + passenger.Lastname : 'Passenger ' + ($index + 1) }}\n" +
    "                    <span class=\"pass_infant\" ng-if=\"passenger.Infants.length > 0\">+ Infant</span>\n" +
    "                    <span class=\"has-completed\" ng-if=\"passenger.Infants.length == 0\"></span>\n" +
    "                    <span ng-repeat=\"pax in checkinStep2.Segments[0].PaxList\">\n" +
    "                        <span class=\"has-seat\" ng-if=\"(pax.PaxGroupID == passenger.PaxGroupID) && (pax.RowNumber && pax.SeatNumber)\">\n" +
    "                            {{pax.RowNumber + pax.SeatNumber}}\n" +
    "                        </span>\n" +
    "                    </span>\n" +
    "                </p>\n" +
    "                <p class=\"info_with_infant\" ng-if=\"passenger.Infants.length > 0\">{{ 'DATA_MESSAGE_WITH_INFANT_CHECKIN_AIRPORT' | translate }}</p>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"button_next_step\"> \n" +
    "        <button ng-click=\"selectSeat()\" ng-if=\"isCheckin == true && checkinStep2.countNoInfantNoCheckin == false\">{{ 'BUTTON_CHECK_IN' | translate }}</button>\n" +
    "        <button ng-click=\"toHome()\" ng-if=\"isCheckin == false || checkinStep2.countNoInfantNoCheckin == true\">{{ 'BUTTON_HOME' | translate }}</button>\n" +
    "    </div>\n" +
    "</form>\n" +
    "");
  $templateCache.put("checkin/views/step_3_1.html",
    "<div class=\"checkin_step_status\"><span class=\"step step3\"></span></div>\n" +
    "<form name=\"step31Form\">\n" +
    "    <div class=\"wrap_infomation_checkin\">\n" +
    "        <div class=\"flightname\">\n" +
    "            <p>{{ 'DATA_GUESTS_SEATED' | translate }}:</p>\n" +
    "        </div>\n" +
    "        <div class=\"infomation\">\n" +
    "            <p>{{ 'DATA_PHYSICALLY_MENTALLY' | translate }}</p>\n" +
    "            <p>{{ 'DATA_CAPABLE' | translate }}</p>\n" +
    "            <p>{{ 'DATA_MESSAGE_NOT_STAGE_OF_PREGNACY' | translate }}</p>\n" +
    "            <p>{{ 'DATA_MESSAGE_NOT_TRAVELLING_INFANT_NOT_PURCHASE' | translate }}</p>\n" +
    "            <p>{{ 'DATA_SAFETY RESONS' | translate }}</p>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"wrap_infomation_checkin\">\n" +
    "        <div class=\"flightname\">\n" +
    "            <p>{{ 'DATA_LEGEND' | translate }}</p>\n" +
    "        </div>\n" +
    "        <div class=\"infomation infomation_help_seat\">\n" +
    "            <div>\n" +
    "                <p class=\"available\">{{ 'DATA_SEAT_AVAILABLE' | translate }}.</p>\n" +
    "                <p class=\"unAvailable\">{{ 'DATA_SEAT_UNAVAILABLE' | translate }}.</p>\n" +
    "                <p class=\"travelingparty\">{{ 'DATA_SEAT_TRAVELING_PARTY' | translate }}.</p>\n" +
    "                <p class=\"occupied\">{{ 'DATA_SEAT_OCCUPIED' | translate }}.</p>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"wrap_infomation_checkin wrap_seatmap\">\n" +
    "        <table>\n" +
    "            <thead>\n" +
    "                <td></td>\n" +
    "                <td ng-repeat=\"seat in seatmap[0]\">{{seat.SeatNumber}}</td>\n" +
    "                <td></td>\n" +
    "            </thead>\n" +
    "            <tbody>\n" +
    "                <tr class=\"seat_row\" ng-repeat=\"seatrow in seatmap\">\n" +
    "                    <td>{{$index + 1}}</td>\n" +
    "                    <td class=\"td_seat\" ng-repeat=\"seat in seatrow\" ng-click=\"chooseSeat(seat)\">\n" +
    "                        <span class=\"seat_type_{{seat.Availability}}\"\n" +
    "                            ng-class=\"{'is_selected': seat.isSelected}\"></span>\n" +
    "                    </td>\n" +
    "                    <td>{{$index + 1}}</td>\n" +
    "                </tr>\n" +
    "            </tbody>\n" +
    "            <tfoot>\n" +
    "                <td></td>\n" +
    "                <td ng-repeat=\"seat in seatmap[0]\">{{seat.SeatNumber}}</td>\n" +
    "                <td></td>\n" +
    "            </tfoot>\n" +
    "\n" +
    "        </table>\n" +
    "    </div>\n" +
    "    <div class=\"button_next_step\">\n" +
    "        <button ng-click=\"selectSeat()\">{{ 'BUTTON_CHECK_IN' | translate }}</button>\n" +
    "    </div>\n" +
    "</form>\n" +
    "");
  $templateCache.put("checkin/views/step_3_2.html",
    "<div class=\"checkin_step_status\"><span class=\"step step3\"></span></div>\n" +
    "<form name=\"step1Form\">\n" +
    "    <div class=\"wrap_infomation_checkin\">\n" +
    "        <div class=\"flightname\">\n" +
    "            <p>{{ 'DATA_FLIGHT' | translate }} {{$index + 1}}: {{checkinStep2.Segments[0].DepartureAirportName}} to {{checkinStep2.Segments[0].ArrivalAirportName}} - {{checkinStep2.Segments[0].DepartureLocal  | date:'longDate'}}</p>\n" +
    "        </div>\n" +
    "        <div class=\"infomation_passenger checkin_passenger\">\n" +
    "            <div class=\"passenger\" ng-repeat=\"passenger in checkinStep2.Passengers\">\n" +
    "                <p class=\"passenger_name\">\n" +
    "                    {{passenger.Firstname ? passenger.Firstname + ' ' + passenger.Lastname : 'Passenger ' + ($index + 1) }}\n" +
    "                    <span class=\"pass_infant\" ng-if=\"passenger.Infants.length > 0\">+ Infant</span>\n" +
    "                    <span class=\"has-completed\" ng-if=\"passenger.Infants.length == 0\"></span>\n" +
    "                    <span class=\"has-seat\" ng-if=\"passenger.RowNumber && passenger.SeatNumber\">{{passenger.RowNumber + passenger.SeatNumber}}</span>\n" +
    "                    \n" +
    "                </p>\n" +
    "                <p class=\"info_with_infant\" ng-if=\"passenger.Infants.length > 0\">{{ 'DATA_MESSAGE_WITH_INFANT_CHECKIN_AIRPORT' | translate }}</p>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"button_next_step\">\n" +
    "        <button ng-click=\"confirmSeat()\">{{ 'BUTTON_CONFIRM' | translate }}</button>\n" +
    "    </div>\n" +
    "</form>\n" +
    "");
  $templateCache.put("checkin/views/step_4.html",
    "<div class=\"checkin_step_status\"><span class=\"step step4\"></span></div>\n" +
    "<form name=\"step1Form\">\n" +
    "    <div class=\"infomation_booking\">\n" +
    "        <div class=\"wrap_infomation_checkin\">\n" +
    "            <div class=\"flightname\">\n" +
    "                <p>{{ 'DATA_FLIGHT' | translate }} {{$index + 1}}: {{DepartureAirportName}} to {{ArrivalAirportName}} - {{checkinStep2.Segments[0].DepartureLocal  | date:'longDate'}}</p>\n" +
    "            </div>\n" +
    "            <div class=\"valuefare\">\n" +
    "                <div class=\"flight_fly\">\n" +
    "                    <div class=\"expan_ticked_info\">\n" +
    "                        <div class=\"detail_flight\">\n" +
    "                            <div class=\"indirect_icon_ticked\" ng-class=\"{'direct_icon_ticked': checkinStep2.Segments.length == 1}\"></div>\n" +
    "                            <div class=\"transitgroup cf\">\n" +
    "                                <div class=\"transit cf\" ng-repeat=\"segmentOption in checkinStep2.SegmentsTemp\">\n" +
    "                                    <div ng-show=\"segmentOption.checkAirCraft == false\" class=\"segment_view cf\">\n" +
    "                                        <div class=\"time_to_time\">\n" +
    "                                            <p class=\"timesheet\">\n" +
    "                                                <span class=\"time_from\">{{segmentOption.DepartureLocal | date:'HH:mm'}}</span>\n" +
    "                                                <span class=\"fly_to_icon\"></span>\n" +
    "                                                <span class=\"time_to\">{{segmentOption.ArrivalLocal | date:'HH:mm'}}</span>\n" +
    "                                            </p>\n" +
    "                                            <p class=\"flight_name\">\n" +
    "                                                <span class=\"name\">{{segmentOption.Flight}} - {{segmentOption.AircraftName}}</span>\n" +
    "                                            </p>\n" +
    "                                        </div>\n" +
    "                                        <div class=\"airport_status\">\n" +
    "                                            <p class=\"airport_from\" ng-click=\"loadAirportDetail(segmentOption.DepartureAirportCode, segmentOption.DepartureAirportName)\">{{segmentOption.DepartureAirportCode}}</p>\n" +
    "                                        <p class=\"airport_to\" ng-click=\"loadAirportDetail(segmentOption.ArrivalAirportCode, segmentOption.ArrivalAirportName)\">{{segmentOption.ArrivalAirportCode}}</p>\n" +
    "                                        </div>\n" +
    "                                    </div>\n" +
    "                                    <div ng-show=\"segmentOption.checkAirCraft\" class=\"segment_stop cf\">\n" +
    "                                        <div>\n" +
    "                                            <span class=\"stop_lable\">{{ 'DATA_STOP' | translate }} {{segmentOption.countStop}}: </span>\n" +
    "                                            <span class=\"stop_value airport_from\" ng-click=\"loadAirportDetail(segmentOption.DepartureAirportCode, segmentOption.DepartureAirportName)\">{{segmentOption.DepartureAirportCode}}</span>\n" +
    "                                            <span class=\"stop_lable\">- {{ 'DATA_CONNECTION' | translate }}: </span>\n" +
    "                                            <span class=\"stop_value\">{{segmentOption.TimeTransit }}</span>\n" +
    "                                        </div>\n" +
    "                                    </div>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <div class=\"slecttion_package_price check_in_step2_view\">\n" +
    "                            <div class=\"pre_price {{checkinStep2.StatusClass}}\">\n" +
    "                                <span class=\"package_name _{{checkinStep2.StatusClass}} readdy_to_flight\">{{ 'DATA_READY_FLIGHT' | translate }}</span>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"infomation_passenger checkin_passenger\">\n" +
    "                <div class=\"passenger\" ng-repeat=\"passenger in checkinStep2.Passengers\">\n" +
    "                    <p class=\"passenger_name\">\n" +
    "                        {{passenger.Firstname ? passenger.Firstname + ' ' + passenger.Lastname : 'Passenger ' + ($index + 1) }}\n" +
    "                    <span class=\"pass_infant\" ng-if=\"passenger.Infants.length > 0\">+ Infant</span>\n" +
    "                        <span class=\"has-completed\" ng-if=\"passenger.Infants.length == 0\"></span>\n" +
    "                        <span class=\"has-seat\" ng-if=\"passenger.RowNumber && passenger.SeatNumber\">{{passenger.RowNumber + passenger.SeatNumber}}</span>\n" +
    "\n" +
    "                    </p>\n" +
    "                    <p class=\"info_with_infant\" ng-if=\"passenger.Infants.length > 0\">{{ 'DATA_MESSAGE_WITH_INFANT_CHECKIN_AIRPORT' | translate }}</p>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"button_next_step\">\n" +
    "        <button ng-click=\"toHome()\">{{ 'BUTTON_FINISH' | translate }}</button>\n" +
    "    </div>\n" +
    "</form>\n" +
    "");
  $templateCache.put("core/views/core.html",
    "<div id=\"page\" data-role=\"page\">\n" +
    "     <div ng-show=\"state=='login'\">\n" +
    "            <div class=\"logo_header logo_header_page\" align=\"center\">\n" +
    "                <div class=\"on_menu_open\" ng-class=\"{'on_menu_open_true': dropdownOpenned}\">\n" +
    "                    <a class=\"onclick_open_menu\" ng-click=\"toogleDropdown()\"></a>\n" +
    "                </div>\n" +
    "\n" +
    "                <div class=\"on_logo\">\n" +
    "                    <h2>{{ title_name }}</h2>\n" +
    "                </div>\n" +
    "\n" +
    "                <div class=\"home_button\">\n" +
    "                    <a class=\"home_button_top_right\" href=\"home\"></a>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"menu-top-open menu\">\n" +
    "                <div class=\"menu-home-container menu-top-open-container\" ng-class=\"{'menu-top-open-container-true': dropdownOpenned}\">\n" +
    "                    <ul id=\"Ul1\" class=\"menu index-menu cf\">\n" +
    "                        <li class=\"iconfly\"><a href=\"bookingflight\" ng-click=\"toogleDropdown()\">{{ 'DATA_BOOKING_FLIGHT' | translate }}</a></li>\n" +
    "                        <!--<li class=\"iconcheckin\"><a href=\"checkin\" ng-click=\"toogleDropdown()\">{{ 'BUTTON_CHECK_IN' | translate }}</a></li>-->\n" +
    "                        <li  ng-if=\"!isLogin\" class=\"iconmember\"><a href=\"user\" ng-click=\"toogleDropdown()\">{{ 'DATA_MEMBER' | translate }}</a></li>\n" +
    "                         <li  ng-if=\"isLogin\" class=\"iconmember\"><a href=\"user\" ng-click=\"toogleDropdown()\">{{ 'DATA_MEMBER' | translate }}</a></li>\n" +
    "                        <!--<li class=\"iconstatus\"><a href=\"flightstatus\" ng-click=\"toogleDropdown()\">{{ 'DATA_FLIGHT_STATUS' | translate }}</a></li>-->\n" +
    "                        <li class=\"iconinformation\"><a href=\"information\" ng-click=\"toogleDropdown()\">{{ 'DATA_INFO' | translate }}</a></li>\n" +
    "                        <li class=\"iconlang\"><a href=\"contactus\" ng-click=\"toogleDropdown()\">{{ 'DATA_CONTACT_IAS' | translate }}</a></li>\n" +
    "                    </ul>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    <header id=\"masthead\" class=\"site-header\" role=\"banner\">\n" +
    "        <div ng-show=\"state=='home' || state=='login'\">\n" +
    "            <div class=\"logo_header home\" align=\"center\">\n" +
    "                <!--<div class=\"on_menu_open\">\n" +
    "                    <a class=\"onclick_open_menu\" href=\"#\">\n" +
    "                        <img src=\"../app/img/menu_icon.png\">\n" +
    "                    </a>\n" +
    "                </div>-->\n" +
    "                <div class=\"on_logo\">\n" +
    "                    <a href=\"home\">\n" +
    "                        <!--<img src=\"app/img/logo.png\" />-->\n" +
    "                        <!--<img src=\"app/img/viaair/via_air_logo.png\"/>-->\n" +
    "                        <!--<img src=\"app/img/cma/cma_logo.png\"/>-->\n" +
    "                        <img src=\"app/img/logo.png\"   width=\"309px\" />\n" +
    "                    </a>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"sliders\" ng-controller=\"SliderController\">\n" +
    "                <!--<div start-slider></div>-->\n" +
    "                <img src=\"{{pictures}}\" style=\"width: 100%\" alt=\"GMS\" />\n" +
    "            </div>\n" +
    "            <script>\n" +
    "                \n" +
    "            </script>\n" +
    "        </div>\n" +
    "        <div ng-hide=\"state=='home' || state=='login'\">\n" +
    "            <div class=\"logo_header logo_header_page\" align=\"center\">\n" +
    "                <div class=\"on_menu_open\" ng-class=\"{'on_menu_open_true': dropdownOpenned}\">\n" +
    "                    <a class=\"onclick_open_menu\" ng-click=\"toogleDropdown()\"></a>\n" +
    "                </div>\n" +
    "\n" +
    "                <div class=\"on_logo\">\n" +
    "                    <h2>{{ title_name }}</h2>\n" +
    "                </div>\n" +
    "\n" +
    "                <div class=\"home_button\">\n" +
    "                    <a class=\"home_button_top_right\" href=\"home\"></a>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"menu-top-open menu\">\n" +
    "                <div class=\"menu-home-container menu-top-open-container\" ng-class=\"{'menu-top-open-container-true': dropdownOpenned}\">\n" +
    "                    <ul id=\"menu-home\" class=\"menu index-menu cf\">\n" +
    "                        <li class=\"iconfly\"><a href=\"bookingflight\" ng-click=\"toogleDropdown()\">{{ 'DATA_BOOKING_FLIGHT' | translate }}</a></li>\n" +
    "                        <!--<li class=\"iconcheckin\"><a href=\"checkin\" ng-click=\"toogleDropdown()\">{{ 'BUTTON_CHECK_IN' | translate }}</a></li>-->\n" +
    "                        <li class=\"iconmember\"><a href=\"user\" ng-click=\"toogleDropdown()\">{{ 'DATA_MEMBER' | translate }}</a></li>\n" +
    "                        <!--<li class=\"iconstatus\"><a href=\"flightstatus\" ng-click=\"toogleDropdown()\">{{ 'DATA_FLIGHT_STATUS' | translate }}</a></li>-->\n" +
    "                        <li class=\"iconinformation\"><a href=\"information\" ng-click=\"toogleDropdown()\">{{ 'DATA_INFO' | translate }}</a></li>\n" +
    "                        <li class=\"iconlang\"><a href=\"contactus\" ng-click=\"toogleDropdown()\">{{ 'DATA_CONTACT_IAS' | translate }}</a></li>\n" +
    "                    </ul>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </header>\n" +
    "\n" +
    "    <!-- #begin main -->\n" +
    "    <div id=\"main\" class=\"site-main\">\n" +
    "        <div id=\"primary\" class=\"content-area\">\n" +
    "            <div ui-view='main'></div>\n" +
    "        </div>\n" +
    "        <!-- #primary -->\n" +
    "    </div>\n" +
    "    <!-- #main -->\n" +
    "    <!-- #begin colophon footer -->\n" +
    "    <footer id=\"colophon\" class=\"site-footer\" role=\"contentinfo\">\n" +
    "        <div class=\"menu-menufooter-container\">\n" +
    "            <ul id=\"menu-menufooter\" class=\"menu fdiv footer-menu\">\n" +
    "                <li><a href=\"http://www.intelisysaviation.com/\">Full Site<span>|</span></a></li>\n" +
    "                <!--<li><a href=\"policy\">Policy<span>|</span></a></li>-->\n" +
    "                <li><a href=\"contactus\">Contact<span></span></a></li>\n" +
    "                <!--<li>v1.4.1</li>-->\n" +
    "            </ul>\n" +
    "        </div>\n" +
    "    </footer>\n" +
    "    <!-- #colophon -->\n" +
    "</div>\n" +
    "");
  $templateCache.put("core/views/footer.html",
    "Footer");
  $templateCache.put("core/views/header.html",
    "Header");
  $templateCache.put("core/views/loading.html",
    "<div class=\"modal\" tabindex=\"-1\" role=\"dialog\">\n" +
    "    <div class=\"modal-dialog\">\n" +
    "        <div class=\"modal-content\">\n" +
    "            <div class=\"modal-header\">\n" +
    "                <h4 class=\"modal-title\">{{ 'DATA_LOADING_DATA' | translate }}</h4>\n" +
    "            </div>\n" +
    "            <div class=\"modal-body\">\n" +
    "                <div class=\"wrap_loading\">\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            \n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
  $templateCache.put("flightstatus/views/step_1.html",
    "<form name=\"step1Form\">\n" +
    "    <div>\n" +
    "        <div class=\"flightname\">\n" +
    "            <p>{{ 'DATA_RESERVATION_INFO' | translate }}</p>\n" +
    "        </div>\n" +
    "        <div>\n" +
    "            <div class=\"select_reservation_code\">\n" +
    "                <div class=\"form-group cf\"  show-errors>\n" +
    "                    <label class=\"col-sm-2 control-label\">{{ 'DATA_RESERVATION_CODE' | translate }}</label>\n" +
    "                    <div class=\"col-sm-10\">\n" +
    "                        <input type=\"tel\" class=\"form-control\" name=\"reservationCodeValue\" ng-model=\"reservationCodeValue\" required>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"select_firstname\">\n" +
    "                <div class=\"form-group cf\" show-errors>\n" +
    "                    <label class=\"col-sm-2 control-label\">{{ 'DATA_FIRST_NAME' | translate }}</label>\n" +
    "                    <div class=\"col-sm-10\">\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"firstNameValue\" ng-model=\"firstNameValue\" required>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"select_lastname\">\n" +
    "                <div class=\"form-group cf\" show-errors>\n" +
    "                    <label class=\"col-sm-2 control-label\">{{ 'DATA_LAST_NAME' | translate }}</label>\n" +
    "                    <div class=\"col-sm-10\">\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"lastNameValue\" ng-model=\"lastNameValue\" required>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"button_next_step\">\n" +
    "        <button ng-click=\"flightStatus()\">{{ 'BUTTON_CHECK_STATUS' | translate }}</button>\n" +
    "    </div>\n" +
    "</form>\n" +
    "");
  $templateCache.put("flightstatus/views/step_2.html",
    "<form name=\"step2Form\">\n" +
    "    <div class=\"infomation_booking\" ng-if=\"vaildResevation  == true\">\n" +
    "        <div>\n" +
    "            <div class=\"flightname\">\n" +
    "                <p>{{ 'DATA_RESERVATION_INFO' | translate }}</p>\n" +
    "            </div>\n" +
    "            <div class=\"flightstatus_passenger_info\">\n" +
    "                <p>{{ 'DATA_RESERVATION_NO' | translate }}.<span>{{reservationInvoiceResult.ReservationNumber}}</span></p>\n" +
    "                <p>{{ 'DATA_CONTACT_NAME' | translate }}<span>{{reservationInvoiceResult.ContactInfo.Name}}</span></p>\n" +
    "                <p>{{ 'DATA_EMAIL' | translate }}<span>{{reservationInvoiceResult.ContactInfo.Email}}</span></p>\n" +
    "                <p>{{ 'DATA_PHONE_NO' | translate }}<span>{{reservationInvoiceResult.ContactInfo.Telephone}}</span></p>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div ng-repeat=\"s2Leg in s2Legs\">\n" +
    "            <div class=\"flightname\">\n" +
    "                <p>{{ 'DATA_FLIGHT' | translate }} {{$index + 1}}: {{s2Leg.DepartureAirportName}} {{ 'DATA_TO' | translate }} {{s2Leg.ArrivalAirportName}} - {{s2Leg.Segments[0].DepartureLocal  | date:'longDate'}}</p>\n" +
    "            </div>\n" +
    "            <div class=\"valuefare\">\n" +
    "                <div class=\"flight_fly\">\n" +
    "                    <div class=\"expan_ticked_info\">\n" +
    "                        <div class=\"detail_flight\">\n" +
    "                            <div class=\"indirect_icon_ticked\" ng-class=\"{'direct_icon_ticked': s2Leg.Segments.length == 1}\"></div>\n" +
    "                            <div class=\"transitgroup cf\">\n" +
    "                                <div class=\"transit cf\" ng-repeat=\"segmentOption in s2Leg.SegmentsTemp\">\n" +
    "                                    <div ng-show=\"segmentOption.checkAirCraft == false\" class=\"segment_view cf\">\n" +
    "                                        <div class=\"time_to_time\">\n" +
    "                                            <p class=\"timesheet\">\n" +
    "                                                <span class=\"time_from\">{{segmentOption.DepartureLocal | date:'HH:mm'}}</span>\n" +
    "                                                <span class=\"fly_to_icon\"></span>\n" +
    "                                                <span class=\"time_to\">{{segmentOption.ArrivalLocal | date:'HH:mm'}}</span>\n" +
    "                                            </p>\n" +
    "                                            <p class=\"flight_name\">\n" +
    "                                                <span class=\"name\">{{segmentOption.Flight}} - {{segmentOption.AircraftName}}</span>\n" +
    "                                            </p>\n" +
    "                                        </div>\n" +
    "                                        <div class=\"airport_status\">\n" +
    "                                            <p class=\"airport_from\" ng-click=\"loadAirportDetail(segmentOption.DepartureAirportCode, segmentOption.DepartureAirportName)\">{{segmentOption.DepartureAirportCode}}</p>\n" +
    "                                        <p class=\"airport_to\" ng-click=\"loadAirportDetail(segmentOption.ArrivalAirportCode, segmentOption.ArrivalAirportName)\">{{segmentOption.ArrivalAirportCode}}</p>\n" +
    "                                        </div>\n" +
    "                                    </div>\n" +
    "                                    <div ng-show=\"segmentOption.checkAirCraft\" class=\"segment_stop cf\">\n" +
    "                                        <div>\n" +
    "                                            <span class=\"stop_lable\">{{ 'DATA_STOP' | translate }} {{segmentOption.countStop}}: </span>\n" +
    "                                            <span class=\"stop_value airport_from\" ng-click=\"loadAirportDetail(segmentOption.DepartureAirportCode, segmentOption.DepartureAirportName)\">{{segmentOption.DepartureAirportCode}}</span>\n" +
    "                                            <span class=\"stop_lable\">- {{ 'DATA_CONNECTION' | translate }}: </span>\n" +
    "                                            <span class=\"stop_value\">{{segmentOption.TimeTransit }}</span>\n" +
    "                                        </div>\n" +
    "                                    </div>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <div class=\"slecttion_package_price check_in_step2_view\">\n" +
    "                            <div class=\"pre_price {{s2Leg.StatusClass}}\">\n" +
    "                                <span class=\"package_name package_name_status  _{{s2Leg.StatusClass}}\">{{s2Leg.Status}}</span>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <p class=\"with_infant_no_check_in\" ng-if=\"s2Leg.countNoInfantNoCheckin == true\">{{ 'DATA_MESSAGE_PASSENGER_WITH_INFANT_NOT_CHECKIN_ONLINE' | translate }}</p>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <p class=\"not_payment_detect\" ng-if=\"checkinStep2DataTotal.ResStatus.Balance > 0\">{{ 'DATA_MESSAGE_RESERVATION_BALANCE_OWING' | translate }} {{checkinStep2DataTotal.ResStatus.Balance | isoCurrency:checkinStep2DataTotal.CurrencyInfo.Abbreviation:fractionSize}}. {{ 'DATA_MESSAGE_UNABLE_CHECKIN_BALANCE_PAID' | translate }}.</p>\n" +
    "    <div class=\"check_in_no_flight\" ng-if=\"vaildResevation  == false\">\n" +
    "        <div class=\"no_flight\"></div>\n" +
    "        <h2>{{ 'DATA_MESSAGE_NOT_FLIGHT' | translate }}</h2>\n" +
    "        <p>{{ 'DATA_MESSAGE_CHECK_RESERVATION' | translate }}</p>\n" +
    "    </div>\n" +
    "    <div class=\"button_next_step\">\n" +
    "        <button ng-click=\"viewPaxList()\">{{ 'BUTTON_VIEW_SEAT' | translate }}</button>\n" +
    "    </div>\n" +
    "</form>\n" +
    "");
  $templateCache.put("flightstatus/views/step_3.html",
    "<form name=\"step1Form\">\n" +
    "    <div class=\"wrap_infomation_checkin\" ng-repeat=\"leg in flightstatusStep2.Legs\">\n" +
    "        <div class=\"flightname\">\n" +
    "            <p>{{ 'DATA_FLIGHT' | translate }} {{leg.LegNumber}}: {{leg.Segments[0].DepartureAirportName}} {{ 'DATA_TO' | translate }} {{leg.Segments[leg.Segments.length -1].ArrivalAirportName}} - {{leg.Segments[0].DepartureLocal  | date:'longDate'}}</p>\n" +
    "        </div>\n" +
    "        <div class=\"infomation_passenger checkin_passenger\">\n" +
    "            <div class=\"passenger\" ng-repeat=\"passenger in flightstatusStep2.Passengers\">\n" +
    "                <p class=\"passenger_name\">\n" +
    "                    {{passenger.Firstname ? passenger.Firstname + ' ' + passenger.Lastname : 'Passenger ' + ($index + 1) }}\n" +
    "                    <span class=\"pass_infant\" ng-if=\"passenger.Infants.length > 0\">+ {{ 'DATA_BOOK_INFANT' | translate }}</span>\n" +
    "                    <span ng-repeat=\"pax in leg.Segments[0].PaxList\">\n" +
    "                        <span class=\"has-completed\" ng-if=\"(pax.PaxGroupID == passenger.PaxGroupID) && (pax.RowNumber && pax.SeatNumber)\"></span>\n" +
    "                        <span class=\"has-seat\" ng-if=\"(pax.PaxGroupID == passenger.PaxGroupID) && (pax.RowNumber && pax.SeatNumber)\">{{pax.RowNumber + pax.SeatNumber}}\n" +
    "                        </span>\n" +
    "                    </span>\n" +
    "                </p>\n" +
    "                <p class=\"info_with_infant\" ng-if=\"passenger.Infants.length > 0\">{{ 'DATA_MESSAGE_WITH_INFANT_CHECKIN_AIRPORT' | translate }}</p>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"button_next_step\">\n" +
    "        <button ng-click=\"toHome()\">{{ 'BUTTON_SEARCH_OTHER' | translate }}</button>\n" +
    "    </div>\n" +
    "</form>\n" +
    "");
  $templateCache.put("home/views/home.html",
    "<div id=\"content\" class=\"site-content index-content\" role=\"main\">\n" +
    "    <div class=\"menu-home-container\">\n" +
    "        <ul id=\"menu-home\" class=\"menu index-menu cf\">\n" +
    "            <!--<li class=\"iconfly\"><a href=\"bookingflight\">{{ 'DATA_BOOK_FLIGHT' | translate }} </a></li>-->\n" +
    "            <li class=\"iconfly\"><a href=\"bookingflight\">{{ 'DATA_BOOKING_FLIGHT' | translate }} </a></li>\n" +
    "            <li class=\"iconcheckin\"><a href=\"checkin\">{{ 'BUTTON_CHECK_IN' | translate }}</a></li>\n" +
    "            <li ng-if=\"!isLogin\" class=\"iconmember\"><a href=\"login\">{{ 'DATA_MEMBER' | translate }}</a></li>\n" +
    "            <li ng-if=\"isLogin\" class=\"iconmember\"><a href=\"user\">{{ 'DATA_MEMBER' | translate }}</a></li>\n" +
    "            <li class=\"iconstatus\"><a href=\"flightstatus\">{{ 'DATA_FLIGHT_STATUS' | translate }}</a></li>\n" +
    "            <li class=\"iconinformation\"><a href=\"information\">{{ 'DATA_INFO' | translate }}</a></li>\n" +
    "            <li class=\"iconlang\"><a href=\"language\">{{ 'DATA_LANGUAGE' | translate }}</a></li>                                \n" +
    "        </ul>\n" +
    "    </div>\n" +
    "</div> <!-- #content -->\n" +
    "");
  $templateCache.put("information/views/contactus.html",
    "<ui-gmap-google-map center=\"map.center\" zoom=\"map.zoom\" draggable=\"true\" options=\"options\">\n" +
    "        <ui-gmap-marker idkey=\"marker.id\" coords=\"marker.coords\" options=\"marker.options\" click=\"onClick()\" events=\"marker.events\" >\n" +
    "            <ui-gmap-window options=\"windowOptions\" closeClick=\"closeClick()\">\n" +
    "                <div>{{title}}</div>\n" +
    "            </ui-gmap-window>\n" +
    "        </ui-gmap-marker>\n" +
    "    </ui-gmap-google-map>\n" +
    "\n" +
    "<div class=\"content_contact_page content_infomation_page cf\">\n" +
    "    <div class=\"form-group cf\">\n" +
    "        <label for=\"passengerGender\" class=\"col-sm-2 control-label\">{{ 'DATA_PHONE_NO' | translate }}</label>\n" +
    "        <div class=\"col-sm-10\">\n" +
    "            <div ng-bind-html=\"contactPhone\"></div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"form-group cf\">\n" +
    "        <label for=\"passengerGender\" class=\"col-sm-2 control-label\">{{ 'DATA_ADDRESS' | translate }}</label>\n" +
    "        <div class=\"col-sm-10\">\n" +
    "            <div ng-bind-html=\"contactAddress\"></div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"form-group cf\">\n" +
    "        <label for=\"passengerGender\" class=\"col-sm-2 control-label\">{{ 'DATA_CONTACT_FOR' | translate }}</label>\n" +
    "        <div class=\"col-sm-10\">\n" +
    "            <div ng-bind-html=\"contactfor\"></div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div ng-bind-html=\"contactContent\" class=\"content\"></div>\n" +
    "</div>\n" +
    "");
  $templateCache.put("information/views/index.html",
    "<div id=\"content\" class=\"site-content index-content\" role=\"main\">\n" +
    "    <div class=\"menu-information-container\">\n" +
    "        <ul id=\"menu-information\" class=\"menu information-menu cf\">\n" +
    "            <li ng-repeat=\"informationpage in infomationList\">\n" +
    "                <a ng-click=\"choosePage(informationpage.InfoSlug)\">{{informationpage.InfoTitle}}</a></li>       \n" +
    "            <li><a href=\"contactus\">{{ 'DATA_CONTACT_IAS' | translate }}</a></li>                             \n" +
    "        </ul>\n" +
    "    </div>\n" +
    "</div> <!-- #content -->");
  $templateCache.put("information/views/infomationdetail.html",
    "<div class=\"content_infomation_page\">\n" +
    "    <div class=\"flightname\">\n" +
    "        <p>{{titleInfomation}}</p>\n" +
    "    </div>\n" +
    "    <div ng-bind-html=\"contentInfomation\" class=\"content\"></div>\n" +
    "</div>\n" +
    "");
  $templateCache.put("language/views/index.html",
    "<div id=\"content\" class=\"site-content index-content\" role=\"main\">\n" +
    "    <div class=\"menu-information-container\">\n" +
    "        <ul id=\"menu-information\" class=\"menu information-menu cf\">\n" +
    "            <li ng-repeat=\"language in languageList\"><a ng-click=\"chooseLanguage(language.LanguageCode)\">{{language.LanguageName}}</a></li>       \n" +
    "        </ul>\n" +
    "    </div>\n" +
    "</div> <!-- #content -->");
  $templateCache.put("userprofile/views/accountcreated.html",
    "<div class=\"register_finish_infomation\">\n" +
    "    <div class=\"register_detail\">\n" +
    "        <img src=\"app/img/logo.png\" />\n" +
    "        <h1 ng-bind-html=\"headdingWellcom\"></h1>\n" +
    "        <p ng-bind-html=\"headdingContent\"></p>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"register_finish_info\" ng-bind-html=\"contentNote\"></div>\n" +
    "\n" +
    "</div>\n" +
    "</div>\n" +
    "<div class=\"button_next_step\">\n" +
    "    <button ng-click=\"toHome()\">{{ 'BUTTON_HOME' | translate }}</button>\n" +
    "</div>\n" +
    "");
  $templateCache.put("userprofile/views/changepass.html",
    "<form class=\"form-horizontal infomation_passenger\" name=\"registerForm\">\n" +
    "    <div class=\"user_profile\">\n" +
    "        <div class=\"infomation_input_user_profile\">\n" +
    "            <div class=\"flightname\">\n" +
    "                <p>{{ 'DATA_CHANGE_PASSWORD' | translate }}</p>\n" +
    "            </div>\n" +
    "             <div class=\"form-group\" show-errors>\n" +
    "                <label class=\"col-sm-2 control-label\">{{ 'DATA_OLD_PASSWORD' | translate }}</label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <input type=\"password\" class=\"form-control\" placeholder=\"Old Password\" name=\"userOldPassword\" ng-model=\"userOldPassword\" required>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "             <div class=\"form-group\" show-errors>\n" +
    "                <label class=\"col-sm-2 control-label\">{{ 'DATA_NEW_PASSWORD' | translate }}</label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <input type=\"password\" class=\"form-control\" placeholder=\"New Password\" name=\"userPassword\" ng-model=\"userPassword\" required>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group\" show-errors>\n" +
    "                <label class=\"col-sm-2 control-label\">{{ 'DATA_VERIFY_PASSWORD' | translate }}</label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <input type=\"password\" class=\"form-control\" placeholder=\"Verify New Password\" name=\"userVerifyPassword\" ng-model=\"userVerifyPassword\" data-match=\"userPassword\" required>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"button_next_step\">\n" +
    "        <button ng-click=\"submitUpdateProfile()\">{{ 'BUTTON_UPDATE' | translate }}</button>\n" +
    "    </div>\n" +
    "</form>\n" +
    "");
  $templateCache.put("userprofile/views/forgotpass.html",
    "forgot pass");
  $templateCache.put("userprofile/views/homeuser.html",
    "<div id=\"content\" class=\"site-content index-content\" role=\"main\">\n" +
    "    <div class=\"menu-information-container\">\n" +
    "        <ul id=\"menu-information\" class=\"menu information-menu cf\">\n" +
    "            <li><a href=\"updateprofile\">{{ 'DATA_UPDATE_PROFILE' | translate }}</a></li>\n" +
    "            <li><a href=\"bookingflight\">{{ 'DATA_BOOKING_FILGHT' | translate }}</a></li>\n" +
    "            <li><a href=\"reservationhistory\">{{ 'DATA_RESERVATION_HISTORY' | translate }}</a></li>\n" +
    "            <li><a href=\"changepassword\">{{ 'DATA_CHANGE_PASSWORD' | translate }}</a></li>\n" +
    "            <li><a href=\"#\" ng-click=\"logout()\">{{ 'DATA_LOGOUT' | translate }}</a></li>       \n" +
    "        </ul>\n" +
    "    </div>\n" +
    "</div> <!-- #content -->");
  $templateCache.put("userprofile/views/login.html",
    "<form class=\"form-horizontal login_form\" name=\"registerForm\">\n" +
    "    <div class=\"form-group\" show-errors>\n" +
    "        <input type=\"email\" class=\"form-control userName\" placeholder=\"E-mail address\" name=\"userName\" ng-model=\"userName\" required>\n" +
    "    </div>\n" +
    "    <div class=\"form-group\" show-errors>\n" +
    "        <input type=\"password\" class=\"form-control userPassword\" placeholder=\"*****\" name=\"userPassword\" ng-model=\"userPassword\" required>\n" +
    "    </div>\n" +
    "    <div class=\"button_next_step\">\n" +
    "        <button ng-click=\"login()\">{{ 'BUTTON_SIGN_IN' | translate }}</button>\n" +
    "    </div>\n" +
    "    <div class=\"link_wrap_under_button cf\">\n" +
    "        <!--<a class=\"left_link\" href=\"forgotpassword\" >Forgot Password?</a>-->\n" +
    "        <a class=\"right_link\" href=\"register\">{{ 'BUTTON_NEW_HERE_SIGN_UP' | translate }}</a>\n" +
    "        <br />\n" +
    "        \n" +
    "    </div>\n" +
    "</form>\n" +
    "");
  $templateCache.put("userprofile/views/register.html",
    "<form class=\"form-horizontal infomation_passenger\" name=\"registerForm\">\n" +
    "    <div class=\"user_profile\">\n" +
    "        <div class=\"infomation_input_user_profile\">\n" +
    "            <div class=\"flightname\">\n" +
    "                <p>{{ 'DATA_NEW_ACCOUNT' | translate }}</p>\n" +
    "            </div>\n" +
    "            <div class=\"form-group\" show-errors>\n" +
    "                <label class=\"col-sm-2 control-label\">{{ 'DATA_EMAIL' | translate }}</label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <input type=\"email\" class=\"form-control\" placeholder=\"Email\" name=\"userEmail\" ng-model=\"userEmail\" required>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group\" show-errors>\n" +
    "                <label class=\"col-sm-2 control-label\">{{ 'DATA_PASSWORD' | translate }}</label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <input type=\"password\" class=\"form-control\" placeholder=\"Password\" name=\"userPassword\" ng-model=\"userPassword\" required>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group\" show-errors>\n" +
    "                <label class=\"col-sm-2 control-label\">{{ 'DATA_VERIFY_PASSWORD' | translate }}</label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <input type=\"password\" class=\"form-control\" placeholder=\"Verify Password\" name=\"userVerifyPassword\" ng-model=\"userVerifyPassword\" data-match=\"userPassword\" required>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group\">\n" +
    "                <label class=\"col-sm-2 control-label\"></label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <p class=\"userprofile_note\">{{ 'DATA_CHOOSE_QUESTION_ANSWER_VERIFY_IF_FORGET_PWD' | translate }}</p>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"form-group\" show-errors>\n" +
    "                <label class=\"col-sm-2 control-label\">{{ 'DATA_QUESTION' | translate }}</label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"userQuestion\" ng-model=\"userQuestion\" required>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group\" show-errors>\n" +
    "                <label class=\"col-sm-2 control-label\">{{ 'DATA_ANSWER' | translate }}</label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"userAnswer\" ng-model=\"userAnswer\" required>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"flightname\">\n" +
    "                <p>{{ 'DATA_ACCOUNT_INFORMATION' | translate }}</p>\n" +
    "            </div>\n" +
    "            <div class=\"form-group\" show-errors>\n" +
    "                <label for=\"passengerGender\" class=\"col-sm-2 control-label\">{{ 'DATA_TITLE' | translate }}</label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <select class=\"form-control\" name=\"userTitle\" ng-model=\"userTitle\" required>\n" +
    "                        <option value=\"\">{{ 'DATA_CHOOSE_TITLE' | translate }}</option>\n" +
    "                        <option>Mr</option>\n" +
    "                        <option>Ms</option>\n" +
    "                    </select>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group\" show-errors>\n" +
    "                <label class=\"col-sm-2 control-label\">{{ 'DATA_FIRST_NAME' | translate }}</label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"userFirstName\" ng-model=\"userFirstName\" required>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group\" show-errors>\n" +
    "                <label class=\"col-sm-2 control-label\">{{ 'DATA_LAST_NAME' | translate }}</label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"userLastName\" ng-model=\"userLastName\" required>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group\" show-errors>\n" +
    "                <label class=\"col-sm-2 control-label\">{{ 'DATA_DOB' | translate }}</label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <input type=\"text\" class=\"form-control\" data-max-date=\"today\" data-autoclose=\"1\"  data-use-native=\"true\"  name=\"userBirthday\" ng-model=\"userBirthday\" bs-datepicker required>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group cf\" show-errors>\n" +
    "                <label class=\"col-sm-2 control-label\">{{ 'DATA_MOBILE' | translate }}</label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <input type=\"tel\" class=\"form-control\" name=\"userMobile\" ng-model=\"userMobile\" pattern=\"[0-9-+() ]+\" title=\"Phone number invalid\" required>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group\" show-errors>\n" +
    "                <label class=\"col-sm-2 control-label\">{{ 'DATA_PASPORT_NO' | translate }}.</label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"userPasport\" ng-model=\"userPasport\" required>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group cf\" show-errors>\n" +
    "                <label class=\"col-sm-2 control-label\">{{ 'DATA_ADDRESS' | translate }}</label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"userAddress\" ng-model=\"userAddress\" required>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group cf\" show-errors>\n" +
    "                <label for=\"select_origin\" class=\"col-sm-2 control-label\">{{ 'DATA_COUNTRY' | translate }}</label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <select class=\"form-control\" ng-change=\"changeCountry()\" name=\"userCountry\" ng-model=\"userCountry\" ng-options=\"country.Code as country.Name for country in countryResult\" required>\n" +
    "                        <option value=\"\">{{ 'DATA_SELECT' | translate }}</option>\n" +
    "                    </select>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group cf\" show-errors ng-show=\"isProvince\">\n" +
    "                <label class=\"col-sm-2 control-label\">{{ 'DATA_TOWN_CITY' | translate }}</label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <select class=\"form-control\" name=\"userCity\" ng-model=\"userCity\" required ng-options=\"province.Code as province.Name for province in provinceResult\" required>\n" +
    "                        <option value=\"\">{{ 'DATA_SELECT' | translate }}</option>\n" +
    "                    </select>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group checkbox accept_payment\" show-errors>\n" +
    "                <label class=\"col-sm-2 control-label\"></label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <label data-placement=\"bottom-left\" data-type=\"info\" data-animation=\"am-fade-and-scale\" bs-tooltip=\"tooltip\">\n" +
    "                        <input type=\"checkbox\" name=\"checked_payment\" ng-model=\"tooltip.checked\" required>\n" +
    "                        {{ 'DATA_READ_UNDERSTOOD_ACCEPTED' | translate }} <a ng-click=\"termsConditionsHelp()\">{{ 'DATA_TERMS_CONDITIONS' | translate }}</a> {{ 'DATA_OF_IAS' | translate }} ...\n" +
    "                    </label>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"button_next_step\">\n" +
    "        <button ng-click=\"submitCreateAccount()\">{{ 'BUTTON_CREATE_NEW_ACCOUNT' | translate }}</button>\n" +
    "    </div>\n" +
    "</form>\n" +
    "");
  $templateCache.put("userprofile/views/reservationdetail.html",
    "<form name=\"step2Form\">\n" +
    "    <div class=\"infomation_booking\" ng-if=\"vaildResevation  == true\">\n" +
    "        <div>\n" +
    "            <div class=\"flightname\">\n" +
    "                <p>{{ 'DATA_RESERVATION_INFO' | translate }}</p>\n" +
    "            </div>\n" +
    "            <div class=\"flightstatus_passenger_info\">\n" +
    "                <p>{{ 'DATA_RESERVATION_NO' | translate }}.<span>{{reservationInvoiceResult.ReservationNumber}}</span></p>\n" +
    "                <p>{{ 'DATA_CONTACT_NAME' | translate }}<span>{{reservationInvoiceResult.ContactInfo.Name}}</span></p>\n" +
    "                <p>{{ 'DATA_EMAIL' | translate }}<span>{{reservationInvoiceResult.ContactInfo.Email}}</span></p>\n" +
    "                <p>{{ 'DATA_PHONE_NO' | translate }}<span>{{reservationInvoiceResult.ContactInfo.Telephone}}</span></p>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div ng-repeat=\"s2Leg in s2Legs\">\n" +
    "            <div class=\"flightname\">\n" +
    "                <p>{{ 'DATA_FLIGHT' | translate }} {{$index + 1}}: {{s2Leg.DepartureAirportName}} {{ 'DATA_TO' | translate }} {{s2Leg.ArrivalAirportName}} - {{s2Leg.Segments[0].DepartureLocal  | date:'longDate'}}</p>\n" +
    "            </div>\n" +
    "            <div class=\"valuefare\">\n" +
    "                <div class=\"flight_fly\">\n" +
    "                    <div class=\"expan_ticked_info\">\n" +
    "                        <div class=\"detail_flight\">\n" +
    "                            <div class=\"indirect_icon_ticked\" ng-class=\"{'direct_icon_ticked': s2Leg.Segments.length == 1}\"></div>\n" +
    "                            <div class=\"transitgroup cf\">\n" +
    "                                <div class=\"transit cf\" ng-repeat=\"segmentOption in s2Leg.SegmentsTemp\">\n" +
    "                                    <div ng-show=\"segmentOption.checkAirCraft == false\" class=\"segment_view cf\">\n" +
    "                                        <div class=\"time_to_time\">\n" +
    "                                            <p class=\"timesheet\">\n" +
    "                                                <span class=\"time_from\">{{segmentOption.DepartureLocal | date:'HH:mm'}}</span>\n" +
    "                                                <span class=\"fly_to_icon\"></span>\n" +
    "                                                <span class=\"time_to\">{{segmentOption.ArrivalLocal | date:'HH:mm'}}</span>\n" +
    "                                            </p>\n" +
    "                                            <p class=\"flight_name\">\n" +
    "                                                <span class=\"name\">{{segmentOption.Flight}} - {{segmentOption.AircraftName}}</span>\n" +
    "                                            </p>\n" +
    "                                        </div>\n" +
    "                                        <div class=\"airport_status\">\n" +
    "                                            <p class=\"airport_from\" ng-click=\"loadAirportDetail(segmentOption.DepartureAirportCode, segmentOption.DepartureAirportName)\">{{segmentOption.DepartureAirportCode}}</p>\n" +
    "                                        <p class=\"airport_to\" ng-click=\"loadAirportDetail(segmentOption.ArrivalAirportCode, segmentOption.ArrivalAirportName)\">{{segmentOption.ArrivalAirportCode}}</p>\n" +
    "                                        </div>\n" +
    "                                    </div>\n" +
    "                                    <div ng-show=\"segmentOption.checkAirCraft\" class=\"segment_stop cf\">\n" +
    "                                        <div>\n" +
    "                                            <span class=\"stop_lable\">{{ 'DATA_STOP' | translate }} {{segmentOption.countStop}}: </span>\n" +
    "                                            <span class=\"stop_value airport_from\" ng-click=\"loadAirportDetail(segmentOption.DepartureAirportCode, segmentOption.DepartureAirportName)\">{{segmentOption.DepartureAirportCode}}</span>\n" +
    "                                            <span class=\"stop_lable\">- {{ 'DATA_CONNECTION' | translate }}: </span>\n" +
    "                                            <span class=\"stop_value\">{{segmentOption.TimeTransit }}</span>\n" +
    "                                        </div>\n" +
    "                                    </div>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <div class=\"slecttion_package_price\">\n" +
    "                            <div class=\"pre_price\">\n" +
    "                                <span class=\"select_tion check\"></span>\n" +
    "                                <span class=\"package_name\" ng-click=\"loadFare(s2Leg.Segments[0].PaxList[0].FareIdent)\" ng-if=\"s2Leg.Segments[0].PaxList[0].FareIdent != ''\">{{s2Leg.Segments[0].PaxList[0].FareIdent}}</span>\n" +
    "                                <span class=\"package_name\" ng-if=\"s2Leg.Segments[0].PaxList[0].FareIdent == '' \" style=\" text-decoration: none; \">-</span>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <p class=\"not_payment_detect\" ng-if=\"checkinStep2DataTotal.ResStatus.Balance > 0\">{{ 'DATA_MESSAGE_RESERVATION_BALANCE_OWING' | translate }} {{checkinStep2DataTotal.ResStatus.Balance | isoCurrency:checkinStep2DataTotal.CurrencyInfo.Abbreviation:fractionSize}}. {{ 'DATA_MESSAGE_UNABLE_CHECKIN_BALANCE_PAID' | translate }}.</p>\n" +
    "    <div class=\"check_in_no_flight\" ng-if=\"vaildResevation  == false\">\n" +
    "        <div class=\"no_flight\"></div>\n" +
    "        <h2>{{ 'DATA_MESSAGE_NOT_FLIGHT' | translate }}</h2>\n" +
    "        <p>{{ 'DATA_MESSAGE_CHECK_RESERVATION' | translate }}</p>\n" +
    "    </div>\n" +
    "    <div class=\"button_next_step\">\n" +
    "        <button ng-click=\"goBack()\">{{ 'DATA_DONE' | translate }}</button>\n" +
    "    </div>\n" +
    "</form>\n" +
    "");
  $templateCache.put("userprofile/views/reservationhistory.html",
    "<form name=\"step2Form\">\n" +
    "    <div class=\"infomation_booking\">\n" +
    "        <div ng-repeat=\"reservation in reservationHistoryResults\">\n" +
    "            <div class=\"flightname\">\n" +
    "                <p>{{reservation.LegSummary}}</p>\n" +
    "            </div>\n" +
    "            <div class=\"userprofile_reservation_info\">\n" +
    "                <p ng-click=\"viewReservationDetail(reservation.ReservationNumber)\">{{ 'DATA_RESERVATION_NO' | translate }}.<span>{{reservation.ReservationNumber}}</span></p>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</form>\n" +
    "");
  $templateCache.put("userprofile/views/updateprofile.html",
    "<form class=\"form-horizontal infomation_passenger\" name=\"registerForm\">\n" +
    "    <div class=\"user_profile\">\n" +
    "        <div class=\"infomation_input_user_profile\">\n" +
    "            <div class=\"flightname\">\n" +
    "                <p>{{ 'DATA_UPDATE_PROFILE' | translate }}</p>\n" +
    "            </div>\n" +
    "            <div class=\"form-group\" show-errors>\n" +
    "                <label for=\"passengerGender\" class=\"col-sm-2 control-label\">{{ 'DATA_TITLE' | translate }}</label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <select class=\"form-control\" name=\"userTitle\" ng-model=\"userTitle\" required>\n" +
    "                        <option value=\"\">{{ 'DATA_CHOOSE_TITLE' | translate }}</option>\n" +
    "                        <option>Mr</option>\n" +
    "                        <option>Ms</option>\n" +
    "                    </select>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group\" show-errors>\n" +
    "                <label class=\"col-sm-2 control-label\">{{ 'DATA_FIRST_NAME' | translate }}</label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"userFirstName\" ng-model=\"userFirstName\" required>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group\" show-errors>\n" +
    "                <label class=\"col-sm-2 control-label\">{{ 'DATA_LAST_NAME' | translate }}</label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"userLastName\" ng-model=\"userLastName\" required>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group\" show-errors>\n" +
    "                <label class=\"col-sm-2 control-label\">{{ 'DATA_DOB' | translate }}</label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <input type=\"text\" class=\"form-control\" data-max-date=\"today\" data-autoclose=\"1\"  data-use-native=\"true\"  name=\"userBirthday\" ng-model=\"userBirthday\" bs-datepicker required>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group cf\" show-errors>\n" +
    "                <label class=\"col-sm-2 control-label\">{{ 'DATA_MOBILE' | translate }}</label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <input type=\"tel\" class=\"form-control\" name=\"userMobile\" ng-model=\"userMobile\" pattern=\"[0-9-+() ]+\" title=\"Phone number invalid\" required>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group\" show-errors>\n" +
    "                <label class=\"col-sm-2 control-label\">{{ 'DATA_PASPORT_NO' | translate }}.</label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"userPasport\" ng-model=\"userPasport\" required>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group cf\" show-errors>\n" +
    "                <label class=\"col-sm-2 control-label\">{{ 'DATA_ADDRESS' | translate }}</label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"userAddress\" ng-model=\"userAddress\" required>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group cf\" show-errors>\n" +
    "                <label for=\"select_origin\" class=\"col-sm-2 control-label\">{{ 'DATA_COUNTRY' | translate }}</label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <select class=\"form-control\" ng-change=\"changeCountry()\" name=\"userCountry\" ng-model=\"userCountry\" ng-options=\"country.Code as country.Name for country in countryResult\" required>\n" +
    "                        <option value=\"\">{{ 'DATA_SELECT' | translate }}</option>\n" +
    "                    </select>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group cf\" show-errors ng-show=\"isProvince\">\n" +
    "                <label class=\"col-sm-2 control-label\">{{ 'DATA_TOWN_CITY' | translate }}</label>\n" +
    "                <div class=\"col-sm-10\">\n" +
    "                    <select class=\"form-control\" name=\"userCity\" ng-model=\"userCity\" required ng-options=\"province.Code as province.Name for province in provinceResult\" required>\n" +
    "                        <option value=\"\">{{ 'DATA_SELECT' | translate }}</option>\n" +
    "                    </select>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"button_next_step\">\n" +
    "        <button ng-click=\"submitUpdateProfile()\">{{ 'BUTTON_UPDATE' | translate }}</button>\n" +
    "    </div>\n" +
    "</form>\n" +
    "");
}]);
